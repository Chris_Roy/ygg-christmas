package com.cupcake
{
	import com.cupcake.Utils;
	import com.greensock.TweenLite;
	import com.greensock.easing.Circ;
	
	import flash.events.Event;
	import flash.geom.Point;
	
	import org.osflash.signals.Signal;
	
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.TouchEvent;
	import starling.filters.BlurFilter;
	import starling.text.TextField;
	import starling.textures.TextureAtlas;
	import starling.utils.AssetManager;
	
	public class ScrollingPageSelector extends Sprite
	{
		public var closed:Signal			= new Signal;
		public var pageSelected:Signal		= new Signal( int );
		
		private var applyGlow:Boolean		= false;
		private var assetsExt:AssetManager	= null;
		private var barTop:int				= 548;
		private var bgFile:String			= "textures/%SCALE%x/Pages/scrollBG.png";
		private var centerCurrent:Boolean	= true;
		private var glowColor:uint			= 0x0000FF;
		private var selectorFile:String		= "textures/%SCALE%x/Pages/pageSelector.png";
		private var selectorX:int			= -5;
		private var selectorY:int			= -5;
		private var menuString:String		= "Menu";
		private var overlay:String			= "";
		private var overlayOffset:Point		= null;
		private var pageString:String		= "Page";
		private var textColor:uint			= 0x000000;
		private var textFont:String			= "_sans";
		private var textFromTop:Boolean		= true;
		private var textHeight:int			= 35;
		private var textPadding:int			= -37;
		private var textSize:int			= 22;
		private var thumbPadding:int		= 15;
		private var thumbTop:int			= 40;
		private var thumbWidth:int			= 227;
		private var thumbsAtlas:String		= "";
		private var thumbsFolder:String		= "textures/%SCALE%x/Pages/Thumbs";
		
		private var _loaded:Boolean			= false;
		private var _shown:Boolean			= false;
		private var barHeight:int			= 0;
		private var externalAssets:Boolean	= false;
		private var firstX:int				= 0;
		private var force:Number			= 0;
		private var forceThreshold:Number	= 1;
		private var friction:Number			= .9;
		private var lastX:int				= 0;
		private var menuLeft:int			= 0;
		private var screenWidth:int;
		private var scrolling:Boolean		= false;
		private var targetButton:int		= 0;
		private var waiting:Boolean			= false;
		
		private var atl:TextureAtlas;
		private var assets:AssetManager;
		private var bg:Image;
		private var btns:Vector.<Button>	= new Vector.<Button>;
		private var ct:Sprite;
		private var sel:Image;
		private var scroller:Sprite			= new Sprite;
		private var thumbs:Vector.<String>;
		
		public function ScrollingPageSelector( container:Sprite, vars:Object = null )
		{
			super();
			
			ct = container;
			ct.addChild( this );
			
			applyGlow		= Utils.GetProp( vars,	"applyGlow",		applyGlow );		// apply a glow around the text?
			assetsExt		= Utils.GetProp( vars,	"assets", 			assetsExt );		// an optional external AssetManager object (used for bar assets)
			barTop			= Utils.GetProp( vars,	"barTop",			barTop );			// the y coord of the top of the bar when opened
			bgFile			= Utils.GetProp( vars,	"bgFile",			bgFile );			// the filename or asset name of the background
			centerCurrent	= Utils.GetProp( vars,	"centerCurrent",	centerCurrent );	// center the bar on the current page when shown?
			glowColor		= Utils.GetProp( vars,	"glowColor",		glowColor );
			selectorFile	= Utils.GetProp( vars,	"selectorFile",		selectorFile );		// the filename of asset name of the page indicator overlay
			selectorX		= Utils.GetProp( vars,	"selectorX",		selectorX );		// the x offset of the indicator image
			selectorY		= Utils.GetProp( vars,	"selectorY",		selectorY );		// the y offset of the indicator image
			menuString		= Utils.GetProp( vars,	"menuString",		menuString );		// the string to use on the Menu button
			overlay			= Utils.GetProp( vars,	"overlay",			overlay );			// full path to the button overlay
			overlayOffset	= Utils.GetProp( vars,	"overlayOffset",	overlayOffset );	// a Point object representing the offset of the overlay from top left
			pageString		= Utils.GetProp( vars,	"pageString",		pageString );		// the string to use on the Page button
			textColor		= Utils.GetProp( vars,	"textColor",		textColor );
			textFont		= Utils.GetProp( vars,	"textFont",			textFont );
			textFromTop		= Utils.GetProp( vars,	"textFromTop",		textFromTop );		// display text at the top of the button?
			textHeight		= Utils.GetProp( vars,	"textHeight",		textHeight );
			textPadding		= Utils.GetProp( vars,	"textPadding",		textPadding );		// padding between the text and the top/bottom of button
			textSize		= Utils.GetProp( vars,	"textSize",			textSize );
			thumbPadding	= Utils.GetProp( vars,	"thumbPadding",		thumbPadding );		// horizontal padding between buttons
			thumbTop		= Utils.GetProp( vars,	"thumbTop",			thumbTop );			// number of pixels down from top of bar to place buttons
			thumbWidth		= Utils.GetProp( vars,	"thumbWidth",		thumbWidth );
			thumbsAtlas		= Utils.GetProp( vars,	"thumbsAtlas",		thumbsAtlas );		// full path to atlas for thumbs (minus extension), if used
			thumbsFolder	= Utils.GetProp( vars,	"thumbsFolder",		thumbsFolder );		// full path to folder containing thumbs, if used
			
			externalAssets			= ( assetsExt != null );
			var assetsArray:Array	= new Array;
			if ( thumbsAtlas != "" )
			{
				assetsArray.push( thumbsAtlas + Utils.GetImageExtension( thumbsAtlas ) );
				assetsArray.push( thumbsAtlas + ".xml" );
				thumbsAtlas = Utils.StripAssetPath( thumbsAtlas );
			} else { assetsArray.push( thumbsFolder ); }
			
			if ( !externalAssets )		
			{ 
				assetsArray.push( bgFile ); 
				if ( selectorFile != "" )	{ assetsArray.push( selectorFile ); }
				if ( overlay != "" )		{ assetsArray.push( overlay ); }
			}
			
			bgFile			= Utils.StripAssetPath( bgFile );
			selectorFile	= Utils.StripAssetPath( selectorFile );
			overlay			= Utils.StripAssetPath( overlay );
			
			screenWidth		= Costanza.STAGE_WIDTH - ( Costanza.SCALE_FACTOR == 1 ? Costanza.SCREEN_START_X * 2 : 0 );
			
			assets = Utils.CreateAssetManager( assetsArray );
			assets.loadQueue( AssetsLoaded );
		}
		
		public function get loaded():Boolean { return _loaded; }
		public function get shown():Boolean { return _shown; }
		
		public override function dispose():void
		{
			assets.purge();
			assets.dispose();
			Utils.removeHandler( Starling.current.nativeStage, flash.events.Event.ENTER_FRAME, UpdateBar );
			ct.removeEventListener( TouchEvent.TOUCH, ProcessTouch );
			ct.removeChild( this, true );
			closed.removeAll();
			pageSelected.removeAll();
			super.dispose();
		}
		
		public function Hide():void
		{
			_shown = false;
			ct.removeEventListener( TouchEvent.TOUCH, ProcessTouch );
			TweenLite.to( this, .5, { y: Costanza.STAGE_HEIGHT } );
		}
		
		public function Show( currentButtonIndex:int = 0 ):void
		{
			_shown = true;
			ct.setChildIndex( this, ct.numChildren - 1 );
			this.y = Costanza.STAGE_HEIGHT;
			TweenLite.to( this, .5, { y: barTop, onComplete: BarShown } );
			
			if ( sel != null )
			{
				sel.visible = currentButtonIndex >= 0;
				if ( currentButtonIndex >= 0 ) { btns[ currentButtonIndex ].addChild( sel ); } 
			}
			
			if ( centerCurrent )
			{
				var targetX:int = ( screenWidth / 2 ) - ( ( ( thumbWidth + thumbPadding ) * currentButtonIndex ) + thumbWidth / 2 );
				TweenLite.to( scroller, .8, { x: Utils.Clamp( targetX, menuLeft, 0 ), ease: Circ.easeOut } );
			}
		}
		
		
		// *** PRIVATE FUNCTIONS ***
		private function ApplyForce():void { scroller.x = Utils.Clamp( scroller.x + force, menuLeft, 0 ); }
		
		private function AssetsLoaded( ratio:Number ):void
		{
			if ( ratio != 1 ) { return; }
			
			var index:int			= 0;
			var atl:TextureAtlas	= ( thumbsAtlas != "" ) ? assets.getTextureAtlas( thumbsAtlas ) : null;
			_loaded					= true;
			bg						= externalAssets ?	new Image( assetsExt.getTexture( bgFile ) ):
														new Image( assets.getTexture( bgFile ) );
			barHeight				= bg.height;
			thumbs					= atl != null ? atl.getNames() : Utils.GetFilenames( thumbsFolder );
			menuLeft				= Utils.Clamp( - ( ( thumbWidth + thumbPadding ) * thumbs.length + thumbPadding - screenWidth ), int.MIN_VALUE, 0 );
			this.y					= Costanza.STAGE_HEIGHT;
			this.x					= Costanza.SCALE_FACTOR == 1 ? Costanza.SCREEN_START_X : 0;
			
			addChild( bg );
			addChild( scroller );
			
			for each ( var thumb:String in thumbs )
			{
				var btn:Button		= new Button( assets.getTexture( thumb ) );
				btn.name			= thumb;
				btn.x				= ( index * ( thumbWidth + thumbPadding ) ) + thumbPadding;
				btn.y				= thumbTop;	
				btn.scaleWhenDown	= 1;
				btn.addEventListener( starling.events.Event.TRIGGERED, PageSelected );
				
				if ( overlay != "" )
				{
					var over:Image		= externalAssets ?	new Image( assetsExt.getTexture( overlay ) ) :
															new Image( assets.getTexture( overlay ) );
					if ( overlayOffset != null )
					{
						over.x			= overlayOffset.x;
						over.y			= overlayOffset.y;
					}
					
					btn.addChild( over );
				}
				
				var pageText:String	= index == 0 ? menuString : pageString + " " + index;
				var txt:TextField	= new TextField( thumbWidth, textHeight, pageText, textFont, textSize, textColor, true );
				txt.y				= textFromTop ? textPadding : btn.height - textHeight - textPadding;
				txt.filter			= applyGlow ? BlurFilter.createGlow( glowColor, 1, 3, .5 ) : null;
				btn.addChild( txt );
				
				scroller.addChild( btn );
				btns.push( btn );
				index++;	
			}
			
			if ( selectorFile != "" )
			{
				sel		= externalAssets ?	new Image( assetsExt.getTexture( selectorFile ) ) :
											new Image( assets.getTexture( selectorFile ) );
				sel.x	= selectorX;
				sel.y	= selectorY;
			}
		}
		
		private function BarShown():void { ct.addEventListener( TouchEvent.TOUCH, ProcessTouch ); }
		
		private function PageSelected( e:starling.events.Event ):void
		{
			if ( scrolling || waiting ) { return; }
			var buttonIndex:int = thumbs.indexOf( ( e.target as Button ).name );
			waiting = true;
			pageSelected.dispatch( buttonIndex );
			TweenLite.delayedCall( 1, function():void{ waiting = false; } );
		}
		
		private function ProcessTouch( e:TouchEvent ):void
		{
			var touchX:Number = e.touches[0].globalX;
			var touchY:Number = e.touches[0].globalY;
			
			switch( e.touches[0].phase )
			{
				case "began":
					if ( scrolling ) { scrolling = false; }
					Utils.removeHandler( Starling.current.nativeStage, flash.events.Event.ENTER_FRAME, UpdateBar );
					firstX = touchX;
					if ( touchY < barTop || touchY > ( barTop + barHeight ) )
					{
						closed.dispatch();
						Hide();
					}
					break;
				
				case "moved":	
					if ( Math.abs( touchX - firstX ) > 5 ) { scrolling = true; }
					
					if( touchY > barTop && touchY < ( barTop + barHeight ) )
					{						
						force = touchX - lastX;
						ApplyForce();
					}
					break;
				
				case "ended":	
					if ( Starling.current.nativeStage != null && force != 0 ) 
					{ 
						ApplyForce();
						Utils.addHandler( Starling.current.nativeStage, flash.events.Event.ENTER_FRAME, UpdateBar ); 
					}
					break;
			}
			
			lastX = touchX;
		}
		
		private function UpdateBar( e:flash.events.Event ):void
		{
			force *= friction;
			ApplyForce();
			if ( Math.abs( force ) < forceThreshold || scroller.x == menuLeft || scroller.x == 0 )
			{
				force = 0;
				Utils.removeHandler( Starling.current.nativeStage, flash.events.Event.ENTER_FRAME, UpdateBar );
			}
		}
	}
}