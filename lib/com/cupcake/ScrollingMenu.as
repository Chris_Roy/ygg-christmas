﻿package com.cupcake
{
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.display.Stage;
	
	import com.cupcake.IControllable;
	import com.cupcake.Utils;
	
	public class ScrollingMenu extends MovieClip implements IControllable
	{
		public static var addEnterFrame:Function		= null;
		public static var addMouseUp:Function			= null;
		public static var removeEnterFrame:Function		= null;
		public static var removeMouseUp:Function		= null;
		
		public static var DRAG_THRESHOLD_DEFAULT:Number	= 20;
		public var dragThreshold:Number					= DRAG_THRESHOLD_DEFAULT;
		
		public static var SNAP_THRESHOLD_DEFAULT:Number	= 3;
		public var snapThreshold:Number					= SNAP_THRESHOLD_DEFAULT;
		
		public static var SLIDE_THRESHOLD_DEFAULT:Number = 5;
		public var slideThreshold:Number				= SLIDE_THRESHOLD_DEFAULT;
		
		public static var DEFAULT_DECEL:Number			= 0.8;
		public static var decel:Number					= DEFAULT_DECEL;	
		
		public var buttonClips:Array					= [];
		public var clickCallback:Function				= null;
		public var onScroll:Function					= null;
		
		public var mask_mc:MovieClip;
		public var bg_mc:MovieClip;
		public var menu_mc:MovieClip;
		public var highlight_mc:MovieClip;
		
		public var allowSubButtons:Boolean				= false;
		public var horizontal:Boolean					= true;
		public var snapToCells:Boolean					= true;
		
		public var instanceNames:String					= "mask_mc,bg_mc,menu_mc,highlight_mc";
		public var menuType:int;
		
		protected var _dragging:Boolean					= false;
		protected var _mouseDown:Boolean				= false;
		protected var _sliding:Boolean					= false;
		protected var _snapping:Boolean					= false;
		
		private var _currentDelta:Number				= 0;
		private var _curSpeed:Number					= 0.0;
		private var _dragDown:Number					= 0.0;
		private var _dragStart:Number					= 0;
		private var _cellSize:Number					= 0;
		private var _cellsPerScreen:Number				= 0;
		private var _last:Number						= 0;
		private var _minCell:Number						= 0;
		private var _min:Number							= 0;
		private var _max:Number							= 0;
		private var _origin:Number						= 0;
		private var _snap:Number						= 0;
		private var _speed:Number						= 0;
		private var _start:Number						= 0;
		
		private var _initialized:Boolean				= false;
		private var _paused:Boolean						= false;
		private var _running:Boolean					= false;
		
		// Constructor and standard functions
		public function ScrollingMenu(MenuType:int, clickCallbackFunction:Function = null ) 
		{
			menuType = MenuType;
			clickCallback = clickCallbackFunction;			
		}
	
		public function get isInitialized():Boolean { return _initialized; }
		public function get isPaused():Boolean { return _paused; }
		public function get isRunning():Boolean { return _running; }
		
		public function Destroy():void
		{
			if ( isRunning ) Stop();
			stopHandlers();
		}
		
		public function Init():void
		{
			if ( isInitialized ) return;
			
			if ( mask_mc != null ) menu_mc.mask = mask_mc;
			highlight_mc = menu_mc.getChildByName( "highlight_mc" ) as MovieClip;
			
			_initialized = true;
		}
		
		public function Pause():void
		{
			if ( isRunning && !isPaused )
			{
				stopHandlers();
				_paused = true;
			}
		}
		
		public function Reset():void
		{
			if ( !isInitialized ) Init();
			if ( isRunning ) Stop();
			// stub, no need for this atm
		}
		
		public function Resume():void
		{
			if ( isRunning && isPaused )
			{
				startHandlers();
				_paused = false;
			}
		}
		
		public function Start():void
		{
			if ( !isInitialized ) Init();
			if ( isRunning ) Reset();
			startHandlers();
			
			_running = true;
			_paused = false;
		}
		
		public function Stop():void
		{
			if ( isRunning )
			{
				stopHandlers();
				_running = false;
			}
		}
		
		// Public functions
		public function get dragging():Boolean { return _dragging; }
		public function get mouseDown():Boolean { return _mouseDown; }
		public function get sliding():Boolean { return _sliding; }
		public function get snapping():Boolean { return _snapping; }
		
		public function InitializeButtons():void 
		{ 
			var clip:MovieClip;
			var nameIndex:Number;
			
			// compile array of buttons
			for ( var t:int = 0 ; t < menu_mc.numChildren ; t++ )
			{
				clip = menu_mc.getChildAt( t ) as MovieClip;
				
				// strip the leading "button_" from the instance name
				nameIndex = Number( clip.name.substr( 7, clip.name.length - 7 ) );
				
				if ( clip != highlight_mc && !isNaN( nameIndex ) )
				{
					buttonClips[ nameIndex ] = clip;
					if ( !allowSubButtons ) { clip.mouseChildren = false; }
				}
			}
			
			_origin = horizontal ? menu_mc.x : menu_mc.y;
			
			if ( snapToCells )
			{
				_cellSize = ( buttonClips.length > 1 ) ? ( horizontal ? ( buttonClips[ 1 ].x - buttonClips[ 0 ].x ) : ( buttonClips[ 1 ].y - buttonClips[ 0 ].y ) ): 0;
				_cellsPerScreen = Math.round( ( horizontal ? mask_mc.width : mask_mc.height/Costanza.SCALE_FACTOR ) / _cellSize );
				_minCell = buttonClips.length < _cellsPerScreen ? 0 : -( buttonClips.length - _cellsPerScreen );
				
				_max = buttonClips.length < _cellsPerScreen ?
					   ( horizontal ? ( mask_mc.width - menu_mc.width ) : ( mask_mc.height/Costanza.SCALE_FACTOR - menu_mc.height ) ) :
					   ( horizontal ? menu_mc.x : menu_mc.y );
				
				_min = -( _cellSize * buttonClips.length ) + ( horizontal ? mask_mc.width : mask_mc.height/Costanza.SCALE_FACTOR );
				_min = ( _min > _origin ) ? _origin : _min;
			}
			else
			{
				if ( horizontal )
				{
					//_max = mask_mc.width > menu_mc.width ? ( mask_mc.width - menu_mc.width ) : _origin;
					_max = _origin;
					_min = mask_mc.width > menu_mc.width ? _origin : _origin - ( menu_mc.width - mask_mc.width );
				}
				else
				{
					//_max = mask_mc.height/Costanza.SCALE_FACTOR > menu_mc.height ? ( mask_mc.height/Costanza.SCALE_FACTOR - menu_mc.height ) : _origin;
					_max = _origin;
					_min = mask_mc.height/Costanza.SCALE_FACTOR > menu_mc.height ? _origin : _origin - ( menu_mc.height - mask_mc.height/Costanza.SCALE_FACTOR );
				}
			}
			
			//App.Log( "Scrolling menu max: " + _max );
			//App.Log( "Scrolling menu min: " + _min );
		}
		
		public function setCurrent( index:int ):void
		{
			if ( highlight_mc != null && index <= int(buttonClips.length) )
			{
				if ( index < 0 ) highlight_mc.visible = false;
				else
				{
					highlight_mc.visible = true;
					highlight_mc.x = buttonClips[ index ].x;
					highlight_mc.y = buttonClips[ index ].y; // trying this, get rid of it if highlights go wonky
					snapMenu( index );
				}
			}
		}
		
		// Private and protected functions
		protected function endSnap():void
		{
			_snapping = false;
			if ( horizontal ) { menu_mc.x = _snap; }
				else { menu_mc.y = _snap; }
		}
		
		protected function handleDown( e:MouseEvent ):void
		{
			if ( isRunning && !isPaused )
			{
				trace("this");
				_mouseDown = true;
				_last = horizontal ? stage.mouseX : stage.mouseY;
				_currentDelta = 0;
				_dragDown = horizontal ? stage.mouseX : stage.mouseY;
			}
		}
		
		public function handleFrame( e:Event ):void
		{
			if ( isRunning && !isPaused )
			{
				if ( _mouseDown )
				{
					trace(stage.mouseY);
					_currentDelta = ( horizontal ? stage.mouseX : stage.mouseY ) - _last;
					_last = horizontal ? stage.mouseX : stage.mouseY;				
				}
				
				if ( _dragging )
				{
					if ( horizontal )
					{
						menu_mc.x = _dragStart + ( stage.mouseX - _start );
						menu_mc.x = Utils.Clamp( menu_mc.x, _min, _max );
					}
					else
					{
						menu_mc.y = _dragStart + ( stage.mouseY - _start );
						menu_mc.y = Utils.Clamp( menu_mc.y, _min, _max );
					}
				}
				else if ( _snapping )
				{
					if ( horizontal )
					{
						menu_mc.x -=  ( menu_mc.x - _snap ) / 3;
						if ( Math.abs( _snap - menu_mc.x ) < snapThreshold )
							{ endSnap(); }
					}
					else
					{
						menu_mc.y -=  ( menu_mc.y - _snap ) / 3;
						if ( Math.abs( _snap - menu_mc.y ) < snapThreshold )
							{ endSnap(); }
					}
					
				}
				else if ( _sliding )
				{
					if ( horizontal ) { menu_mc.x += _curSpeed; }
						else { menu_mc.y += _curSpeed; }
					_curSpeed *= decel;
					
					if ( Math.abs( _curSpeed ) < slideThreshold )
					{
						if ( snapToCells ) { snapMenu(); }
						else { _sliding = false; }
						
					}
					else if	( ( horizontal && menu_mc.x < _min ) || 
						  ( !horizontal && menu_mc.y < _min ) || 
						  ( horizontal && menu_mc.x > _max ) ||
						  ( !horizontal && menu_mc.y > _max ) )
							{ snapMenu(); }
				}
				else if ( _mouseDown && _dragging == false && 
						  ( ( horizontal && Math.abs( _dragDown - stage.mouseX ) > dragThreshold ) ||
						  	( !horizontal && Math.abs( _dragDown - stage.mouseY ) > dragThreshold ) )
						)
				{
					_dragging = true;
					_start = horizontal ? stage.mouseX : stage.mouseY;
					_dragStart = horizontal ? menu_mc.x : menu_mc.y;
					if ( onScroll != null ) { onScroll(); }
					if ( allowSubButtons )
					{
						for ( var t:int = 0 ; t < buttonClips.length ; t ++ ) { buttonClips[ t ].mouseChildren = false; }
					}
				}
			}
		}
		
		protected function handleRelease( e:MouseEvent ):void
		{
			if ( _mouseDown && isRunning && !isPaused )
			{
				_mouseDown = false;
				
				if ( _dragging && allowSubButtons )
				{
					for ( var t:int = 0 ; t < buttonClips.length ; t ++ ) 
						{ buttonClips[ t ].mouseChildren = true; }
				}
				
				if ( Math.abs( _currentDelta ) > dragThreshold )
					{ slideMenu(); }
				else if ( _dragging )
				{ 
					if ( snapToCells ) { snapMenu(); } 
					_dragging = false;
				}
				else
				{
					var tName:String = e.target.name;
					if ( tName == null ) { App.Log( "Null target name in ScrollingMenu.handleRelease()" ); return; }
					
					if ( tName.indexOf( "button_" ) > -1 && clickCallback != null )
					{
						// strip off the button name prefix and call the click function
						clickCallback( int( tName.substr( 7, tName.length - 7 ) ) );
					}
				}
			}
			else { clickCallback( -1 ); }
		}		
		
		protected function slideMenu( ):void
		{
			_dragging = false;
			_snapping = false;
			_sliding = true;
			_curSpeed = _currentDelta;
		}
		
		protected function snapMenu( targetCell:int = -1 ):void
		{
			_dragging = false;
			_sliding = false;
			_snapping = true;
			
			if ( snapToCells )
			{
				var cell:Number = targetCell < 2 ? Math.round( ( horizontal ? menu_mc.x : menu_mc.y ) / _cellSize ) : -( targetCell - 2 );
				cell = ( cell > 0 ) ? 0 :
					   ( cell < _minCell ) ? _minCell : cell;
				_snap = _origin + ( cell * _cellSize );
				//App.Log( "ScrollingMenu.snapMenu() - snapping menu to : " + _snap );
			}
			else if ( ( horizontal && menu_mc.x < _min ) || ( !horizontal && menu_mc.y < _min ) )
			{
				_snap = _min;
			}
			else if ( ( horizontal && menu_mc.x > _max ) || ( !horizontal && menu_mc.y > _max ) )
			{
				_snap = _max;
			}
			
		}
		
		protected function startHandlers():void
		{
			Utils.addHandler( this, MouseEvent.MOUSE_DOWN, handleDown );
			if ( addMouseUp != null ) addMouseUp( handleRelease );
				else Utils.addHandler( this.stage, MouseEvent.MOUSE_UP, handleRelease );
			if ( addEnterFrame != null ) addEnterFrame( handleFrame );
				else Utils.addHandler( this, Event.ENTER_FRAME, handleFrame );
		}
		
		protected function stopHandlers():void
		{
			Utils.removeHandler( this, MouseEvent.MOUSE_DOWN, handleDown );
			
			if ( removeMouseUp != null ) removeMouseUp( handleRelease );
				else Utils.removeHandler( this.stage, MouseEvent.MOUSE_UP, handleRelease );
			if ( removeEnterFrame != null ) removeEnterFrame( handleFrame );
				else Utils.removeHandler( this, Event.ENTER_FRAME, handleFrame );
		}
	}
}
