﻿package com.cupcake
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.utils.getTimer;
	import com.cupcake.Utils;
	import com.cupcake.IControllable;
	
	public class DragTracker extends EventDispatcher implements IControllable
	{
		public static const CONSTRAIN_NONE:uint				= 0;
		public static const CONSTRAIN_VERTICAL:uint			= 1;
		public static const CONSTRAIN_HORIZONTAL:uint		= 2;
		
		public static const DRAG:String						= "DragTracker.DragEvent";
		public static const DRAG_END:String					= "DragTracker.DragEndEvent";
		public static const DRAG_START:String				= "DragTracker.DragStartEvent";
		public static const TAP:String						= "DragTracker.TapEvent";
		
		public static const DRAG_FACTOR_DEFAULT:Number		= 1;
		public static const DRAG_THRESHOLD_DEFAULT:Number	= 5.0;
		public static const DRAG_LIMIT_DEFAULT:Number		= 0.0;
		public static const MIN_SHAKE_DEFAULT:Number		= 500;
		public static const MIN_SWOOSH_DEFAULT:Number		= 200;
		public static const SHAKE_THRESHOLD_DEFAULT:Number	= 40;
		public static const SHAKE_TIME_DEFAULT:Number		= 500;
		
		public static var registerFrameHandler:Function		= null;
		public static var registerMouseDown:Function		= null;
		public static var registerMouseUp:Function			= null;
		public static var removeFrameHandler:Function		= null;
		public static var removeMouseDown:Function			= null;
		public static var removeMouseUp:Function			= null;
		
		public var bringToTop:Boolean						= true;
		public var constraint:uint							= CONSTRAIN_NONE;
		public var dragFactor:Number						= DRAG_FACTOR_DEFAULT;
		public var dragLimit:Number							= DRAG_LIMIT_DEFAULT;			
		public var dragThreshold:Number						= DRAG_THRESHOLD_DEFAULT;
		public var lastShake:Number							= 0;
		public var lastSwoosh:Number						= 0;
		public var minShakeTime:Number						= MIN_SHAKE_DEFAULT;
		public var minSwooshTime:Number						= MIN_SWOOSH_DEFAULT;
		public var shakeThreshold:Number					= SHAKE_THRESHOLD_DEFAULT;
		public var shakeTime:Number							= SHAKE_TIME_DEFAULT;
		public var shakeLeft:Number							= 0;
		public var shakeRight:Number						= 0;
		public var shakeUp:Number							= 0;
		public var shakeDown:Number							= 0;
		public var tapOnly:Boolean							= false;
		
		public var onDrag:Function							= null;
		public var onDragEnd:Function						= null;
		public var onDragStart:Function						= null;
		public var onShake:Function							= null;
		public var onSwoosh:Function						= null;
		public var onTap:Function							= null;
		
		public var currentPoint:Point						= new Point;
		public var dragStartPoint:Point						= new Point;
		public var lastPoint:Point							= new Point;
		public var mousePoint:Point							= new Point;
		public var objPoint:Point							= new Point;
		
		protected var _currentX:Number						= 0.0;
		protected var _currentY:Number						= 0.0;
		private var _distance:Number						= 0.0;
		private var _dragging:Boolean						= false;
		private var _dragStartX:Number						= 0.0;
		private var _dragStartY:Number						= 0.0;
		private var _lastDelta:Number						= 0.0;
		protected var _lastX:Number							= 0.0;
		protected var _lastY:Number							= 0.0;
		private var _mouseDown:Boolean						= false;
		private var _startX:Number							= 0.0;
		private var _startY:Number							= 0.0;
		
		protected var _dragObj:DisplayObject;
		protected var _hitObj:DisplayObject;
		
		private var _initialized:Boolean					= false;
		private var _paused:Boolean							= false;
		private var _running:Boolean						= false;

		// Constructor and standard functions
		public function DragTracker( dragObject:DisplayObject, hitObject:DisplayObject = null, autoStart:Boolean = true ) 
		{
			_dragObj = dragObject;
			_hitObj = hitObject == null ? _dragObj : hitObject;
			if ( _hitObj != _dragObj ) _hitObj.alpha = 0;
			if ( autoStart ) Start();
		}
		
		// Public functions
		public function get angle():Number
		{ return Utils.pointAngle( dragStartPoint, currentPoint ); }
		
		public function get distance():Number { return _distance; }
		public function get mouseDown():Boolean { return _mouseDown; }
		public function get startX():Number { return _startX; }
		public function get startY():Number { return _startY; }
		
		public function endObjectDrag():void
		{
			_dragging = false;
			_mouseDown = false;
			stopMouseUp();
			stopFrameHandler();
			if ( onDragEnd != null ) onDragEnd( this );
			if ( hasEventListener( DragTracker.DRAG_START ) ) dispatchEvent( new Event( DragTracker.DRAG_END ) );
		}
		
		public function startObjectDrag():void
		{
			if ( isRunning && !isPaused )
			{
				_dragging = true;
				if ( bringToTop ) _dragObj.parent.setChildIndex( _dragObj, _dragObj.parent.numChildren - 1 );
				if ( bringToTop ) _hitObj.parent.setChildIndex( _hitObj, _hitObj.parent.numChildren - 1 );
				if ( onDragStart != null ) onDragStart( this );
				if ( hasEventListener( DragTracker.DRAG_START ) ) dispatchEvent( new Event( DragTracker.DRAG_START ) );
			}
		}
		
		// Standard functions
		public function get isInitialized():Boolean { return _initialized; }
		public function get isPaused():Boolean { return _paused; }
		public function get isRunning():Boolean { return _running; }
		
		public function Destroy():void
		{
			if ( isRunning ) Stop();
			_dragObj = null;
			_hitObj = null;
		}
		
		public function Init():void 
		{ 
			if ( !isInitialized )
			{
				_initialized = true;
			}
		}
		
		public function Pause():void
			{ if ( isRunning && !isPaused ) _paused = true; }
		
		public function Reset():void
		{
			if ( !isInitialized ) Init();
			if ( isRunning ) Stop();

			_distance = 0;
			_dragging = false;
			_dragStartX = 0;
			_dragStartY = 0;
			dragStartPoint.x = 0;
			dragStartPoint.y = 0;
			_lastDelta = 0;
			_lastX = 0;
			_lastY = 0;
			lastPoint.x = 0;
			lastPoint.y = 0;
			_mouseDown = false;
			_paused = false;
			_startX = 0;
			_startY = 0;
		}
		
		public function Resume():void { if ( isRunning && _paused ) _paused = false; }
		
		public function Start():void 
		{
			if ( !isInitialized ) Init();
			if ( isRunning ) Reset();
			startMouseDown();
			_running = true;
			_paused = false; 
		}
		
		public function Stop():void 
		{ 
			if ( isRunning )
			{
				_running = false;
				stopMouseDown();
				stopFrameHandler();
				stopMouseUp();
			}
		}
		
		// Private functions
		private function handleFrame( e:Event ):void
		{
			if ( isRunning && !isPaused )
			{
				if ( tapOnly ) { stopFrameHandler(); return; }
				
				mousePoint.x = _hitObj.stage.mouseX;
				mousePoint.y = _hitObj.stage.mouseY;
				
				_currentX = ( constraint == CONSTRAIN_VERTICAL ) ? 0 : 
							_hitObj.parent.globalToLocal( mousePoint ).x;
				_currentY = ( constraint == CONSTRAIN_HORIZONTAL ) ? 0 : 
							_hitObj.parent.globalToLocal( mousePoint ).y;
							
				currentPoint.x = _currentX;
				currentPoint.y = _currentY;
				_distance = Utils.getDistance( dragStartPoint, currentPoint ) * dragFactor;
				
				if ( _dragging )
				{
					with ( _dragObj )
					{
						_lastX = x;
						_lastY = y;
						lastPoint.x = x;
						lastPoint.y = y;
						x = _startX + ( _currentX - _dragStartX ) * dragFactor;
						y = _startY + ( _currentY - _dragStartY ) * dragFactor;
						
						if ( dragLimit > 0 && _distance > dragLimit )
						{
							var rad:Number = Utils.toRadians( angle );
							x = _startX + Math.cos( rad ) * dragLimit;
							y = _startY + Math.sin( rad ) * dragLimit;
						}
						
						objPoint.x = x;
						objPoint.y = y;
						
						_lastDelta = Utils.getDistance( objPoint, lastPoint );
						if ( _lastDelta >= 1 )
						{
							if ( onDrag != null ) onDrag( this );
							if ( hasEventListener( DragTracker.DRAG ) ) dispatchEvent( new Event( DragTracker.DRAG ) );
						}
						
						var deltaX:Number = x - _lastX;
						var deltaY:Number = y - _lastY;
						var curTime:Number = getTimer();
						var swoosh:Boolean = false;
						
						if ( deltaX > shakeThreshold ) { shakeRight = curTime; swoosh = true; }
						if ( deltaX * -1 > shakeThreshold ) { shakeLeft = curTime; swoosh = true; }
						if ( deltaY > shakeThreshold ) { shakeDown = curTime; swoosh = true; }
						if ( deltaY * -1 > shakeThreshold ) { shakeUp = curTime; swoosh = true; }
						
						if ( swoosh && onSwoosh != null && curTime > lastSwoosh + minSwooshTime ) 
						{
							onSwoosh( this );
							lastSwoosh = curTime;
						}
							
						if ( onShake != null )
						{
							if ( shakeLeft > 0 && shakeRight > 0 && Math.abs( shakeLeft - shakeRight ) < shakeTime && 
								 curTime > lastShake + minShakeTime )
							{
								lastShake = curTime;
								shakeLeft = 0;
								shakeRight = 0;
								if ( onShake != null ) onShake( this );
							}
							
							if ( shakeUp > 0 && shakeDown > 0 && Math.abs( shakeUp - shakeDown ) < shakeTime &&
								 curTime > lastShake + minShakeTime )
							{
								lastShake = curTime;
								shakeUp = 0;
								shakeDown = 0;
								if ( onShake != null ) onShake( this );
							}
						}
					}
				}
				else if ( _distance > dragThreshold ) startObjectDrag();
			}
		}
		
		private function handleMouseDown( e:MouseEvent ):void
		{
			if ( isRunning && !isPaused  &&  ( e.target == _hitObj || e.target.parent == _hitObj ) )
			{
				if ( !tapOnly ) 
				{
					mousePoint.x = _hitObj.stage.mouseX;
					mousePoint.y = _hitObj.stage.mouseY;
					_dragStartX = constraint == CONSTRAIN_VERTICAL ? 0 : 
							  _hitObj.parent.globalToLocal( mousePoint ).x;
					_dragStartY = constraint == CONSTRAIN_HORIZONTAL ? 0 : 
								  _hitObj.parent.globalToLocal( mousePoint ).y;
					dragStartPoint.x = _dragStartX;
					dragStartPoint.y = _dragStartY;
					_currentX = _dragStartX;
					_currentY = _dragStartY;
					currentPoint.x = _currentX;
					currentPoint.y = _currentY;
					_lastX = _dragStartX;
					_lastY = _dragStartY;
					lastPoint.x = _lastX;
					lastPoint.y = _lastY;
					_startX = _dragObj.x;
					_startY = _dragObj.y;
					_mouseDown = true;
					startFrameHandler();
					startMouseUp();
				} else objectTap();
				
				e.stopImmediatePropagation();
			} //else ( App.Log( e.target.name ) );
		}
		
		private function handleMouseUp( e:MouseEvent ):void
		{
			if ( isRunning && _mouseDown )
			{
				stopFrameHandler();
				stopMouseUp();
							
				if ( _dragging ) endObjectDrag();
					else objectTap();
				e.stopImmediatePropagation();
			}
		}
		
		protected function objectTap( ):void
		{
			if ( isRunning && !isPaused )
			{
				if ( onTap != null ) onTap( this );
				if ( hasEventListener( DragTracker.TAP ) ) dispatchEvent( new Event( DragTracker.TAP ) );
			}
		}
		
		private function startFrameHandler():void
		{
			if ( registerFrameHandler != null ) registerFrameHandler( handleFrame );
				else Utils.addHandler( _hitObj, Event.ENTER_FRAME, handleFrame );
		}
		
		private function startMouseDown():void
		{
			if ( registerMouseDown != null ) registerMouseDown( handleMouseDown );
				else Utils.addHandler( _hitObj, MouseEvent.MOUSE_DOWN, handleMouseDown );
		}
		
		private function startMouseUp():void
		{
			if ( registerMouseUp != null ) registerMouseUp( handleMouseUp );
				else Utils.addHandler( _hitObj.stage, MouseEvent.MOUSE_UP, handleMouseUp );
		}
		
		private function stopFrameHandler():void
		{
			if ( removeFrameHandler != null ) removeFrameHandler( handleFrame );
				else Utils.removeHandler( _hitObj, Event.ENTER_FRAME, handleFrame );
		}
		
		private function stopMouseDown():void
		{
			if ( removeMouseDown != null ) removeMouseDown( handleMouseDown );
				else Utils.removeHandler( _hitObj, MouseEvent.MOUSE_DOWN, handleMouseDown );
		}
		
		private function stopMouseUp():void
		{
			if ( removeMouseUp != null ) removeMouseUp( handleMouseUp );
				else if ( _hitObj != null ) Utils.removeHandler( _hitObj.stage, MouseEvent.MOUSE_UP, handleMouseUp );
			_mouseDown = false;
		}
	}
}
