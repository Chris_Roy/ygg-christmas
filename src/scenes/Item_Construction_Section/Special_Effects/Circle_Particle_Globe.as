package scenes.Item_Construction_Section.Special_Effects{
	
	import com.reyco1.physinjector.PhysInjector;
	import com.reyco1.physinjector.data.PhysicsObject;
	import com.reyco1.physinjector.data.PhysicsProperties;
	
	import flash.geom.Point;
	
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2FilterData;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	
	public class Circle_Particle_Globe extends Sprite{
		
		//Core props.
		private var Circle_Size:Number;
		private var Particle_Count:Number;
		private var Physic_Objects:Array;
		private var Creation_Objects:Array;	
		
		//Movements.
		private var Last_Location:Point;
		private var New_Location:Point;
		
		private var Backplate:Quad;
		
		public function Circle_Particle_Globe(Phys:PhysInjector, Size:Number, Flakes:int, Devisions:int, Bar_Small:Boolean = false){
			
			super();		
			
			//Offset screen.
			Phys.globalOffsetX 		= -Costanza.STAGE_OFFSET;
			
			//Core props.
			Circle_Size 			= Size;
			Particle_Count 			= Flakes;			
			Physic_Objects 			= new Array();
			Creation_Objects		= new Array();
			Last_Location			= new Point(0, 0);
			New_Location			= new Point(0, 0);
			Backplate				= new Quad((Size * 2), (Size * 2), 0x000000);			
			Backplate.x 			= -(Backplate.width / 2);
			Backplate.y 			= -(Backplate.height / 2);
			Backplate.alpha 		= 0.0;
			this.addChild(Backplate);
			
			//Snow_Flakes.
			for(var F:int = 0; F < Flakes; F++){
				Create_Snow_Flake(Phys);
			}			
			
			//Walls.
			for(var W:int = 0; W < 9; W++){
				Create_Walls(Phys, ((360 / Devisions) * W), Circle_Size, (Circle_Size * 0.39));
			}		
			
			if(Bar_Small){
				//Create final wall base.
				Create_Walls(Phys, 90, (Circle_Size - 23), Circle_Size + 20);
			}else{
				//Create final wall base.
				Create_Walls(Phys, 90, (Circle_Size - 10), Circle_Size);
			}
			
		}	
		public function Dispose_Particles(Phys:PhysInjector):void{
			
			//Dispose physic.
			for(var X:int = 0; X < Creation_Objects.length; X++){				
				Phys.removePhysics(Creation_Objects[X], true);
			}
			
			//Dispose physic.
			for(var j:int = 0; j < Physic_Objects.length; j++){				
				if((Physic_Objects[j] as PhysicsObject) != null){
					(Physic_Objects[j] as PhysicsObject).dispose();
				}
			}
			
			this.removeChild(Backplate);
			Backplate.dispose();
			
		}
		public function Finalize_Walls(Phys:PhysInjector):void{
			//Walls.
			for(var W:int = 9; W < 16; W++){
				Create_Walls(Phys, ((360 / 16) * W), Circle_Size, (Circle_Size * 0.39));
			}	
		}
		
		//Create walls.
		private function Create_Walls(Phys:PhysInjector, Angle:Number, Distance:Number, Length:Number):void{
			
			//Instantiate.
			var Wall_Quad:Quad 		= new Quad(Length, 5, 0xe80803);
			var Wall_Holder:Sprite 	= new Sprite();
			var Location:Point 		= new Point((Distance * Math.cos((Angle * (Math.PI / 180)))),
												(Distance * Math.sin((Angle * (Math.PI / 180)))));
			
			//Position.
			Wall_Quad.x 			= -(Wall_Quad.width  / 2);
			Wall_Quad.y 			= -(Wall_Quad.height / 2);
			Wall_Quad.alpha			= 0.0;
			Wall_Holder.x 			= Location.x - Phys.globalOffsetX;
			Wall_Holder.y 			= Location.y - Phys.globalOffsetY;			
			Wall_Holder.rotation 	= ((Angle - 90) * (Math.PI / 180));
			
			//Populate.
			Wall_Holder.addChild(Wall_Quad);
			this.addChild(Wall_Holder);
			
			//Add to physics.
			var Wall_Physic:PhysicsObject 	= Phys.injectPhysics(	Wall_Holder,
																	PhysInjector.SQUARE,
																	new PhysicsProperties( {	isDynamic:false,
																								linearDamping:0,
																								friction:8,
																								Density:9,
																								restitution:1	} ));
			
			var FD:b2FilterData 	= new b2FilterData();
			FD.groupIndex 			= -2;
			Wall_Physic.fixture.SetFilterData(FD);
			Wall_Physic.name 		= "Wally";
			
			//Populate array.
			Physic_Objects.push(Wall_Physic);
			Creation_Objects.push(Wall_Holder);
		}	
		
		//Create a flake.
		public function Create_Snow_Flake(Phys:PhysInjector, Big:Boolean = false):void{
			
			var Particle_Name:String 			= ("Shape0" + Math.ceil(Math.random() * 4) + "_Snowflake_0001");
			var Particle_Holder:Sprite 			= new Sprite();
			var Particle_Image:Image 			= new Image(Root.assets.getTexture(Particle_Name));				
			
			//Position.
			Particle_Image.x 					= -(Particle_Image.width / 2);
			Particle_Image.y 					= -(Particle_Image.height / 2);	
			
			if(!Big){
				Particle_Holder.x 					= -Phys.globalOffsetX;
				Particle_Holder.y 					= -Phys.globalOffsetY;
			}else{			
				Particle_Holder.x 					= -400 + ((Math.random() * 200) - 100);
				Particle_Holder.y 					= -600 + ((Math.random() * 200) - 100);				
			}
			
			Particle_Holder.scaleX				= 0.1;
			Particle_Holder.scaleY				= 0.1;
			
			//Populate.
			Particle_Holder.addChild(Particle_Image);			
			this.addChild(Particle_Holder);
			
			//Add to physics.
			var Particle_Physic:PhysicsObject 	= Phys.injectPhysics(	Particle_Holder,
																		PhysInjector.CIRCLE,
																		new PhysicsProperties( { 	isDynamic:true,
																									friction:0.1,
																									linearDamping:1,
																									Density:0.5,
																									Mass:3,
																									restitution:0.5,																									
																									linearVelocity:new b2Vec2(-2, 10)} ));			
			
			var FD:b2FilterData 	= new b2FilterData();
			FD.groupIndex 			= -1;
			Particle_Physic.fixture.SetFilterData(FD);
			Particle_Physic.name 	= "Colly";
			
			
			//Populate array.
			Physic_Objects.push(Particle_Physic);
			Creation_Objects.push(Particle_Holder);
		}
		
		//Update are physic.
		public function Update_Particles(Phys:PhysInjector):void{				
			
			//Update New position.
			New_Location.x 			= this.x;
			New_Location.y 			= this.y;			
			
			//Set globe location.
			Phys.globalOffsetX 		= this.x;
			Phys.globalOffsetY 		= this.y;
			
			//Strength of the push.			
			var Force:Number 		= Point.distance(Last_Location, New_Location) * 9;
			var Direction:Point 	= new Point((Last_Location.x - New_Location.x),
												(Last_Location.y - New_Location.y));			
			
			Phys.update();
			
			//Update last position.
			Last_Location.x = this.x;
			Last_Location.y = this.y;
			
		}
				
		public function Force_Addition(Phys:PhysInjector):void{
			
			for(var X:int = 0; X < Phys.bodies.length; X++){			
				var Force_Y:Number = (Math.random() * -80);	
				var Force_X:Number = ((Math.random() * 100) - 50);	
				Phys.bodies[X].ApplyForce(new b2Vec2(Force_X, Force_Y), Phys.bodies[X].GetWorldCenter());				
			}	
			
		}
		
		public function Force_Side(Phys:PhysInjector):void{
			
			for(var X:int = 0; X < Phys.bodies.length; X++){				
				var Force_X:Number = ((Math.random() * 20) - 10);				
				Phys.bodies[X].ApplyForce(new b2Vec2(Force_X, 0), Phys.bodies[X].GetWorldCenter());				
			}	
			
		}	
	}
}