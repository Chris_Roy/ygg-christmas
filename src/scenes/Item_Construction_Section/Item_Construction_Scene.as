package scenes.Item_Construction_Section
{
	import com.cupcake.Utils;
	import com.greensock.TweenMax;
	import com.reyco1.physinjector.PhysInjector;
	
	import flash.filesystem.File;
	import flash.geom.Point;
	import flash.system.System;
	
	import Box2D.Common.Math.b2Vec2;
	
	import dragonBones.animation.WorldClock;
	
	import scenes.Item_Construction_Section.Special_Effects.Circle_Particle_Globe;
	import scenes.Particle_Effects.Burst_Item_Building_Particle;
	import scenes.Particle_Effects.Center_To_Full_Burst;
	import scenes.Particle_Effects.Glitter_Box_Particle;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.extensions.SoundManager;
	import starling.utils.AssetManager;
	
	import utils.Basic_Dragon_Bones_Model;

	public class Item_Construction_Scene extends Sprite
	{
		//Core.
		private var soundmanager:SoundManager;
		private var assets:AssetManager;
		private var sceneRoot:Sprite;
		private var assetsLoaded:Boolean 			= false;
		private var appDir:File 					= File.applicationDirectory;

		//Scene assets.
		private var Background:Image;
		private var Brodys_Arm:Image;
		private var Item_To_Place:Image;

		//Dragons.
		private var Final_Image_Capture:Sprite;
		private var Character_Dragon:Basic_Dragon_Bones_Model;
		private var Creation_Item_Dragon:Basic_Dragon_Bones_Model;
		private var Idea_Item_Dragon:Basic_Dragon_Bones_Model;
		private var Idea_Bubble_Dragon:Basic_Dragon_Bones_Model;

		//Particles.
		private var Placement_Explosion:Glitter_Box_Particle;
		private var Item_Spray_Explosion:Burst_Item_Building_Particle;
		private var Final_Blast:Center_To_Full_Burst;

		//Scene variables.
		private var Game_Mode:String;
		private var Build_States:Array;
		private var Mouse_Enabled:Boolean;
		private var Tap_Timmer:Number;
		
		private var animationPlayed:int = 0;

		//Specials.
		private var Snow_Globe_Holder:Sprite;
		private var Snow_Globe_Particles:Circle_Particle_Globe;
		private var Small_Snow_Globe_Particles:Circle_Particle_Globe;

		//Temp
		private var Clock_Active:Boolean = true;

		//The Physic.
		private var physics:PhysInjector;
		private var Physic_Update:Boolean;
		private var reactionSounds:Array = new Array();
		
		private var tapSound:int = 1;


		public function Item_Construction_Scene(Value:String = "")
		{
			super();

			//Set the item we will be using.
			Root.Gaba_Core_Data.Set_Next_Building_Item();

			//Special conditions of loading.
			var Item_Load_Section:String = ("textures/%SCALE%x/Building_Item_Screen/Items/" + Root.Gaba_Core_Data.Current_Building_Item);
			switch(Root.Gaba_Core_Data.Current_Building_Item){
				case "Ginger_Bread":{
					Item_Load_Section = ("textures/%SCALE%x/Building_Item_Screen/Items/" 	+ Root.Gaba_Core_Data.Current_Building_Item + "/" + Root.Gaba_Core_Data.Get_Ginger_Bread_Sex());
				}break;
			}

			assets = Utils.CreateAssetManager([
				//Particles.
				"textures/%SCALE%x/Particles/Star_Particle.png",

				//The Burst
				"textures/%SCALE%x/Building_Item_Screen/Bursts/" 		+ (Root.Gaba_Core_Data.Current_Character + "_Burst" + ".png"),

				//The Idea selection.
				//"textures/%SCALE%x/Building_Item_Screen/Idea_Bubble/" 	+ Root.Gaba_Core_Data.Current_Character,
				"textures/%SCALE%x/Building_Item_Screen/Idea_Bubble/" 	+ "Muno",
				"textures/%SCALE%x/Building_Item_Screen/Item_Display/" 	+ Root.Gaba_Core_Data.Current_Building_Item,

				//Elements for screen.
				"textures/%SCALE%x/Building_Item_Screen/Characters/" 	+ Root.Gaba_Core_Data.Current_Character,
				"textures/%SCALE%x/Building_Item_Screen/Build_Peices/" 	+ Root.Gaba_Core_Data.Current_Building_Item,
				Item_Load_Section,
				"textures/%SCALE%x/Screen_Global_Elements/Backgrounds/"	+ (Root.Gaba_Core_Data.Current_Character + "_Background" + ".png"),
				"textures/%SCALE%x/Screen_Global_Elements/Globals",

				//Audio.
				"audio/Music/Music_" + Root.Gaba_Core_Data.Current_Character.toString() + ".mp3",
				"audio/Global",
				"audio/Building_Items/"+Root.Gaba_Core_Data.Current_Building_Item.toString(),
				"audio/Building_Items/"+Root.Gaba_Core_Data.Current_Character.toString(),
				"audio/Building_Items/GenericItems"
			]);

			Root.currentAssetsProxy = assets;

			assets.loadQueue(function onProgress(ratio:Number):void
			{
				if (ratio == 1){
					Starling.juggler.delayCall(function():void
					{
						// now would be a good time for a clean-up 
						System.pauseForGCIfCollectionImminent(0);
						System.gc();
					}, 0.15);
					addObjects();
				};
			});
		}

		private function addObjects():void
		{
			//Add Dispose Event.
			addEventListener(Root.DISPOSE,  disposeScene);
			
			//Set are physic.
			Physic_Update			= true;
			PhysInjector.STARLING 	= true;
			physics 				= new PhysInjector(	Starling.current.nativeStage,
														new b2Vec2(0, 2),
														false	);

			//The states of play.
			Build_States					= Root.Gaba_Core_Data.Get_Build_Item_States();

			//Create Scene.
			sceneRoot 						= new Sprite();
			sceneRoot.x 					= Costanza.STAGE_OFFSET;
			addChild(sceneRoot);

			//Create Sounds.
			soundmanager = new SoundManager();
			Root.AddSM( soundmanager );

			//Music.

			//SFX.
			soundmanager.addSound("Foo_Too_Explo",					assets.getSound("Foo_Too_Explo"));
			soundmanager.addSound("Muno_Plex_Explo",				assets.getSound("Muno_Plex_Explo"));
			soundmanager.addSound("Small_Explosion",				assets.getSound("Small_Explosion"));
			soundmanager.addSound("Drop_Item_Miss",					assets.getSound("Drop_Item_Miss"));
			soundmanager.addSound("Pick_Up_Item",					assets.getSound("Pick_Up_Item"));
			soundmanager.addSound("Back",							assets.getSound("Back_Button"));

			switch(Root.Gaba_Core_Data.Current_Building_Item)
			{
				case "Hot_Cocoa":
				{
					//Hot Cocoa.
					soundmanager.addSound("EGGNOG_Cream",					assets.getSound("EGGNOG_Cream"));
					soundmanager.addSound("EGGNOG_Marshmellow",				assets.getSound("EGGNOG_Marshmellow"));
					soundmanager.addSound("EGGNOG_Milk",					assets.getSound("EGGNOG_Milk"));
					soundmanager.addSound("EGGNOG_Shaker",					assets.getSound("EGGNOG_Shaker"));
					soundmanager.addSound("EGGNOG_Straw",					assets.getSound("EGGNOG_Straw"));
					soundmanager.addSound("EGGNOG_Syrup",					assets.getSound("EGGNOG_Syrup"));
					break;
				}break;

				case "Christmas_Tree":
				{
					//Christams tree.
					soundmanager.addSound("Christmas_Garland",				assets.getSound("Christmas_Garland"));
					soundmanager.addSound("Christmas_Star",					assets.getSound("Christmas_Star"));
					soundmanager.addSound("Christmas_Tree_Cady_Cade",		assets.getSound("Christmas_Tree_Cady_Cade"));
					soundmanager.addSound("Christmas_Tree_Lights",			assets.getSound("Christmas_Tree_Lights"));
					break;
				}break;
				
				case "Snow_Man":{
					//Snow Man.
					soundmanager.addSound("Snow_Man_Arm_1",					assets.getSound("Snow_Man_Arm_1"));
					soundmanager.addSound("Snow_Man_Arm_2",					assets.getSound("Snow_Man_Arm_2"));
					soundmanager.addSound("Snow_Man_Ball_1",				assets.getSound("Snow_Man_Ball_1"));
					soundmanager.addSound("Snow_Man_Ball_2",				assets.getSound("Snow_Man_Ball_2"));
					soundmanager.addSound("Snow_Man_Face",					assets.getSound("Snow_Man_Face"));
					soundmanager.addSound("Snow_Man_Hat",					assets.getSound("Snow_Man_Hat"));
					break;
				}break;

				case "Ginger_Bread":{
					//Ginger Bread.
					soundmanager.addSound("Ginger_Bread_Candy",				assets.getSound("Ginger_Bread_Candy"));
					soundmanager.addSound("Ginger_Bread_Cutout",			assets.getSound("Ginger_Bread_Cutout"));
					soundmanager.addSound("Ginger_Bread_Icing",				assets.getSound("Ginger_Bread_Icing"));
					soundmanager.addSound("Ginger_Bread_Shaker",			assets.getSound("Ginger_Bread_Shaker"));
					break;
				}break;

				case "Snow_Globe":{
					//Snow Globe
					soundmanager.addSound("Snow_Globe_Fill",				assets.getSound("Snow_Globe_Fill"));
					soundmanager.addSound("Snow_Globe_Place_Ornament",		assets.getSound("Snow_Globe_Place_Ornament"));
					soundmanager.addSound("Snow_Globe_Shaker",				assets.getSound("Snow_Globe_Shaker"));
					break;
				}break;				
			}

			//Play music.
			switch(Root.Gaba_Core_Data.Current_Character)
			{				
				case "Muno":
				{
					soundmanager.addSound("Music_Muno",						assets.getSound("Music_Muno"));
					soundmanager.addSound("Muno_1",							assets.getSound("Muno_1"));
					soundmanager.addSound("Muno_2",							assets.getSound("Muno_2"));
					soundmanager.addSound("Muno_3",							assets.getSound("Muno_3"));
					soundmanager.addSound("Muno_4",							assets.getSound("Muno_4"));
					reactionSounds = ["MunoVO1","MunoVO2","MunoVO3"];
					soundmanager.addSound("MunoVO1", assets.getSound("Muno VO - yeah"));
					soundmanager.addSound("MunoVO2", assets.getSound("Muno VO - ok yeah"));
					soundmanager.addSound("MunoVO3", assets.getSound("Muno VO - you are awesome"));
					
					soundmanager.addSound("Muno_Last_Item",					assets.getSound("Muno_Last_Item"));
				}break;
				case "Brobee":
				{
					soundmanager.addSound("Music_Brobee",					assets.getSound("Music_Brobee"));
					soundmanager.addSound("Brobee_1",						assets.getSound("Brobee_1"));
					soundmanager.addSound("Brobee_2",						assets.getSound("Brobee_2"));
					soundmanager.addSound("Brobee_3",						assets.getSound("Brobee_3"));
					soundmanager.addSound("Brobee_4",						assets.getSound("Brobee_4"));
					reactionSounds = ["BrobeeVO1","BrobeeVO2","BrobeeVO3"];
					soundmanager.addSound("BrobeeVO1", assets.getSound("Brobee VO - Hooray"));
					soundmanager.addSound("BrobeeVO2", assets.getSound("Brobee VO - Hooray"));
					soundmanager.addSound("BrobeeVO3", assets.getSound("Brobee VO - laughs"));
					
					soundmanager.addSound("Brobee_Last_Item",				assets.getSound("Brobee_Last_Item"));
				}break;
				case "Plex":
				{
					soundmanager.addSound("Music_Plex",						assets.getSound("Music_Plex"));
					soundmanager.addSound("Plex_1",							assets.getSound("Plex_1"));
					soundmanager.addSound("Plex_2",							assets.getSound("Plex_2"));
					soundmanager.addSound("Plex_3",							assets.getSound("Plex_3"));
					soundmanager.addSound("Plex_4",							assets.getSound("Plex_4"));
					reactionSounds = ["PlexVO1","PlexVO2","PlexVO3"];
					soundmanager.addSound("PlexVO1", assets.getSound("Plex VO - alright"));
					soundmanager.addSound("PlexVO2", assets.getSound("Plex VO - amazing"));
					soundmanager.addSound("PlexVO3", assets.getSound("Plex VO - way to go"));
					
					soundmanager.addSound("Plex_Last_Item",					assets.getSound("Plex_Last_Item"));
				}break;					
				case "Toodee":
				{
					soundmanager.addSound("Music_Toodee",					assets.getSound("Music_Toodee"));
					soundmanager.addSound("Toodee_1",						assets.getSound("Toodee_1"));
					soundmanager.addSound("Toodee_2",						assets.getSound("Toodee_2"));
					soundmanager.addSound("Toodee_3",						assets.getSound("Toodee_3"));
					soundmanager.addSound("Toodee_4",						assets.getSound("Toodee_4"));
					reactionSounds = ["ToodeeVO1","ToodeeVO2","ToodeeVO3"];
					soundmanager.addSound("ToodeeVO1", assets.getSound("Toodee VO - don't stop, you just got started"));
					soundmanager.addSound("ToodeeVO2", assets.getSound("Toodee VO - fun"));
					soundmanager.addSound("ToodeeVO3", assets.getSound("Toodee VO - yeah woo"));
					
					soundmanager.addSound("Toodee_Last_Item",				assets.getSound("Toodee_Last_Item"));
				}break;
				case "Foofa":
				{
					soundmanager.addSound("Music_Foofa",					assets.getSound("Music_Foofa"));
					soundmanager.addSound("Foofa_1",						assets.getSound("Foofa_1"));
					soundmanager.addSound("Foofa_2",						assets.getSound("Foofa_2"));
					soundmanager.addSound("Foofa_3",						assets.getSound("Foofa_3"));
					soundmanager.addSound("Foofa_4",						assets.getSound("Foofa_4"));
					reactionSounds = ["FoofaVO1","FoofaVO2","FoofaVO3"];
					
					soundmanager.addSound("FoofaVO1", assets.getSound("Foofa VO - try this"));
					soundmanager.addSound("FoofaVO2", assets.getSound("Foofa VO - way to go"));
					soundmanager.addSound("FoofaVO3", assets.getSound("Foofa VO - wonderful"));
					
					soundmanager.addSound("Foofa_Last_Item",				assets.getSound("Foofa_Last_Item"));
				}break;						
			}
			
			//Play music.
			switch(Root.Gaba_Core_Data.Current_Character){				
				case "Muno":{
					soundmanager.playSound("Music_Muno", 		Costanza.musicVolume,	999);
				}break;
				case "Brobee":{
					soundmanager.playSound("Music_Brobee", 		Costanza.musicVolume,	999);
				}break;
				case "Plex":{
					soundmanager.playSound("Music_Plex", 		Costanza.musicVolume,	999);
				}break;					
				case "Toodee":{
					soundmanager.playSound("Music_Toodee", 		Costanza.musicVolume,	999);
				}break;
				case "Foofa":{
					soundmanager.playSound("Music_Foofa", 		Costanza.musicVolume,	999);
				}break;						
			}

			//Background.
			Background 						= new Image(assets.getTexture((Root.Gaba_Core_Data.Current_Character + "_Background")));				
			Background.x 					= 0;
			Background.y 					= 0;
			sceneRoot.addChild(Background);

			//The Tought Bubble.
			Idea_Bubble_Dragon				= new Basic_Dragon_Bones_Model(assets, "Idea_Bubble", "Idea_Bubble", "!!Characters", sceneRoot);
			Idea_Bubble_Dragon.x 			= Root.Gaba_Core_Data.Current_Idea_Bubble_Location.x;
			Idea_Bubble_Dragon.y 			= Root.Gaba_Core_Data.Current_Idea_Bubble_Location.y;

			//The Item Chosen.
			Idea_Item_Dragon				= new Basic_Dragon_Bones_Model(assets, (Root.Gaba_Core_Data.Current_Building_Item + "_ItemBubble"), (Root.Gaba_Core_Data.Current_Building_Item + "_ItemBubble"), "!!Characters", sceneRoot);			
			Idea_Item_Dragon.x 				= (Root.Gaba_Core_Data.Current_Idea_Item_Location.x - (Idea_Item_Dragon.width  / 2));
			Idea_Item_Dragon.y 				= (Root.Gaba_Core_Data.Current_Idea_Item_Location.y - (Idea_Item_Dragon.height / 2));	

			//Small Snow Globe Particles.
			if(Root.Gaba_Core_Data.Current_Building_Item == "Snow_Globe"){
				Small_Snow_Globe_Particles	= new Circle_Particle_Globe(physics, 88.0, 10, 16, true);
				Small_Snow_Globe_Particles.Finalize_Walls(physics);
				Small_Snow_Globe_Particles.x = 428;
				Small_Snow_Globe_Particles.y = 255;
				sceneRoot.addChild(Small_Snow_Globe_Particles);
				Small_Snow_Globe_Particles.Force_Addition(physics);
				Small_Snow_Globe_Particles.Force_Side(physics);
			}		

			//Final image holder.
			Final_Image_Capture				= new Sprite();

			//Snow Globe Particles.
			//Snow_Globe_Particles 			= new Circle_Particle_Globe(physics, 165.0, 0, 16);
			//Final_Image_Capture.addChild(Snow_Globe_Particles);	
			//Snow_Globe_Particles.Force_Addition(physics);
			//Snow_Globe_Particles.Force_Side(physics);		

			//Build Item.
			Creation_Item_Dragon			= new Basic_Dragon_Bones_Model(assets, Root.Gaba_Core_Data.Current_Building_Item, Root.Gaba_Core_Data.Current_Building_Item, "!!Characters", Final_Image_Capture);
			Creation_Item_Dragon.scaleX 	= Creation_Item_Dragon.scaleY = Root.Gaba_Core_Data.Current_Items_Build_Scale;
			Creation_Item_Dragon.x 			= Root.Gaba_Core_Data.Current_Items_Build_Location.x;
			Creation_Item_Dragon.y 			= Root.Gaba_Core_Data.Current_Items_Build_Location.y;
			Swap_Bones();
			Creation_Item_Dragon.Play_This(Build_States[0][1]);
			Build_States.splice(0, 1);		
			Creation_Item_Dragon.visible 	= false;	

			if(Root.Gaba_Core_Data.Current_Building_Item == "Snow_Globe"){
				Idea_Item_Dragon.Spawp_Bone_Image(		"sparklesGrey", "Visible_Off", assets);
				Creation_Item_Dragon.Spawp_Bone_Image(	"sparkles", 	"Visible_Off", assets);
				Creation_Item_Dragon.Spawp_Bone_Image(	"sparkles0", 	"Visible_Off", assets);
			}

			sceneRoot.addChild(Final_Image_Capture);	

			//Burst.
			Item_Spray_Explosion 			= new Burst_Item_Building_Particle(assets.getTexture((Root.Gaba_Core_Data.Current_Character + "_Burst")), Root.Gaba_Core_Data.Current_Character);

			//Character.
			Character_Dragon				= new Basic_Dragon_Bones_Model(assets, Root.Gaba_Core_Data.Current_Character, Root.Gaba_Core_Data.Current_Character, "!!Characters", sceneRoot);
			Character_Dragon.x 				= (947 - (Character_Dragon.width / 2));
			Character_Dragon.y 				= (728 - Character_Dragon.height);
			Character_Dragon.Play_This(Root.Gaba_Core_Data.Get_Character_Selection_Animation());

			//Broodys arm.
			Brodys_Arm						= new Image(assets.getTexture("DJ_Hand"));	
			Brodys_Arm.x 					= 1366;
			Brodys_Arm.y 					= 200;
			sceneRoot.addChild(Brodys_Arm);	

			//Particles.
			Placement_Explosion 			= new Glitter_Box_Particle(assets.getTexture("Star_Particle"), new Point(500, 400));
			Placement_Explosion.x 			= 0;
			Placement_Explosion.y 			= 0;
			sceneRoot.addChild(Placement_Explosion);

			//Final snowflake blast.
			Final_Blast						= new Center_To_Full_Burst();
			Final_Blast.x 					= 0;
			Final_Blast.y 					= 0;

			//Game variables.
			Game_Mode						= "First_Begin";
			Tap_Timmer						= 0;

			//SCene is loaded.
			dispatchEventWith(Root.SCENE_LOADED, true);

			//Add Listeners.
			addEventListener(starling.events.EnterFrameEvent.ENTER_FRAME, 					ActivitySelection_Update_Scene);
			addEventListener(starling.events.TouchEvent.TOUCH, 								ActivitySelection_Mouse_Scene);

			//Enable mouse.
			Mouse_Enabled					= true;
		}

		//Buttons functionalities.
		private function goBack(e:starling.events.Event):void{	
			//Remove Listeners.
			removeEventListener(starling.events.EnterFrameEvent.ENTER_FRAME, 				ActivitySelection_Update_Scene);
			removeEventListener(starling.events.TouchEvent.TOUCH, 							ActivitySelection_Mouse_Scene);			
			Root.Gaba_Core_Data.Call_To_Reset();
			TweenMax.delayedCall(0.3, dispatchEventWith,[Root.LOAD_ROOM, true, {room:8, Character:"Mira"}]);			
		}
		
		//Leave.
		private function Exit_Game():void{
			//Remove Listeners.
			removeEventListener(starling.events.EnterFrameEvent.ENTER_FRAME, 				ActivitySelection_Update_Scene);
			removeEventListener(starling.events.TouchEvent.TOUCH, 							ActivitySelection_Mouse_Scene);
			TweenMax.delayedCall(0.3, dispatchEventWith,[Root.LOAD_ROOM, true, {room:7, Character:"Mira"}]);
		}
		
		

		//Clean.
		private function disposeScene():void
		{
			//Clear Sounds.
			soundmanager.stopAllSounds();
			Root.RemoveSM( soundmanager );
			soundmanager.dispose();
			
			
			//Dispose Dragons.
			if(Snow_Globe_Particles != null){
				Final_Image_Capture.removeChild(Snow_Globe_Particles);
			}
			Character_Dragon.Dispose(sceneRoot);
			Creation_Item_Dragon.Dispose(Final_Image_Capture);
			Final_Image_Capture.dispose();
			if(Snow_Globe_Particles != null){
				Snow_Globe_Particles.Dispose_Particles(physics);
				Snow_Globe_Particles.dispose();
				Snow_Globe_Particles = null;
			}
			if(sceneRoot.contains(Idea_Bubble_Dragon)){Idea_Bubble_Dragon.Dispose(sceneRoot);}	
			if(sceneRoot.contains(Idea_Item_Dragon)){Idea_Item_Dragon.Dispose(sceneRoot);}	
			
			if(Small_Snow_Globe_Particles != null)
			{						
				if(sceneRoot.contains(Small_Snow_Globe_Particles))
				{
					sceneRoot.removeChild(Small_Snow_Globe_Particles);
					Small_Snow_Globe_Particles.Dispose_Particles(physics);
					Small_Snow_Globe_Particles.dispose();
					Small_Snow_Globe_Particles = null;
				}
			}
			
			//Dispose Images.
			if(sceneRoot.contains(Background)){sceneRoot.removeChild(Background);}
			Background.base.dispose();
			Background.dispose();
			
			if(sceneRoot.contains(Brodys_Arm)){sceneRoot.removeChild(Brodys_Arm);}
			Brodys_Arm.base.dispose();
			Brodys_Arm.dispose();
			
			if(sceneRoot.contains(Item_To_Place)){sceneRoot.removeChild(Item_To_Place);}
			if(Item_To_Place != null){
				Item_To_Place.base.dispose();
				Item_To_Place.dispose();
			}
			
			//Particles.
			sceneRoot.removeChild(Placement_Explosion);
			sceneRoot.removeChild(Final_Blast);
			Placement_Explosion.Dispose_Glitter_Box_Particle();
			Final_Blast.Dispose_Center_To_Full_Burst();

			//Clean remaning is forgoten.
			sceneRoot.removeChildren(0, (sceneRoot.numChildren - 1), true);
			
			//Clear Asset Manager.
			assets.purge();
			assets.dispose();
		}
		
		private function PlayAnimSound(intNum:int):void
		{
			soundmanager.playSound(reactionSounds[intNum]);
		}
		
		private function Activate_Action_Placement():void
		{
			//Clean image.
			TweenMax.killTweensOf(Item_To_Place);
			if(sceneRoot.contains(Item_To_Place)){sceneRoot.removeChild(Item_To_Place);}
			//var randomAnimNum:int = Math.floor(Math.random() * Root.Gaba_Core_Data.Current_Character_Reaction_Animations.length)
			animationPlayed++;
			if(animationPlayed > 2)
			{
				animationPlayed = 0;
			}
			//Edit the dragon model.
			Swap_Bones();
			Creation_Item_Dragon.Play_This(Build_States[0][1]);
			Character_Dragon.Play_This(Root.Gaba_Core_Data.Get_Character_Selection_Animation("Reaction",animationPlayed));
			PlayAnimSound(animationPlayed);
			
			//Play Explosion.
			sceneRoot.setChildIndex(Placement_Explosion, (sceneRoot.numChildren - 1));			
			Placement_Explosion.x = Build_States[0][4][1];
			Placement_Explosion.y = Build_States[0][4][2];
			Placement_Explosion.Activate_Glitter_Box_Particle(Build_States[0][4]);
			
			if(Build_States[0][0][1] == "sparkleShaker")
			{
				//Delay drop Snow.
				TweenMax.delayedCall(1.0, Delay_Calls, ["Sparkle_Drop"]);
				TweenMax.delayedCall(1.2, Delay_Calls, ["Sparkle_Drop"]);
				TweenMax.delayedCall(1.4, Delay_Calls, ["Sparkle_Drop"]);
			}

			if(Build_States[0][5])
			{
				TweenMax.delayedCall(Build_States[0][5][0], Delay_Sounds, [Build_States[0][5][1]]);
			}

			//Remove a state.
			Build_States.splice(0, 1);

			//Delay for next prep.
			//TweenMax.delayedCall(0.2, Clock_State, ["Shutdown"]);
			TweenMax.delayedCall(2.0, Delay_Calls, ["Get_New_Item"]);
			Game_Mode = "Pergatory";
		}

		//Call to update bones and images.
		private function Swap_Bones():void
		{
			if(Build_States[0][2].length != 0)
			{
				for(var X:int = 0; X < Build_States[0][2].length; X++)
				{
					if(Build_States[0][3].length != 0)
					{
						Creation_Item_Dragon.Spawp_Bone_Image(Build_States[0][2][X][0], Build_States[0][2][X][1], assets, Build_States[0][3][X]);
					}else{
						Creation_Item_Dragon.Spawp_Bone_Image(Build_States[0][2][X][0], Build_States[0][2][X][1], assets);
					}
				}
			}
		}

		//Update.
		private function ActivitySelection_Update_Scene(e:starling.events.EnterFrameEvent):void{
			Tap_Timmer -= 33;
			if(Tap_Timmer <= 0){Tap_Timmer = 0;}
			
			switch(Game_Mode)
			{
				case "First_Begin":{					
					TweenMax.delayedCall(1.0, Delay_Calls, ["First_Begin"]);
					TweenMax.delayedCall(3.0, Delay_Calls, ["Small_Globe"]);
					Game_Mode = "Pergatory";
				}break;				
				
				//Click to pop.
				case "Pop_Action_Click":{
					
				}break;
				case "Pop_Action_Update":{
					//Spawn Particles.
					Setup_Burst();
					Game_Mode 						= "Pergatory";
				}break;
				case "Pop_Action_Clear":{
					Character_Dragon.Play_This(Root.Gaba_Core_Data.Get_Character_Selection_Animation("Tap"));
					Creation_Item_Dragon.Play_This(Build_States[0][1]);
					Build_States.splice(0, 1);
					Game_Mode 						= "Prep_Item";
				}break;	
				
				//Here we create the item we will place on the Object.
				case "Prep_Item":
				{
					//Check if done.
					if(Build_States.length == 1)
					{
						//Play End character sound.
						switch(Root.Gaba_Core_Data.Current_Character)
						{
							case "Muno":{	soundmanager.playSound("Muno_Last_Item", Costanza.soundVolume);}break;
							case "Brobee":{	soundmanager.playSound("Brobee_Last_Item", Costanza.soundVolume);}break;
							case "Plex":{	soundmanager.playSound("Plex_Last_Item", Costanza.soundVolume);}break;
							case "Toodee":{	soundmanager.playSound("Toodee_Last_Item", Costanza.soundVolume);}break;
							case "Foofa":{	soundmanager.playSound("Foofa_Last_Item", Costanza.soundVolume);}break;
						}
						Game_Mode = "Wait_Celebrate";
						return;
					}

					//Clean Item.
					if(Item_To_Place != null)
					{
						if(sceneRoot.contains(Item_To_Place)){sceneRoot.removeChild(Item_To_Place);}
						Item_To_Place.base.dispose();
						Item_To_Place.dispose();
						Item_To_Place = null;
					}
					//Create item.
					Item_To_Place 			= new Image(assets.getTexture(Build_States[0][0][0]));
					Item_To_Place.scaleX 	= Item_To_Place.scaleY = Root.Gaba_Core_Data.Current_Items_Build_Scale;
					Game_Mode 				= "Show_Item";
				}break;
				case "Show_Item":{
					Item_To_Place.x = ((Brodys_Arm.x + 160) - (Item_To_Place.width  / 2));
					Item_To_Place.y = ((Brodys_Arm.y + 190) - (Item_To_Place.height / 2));
					if(!sceneRoot.contains(Item_To_Place)){sceneRoot.addChild(Item_To_Place);}

					TweenMax.killTweensOf(Brodys_Arm);
					TweenMax.to(Brodys_Arm, 2.0, {x:800, onComplete:Delay_Calls, onCompleteParams:["Show_Item"]});
					
					Game_Mode = "Move_Item";
				}break;
				case "Move_Item":{
					Item_To_Place.x = ((Brodys_Arm.x + 160) - (Item_To_Place.width  / 2));
					Item_To_Place.y = ((Brodys_Arm.y + 190) - (Item_To_Place.height / 2));
					
				}break;		
				case "Pick_Up_Item":{					
					
				}break;	
				case "Moving_Item":{
					
				}break;
				case "Falling_Item":{				
					
				}break;
				case "Wait_Celebrate":{	
					Creation_Item_Dragon.Play_This(Build_States[0][4]);
					Mouse_Enabled	= false;
					Game_Mode 		= "Pergatory";
					TweenMax.delayedCall(2.0, Delay_Calls, ["Wait_Celebrate"]);						
				}break;	
				
			}
			
			if(Clock_Active){
				WorldClock.clock.advanceTime(-1);	
				if(Physic_Update){
					if(Root.Gaba_Core_Data.Current_Building_Item == "Snow_Globe"){
						if(Snow_Globe_Particles != null){
							Snow_Globe_Particles.Update_Particles(physics);
						}
						if(Small_Snow_Globe_Particles != null){	
							Small_Snow_Globe_Particles.Update_Particles(physics);
						}
					}
				}
			}		
			
			if(Physic_Update){
				if(Root.Gaba_Core_Data.Current_Building_Item == "Snow_Globe"){					
					if(Small_Snow_Globe_Particles != null){	
						if(sceneRoot.contains(Idea_Item_Dragon)){							
							Small_Snow_Globe_Particles.x = (Idea_Item_Dragon.Get_Bone_Location("globes").x + 428);
							Small_Snow_Globe_Particles.y = (Idea_Item_Dragon.Get_Bone_Location("globes").y + 255);
						}
					}
					if(Snow_Globe_Particles != null){						
						Snow_Globe_Particles.x = (Creation_Item_Dragon.Get_Bone_Location("globeLargeLineOL").x + 427);
						Snow_Globe_Particles.y = (Creation_Item_Dragon.Get_Bone_Location("globeLargeLineOL").y + 220);
					}
				}
			}
			
		}
		private function Delay_Sounds(The_Sound:String):void{
			soundmanager.playSound(The_Sound);				
		}
		private function Delay_Calls(The_Call:String):void{
			
			switch(The_Call){
				
				case "First_Begin":{				
				
					Game_Mode = "Pop_Action_Click";
					
				}break;	
				case "Small_Globe":{
					if(Small_Snow_Globe_Particles != null){
						Small_Snow_Globe_Particles.Force_Addition(physics);
						Small_Snow_Globe_Particles.Force_Side(physics);
						TweenMax.delayedCall(1.8, Delay_Calls, ["Small_Globe"]);
					}
				}break;
				case "Burst":{
					
					if(Small_Snow_Globe_Particles != null){						
						if(sceneRoot.contains(Small_Snow_Globe_Particles)){
							//trace("Clean");
							sceneRoot.removeChild(Small_Snow_Globe_Particles);
							Small_Snow_Globe_Particles.Dispose_Particles(physics);
							Small_Snow_Globe_Particles.dispose();
							Small_Snow_Globe_Particles = null;
							
							physics 				= new PhysInjector(	Starling.current.nativeStage,
																		new b2Vec2(0, 2),
																		false	);	
							
							//Snow Globe Particles.
							Snow_Globe_Particles 			= new Circle_Particle_Globe(physics, 165.0, 0, 16);
							Final_Image_Capture.addChildAt(Snow_Globe_Particles, 0);
							Snow_Globe_Particles.Force_Addition(physics);
							Snow_Globe_Particles.Force_Side(physics);
						}
					}
					
					soundmanager.playSound("Small_Explosion");			
					
					Item_Spray_Explosion.Activate_Burst("Begin", Halt_Burst);
					//Remove bubble stuff.
					if(sceneRoot.contains(Idea_Bubble_Dragon)){Idea_Bubble_Dragon.Dispose(sceneRoot);}	
					if(sceneRoot.contains(Idea_Item_Dragon)){Idea_Item_Dragon.Dispose(sceneRoot);}	
					
					//Show building item.
					Creation_Item_Dragon.visible 	= true;					
					Game_Mode = "Pergatory";
				}break;
				case "Wait_Burst_Clear":{	
					Game_Mode = "Pop_Action_Clear";
				}break;
				case "Show_Item":{
					Game_Mode = "Pick_Up_Item";
				}break;
				case "Falling_Item":{
					Game_Mode = "Show_Item";
				}break;
				case "Moving_Item_To_Drop_Spot":{
					Activate_Action_Placement();
				}break;
				case "Get_New_Item":{
					Game_Mode = "Prep_Item";
				}break;
				case "Sparkle_Drop":{
					for(var X:int = 0; X < 16; X++){
						Snow_Globe_Particles.Create_Snow_Flake(physics, true);
					}					
				}break;
				case "Wait_Celebrate":{	
					
					//Hide Sparkles.
					switch(Root.Gaba_Core_Data.Current_Building_Item){
						
						case "Hot_Cocoa":{
							//Hot Cocoa.
							Creation_Item_Dragon.Spawp_Bone_Image("sparkle02", 	"Visible_Off", assets);
							Creation_Item_Dragon.Spawp_Bone_Image("sparkle01", 	"Visible_Off", assets);
						}break;
						
						case "Christmas_Tree":{
							//Christams tree.
							Creation_Item_Dragon.Spawp_Bone_Image("sparkle", 	"Visible_Off", assets);
							Creation_Item_Dragon.Spawp_Bone_Image("sparkle01", 	"Visible_Off", assets);
						}break;
						
						case "Snow_Man":{
							Creation_Item_Dragon.Spawp_Bone_Image("sparkle0", 	"Visible_Off", assets);
							Creation_Item_Dragon.Spawp_Bone_Image("sparkle01", 	"Visible_Off", assets);
							Creation_Item_Dragon.Spawp_Bone_Image("sparkle", 	"Visible_Off", assets);
						}break;
						
						case "Ginger_Bread":{
							Creation_Item_Dragon.Spawp_Bone_Image("sparkle01", 	"Visible_Off", assets);
							Creation_Item_Dragon.Spawp_Bone_Image("sparkle02", 	"Visible_Off", assets);
							Creation_Item_Dragon.Spawp_Bone_Image("sparkle03", 	"Visible_Off", assets);
							Creation_Item_Dragon.Spawp_Bone_Image("sparkles04", "Visible_Off", assets);
						}break;
						
						case "Snow_Globe":{
							//Snow Globe
							Creation_Item_Dragon.Spawp_Bone_Image("sparkle0", 	"Visible_Off", assets);
							Creation_Item_Dragon.Spawp_Bone_Image("sparkle", 	"Visible_Off", assets);
						}break;				
					}			
					
					//Take snapshot.						
					if(Root.Gaba_Core_Data.Current_Building_Item == "Snow_Globe"){
						//Update special location.
						Root.Gaba_Core_Data.Set_Prize_Room_Trophy(Final_Image_Capture, 0.5, Root.Gaba_Core_Data.Current_Building_Item);
					}else{
						Root.Gaba_Core_Data.Set_Prize_Room_Trophy(Creation_Item_Dragon, 0.5, Root.Gaba_Core_Data.Current_Building_Item);
					}
									
					//Set Item final State.
					Swap_Bones();
					Creation_Item_Dragon.Play_This(Build_States[0][1]);
					
					//Set Character final State.
					Character_Dragon.Play_This(Root.Gaba_Core_Data.Get_Character_Selection_Animation("Final"));
					
					//Final Blast.
					sceneRoot.addChild(Final_Blast);
					Final_Blast.Activate_Particles_Blast();
					//Stop music.
					switch(Root.Gaba_Core_Data.Current_Character){				
						case "Muno":{
							soundmanager.stopSound("Music_Muno");
						}break;
						case "Brobee":{
							soundmanager.stopSound("Music_Brobee");
						}break;
						case "Plex":{
							soundmanager.stopSound("Music_Plex");
						}break;					
						case "Toodee":{
							soundmanager.stopSound("Music_Toodee");
						}break;
						case "Foofa":{
							soundmanager.stopSound("Music_Foofa");
						}break;						
					}
					//Play final music.
					switch(Root.Gaba_Core_Data.Current_Character){
						
						case "Muno":
						case "Brobee":
						case "Plex":{
							soundmanager.playSound("Muno_Plex_Explo");
						}break;						
						
						case "Toodee":
						case "Foofa":{
							soundmanager.playSound("Foo_Too_Explo");
						}break;						
					}					
					
					//Exit wait.
					TweenMax.delayedCall(8.1, Exit_Game);	
					Game_Mode = "Pergatory";
					
					//Shake it.
					if(Snow_Globe_Particles != null){
						Snow_Globe_Particles.Finalize_Walls(physics);
					}
					TweenMax.delayedCall(0.2, Delay_Calls, ["Shake"]);
					
				}break;
				
				case "Shake":{	
					if(Snow_Globe_Particles != null){
						Snow_Globe_Particles.Force_Addition(physics);
						Snow_Globe_Particles.Force_Side(physics);
						TweenMax.delayedCall(4.2, Delay_Calls, ["Done"]);
					}
				}break;
				
				case "Done":{
					if(Snow_Globe_Particles != null){
						Snow_Globe_Particles.Force_Addition(physics);
						Snow_Globe_Particles.Force_Side(physics);
					}
				}break;
			}
		}

		private function ActivitySelection_Mouse_Scene(e:starling.events.TouchEvent):void{
			if(!Mouse_Enabled){return;}
			var Current_Touch:Touch = e.getTouch(sceneRoot);
			if(Current_Touch 		== null){return;}
			var Location:Point 		= new Point(Current_Touch.globalX, Current_Touch.globalY);
			
			switch(Current_Touch.phase)
			{				
				case "began":
				{
					//Character animation tap.
					if(Tap_Timmer <= 0){
						if(!Brodys_Arm.hitTest(Brodys_Arm.globalToLocal(Location))){	
							if(Character_Dragon.hitTest(Character_Dragon.globalToLocal(Location))){	
								
								//Play tap sound.
								switch(Root.Gaba_Core_Data.Current_Character)
								{
									case "Muno":{	soundmanager.playSound(("Muno_" + tapSound.toString()), Costanza.soundVolume);}break;
									case "Brobee":{	soundmanager.playSound(("Brobee_" + tapSound.toString()), Costanza.soundVolume);}break;
									case "Plex":{	soundmanager.playSound(("Plex_" + tapSound.toString()), Costanza.soundVolume);}break;
									case "Toodee":{	soundmanager.playSound(("Toodee_" + tapSound.toString()), Costanza.soundVolume);}break;
									case "Foofa":{	soundmanager.playSound(("Foofa_" + tapSound.toString()), Costanza.soundVolume);}break;
								}
								tapSound+=1;
								if(tapSound > 3)
								{
									tapSound = 1;
								}
								Tap_Timmer = 3000;
								Character_Dragon.Play_This(Root.Gaba_Core_Data.Get_Character_Selection_Animation("Tap"));
							}
						}
					}
					
					switch(Game_Mode)
					{
						//Click to pop.
						case "Pop_Action_Click":{
							
							if(Idea_Bubble_Dragon.hitTest(Idea_Bubble_Dragon.globalToLocal(Location))){
								Physic_Update = false;
								//Play Start character sound.
								switch(Root.Gaba_Core_Data.Current_Character){
									case "Muno":{	soundmanager.playSound("Muno_4", Costanza.soundVolume);}break;
									case "Brobee":{	soundmanager.playSound("Brobee_4", Costanza.soundVolume);}break;
									case "Plex":{	soundmanager.playSound("Plex_4", Costanza.soundVolume);}break;
									case "Toodee":{	soundmanager.playSound("Toodee_4", Costanza.soundVolume);}break;
									case "Foofa":{	soundmanager.playSound("Foofa_4", Costanza.soundVolume);}break;
								}
								Game_Mode = "Pop_Action_Update";
							}
						}break;
						case "Move_Item":
						case "Pick_Up_Item":
						{
							if((Brodys_Arm.hitTest(Brodys_Arm.globalToLocal(Location))) || (Item_To_Place.hitTest(Item_To_Place.globalToLocal(Location))))
							{
								soundmanager.playSound("Pick_Up_Item");
								TweenMax.killTweensOf(Brodys_Arm);
								Game_Mode = "Moving_Item";
								TweenMax.to(Brodys_Arm, 1.0, {x:1366});
								Item_To_Place.x = Location.x - (Item_To_Place.width/2);
								Item_To_Place.y = Location.y - (Item_To_Place.height/2);
							}
						}break;
					}
				}break;

				case "moved":
				{
					switch(Game_Mode)
					{
						case "Moving_Item":
						{
							Item_To_Place.x = (Location.x - (Item_To_Place.width  / 2)) - Costanza.STAGE_OFFSET;
							Item_To_Place.y = (Location.y - (Item_To_Place.height / 2));
						}break;
					}
				}break;
				case "ended":{
					switch(Game_Mode)
					{
						case "Moving_Item":
						{
							if(sceneRoot.globalToLocal(Location).x < 680)
							{
								//Get item to go position.
								var Go_Loc:Point;
								
								//Move item to location.
								if(Build_States[0][0][1] == "")
								{
									Go_Loc 				= new Point((Creation_Item_Dragon.x + (Creation_Item_Dragon.width  / 2)  + Build_States[0][0][2].x),
																	(Creation_Item_Dragon.y + (Creation_Item_Dragon.height / 2)) + Build_States[0][0][2].y);
								}else
								{
									Go_Loc 				= Creation_Item_Dragon.Get_Bone_Location(Build_States[0][0][1], Build_States[0][0][2]);								
									Go_Loc.x 			+= Creation_Item_Dragon.x;
									Go_Loc.y 			+= Creation_Item_Dragon.y;
								}
								//Move to location.
								TweenMax.killTweensOf(Item_To_Place);
								TweenMax.to(Item_To_Place, 0.5, {x:Go_Loc.x, y:Go_Loc.y, onComplete:Delay_Calls, onCompleteParams:["Moving_Item_To_Drop_Spot"]});								
								
								Game_Mode 			= "Pergatory";								
								
							}else
							{								
								soundmanager.playSound("Drop_Item_Miss");
								
								//Reset.
								TweenMax.killTweensOf(Item_To_Place);
								TweenMax.to(Item_To_Place, 1.0, {y:3200, onComplete:Delay_Calls, onCompleteParams:["Falling_Item"]});
								Game_Mode = "Pergatory";
							}
						}break;
					}
				}break;
			}
		}			
		
		//The Burst.
		private function Setup_Burst():void{
			
			//Setup Burst.
			Item_Spray_Explosion.Prep_Burst(Root.Gaba_Core_Data.Current_Character);
			sceneRoot.addChildAt(Item_Spray_Explosion, sceneRoot.getChildIndex(Character_Dragon));
			
			//Next call.
			TweenMax.delayedCall(0.0, Delay_Calls, ["Burst"]);	
			
		}
		private function Halt_Burst():void{			
			
			//Next call.
			Item_Spray_Explosion.Activate_Burst("Finish", Burst_Completed);
						
		}
		private function Burst_Completed():void
		{	
			//Clean Burst.
			sceneRoot.removeChild(Item_Spray_Explosion);
			Item_Spray_Explosion.Dispose_Me();
			Item_Spray_Explosion.dispose();
			
			//Next Section.
			Game_Mode = "Pop_Action_Clear";	
			Physic_Update = true;
		}
		
		//Testing.
		private function Clock_State(State:String):void
		{
			switch(State){
				case "Activate":{
					Clock_Active = true;
				}break;
				case "Shutdown":{
					Clock_Active = false;
				}break;
			}
		}
	}
}