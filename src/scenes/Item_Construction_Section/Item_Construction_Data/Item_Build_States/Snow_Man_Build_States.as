package scenes.Item_Construction_Section.Item_Construction_Data.Item_Build_States{
	
	import scenes.Item_Construction_Section.Item_Construction_Data.Item_Build_States.Item_Build_States_Utils.Build_State_Section;
	
	public class Snow_Man_Build_States{		
		
		//States for the selection.
		private var Snow_Ball_2:Build_State_Section;
		private var Snow_Ball_3:Build_State_Section;	
		private var Hats:Build_State_Section;	
		private var Scarf:Build_State_Section;
		private var Face:Build_State_Section;
		private var Left_Arm:Build_State_Section;
		private var Right_Arm:Build_State_Section;
		
		public function Snow_Man_Build_States(){
			
			Snow_Ball_2 	= new Build_State_Section(	["SnowBall_Skin_2_Frame"],
														["SnowBall_Skin_2_Frame"],
														[[0, 0]],
														[[0, 0]]);
			
			Snow_Ball_3 	= new Build_State_Section(	["SnowBall_Skin_3_Frame"],
														["SnowBall_Skin_3_Frame"],
														[[0, 0]],
														[[0, 0]]);
						
			Hats 			= new Build_State_Section(	["Hat_Skin_1_Frame", "Hat_Skin_2_Frame", "Hat_Skin_3_Item"],
														["Hat_Skin_1_Frame", "Hat_Skin_2_Frame", "Hat_Skin_3_Frame"],
														[[0, 0], [30, -65], [30, -25]],
														[[0, 0], [30, -65], [30, -25]]);
			
			Scarf 			= new Build_State_Section(	["Scarf_Skin_1_Item",  "Scarf_Skin_2_Item",  "Scarf_Skin_3_Item"],
														["Scarf_Skin_1_Frame", "Scarf_Skin_2_Frame", "Scarf_Skin_3_Frame"],
														[[0, 0], [0, 0], [-25, 0]],
														[[0, 0], [0, 0], [-25, 0]]);
			
			Face 			= new Build_State_Section(	["Face_Skin_1_Item",  "Face_Skin_2_Item",  "Face_Skin_3_Item"],
														["Face_Skin_1_Frame", "Face_Skin_2_Frame", "Face_Skin_3_Frame"],
														[[0, 0], [0, 0], [0, 0]],
														[[0, 0], [0, 0], [0, 0]]);
			
			
			Left_Arm 		= new Build_State_Section(	["Arm_Left_Skin_1_Frame", "Arm_Left_Skin_2_Frame"],
														["Arm_Left_Skin_1_Frame", "Arm_Left_Skin_2_Frame"],
														[[0, 0], [60, -10]],
														[[0, 0], [60, -10]]);
			
			Right_Arm 		= new Build_State_Section(	["Arm_Right_Skin_1_Frame", "Arm_Right_Skin_2_Frame"],
														["Arm_Right_Skin_1_Frame", "Arm_Right_Skin_2_Frame"],
														[[0, 0], [10, 130]],
														[[0, 0], [10, 130]]);			
			
		}
		
		
		public function Build_State():Array{
			
			//Setup the next section.
			Snow_Ball_2.Set_Next_State_Section();
			Snow_Ball_3.Set_Next_State_Section();
			Hats.Set_Next_State_Section();
			Scarf.Set_Next_State_Section();
			Face.Set_Next_State_Section();
			Left_Arm.Set_Next_State_Section(Right_Arm.Set_Next_State_Section()); 
			
			
			return [
				
				//Load setup.
				[
					"",
					"snowballA",
					[],
					[]
				],	
				
				//After burst play animation.
				[					
					"",
					"snowballA",
					[],
					[]
				],	
				
				//Select Ball 2.
				[
					[Snow_Ball_2.Get_Show_Selected(), "snowmanSnowball02", Snow_Ball_2.Get_Show_Offset_Selected()],					
					"snowballB",
					[
						["snowmanSnowball02", Snow_Ball_2.Get_Skin_Selected()]
					],
					[
						Snow_Ball_2.Get_Skin_Offset_Selected()
					],
					[
						0.5, 474, 511, 300, 400	
					],
					[
						0.1, "Snow_Man_Ball_1"
					]
				],	
				
				//Select Ball 3.
				[
					[Snow_Ball_3.Get_Show_Selected(), "snowmanSnowbal03", Snow_Ball_3.Get_Show_Offset_Selected()],		
					"snowballC",
					[
						["snowmanSnowbal03", Snow_Ball_3.Get_Skin_Selected()]
					],
					[
						Snow_Ball_3.Get_Skin_Offset_Selected()
					],
					[
						0.5, 474, 438, 300, 600	
					],
					[
						0.1, "Snow_Man_Ball_2"
					]
				],	
				
				//Select Hats.
				[
					[Hats.Get_Show_Selected(), "hat", Hats.Get_Show_Offset_Selected()],	
					"hat",
					[
						["hat", Hats.Get_Skin_Selected()]
					],
					[
						Hats.Get_Skin_Offset_Selected()
					],
					[
						0.5, 474, 390, 300, 640	
					],
					[
						0.1, "Snow_Man_Hat"
					]
				],	
				
				//Select Scarf.
				[
					[Scarf.Get_Show_Selected(), "scarf", Scarf.Get_Show_Offset_Selected()],	
					"scarf",
					[						
						["scarf", Scarf.Get_Skin_Selected()]
					],
					[
						Scarf.Get_Skin_Offset_Selected()
					],
					[
						0.5, 474, 390, 300, 640	
					],
					[
						0.1, "Snow_Man_Hat"
					]
				],	
				
				//Select Face.
				[
					[Face.Get_Show_Selected(), "face", Face.Get_Show_Offset_Selected()],	
					"face",
					[						
						["face", Face.Get_Skin_Selected()]
					],
					[
						Face.Get_Skin_Offset_Selected()
					],
					[
						0.5, 474, 390, 300, 640	
					],
					[
						0.1, "Snow_Man_Face"
					]
				],	
				
				//Left Arm.
				[
					[Left_Arm.Get_Show_Selected(), "armL", Left_Arm.Get_Show_Offset_Selected()],	
					"armL",
					[						
						["armL", Left_Arm.Get_Skin_Selected()]
					],
					[
						Left_Arm.Get_Skin_Offset_Selected()
					],
					[
						0.5, 474, 390, 300, 640	
					],
					[
						0.1, "Snow_Man_Arm_1"
					]
				],	
				
				//Right Arm.
				[
					[Right_Arm.Get_Show_Selected(), "armR", Right_Arm.Get_Show_Offset_Selected()],	
					"armR",
					[						
						["armR", Right_Arm.Get_Skin_Selected()]
					],
					[
						Right_Arm.Get_Skin_Offset_Selected()
					],
					[
						0.5, 474, 390, 300, 640	
					],
					[
						0.1, "Snow_Man_Arm_2"
					]
				],	
				
				//Celebrate phase.
				[
					"",
					"dance",
					[],
					[],
					"sparkle"
				]
				
			];
			
		}	
		
		//Full reset of values.
		public function Reset_States():void{
			
			//Setup the next section.
			Snow_Ball_2.Reset_State_Section();
			Snow_Ball_3.Reset_State_Section();
			Hats.Reset_State_Section();
			Scarf.Reset_State_Section();
			Face.Reset_State_Section();
			Left_Arm.Reset_State_Section();
			Right_Arm.Reset_State_Section();
			
		}
		
	}
}