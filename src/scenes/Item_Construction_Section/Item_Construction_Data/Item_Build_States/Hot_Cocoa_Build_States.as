package scenes.Item_Construction_Section.Item_Construction_Data.Item_Build_States{
	
	import flash.geom.Point;
	
	import scenes.Item_Construction_Section.Item_Construction_Data.Item_Build_States.Item_Build_States_Utils.Build_State_Section;
	
	public class Hot_Cocoa_Build_States{		
		
		//States for the selection.		
		private var Marshmellow:Build_State_Section;	
		private var Candy:Build_State_Section;
		
		public function Hot_Cocoa_Build_States(){
			
			Marshmellow 	= new Build_State_Section(	["Marshmellow_Skin_1_Frame", "Marshmellow_Skin_2_Frame", "Marshmellow_Skin_3_Frame"],
														["Marshmellow_Skin_1_Frame", "Marshmellow_Skin_2_Frame", "Marshmellow_Skin_3_Frame"],
														[[0, 0], [0, 0], [0, 0]],
														[[0, 0], [0, 0], [0, 0]]);
			
			Candy 			= new Build_State_Section(	["Sprinkle_Skin_1_Frame", "Sprinkle_Skin_2_Frame", "Sprinkle_Skin_3_Frame"],
														["Sprinkle_Skin_1_Frame", "Sprinkle_Skin_2_Frame", "Sprinkle_Skin_3_Frame"],
														[[0, -70], [0, -70], [0, -70]],
														[[0, 0], [0, 0], [0, 0]]);
			
		}
		
		public function Build_State():Array{
			
			//Setup the Snow Man props.		
			Marshmellow.Set_Next_State_Section();
			Candy.Set_Next_State_Section();
			
			return [
				
				//Load setup.
				[
					"",
					"mugPlacement",
					[],
					[]
				],		
				
				//After burst play animation.
				[					
					"",
					"mugPlacement",
					[],
					[]
				],	
				
				//Select Milk.
				[
					
					["Milk_Bottle_Skin_1_Frame", "milkCarton", new Point(0, -130)],
					"milkPlacement",
					[],
					[],
					[
						1.0, 453, 503, 300, 400	
					],
					[
						0.3, "EGGNOG_Milk"
					]
				],
				
				//Select Chocolat.
				[
					["Chocolate_Bottle_Skin_1_Frame", "chocolateBottle", new Point(-20, -100)],
					"chocolateMilkPlacement",
					[],
					[],
					[
						0.8, 453, 503, 300, 400	
					],
					[
						0.3, "EGGNOG_Syrup"
					]
				],
				
				//Select Marshmellow.
				[
					[Marshmellow.Get_Show_Selected(), "bigMarshmallows03", Marshmellow.Get_Show_Offset_Selected()],
					"toppingsAPlacement",
					[
						["bigMarshmallows03", 		Marshmellow.Get_Skin_Selected()],
						["whippedCreamCanister", 	"Visible_Off"],
						["whippedCream", 			"Visible_Off"],
						["miniMarshmallows", 		"Visible_Off"]
					],
					[
						Marshmellow.Get_Skin_Offset_Selected()
					],
					[
						0.2, 453, 503, 300, 400	
					],
					[
						0.15, "EGGNOG_Marshmellow"
					]
				],
				
				//Select Cream.
				[
					["Cream_Can_1_Frame", "", new Point(-100, -200)],
					"toppingsAPlacement",
					[
						["whippedCreamCanister", 	"Visible_On"],
						["whippedCream", 			"Visible_On"]
					],
					[],
					[
						1.8, 453, 503, 300, 400	
					],
					[
						0.3, "EGGNOG_Cream"
					]
				],
				
				//Select Candy.
				[
					[Candy.Get_Show_Selected(), "candyCanes03", Candy.Get_Show_Offset_Selected()],
					"candyCanesPlacement",
					[
						["candyCanes03", Candy.Get_Skin_Selected()]
					],
					[
						Candy.Get_Skin_Offset_Selected()
					],
					[
						1.0, 453, 503, 300, 400	
					],
					[
						0.3, "EGGNOG_Straw"
					]
				],
				
				//Select Sprinkles.
				[
					["Sprinkle_Can_1", "sprinkleShaker", new Point(0, -80)],
					"toppingsBPlacement",
					[],
					[],
					[
						1.5, 453, 503, 300, 400	
					],
					[
						0.3, "EGGNOG_Shaker"
					]
				],
				
				//Celebrate phase.
				[
					"",
					"finished01",
					[],
					[],
					"sparkle"
				]
				
			];
			
		}	
		
		//Full reset of values.
		public function Reset_States():void{
			
			//Setup the next section.			
			Marshmellow.Reset_State_Section();
			Candy.Reset_State_Section();
			
		}
		
	}
}