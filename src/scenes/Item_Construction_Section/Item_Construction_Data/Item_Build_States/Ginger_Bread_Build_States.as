package scenes.Item_Construction_Section.Item_Construction_Data.Item_Build_States{
	import flash.geom.Point;
	
	import scenes.Item_Construction_Section.Item_Construction_Data.Item_Build_States.Item_Build_States_Utils.Build_State_Section;
	
	public class Ginger_Bread_Build_States{
		
		//States for the selection.				
		private var Boy_Candy:Build_State_Section;
		private var Boy_Icing:Build_State_Section;	
		private var Boy_Sprinkles:Build_State_Section;	
		
		private var Girl_Candy:Build_State_Section;
		private var Girl_Icing:Build_State_Section;	
		private var Girl_Sprinkles:Build_State_Section;	
		
		public function Ginger_Bread_Build_States(){
			
			Boy_Candy 		= new Build_State_Section(	["Candy_Skin_1_Item", "Candy_Skin_2_Item", "Candy_Skin_3_Item"],
														["Boy_Candy_Skin_1_Frame", "Boy_Candy_Skin_2_Frame", "Boy_Candy_Skin_3_Frame"],
														[[-40, -60], [-40, -60], [-10, -20]],
														[[0, 0], [-30, 10], [-4, 35]]);
			
			Boy_Icing 		= new Build_State_Section(	["Icing_1_Item", "Icing_2_Item", "Icing_3_Item", "Icing_4_Item"],
														["Boy_Icing_1_Frame", "Boy_Icing_2_Frame", "Boy_Icing_3_Frame", "Boy_Icing_4_Frame"],
														[[0, 0], [0, 0], [0, 0], [0, 0]],
														[[0, 0], [0, 0], [0, 0], [0, 0]]);
			
			Boy_Sprinkles 	= new Build_State_Section(	["shaker", "shaker", "shaker", "shaker"],
														["Boy_Sprinkle_Skin_1_Frame", "Boy_Sprinkle_Skin_2_Frame", "Boy_Sprinkle_Skin_3_Frame", "Boy_Sprinkle_Skin_4_Frame"],
														[[0, 0], [0, 0], [0, 0], [0, 0]],
														[[0, 0], [0, 0], [0, 0], [0, 0]]);
			
			Girl_Candy 		= new Build_State_Section(	["Candy_Skin_1_Item", "Candy_Skin_2_Item", "Candy_Skin_3_Item"],
														["Boy_Candy_Skin_1_Frame", "Boy_Candy_Skin_2_Frame", "Girl_Candy_Skin_3_Frame"],
														[[-40, -60], [-40, -60], [-10, -20]],
														[[0, 0], [-30, 10], [0, 35]]);
			
			Girl_Icing 		= new Build_State_Section(	["Icing_1_Item", "Icing_2_Item", "Icing_3_Item", "Icing_4_Item"],
														["Girl_Icing_1_Frame", "Girl_Icing_2_Frame", "Girl_Icing_3_Frame", "Girl_Icing_4_Frame"],
														[[0, 0], [0, 0], [0, 0], [0, 0]],
														[[0, 0], [0, 0], [0, 0], [0, 0]]);
			
			Girl_Sprinkles 	= new Build_State_Section(	["shaker", "shaker", "shaker", "shaker"],
														["Girl_Sprinkle_Skin_1_Frame", "Girl_Sprinkle_Skin_2_Frame", "Girl_Sprinkle_Skin_3_Frame", "Girl_Sprinkle_Skin_4_Frame"],
														[[0, 0], [0, 0], [0, 0], [0, 0]],
														[[-6, -2], [-6, -2], [-6, -2], [-6, -2]]);
			
		}
		
		public function Build_State(Selected_Ginger_Bread_Sex:String):Array{	
			
			//Setup the Snow Man props.		
			Boy_Candy.Set_Next_State_Section(0);
			Boy_Icing.Set_Next_State_Section(0);	
			Boy_Sprinkles.Set_Next_State_Section(0);	
			
			Girl_Candy.Set_Next_State_Section();
			Girl_Icing.Set_Next_State_Section();	
			Girl_Sprinkles.Set_Next_State_Section();	
			
			//States to hold selection.
			var Sex_Candy:Build_State_Section;
			var Sex_Icing:Build_State_Section;
			var Sex_Sprinkles:Build_State_Section;
			var Sex_Item_CookieCutter_Location:Point;
			
			switch(Selected_Ginger_Bread_Sex){
				
				case "Boy":{
					Sex_Candy 						= Boy_Candy;
					Sex_Icing 						= Boy_Icing;
					Sex_Sprinkles 					= Boy_Sprinkles;
					Sex_Item_CookieCutter_Location 	= new Point(30, -228);
				}break;
				
				case "Girl":{
					Sex_Candy 		= Girl_Candy;
					Sex_Icing 		= Girl_Icing;
					Sex_Sprinkles 	= Girl_Sprinkles;
					Sex_Item_CookieCutter_Location 	= new Point(36, -226);
				}break;
				
			}			
			
			return [
				
				//Load setup.
				[
					"",
					"rawDough",
					[],
					[]
				],	
				
				//After burst play animation.
				[
					"",
					"rawDough",
					[],
					[]
				],		
				
				//Cut cookie phase.
				[
					[(Selected_Ginger_Bread_Sex + "_Cookie_Cutter_Item"), "", Sex_Item_CookieCutter_Location],
					"cutCookie",
					[],
					[],
					[
						1.5, 434, 449, 260, 400	
					],
					[
						0.3, "Ginger_Bread_Cutout"
					]
				],				
				
				//Icing phase.
				[
					[Sex_Icing.Get_Show_Selected(), "", new Point(-195, -290)],
					"icing",
					[
						["icing", 		Sex_Icing.Get_Skin_Selected()], 
						["tube", 		Sex_Icing.Get_Show_Selected()],
						["drop", 		"Icing_Drop_" + Get_Selection_Number(Sex_Icing.Get_Skin_Selected()) + "_Frame"]
					],
					[
						Sex_Icing.Get_Skin_Offset_Selected(),
						new Point(0, 0),
						new Point(0, 0)
					],
					[
						1.8, 434, 449, 260, 400	
					],
					[
						0.3, "Ginger_Bread_Icing"
					]
				],
				
				//Candy phase.
				[
					[Sex_Candy.Get_Show_Selected(), "candy", Sex_Candy.Get_Show_Offset_Selected()],
					"candies",
					[
						["candy", 	Sex_Candy.Get_Skin_Selected()]
					],
					[
						Sex_Candy.Get_Skin_Offset_Selected()
					],
					[
						0.5, 434, 449, 260, 400	
					],
					[
						0.1, "Ginger_Bread_Candy"
					]
				],
				
				//Sprinkle phase.
				[
					[Sex_Sprinkles.Get_Show_Selected(), "sprinkle", new Point(180, 120)],
					"sprinkles",
					[
						["sprinkle", 		Sex_Sprinkles.Get_Skin_Selected()],
						["sprinkleFall", 	"Falling_Sprikles_" + Get_Selection_Number(Sex_Sprinkles.Get_Skin_Selected()) + "_Frame"]
					],
					[
						Sex_Sprinkles.Get_Skin_Offset_Selected(),
						new Point(0, 0)
					],
					[
						1.8, 434, 449, 260, 400	
					],
					[
						0.3, "Ginger_Bread_Shaker"
					]
				],
				
				//Celebrate phase.
				[
					"",
					"dance",
					[],
					[],
					"sparkle"
				]
				
			];
			
		}	
		
		//Full reset of values.
		public function Reset_States():void{
			
			//Setup the next section.	
			Boy_Candy.Reset_State_Section();
			Boy_Icing.Reset_State_Section();	
			Boy_Sprinkles.Reset_State_Section();	
			
			Girl_Candy.Reset_State_Section();
			Girl_Icing.Reset_State_Section();	
			Girl_Sprinkles.Reset_State_Section();	
			
		}
		
		//Get selected option.
		private function Get_Selection_Number(Word:String):String{	
			
			if(Word.indexOf("1") != -1){return "1";}
			if(Word.indexOf("2") != -1){return "2";}
			if(Word.indexOf("3") != -1){return "3";}
			if(Word.indexOf("4") != -1){return "4";}
			if(Word.indexOf("5") != -1){return "5";}
			if(Word.indexOf("6") != -1){return "6";}
			if(Word.indexOf("7") != -1){return "7";}
			if(Word.indexOf("8") != -1){return "8";}
			if(Word.indexOf("9") != -1){return "9";}	
			
			return "1";
			
		}
		
	}
}