package scenes.Item_Construction_Section.Item_Construction_Data.Item_Build_States.Item_Build_States_Utils{	
	
	import flash.geom.Point;
	
	public class Build_State_Section{
		
		//Saved Originals.
		private var Original_Build_State_Shows:Array;
		private var Original_Build_State_Skins:Array;
		private var Original_Build_State_Shows_Offsets:Array;
		private var Original_Build_State_Skins_Offsets:Array;		
		
		//Usable Options.
		private var Build_State_Shows:Array;	
		private var Build_State_Skins:Array;	
		private var Build_State_Shows_Offsets:Array;
		private var Build_State_Skins_Offsets:Array;		
		
		//Selected Options.	
		private var Build_State_Show_Selected:String;
		private var Build_State_Skin_Selected:String;	
		private var Build_State_Show_Offset_Selected:Point;
		private var Build_State_Skin_Offset_Selected:Point;
		
		public function Build_State_Section(Shows:Array, Skins:Array, Show_Offsets:Array, Skin_Offsets:Array){			
			
			Original_Build_State_Shows 					= Shows;
			Original_Build_State_Skins 					= Skins;
			Original_Build_State_Shows_Offsets 			= Show_Offsets;
			Original_Build_State_Skins_Offsets 			= Skin_Offsets;
			
			Reset_State_Section();
			
		}	
		
		public function Set_Next_State_Section(Test_Selection:int = -1):int{			
			
			//Check if we are out of options.
			if(Build_State_Shows.length == 0){Reset_State_Section();}
			
			//Value Selected.
			var Random_Selection:int;
			if(Test_Selection == -1){Random_Selection 	= Math.floor(Math.random() * Build_State_Shows.length);}
			else{Random_Selection 						= Test_Selection;}
			
			//Set the names we will use.
			Build_State_Show_Selected					= Build_State_Shows[Random_Selection];
			Build_State_Skin_Selected					= Build_State_Skins[Random_Selection];		
			Build_State_Show_Offset_Selected			= new Point(Build_State_Shows_Offsets[Random_Selection].x,
																	Build_State_Shows_Offsets[Random_Selection].y);
			Build_State_Skin_Offset_Selected			= new Point(Build_State_Skins_Offsets[Random_Selection].x,
																	Build_State_Skins_Offsets[Random_Selection].y);
			
			//Remove the options.
			Build_State_Shows.splice( 					Random_Selection, 1);
			Build_State_Skins.splice( 					Random_Selection, 1);
			Build_State_Shows_Offsets.splice( 			Random_Selection, 1);	
			Build_State_Skins_Offsets.splice( 			Random_Selection, 1);
			
			return Random_Selection;
		}	
		
		public function Reset_State_Section():void{
			
			if(Build_State_Shows 			!= null){Build_State_Shows.length 	= 0;}
			if(Build_State_Skins 			!= null){Build_State_Skins.length 	= 0;}
			if(Build_State_Shows_Offsets 	!= null){Build_State_Shows_Offsets.length = 0;}
			if(Build_State_Skins_Offsets 	!= null){Build_State_Skins_Offsets.length = 0;}
			
			//Reset choices.
			Build_State_Shows 				= Clone_Master(Original_Build_State_Shows);
			Build_State_Skins 				= Clone_Master(Original_Build_State_Skins);
			Build_State_Shows_Offsets 		= Clone_Master(Original_Build_State_Shows_Offsets);
			Build_State_Skins_Offsets 		= Clone_Master(Original_Build_State_Skins_Offsets);
			
		}
		
		//Clone a master.
		private function Clone_Master(Master:Array):Array
		{
			//New array of props.
			var Wanabe:Array = new Array();		
			
			//Clone.
			for(var X:int = 0; X < Master.length; X++){

				if(Master[X] is String){
					
					var New_String_Value:String = Master[X];
					Wanabe.push(New_String_Value);
				}
				
				if(Master[X] is Array){

					var New_Point_Value:Point = new Point(Master[X][0], Master[X][1]);
					Wanabe.push(New_Point_Value);
				}
			}
			//Return new selections.
			return Wanabe;
		}
		
		//Return values.
		public function Get_Show_Selected():String{	return Build_State_Show_Selected;}
		public function Get_Skin_Selected():String{	return Build_State_Skin_Selected;}
		public function Get_Show_Offset_Selected():Point{return Build_State_Show_Offset_Selected;}	
		public function Get_Skin_Offset_Selected():Point{return Build_State_Skin_Offset_Selected;}
		
	}
}