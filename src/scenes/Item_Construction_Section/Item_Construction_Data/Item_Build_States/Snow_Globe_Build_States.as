package scenes.Item_Construction_Section.Item_Construction_Data.Item_Build_States{
	
	import flash.geom.Point;
	
	import scenes.Item_Construction_Section.Item_Construction_Data.Item_Build_States.Item_Build_States_Utils.Build_State_Section;
	
	public class Snow_Globe_Build_States{		
		
		//States for the selection.
		private var Ornaments:Build_State_Section;
		private var Sparkles:Build_State_Section;		
		
		//Construct.
		public function Snow_Globe_Build_States(){
			
	//		Ornaments 	= new Build_State_Section(	["Reindeer_Item", "Snowman_Item", "Sleigth_Item", "Honika_Item", "House_Item"],
	//											  	["Reindeer_Frame", "Snowman_Frame", "Sleigth_Frame", "Honika_Frame", "House_Frame"],
	//												[[0, 0], [-25, 20], [20, -20], [12, 0], [50, 0]],
	//											  	[[0, 0], [-30, 25], [20, -40], [12, 0], [50, 0]]);
			
			Ornaments 	= new Build_State_Section(	["Reindeer_Item", "Snowman_Item", "Sleigth_Item", "Honika_Item", "House_Item"],
				["Reindeer_Frame", "Snowman_Frame", "Sleigth_Frame", "Honika_Frame", "House_Frame"],
				[[0, 0], [-25, 20], [20, -20], [12, 0], [50, 0]],
				[[0, 0], [-30, 25], [20, -40], [12, 0], [50, 0]]);
			
			Sparkles 	= new Build_State_Section(	["Globe_Glitter_Item", "Globe_Glitter_Item", "Globe_Glitter_Item", "Globe_Glitter_Item"],
												  	["Globe_Glitter_1_Frame", "Globe_Glitter_2_Frame", "Globe_Glitter_3_Frame", "Globe_Glitter_4_Frame"],
													[[0, 0], [0, 0], [0, 0], [0, 0], [0, 0]],
												 	[[0, 0], [0, 0], [0, 0], [0, 0]]);				
			
		}		
		
		//Setup.
		public function Build_State():Array
		{
			//Setup the next section.
			Ornaments.Set_Next_State_Section();
			Sparkles.Set_Next_State_Section();
			
			return [

				//Example Section.
				//[
				//["Example_Drag_Image_Name", "Leg", new Point(0, 0)]	// [(image to show), (bone to go to), (offest point)].
				//"globe",												// animation for Object to play once complete drop.
				//[
				//["items", Snow_Globe_Ornaments_Skin_Selected] 	// All the [bones, images] names to edit.
				//["items", Snow_Globe_Ornaments_Skin_Selected]
				//["items", Snow_Globe_Ornaments_Skin_Selected]
				//],
				//[
				//offset_1,											// All the bones images offset position.
				//offset_2,
				//offset_3
				//]
				//],	
				
				//Load setup.
				[
					[],
					"globe",
					[],
					[]
				],	
				
				//After burst play animation.
				[					
					[],
					"globe",
					[],
					[]
				],	
				
				//Fill water.
				[
					["Water_Item", "", new Point(-80, 0)],
					"water",
					[],
					[],
					[
						2.0, 427, 394, 300, 400	
					],
					[
						0.1, "Snow_Globe_Fill"
					]
				],
				
				//Select Ornament.
				[
					[Ornaments.Get_Show_Selected(), "items", Ornaments.Get_Show_Offset_Selected()],
					"object",
					[
						["items", Ornaments.Get_Skin_Selected()]
					],
					[
						Ornaments.Get_Skin_Offset_Selected()
					],
					[
						0.5, 427, 394, 300, 400	
					],
					[
						0.1, "Snow_Globe_Place_Ornament"
					]
				],				
				
				//Add Sparkles.
				[
					[Sparkles.Get_Show_Selected(), "sparkleShaker", new Point(0, -100)],
					"sparkles",
					[
						["sparkles0", 	Sparkles.Get_Skin_Selected()],
						["sparkles", 	Sparkles.Get_Skin_Selected()]
					],
					[
						Sparkles.Get_Skin_Offset_Selected(),
						Sparkles.Get_Skin_Offset_Selected()
					],
					[
						1.8, 427, 394, 300, 400	
					],
					[
						0.55, "Snow_Globe_Shaker"
					]
				],
				
				//Celebrate phase.
				[
					"",
					"finishDance",
					[],
					[],
					"sparkle"
				]
				
			];
			
		}
		
		//Full reset of values.
		public function Reset_States():void{
			
			//Setup the next section.
			Ornaments.Reset_State_Section();
			Sparkles.Reset_State_Section();
			
		}
	}
}