package scenes.Item_Construction_Section.Item_Construction_Data.Item_Build_States{
	
	import scenes.Item_Construction_Section.Item_Construction_Data.Item_Build_States.Item_Build_States_Utils.Build_State_Section;
	
	public class Christmas_Tree_Build_States{
		
		//States for the selection.
		private var Lights:Build_State_Section;
		private var Garlands:Build_State_Section;
		private var Ornaments:Build_State_Section;
		private var Stars:Build_State_Section;
		
		//Construct.
		public function Christmas_Tree_Build_States(){
			
			Lights 		= new Build_State_Section(	["Lights_Skin_1_Item", "Lights_Skin_2_Item", "Lights_Skin_3_Item", "Lights_Skin_4_Item", "Lights_Skin_5_Item"],
											 		["Lights_Skin_1_Frame", "Lights_Skin_2_Frame", "Lights_Skin_3_Frame", "Lights_Skin_4_Frame", "Lights_Skin_5_Frame"],
													[[64, -25], [35, -30], [-15, -15], [35, -45], [35, -40]],
											  		[[10, -10], [10, -10], [10, -10], [10, -10], [10, -10]]);
			
			Garlands 	= new Build_State_Section(	["Garland_Skin_1_Item", "Garland_Skin_2_Item"],
													["Garland_Skin_1_Frame", "Garland_Skin_2_Frame"],
													[[-50, -42], [-10, -32]],
													[[-50, -10], [0, 0]]);
			
			Ornaments 	= new Build_State_Section(	["Ornaments_Skin_1_Item", "Ornaments_Skin_2_Item", "Ornaments_Skin_3_Item", "Ornaments_Skin_4_Item", "Ornaments_Skin_5_Item"],
													["Ornaments_Skin_1_Frame", "Ornaments_Skin_2_Frame", "Ornaments_Skin_3_Frame", "Ornaments_Skin_4_Frame", "Ornaments_Skin_5_Frame"],
													[[-100, -74], [-80, -55], [-100, -75], [-100, -75], [-70, -30]],
													[[-80, 0], [-60, 0], [-80, 0], [-120, 0], [10, 30]]);
			
			Stars 		= new Build_State_Section(	["Start_Skin_1_Item", "Start_Skin_2_Item"],
													["Start_Skin_1_Frame", "Start_Skin_2_Frame"],
													[[0, 20], [0, 0]],
													[[-10, 10], [0, 0]]);
			
		}
		
		//Setup.
		public function Build_State():Array{
			
			//Setup the next section.
			Lights.Set_Next_State_Section();
			Garlands.Set_Next_State_Section();
			Ornaments.Set_Next_State_Section();
			Stars.Set_Next_State_Section(0);
			
			return [
				
				//Load setup.
				[
					[],
					"tree",
					[],
					[]
				],	
				
				//After burst play animation.
				[					
					[],
					"tree",
					[],
					[]
				],	
				
				//Select Lights.
				[
					[Lights.Get_Show_Selected(), "lights", Lights.Get_Show_Offset_Selected()],
					"mixedlightsPlacement",
					[
						["lights", 	Lights.Get_Skin_Selected()]
					],
					[
						Lights.Get_Skin_Offset_Selected()
					],
					[
						0.2, 480, 413, 400, 660	
					],
					[
						0.1, "Christmas_Tree_Lights"
					]
				],
				
				//Select Garland.
				[
					[Garlands.Get_Show_Selected(), "Garland", Garlands.Get_Show_Offset_Selected()],
					"garlandPlacement",
					[
						["Garland", Garlands.Get_Skin_Selected()]
					],
					[
						Garlands.Get_Skin_Offset_Selected()
					],
					[
						0.2, 480, 413, 400, 660	
					],
					[
						0.1, "Christmas_Garland"
					]
				],
				
				//Select Ornaments.
				[
					[Ornaments.Get_Show_Selected(), "ornaments", Ornaments.Get_Show_Offset_Selected()],
					"ormanentsPlacement",
					[
						["ornaments", Ornaments.Get_Skin_Selected()]
					],
					[
						Ornaments.Get_Skin_Offset_Selected()
					],
					[
						0.2, 480, 413, 400, 660	
					],
					[
						0.1, "Christmas_Tree_Cady_Cade"
					]
				],
				
				//Select Star.
				[
					[Stars.Get_Show_Selected(), "starangel", Stars.Get_Show_Offset_Selected()],
					"starangelPlacement",
					[
						["starangel", Stars.Get_Skin_Selected()]
					],
					[
						Stars.Get_Skin_Offset_Selected()
					],
					[
						0.2, 480, 413, 400, 660	
					],
					[
						0.1, "Christmas_Star"
					]
				],
				
				//Celebrate phase.
				[
					"",
					"sway",
					[],
					[],
					"sparkle"
				]
				
			];
			
		}	
		
		//Full reset of values.
		public function Reset_States():void{
			
			//Setup the next section.
			Lights.Reset_State_Section();
			Garlands.Reset_State_Section();
			Ornaments.Reset_State_Section();
			Stars.Reset_State_Section();
			
		}
		
	}
}