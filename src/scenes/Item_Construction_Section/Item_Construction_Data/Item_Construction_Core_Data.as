package scenes.Item_Construction_Section.Item_Construction_Data{	
	
	import com.greensock.TweenMax;
	
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import scenes.Item_Construction_Section.Item_Construction_Data.Item_Build_States.Christmas_Tree_Build_States;
	import scenes.Item_Construction_Section.Item_Construction_Data.Item_Build_States.Ginger_Bread_Build_States;
	import scenes.Item_Construction_Section.Item_Construction_Data.Item_Build_States.Hot_Cocoa_Build_States;
	import scenes.Item_Construction_Section.Item_Construction_Data.Item_Build_States.Snow_Globe_Build_States;
	import scenes.Item_Construction_Section.Item_Construction_Data.Item_Build_States.Snow_Man_Build_States;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.textures.RenderTexture;
	
	public class Item_Construction_Core_Data{			
		
		//Build States.
		private var Snow_Globe_State:Snow_Globe_Build_States;
		private var Christmas_Tree_State:Christmas_Tree_Build_States;
		private var Snow_Man_State:Snow_Man_Build_States;	
		private var Hot_Cocoa_State:Hot_Cocoa_Build_States;
		private var Ginger_Bread_State:Ginger_Bread_Build_States;
		
		//For if we have no offset for the items.
		private var Static_Location:Point;
		
		public var presentInSleigh:Array = [false,false,false,false,false,false,false];
		
		public function Item_Construction_Core_Data(){	
			
			//Build States.
			Snow_Globe_State 		= new Snow_Globe_Build_States();
			Christmas_Tree_State 	= new Christmas_Tree_Build_States();
			Snow_Man_State			= new Snow_Man_Build_States();
			Hot_Cocoa_State			= new Hot_Cocoa_Build_States();
			Ginger_Bread_State		= new Ginger_Bread_Build_States();
			
			A_Call_To_Reset 		= false;
			Prize_1_Location		= new Point(0, 0);
			Prize_2_Location		= new Point(0, 0);
			Prize_3_Location		= new Point(0, 0);
			Prize_4_Location		= new Point(0, 0);
			Prize_5_Location		= new Point(0, 0);
			Move_Start_Location		= new Point(0, 0);
			Prizes_Holder			= new Sprite();
			Static_Location 		= new Point(0, 0);
			
			Reset_Game();		
		}

		public function Reset_Game():void{
			Reset_Building_Items();
			Reset_All_Building_Items_Flow();
		}

		//The images of prizes.
		private var Prize_1_RT:RenderTexture;
		private var Prize_1_Image:Image;
		private var Prize_1_Location:Point;
		private var Prize_1_Index:int;
		private var Prize_1_Rotation:Number;		
		private var Prize_1_Update_Mode:Array;
		
		private var Prize_2_RT:RenderTexture;
		private var Prize_2_Image:Image;
		private var Prize_2_Location:Point;
		private var Prize_2_Index:int;
		private var Prize_2_Rotation:Number;
		private var Prize_2_Update_Mode:Array;
		
		private var Prize_3_RT:RenderTexture;
		private var Prize_3_Image:Image;
		private var Prize_3_Location:Point;
		private var Prize_3_Index:int;
		private var Prize_3_Rotation:Number;
		private var Prize_3_Update_Mode:Array;
		
		private var Prize_4_RT:RenderTexture;
		private var Prize_4_Image:Image;
		private var Prize_4_Location:Point;
		private var Prize_4_Index:int;
		private var Prize_4_Rotation:Number;
		private var Prize_4_Update_Mode:Array;
	
		private var Prize_5_RT:RenderTexture;
		private var Prize_5_Image:Image;
		private var Prize_5_Location:Point;
		private var Prize_5_Index:int;
		private var Prize_5_Rotation:Number;
		private var Prize_5_Update_Mode:Array;
		
		private var Prizes_Holder:Sprite;
		private var New_Prize:int;
		private var Move_Selected_Prize:String;
		private var Move_Start_Location:Point;
		
		//Prize Room.
		private function Arrange_Size(The_Image:Image):void{
			var Scale_Size_Max:int = 200;
			if(The_Image.width > Scale_Size_Max){				
				The_Image.scaleX = The_Image.scaleY = (Scale_Size_Max / The_Image.width);			
			}else if(The_Image.height > Scale_Size_Max){
				The_Image.scaleX = The_Image.scaleY = (Scale_Size_Max / The_Image.height);		
			}
		}
		//Prize room test.
		public function Test_Prize_Room():void{
			
			//222 310
			//127 187
			//164 206
			//226 375
			//166 208
			
			var Quad_Image:Quad;
			
			if(Prize_1_RT == null){
				Quad_Image = new Quad(222, 310, 0x000000);
			}else if(Prize_2_RT == null){
				Quad_Image = new Quad(127, 187, 0x000000);
			}else if(Prize_3_RT == null){
				Quad_Image = new Quad(164, 206, 0x000000);
			}else if(Prize_4_RT == null){
				Quad_Image = new Quad(226, 375, 0x000000);
			}else if(Prize_5_RT == null){
				Quad_Image = new Quad(166, 208, 0x000000);
			}else{
				Quad_Image = new Quad(32, 32, 0x000000);
			}
			
			var NS_Holder:Sprite 	= new Sprite();
			NS_Holder.addChild(Quad_Image);
			
			Set_Prize_Room_Trophy(NS_Holder, 1.0);
			
			NS_Holder.removeChild(Quad_Image);
			NS_Holder.dispose();
			Quad_Image.dispose();
			
		}
		public function Set_Prize_Room_Trophy(Draw_Sprite:Sprite, Scale:Number = 1.0, Name:String = "Bob"):void{			
			
			var Size:Rectangle 			= Draw_Sprite.getChildAt(0).bounds;
			if(Selected_Building_Item == "Snow_Globe"){
				Size.y 		-= 5;
				Size.height += 85;
			}
			var Draw_Matrix:Matrix 		= new Matrix(1,0,0,1,-Size.x,-Size.y);
			//Draw_Matrix.scale(Scale, Scale);
			//trace(Draw_Sprite.width + " " + Draw_Sprite.height);
			if(Prize_1_RT == null){				
				Prize_1_RT 				= new RenderTexture((Size.width * 1), (Size.height * 1), true, -1);
				Prize_1_RT.draw(Draw_Sprite, Draw_Matrix);
				Prize_1_Image 			= new Image(Prize_1_RT);
				Prize_1_Image.name      = Name;
				Prize_1_Image.scaleX 	= Scale;
				Prize_1_Image.scaleY 	= Scale;
				//Arrange_Size(Prize_1_Image);	
				Prize_1_Image.visible 	= false;
				New_Prize 				= 1;
				return;
			}
			
			if(Prize_2_RT == null){				
				Prize_2_RT 				= new RenderTexture((Size.width * 1), (Size.height * 1), true, -1);
				Prize_2_RT.draw(Draw_Sprite, Draw_Matrix);
				Prize_2_Image 			= new Image(Prize_2_RT);
				Prize_2_Image.name      = Name;
				Prize_2_Image.scaleX 	= Scale;
				Prize_2_Image.scaleY 	= Scale;
				Prize_2_Image.visible 	= false;
				New_Prize 				= 2;
				return;
			}
			
			if(Prize_3_RT == null){				
				Prize_3_RT 				= new RenderTexture((Size.width * 1), (Size.height * 1), true, -1);
				Prize_3_RT.draw(Draw_Sprite, Draw_Matrix);
				Prize_3_Image 			= new Image(Prize_3_RT);
				Prize_3_Image.name      = Name;
				Prize_3_Image.scaleX 	= Scale;
				Prize_3_Image.scaleY 	= Scale;
				Prize_3_Image.visible 	= false;
				New_Prize 				= 3;
				return;
			}
			
			if(Prize_4_RT == null){				
				Prize_4_RT 				= new RenderTexture((Size.width * 1), (Size.height * 1), true, -1);
				Prize_4_RT.draw(Draw_Sprite, Draw_Matrix);
				Prize_4_Image 			= new Image(Prize_4_RT);
				Prize_4_Image.name      = Name;
				Prize_4_Image.scaleX 	= Scale;
				Prize_4_Image.scaleY 	= Scale;
				Prize_4_Image.visible 	= false;
				New_Prize 				= 4;
				return;
			}
			
			if(Prize_5_RT == null){				
				Prize_5_RT 				= new RenderTexture((Size.width * 1), (Size.height * 1), true, -1);
				Prize_5_RT.draw(Draw_Sprite, Draw_Matrix);
				Prize_5_Image 			= new Image(Prize_5_RT);
				Prize_5_Image.name      = Name;
				Prize_5_Image.scaleX 	= Scale;
				Prize_5_Image.scaleY 	= Scale;
				Prize_5_Image.visible 	= false;
				New_Prize 				= 5;
				return;
			}
			
		}
		public function Setup_Prize_Room():void{
			
			//Remove childs.
			if(Prizes_Holder.contains(Prize_1_Image)){Prizes_Holder.removeChild(Prize_1_Image);}
			if(Prizes_Holder.contains(Prize_2_Image)){Prizes_Holder.removeChild(Prize_2_Image);}
			if(Prizes_Holder.contains(Prize_3_Image)){Prizes_Holder.removeChild(Prize_3_Image);}
			if(Prizes_Holder.contains(Prize_4_Image)){Prizes_Holder.removeChild(Prize_4_Image);}
			if(Prizes_Holder.contains(Prize_5_Image)){Prizes_Holder.removeChild(Prize_5_Image);}
			
			//Position.
			if(Prize_1_Image != null){
			
				Prize_1_Image.x = Prize_1_Location.x;
				Prize_1_Image.y = Prize_1_Location.y;
				Prizes_Holder.addChild(Prize_1_Image);
				
			}
			
			if(Prize_2_Image != null){
				
				Prize_2_Image.x = Prize_2_Location.x;
				Prize_2_Image.y = Prize_2_Location.y;
				Prizes_Holder.addChild(Prize_2_Image);
				
			}
			
			if(Prize_3_Image != null){
				
				Prize_3_Image.x = Prize_3_Location.x;
				Prize_3_Image.y = Prize_3_Location.y;
				Prizes_Holder.addChild(Prize_3_Image);
				
			}
			
			if(Prize_4_Image != null){
				
				Prize_4_Image.x = Prize_4_Location.x;
				Prize_4_Image.y = Prize_4_Location.y;
				Prizes_Holder.addChild(Prize_4_Image);
				
			}
			
			if(Prize_5_Image != null){
				
				Prize_5_Image.x = Prize_5_Location.x;
				Prize_5_Image.y = Prize_5_Location.y;
				Prizes_Holder.addChild(Prize_5_Image);
				
			}	
			
			Place_Trophy_Indexes();
			
		}
		public function Reset_Prize_Room():void{	
			
			//Remove childs.
			if(Prizes_Holder.contains(Prize_1_Image)){Prizes_Holder.removeChild(Prize_1_Image);}
			if(Prizes_Holder.contains(Prize_2_Image)){Prizes_Holder.removeChild(Prize_2_Image);}
			if(Prizes_Holder.contains(Prize_3_Image)){Prizes_Holder.removeChild(Prize_3_Image);}
			if(Prizes_Holder.contains(Prize_4_Image)){Prizes_Holder.removeChild(Prize_4_Image);}
			if(Prizes_Holder.contains(Prize_5_Image)){Prizes_Holder.removeChild(Prize_5_Image);}
			
			//Clean.
			if(Prize_1_Image != null){
				Prize_1_Image.base.dispose();
				Prize_1_Image.dispose();
				Prize_1_RT.base.dispose();
				Prize_1_RT.dispose();
				Prize_1_Image 	= null;
				Prize_1_RT 		= null;
			}
			
			if(Prize_2_Image != null){
				Prize_2_Image.base.dispose();
				Prize_2_Image.dispose();
				Prize_2_RT.base.dispose();
				Prize_2_RT.dispose();
				Prize_2_Image 	= null;
				Prize_2_RT 		= null;
			}
			
			if(Prize_3_Image != null){
				Prize_3_Image.base.dispose();
				Prize_3_Image.dispose();
				Prize_3_RT.base.dispose();
				Prize_3_RT.dispose();
				Prize_3_Image 	= null;
				Prize_3_RT 		= null;
			}
			
			if(Prize_4_Image != null){
				Prize_4_Image.base.dispose();
				Prize_4_Image.dispose();
				Prize_4_RT.base.dispose();
				Prize_4_RT.dispose();
				Prize_4_Image 	= null;
				Prize_4_RT 		= null;
			}
			
			if(Prize_5_Image != null){
				Prize_5_Image.base.dispose();
				Prize_5_Image.dispose();
				Prize_5_RT.base.dispose();
				Prize_5_RT.dispose();
				Prize_5_Image 	= null;
				Prize_5_RT 		= null;
			}
			
		}	
		public function Set_Current_Prize():Image{
			
			switch(New_Prize){			
				case 1:{
					if(Prizes_Holder.contains(Prize_1_Image)){Prizes_Holder.removeChild(Prize_1_Image);}
					Prize_1_Image.visible = true;
					return Prize_1_Image;
				}break;	
				case 2:{
					if(Prizes_Holder.contains(Prize_2_Image)){Prizes_Holder.removeChild(Prize_2_Image);}
					Prize_2_Image.visible = true;
					return Prize_2_Image;
				}break;	
				case 3:{
					if(Prizes_Holder.contains(Prize_3_Image)){Prizes_Holder.removeChild(Prize_3_Image);}
					Prize_3_Image.visible = true;
					return Prize_3_Image;
				}break;	
				case 4:{
					if(Prizes_Holder.contains(Prize_4_Image)){Prizes_Holder.removeChild(Prize_4_Image);}
					Prize_4_Image.visible = true;
					return Prize_4_Image;
				}break;	
				case 5:{
					if(Prizes_Holder.contains(Prize_5_Image)){Prizes_Holder.removeChild(Prize_5_Image);}
					Prize_5_Image.visible = true;
					return Prize_5_Image;
				}break;	
			}			
			
			return Prize_1_Image;
			
		}
		public function Get_Prizes():Sprite{
			return Prizes_Holder;
		}
		public function Get_Current_Prize():Image{
			switch(New_Prize){			
				case 1:{
					return Prize_1_Image;
				}break;	
				case 2:{
					return Prize_2_Image;
				}break;	
				case 3:{
					return Prize_3_Image;
				}break;	
				case 4:{
					return Prize_4_Image;
				}break;	
				case 5:{
					return Prize_5_Image;
				}break;	
			}			
			
			return Prize_1_Image;
		}
		public function Get_Current_Prize_Name():String{
			switch(New_Prize){			
				case 1:{
					return Prize_1_Image.name;
				}break;	
				case 2:{
					return Prize_2_Image.name;
				}break;	
				case 3:{
					return Prize_3_Image.name;
				}break;	
				case 4:{
					return Prize_4_Image.name;
				}break;	
				case 5:{
					return Prize_5_Image.name;
				}break;	
			}			
			
			return "Bob";
		}	
		public function Get_Specific_Prize(Prize:int):Image{
			
			switch(Prize){			
				case 0:{
					return Prize_1_Image;
				}break;	
				case 1:{
					return Prize_2_Image;
				}break;	
				case 2:{
					return Prize_3_Image;
				}break;	
				case 3:{
					return Prize_4_Image;
				}break;	
				case 4:{
					return Prize_5_Image;
				}break;	
			}	
			
			return Prize_1_Image;
		}
		public function Drop_Current_Prize():void{
			
			switch(New_Prize){				
				
				case 1:{
					if(!Prizes_Holder.contains(Prize_1_Image)){Prizes_Holder.addChild(Prize_1_Image);}
					Prize_1_Location.x = Prize_1_Image.x;
					Prize_1_Location.y = Prize_1_Image.y;
					Prizes_Holder.setChildIndex(Prize_1_Image, (Prizes_Holder.numChildren - 1));
				}break;
				case 2:{
					if(!Prizes_Holder.contains(Prize_2_Image)){Prizes_Holder.addChild(Prize_2_Image);}
					Prize_2_Location.x = Prize_2_Image.x;
					Prize_2_Location.y = Prize_2_Image.y;
					Prizes_Holder.setChildIndex(Prize_2_Image, (Prizes_Holder.numChildren - 1));
				}break;
				case 3:{
					if(!Prizes_Holder.contains(Prize_3_Image)){Prizes_Holder.addChild(Prize_3_Image);}
					Prize_3_Location.x = Prize_3_Image.x;
					Prize_3_Location.y = Prize_3_Image.y;
					Prizes_Holder.setChildIndex(Prize_3_Image, (Prizes_Holder.numChildren - 1));
				}break;
				case 4:{
					if(!Prizes_Holder.contains(Prize_4_Image)){Prizes_Holder.addChild(Prize_4_Image);}
					Prize_4_Location.x = Prize_4_Image.x;
					Prize_4_Location.y = Prize_4_Image.y;
					Prizes_Holder.setChildIndex(Prize_4_Image, (Prizes_Holder.numChildren - 1));
				}break;
				case 5:{
					if(!Prizes_Holder.contains(Prize_5_Image)){Prizes_Holder.addChild(Prize_5_Image);}
					Prize_5_Location.x = Prize_5_Image.x;
					Prize_5_Location.y = Prize_5_Image.y;
					Prizes_Holder.setChildIndex(Prize_5_Image, (Prizes_Holder.numChildren - 1));
				}break;
				
			}
			
			Set_Trophy_Indexes();
			
		}
		private function Set_Trophy_Indexes():void{		
			
			for(var X:int = 0; X < Prizes_Holder.numChildren; X++){
				
				
				if(Prizes_Holder.getChildAt(X).name == Prize_1_Image.name){
					Prize_1_Index = Prizes_Holder.getChildIndex(Prizes_Holder.getChildAt(X));
				}
				else if(Prizes_Holder.getChildAt(X).name == Prize_2_Image.name){
					Prize_2_Index = Prizes_Holder.getChildIndex(Prizes_Holder.getChildAt(X));
				}
				else if(Prizes_Holder.getChildAt(X).name == Prize_3_Image.name){
					Prize_3_Index = Prizes_Holder.getChildIndex(Prizes_Holder.getChildAt(X));
				}
				else if(Prizes_Holder.getChildAt(X).name == Prize_4_Image.name){
					Prize_4_Index = Prizes_Holder.getChildIndex(Prizes_Holder.getChildAt(X));
				}
				else if(Prizes_Holder.getChildAt(X).name == Prize_5_Image.name){
					Prize_5_Index = Prizes_Holder.getChildIndex(Prizes_Holder.getChildAt(X));
				}
				
				//switch(Prizes_Holder.getChildAt(X).name){
					
					//case "Im_1":{
						//Prize_1_Index = Prizes_Holder.getChildIndex(Prizes_Holder.getChildAt(X));
					//}break;
					//case "Im_2":{
						//Prize_2_Index = Prizes_Holder.getChildIndex(Prizes_Holder.getChildAt(X));
					//}break;
					//case "Im_3":{
						//Prize_3_Index = Prizes_Holder.getChildIndex(Prizes_Holder.getChildAt(X));
					//}break;
					//case "Im_4":{
						//Prize_4_Index = Prizes_Holder.getChildIndex(Prizes_Holder.getChildAt(X));
					//}break;
					//case "Im_5":{
						//Prize_5_Index = Prizes_Holder.getChildIndex(Prizes_Holder.getChildAt(X));
					//}break;
					
				//}
				
			}	
			
			Place_Trophy_Indexes();
			
		}
		private function Place_Trophy_Indexes():void{	
			
		
			for(var X:int = 0; X < 5; X++){
				
				if(Prize_1_Image != null){
					if(Prize_1_Index == X){
						Prizes_Holder.setChildIndex(Prize_1_Image, X);
						continue;
					}
				}
				
				if(Prize_2_Image != null){
					if(Prize_2_Index == X){
						Prizes_Holder.setChildIndex(Prize_2_Image, X);
						continue;
					}
				}
				
				if(Prize_3_Image != null){
					if(Prize_3_Index == X){
						Prizes_Holder.setChildIndex(Prize_3_Image, X);
						continue;
					}
				}
				
				if(Prize_4_Image != null){
					if(Prize_4_Index == X){
						Prizes_Holder.setChildIndex(Prize_4_Image, X);
						continue;
					}
				}
				
				if(Prize_5_Image != null){
					if(Prize_5_Index == X){
						Prizes_Holder.setChildIndex(Prize_5_Image, X);
						continue;
					}
				}
				
			}
			
		}
		public function Select_Trophy(Location:Point):Boolean{			
			
			for(var X:int = (Prizes_Holder.numChildren - 1); X >= 0; X--){
				
				if(Image(Prizes_Holder.getChildAt(X)).hitTest(Image(Prizes_Holder.getChildAt(X)).globalToLocal(Location))){
					Move_Selected_Prize 	= Image(Prizes_Holder.getChildAt(X)).name;
					Move_Start_Location.x 	= (Location.x - Image(Prizes_Holder.getChildAt(X)).x);
					Move_Start_Location.y 	= (Location.y - Image(Prizes_Holder.getChildAt(X)).y);
					Prizes_Holder.setChildIndex(Image(Prizes_Holder.getChildAt(X)), (Prizes_Holder.numChildren - 1));					
					return true;
				}			
				
			}			
			
			return false;
			
		}
		public function Move_Selected_Trophy(Location:Point):void{			
			
			if(Prize_1_Image.name == Move_Selected_Prize){
				Prize_1_Image.x = Location.x - Move_Start_Location.x;
				Prize_1_Image.y = Location.y - Move_Start_Location.y;
			}
			else if(Prize_2_Image.name == Move_Selected_Prize){
				Prize_2_Image.x = Location.x - Move_Start_Location.x;
				Prize_2_Image.y = Location.y - Move_Start_Location.y;
			}
			else if(Prize_3_Image.name == Move_Selected_Prize){
				Prize_3_Image.x = Location.x - Move_Start_Location.x;
				Prize_3_Image.y = Location.y - Move_Start_Location.y;
			}
			else if(Prize_4_Image.name == Move_Selected_Prize){
				Prize_4_Image.x = Location.x - Move_Start_Location.x;
				Prize_4_Image.y = Location.y - Move_Start_Location.y;
			}
			else if(Prize_5_Image.name == Move_Selected_Prize){
				Prize_5_Image.x = Location.x - Move_Start_Location.x;
				Prize_5_Image.y = Location.y - Move_Start_Location.y;
			}
			
		}
		public function Drop_Selected_Trophy():void{			
			
			if(Prize_1_Image.name == Move_Selected_Prize){
				Prize_1_Location.x = Prize_1_Image.x;
				Prize_1_Location.y = Prize_1_Image.y;
			}
			else if(Prize_2_Image.name == Move_Selected_Prize){
				Prize_2_Location.x = Prize_2_Image.x;
				Prize_2_Location.y = Prize_2_Image.y;
			}
			else if(Prize_3_Image.name == Move_Selected_Prize){
				Prize_3_Location.x = Prize_3_Image.x;
				Prize_3_Location.y = Prize_3_Image.y;
			}
			else if(Prize_4_Image.name == Move_Selected_Prize){
				Prize_4_Location.x = Prize_4_Image.x;
				Prize_4_Location.y = Prize_4_Image.y;
			}
			else if(Prize_5_Image.name == Move_Selected_Prize){
				Prize_5_Location.x = Prize_5_Image.x;
				Prize_5_Location.y = Prize_5_Image.y;
			}
			
			Set_Trophy_Indexes();
			
		}
		public function All_Trophys():Boolean{
			
			//Conditions.
			if(!Prizes_Holder.contains(Prize_1_Image)){return false;}
			if(!Prizes_Holder.contains(Prize_2_Image)){return false;}
			if(!Prizes_Holder.contains(Prize_3_Image)){return false;}
			if(!Prizes_Holder.contains(Prize_4_Image)){return false;}
			if(!Prizes_Holder.contains(Prize_5_Image)){return false;}	
			
			//we Good.
			return true;
			
		}
		public function Align_Trophys(Play_Time:Number):void{
			
			//Full length of size.
			var Full_Length:int 	= Prize_1_Image.width + Prize_2_Image.width + Prize_3_Image.width + Prize_4_Image.width + Prize_5_Image.width;
			var Space:int 			= ((1024 - Full_Length) / 5);
			
			TweenMax.to(Prize_1_Image, Play_Time, {	x:(171 + (Space / 2)), 
												   	y:(600 - Prize_1_Image.height)});			
		
			TweenMax.to(Prize_2_Image, Play_Time, {	x:(171 + (Space * 2) + Prize_1_Image.width), 
													y:(600 - Prize_2_Image.height)});			
			
			TweenMax.to(Prize_3_Image, Play_Time, {	x:(171 + (Space * 3) + Prize_1_Image.width + Prize_2_Image.width),  
													y:(600 - Prize_3_Image.height)});			
			
			TweenMax.to(Prize_4_Image, Play_Time, {	x:(171 + (Space * 4) + Prize_1_Image.width + Prize_2_Image.width + Prize_3_Image.width),   
													y:(600 - Prize_4_Image.height)});			
			
			TweenMax.to(Prize_5_Image, Play_Time, {	x:(171 + ((Space * 5) + Prize_1_Image.width + Prize_2_Image.width + Prize_3_Image.width + Prize_4_Image.width) - (Space / 2)),   
													y:(600 - Prize_5_Image.height)});
			
			Prize_1_Update_Mode 	= ["Idle", 0.5, ((Math.random() * 4) + 4)];
			Prize_2_Update_Mode 	= ["Idle", 0.5, ((Math.random() * 4) + 4)];
			Prize_3_Update_Mode 	= ["Idle", 0.5, ((Math.random() * 4) + 4)];
			Prize_4_Update_Mode 	= ["Idle", 0.5, ((Math.random() * 4) + 4)];
			Prize_5_Update_Mode 	= ["Idle", 0.5, ((Math.random() * 4) + 4)];
			
		}
		public function Get_Trophys_Size():void{
			
			trace("-----------------------------------------------");
			trace(Prize_1_Image.width + " " + Prize_1_Image.height);
			trace(Prize_2_Image.width + " " + Prize_2_Image.height);
			trace(Prize_3_Image.width + " " + Prize_3_Image.height);
			trace(Prize_4_Image.width + " " + Prize_4_Image.height);
			trace(Prize_5_Image.width + " " + Prize_5_Image.height);
			trace("-----------------------------------------------");
			
		}
		public function Update_Dancing_Trophys():void{
			
			//dance.			
			Update_Single_Dancing_Trophy(Prize_1_Update_Mode, Prize_1_Image);
			Update_Single_Dancing_Trophy(Prize_2_Update_Mode, Prize_2_Image);
			Update_Single_Dancing_Trophy(Prize_3_Update_Mode, Prize_3_Image);
			Update_Single_Dancing_Trophy(Prize_4_Update_Mode, Prize_4_Image);
			Update_Single_Dancing_Trophy(Prize_5_Update_Mode, Prize_5_Image);			
			
		}
		public function Update_Single_Dancing_Trophy(Properties:Array, Disp:Image):void{
			
			switch(Properties[0]){
				
				case "Jump":{
					
					Disp.y 		-= Properties[2];
					Properties[2] 			-= Properties[1];
					
					if(Properties[2] <= 0.0){
						Properties[2] = 0.0;
						Properties[0] = "Falling";
					}
					
				}break;
				
				case "Falling":{
					
					Disp.y 		+= Properties[1];
					Properties[1] 	+= 0.35;
					
					if(Disp.y >= (600 - Disp.height)){
						Disp.y = (600 - Disp.height);
						Properties[0] = "Idle";
					}
					
				}break;
				
				case "Idle":{
					
					if((Math.random() * 100) > 90){
						if((Math.random() * 100) < 10){
							Properties[0] = "Jump";
							Properties[1] = ((Math.random() * 10) / 10);
							if(Properties[1] < 0.3){Properties[1] = 0.3;}
							Properties[2] = ((Math.random() * 4) + 4);
						}
					}
					
				}break;
				
			}
			
		}
		public function Stop_Dancing_Trophys():void{
			TweenMax.killTweensOf(Prize_1_Image);
			TweenMax.killTweensOf(Prize_2_Image);
			TweenMax.killTweensOf(Prize_3_Image);
			TweenMax.killTweensOf(Prize_4_Image);
			TweenMax.killTweensOf(Prize_5_Image);			
		}
		
		
		
		
		
		
		
		//Core for all scene character.
		private var Selected_Character:String;
		
		//The Location of the items and bubble.
		private var Idea_Bubble_Location:Point;
		private var Idea_Item_Location:Point;
		private var Idea_Item_Max_Height:int;
		
		//Set the character we are using.
		public function set Current_Character(New_Character:String):void{
			
			//Set the character.
			Selected_Character = New_Character;
			Set_Selection_Animations();			
			
			//Set the Idea Bubble location.
			switch("Muno"){				
				
				case "Plex":{	Idea_Bubble_Location 	= new Point(283, 83);}break;
				case "Muno":{	Idea_Bubble_Location 	= new Point(185, 84);}break;
				case "Foofa":{	Idea_Bubble_Location 	= new Point(251, 73);}break;
				case "Brobee":{	Idea_Bubble_Location 	= new Point(178, 8);}break;
				case "Toodee":{	Idea_Bubble_Location 	= new Point(178, 5);}break;	
				
			}
			
			//Set the item location.
			switch("Muno"){				
				
				case "Plex":{	Idea_Item_Location 		= new Point(473, 230);}break;
				case "Muno":{	Idea_Item_Location 		= new Point(428, 277);}break;
				case "Foofa":{	Idea_Item_Location 		= new Point(504, 255);}break;
				case "Brobee":{	Idea_Item_Location 		= new Point(411, 221);}break;
				case "Toodee":{	Idea_Item_Location 		= new Point(416, 209);}break;	
				
			}
			
			//Set the item Height.
			switch(Selected_Character){				
				
				case "Plex":{	Idea_Item_Max_Height 	= 180;}break;
				case "Muno":{	Idea_Item_Max_Height 	= 200;}break;
				case "Foofa":{	Idea_Item_Max_Height 	= 200;}break;
				case "Brobee":{	Idea_Item_Max_Height 	= 200;}break;
				case "Toodee":{	Idea_Item_Max_Height 	= 200;}break;	
				
			}
			
		}
		
		public function get Current_Character():String{
			return Selected_Character;
		}
		
		public function get Current_Idea_Bubble_Location():Point{
			return Idea_Bubble_Location;
		}
		
		public function get Current_Idea_Item_Location():Point{
			return Idea_Item_Location;
		}
		
		public function get Current_Idea_Item_Max_Height():int{
			return Idea_Item_Max_Height;
		}	
		
		//The Items we will be building.
		private var Selected_Building_Item:String;
		private var All_Building_Items:Array;
		private var Selected_Ginger_Bread_Sex:String;
		private var Ginger_Bread_Sex:Array;
		private var A_Call_To_Reset:Boolean;
		
		//Building Items functionality.
		public function Set_Next_Building_Item():void{
			
			//Check contitions.
			if(A_Call_To_Reset){
				Reset_Building_Items();
			}
			if(All_Building_Items == null){
				Reset_Building_Items();
			}
			if(All_Building_Items.length == 0){
				Reset_Building_Items();				
			}
			
			//Grab next project.
			var Selection:int 		= Math.floor(Math.random() * All_Building_Items.length);
			Selected_Building_Item 	= All_Building_Items[Selection];
			//Selected_Building_Item 	= "Snow_Man"; //Specific level to test.
			All_Building_Items.splice(Selection, 1);
			
			//Setup Location for the building of the item.
			Set_Items_Build_Locations();
			//Setup Scale for the building of the item.
			Set_Items_Build_Scale();
			//Setup Particle spawn location of item.
			Set_Items_Build_Particle_Location();
					
		}
		public function Call_To_Reset(The_Call:Boolean = true):void{A_Call_To_Reset = The_Call;}
		public function get Current_Building_Item():String{return Selected_Building_Item;}
		public function Reset_Building_Items():void{
			A_Call_To_Reset 	= false;
			Reset_Prize_Room();
			All_Building_Items 	= [	"Snow_Globe", 
									"Snow_Man", 
									"Ginger_Bread", 
									"Christmas_Tree", 
									"Hot_Cocoa"	];
		}
		public function Get_Ginger_Bread_Sex():String{

			//Check contitions.
			if(Ginger_Bread_Sex == null){
				Ginger_Bread_Sex = ["Boy", "Girl"];
			}
			if(Ginger_Bread_Sex.length == 0){
				Ginger_Bread_Sex = ["Boy", "Girl"];			
			}
			
			//Get Sex.
			var Selection:int 			= Math.floor(Math.random() * Ginger_Bread_Sex.length);
			Selected_Ginger_Bread_Sex 	= Ginger_Bread_Sex[Selection];		
			//Selected_Ginger_Bread_Sex	= "Boy";
			Ginger_Bread_Sex.splice(Selection, 1);
			
			return Selected_Ginger_Bread_Sex;		
			
		}
		
		//item Selections Character animations.
		public var Current_Character_Idle_Animations:Array;
		public var Current_Character_Tap_Animations:Array;
		public var Current_Character_Reaction_Animations:Array;
		public var Current_Character_Final_Animations:Array;
		
		private function Set_Selection_Animations():void
		{			
			if(Current_Character_Idle_Animations == null){Current_Character_Idle_Animations = [];}
			else{Current_Character_Idle_Animations.length = 0;}
			
			if(Current_Character_Tap_Animations == null){Current_Character_Tap_Animations = [];}
			else{Current_Character_Tap_Animations.length = 0;}
			
			if(Current_Character_Reaction_Animations == null){Current_Character_Reaction_Animations = [];}
			else{Current_Character_Reaction_Animations.length = 0;}
			
			if(Current_Character_Final_Animations == null){Current_Character_Final_Animations = [];}
			else{Current_Character_Final_Animations.length = 0;}

			switch(Selected_Character){
				
				case "Muno":{
					Current_Character_Idle_Animations		= ["idle"];
					Current_Character_Tap_Animations 		= ["reaction03"];
					Current_Character_Reaction_Animations 	= ["reaction01", "reaction02", "reaction03"];
					Current_Character_Final_Animations 		= ["successDance"];
				}break;
				
				case "Foofa":{
					Current_Character_Idle_Animations		= ["idle"];
					Current_Character_Tap_Animations 		= ["action01"];
					Current_Character_Reaction_Animations 	= ["reaction01", "reaction02", "reaction03"];
					Current_Character_Final_Animations 		= ["dance"];
				}break;
				
				case "Plex":{
					Current_Character_Idle_Animations		= ["idle"];
					Current_Character_Tap_Animations 		= ["reaction02"];
					Current_Character_Reaction_Animations 	= ["reaction01", "reaction02", "reaction03"];
					Current_Character_Final_Animations 		= ["success01"];
				}break;
				
				case "Toodee":{
					Current_Character_Idle_Animations		= ["idle"];
					Current_Character_Tap_Animations 		= ["action01", "action02"];
					Current_Character_Reaction_Animations 	= ["reaction01", "reaction02", "reaction03"];
					Current_Character_Final_Animations 		= ["successDance"];
				}break;
				
				case "Brobee":{
					Current_Character_Idle_Animations		= ["idle"];
					Current_Character_Tap_Animations 		= ["reaction01", "reaction02"];
					Current_Character_Reaction_Animations 	= ["action01", "action02", "action03"];
					Current_Character_Final_Animations 		= ["successDance"];
				}break;
				
			}
			
		}
		
		public function Get_Character_Selection_Animation(Mode:String = "Nothing", animNum:int = 0):String
		{
			switch(Mode){
				
				case "Tap":{
					return Current_Character_Tap_Animations[Math.floor(Math.random() * Current_Character_Tap_Animations.length)];
				}break;
				
				case "Reaction":{
					return Current_Character_Reaction_Animations[animNum];
				}break;
				
				case "Final":{
					return Current_Character_Final_Animations[Math.floor(Math.random() * Current_Character_Final_Animations.length)];
				}break;
				
				default:{
					return Current_Character_Idle_Animations[Math.floor(Math.random() * Current_Character_Idle_Animations.length)];
				}break;
				
			}
			
			return "Nothing";
		}
		
		//Build Items Location.
		private var Item_Build_Location:Point;		
		//Build Items Scale.
		private var Item_Build_Scale:Number;
		//Particle Spawn Location
		private var Item_Build_Particle_Location:Point;
		
		private function Set_Items_Build_Locations():void{

			//Set the item location.
			switch(Selected_Building_Item){				
				
				case "Snow_Globe":{		Item_Build_Location = new Point(261, 54);}break;
				case "Snow_Man":{		Item_Build_Location = new Point(232, 12);}break;
				case "Ginger_Bread":{	Item_Build_Location = new Point(10, 200);}break;
				case "Christmas_Tree":{	Item_Build_Location = new Point(250, 20);}break;
				case "Hot_Cocoa":{		Item_Build_Location = new Point(260, 94);}break;	
				
			}
			
		}			
		public function Set_Items_Build_Scale():void{
			
			//get the item build states.
			switch(Selected_Building_Item){				
				
				case "Snow_Globe":{		Item_Build_Scale = 1.0;}break;
				case "Snow_Man":{		Item_Build_Scale = 1.0;}break;
				case "Ginger_Bread":{	Item_Build_Scale = 1.0;}break;
				case "Christmas_Tree":{	Item_Build_Scale = 1.0;}break;
				case "Hot_Cocoa":{		Item_Build_Scale = 1.0;}break;	
				
			}		
			
		}
		public function Set_Items_Build_Particle_Location():void{
			
			//get the item build states.
			switch(Selected_Building_Item){				
				
				case "Snow_Globe":{		Item_Build_Particle_Location = new Point(429, 606);}break;
				case "Snow_Man":{		Item_Build_Particle_Location = new Point(473, 723);}break;
				case "Ginger_Bread":{	Item_Build_Particle_Location = new Point(457, 731);}break;
				case "Christmas_Tree":{	Item_Build_Particle_Location = new Point(477, 736);}break;
				case "Hot_Cocoa":{		Item_Build_Particle_Location = new Point(448, 694);}break;	
				
			}		
			
		}		
		
		public function get Current_Items_Build_Location():Point
		{
			return Item_Build_Location;
		}
		
		public function get Current_Items_Build_Scale():Number{
			return Item_Build_Scale;
		}
		
		public function get Current_Items_Build_Particle_Location():Point
		{
			return Item_Build_Particle_Location;
		}
		
		//Items Build States.	
		public function Get_Build_Item_States():Array{
			
			//get the item build states.
			switch(Selected_Building_Item)
			{				
				case "Snow_Globe":{		return Snow_Globe_State.Build_State();}break;
				case "Snow_Man":{		return Snow_Man_State.Build_State();}break;
				case "Ginger_Bread":{	return Ginger_Bread_State.Build_State(Selected_Ginger_Bread_Sex);}break;
				case "Christmas_Tree":{	return Christmas_Tree_State.Build_State();}break;
				case "Hot_Cocoa":{		return Hot_Cocoa_State.Build_State();}break;	
			}		
			
			return [];
			
		}
		
		private function Reset_All_Building_Items_Flow():void{
			
			//Snow Globe.
			Snow_Globe_State.Reset_States();
			Ginger_Bread_State.Reset_States();
			Christmas_Tree_State.Reset_States();
			Snow_Man_State.Reset_States();
			Hot_Cocoa_State.Reset_States();
			
		}	
	}
}