package scenes.Item_Construction_Section{
	
	import com.cupcake.Utils;
	import com.greensock.TweenMax;
	import com.greensock.plugins.BezierPlugin;
	
	import flash.filesystem.File;
	import flash.geom.Point;
	import flash.system.System;
	
	import de.flintfabrik.starling.display.FFParticleSystem;
	
	import dragonBones.Armature;
	import dragonBones.animation.WorldClock;
	import dragonBones.factorys.StarlingFactory;
	import dragonBones.objects.ObjectDataParser;
	import dragonBones.objects.SkeletonData;
	import dragonBones.textures.StarlingTextureAtlas;
	
	import scenes.Particle_Effects.Burst_Item_Building_Particle;
	import scenes.Particle_Effects.Glitter_Box_Particle;
	
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.extensions.SoundManager;
	import starling.textures.Texture;
	import starling.utils.AssetManager;
	import starling.utils.deg2rad;
	
	import utils.Particle_Loader;
	
	public class Item_Construction_Trophy_Scene extends Sprite
	{
		//Core.
		private var soundmanager:SoundManager;
		private var assets:AssetManager;
		private var sceneRoot:Sprite;
		private var assetsLoaded:Boolean 			= false;
		private var appDir:File 					= File.applicationDirectory;
		
		private var armatures:Array = new Array();
		private var armSprites:Array = new Array();
		
		private var factory:StarlingFactory;
		
		//Scene assets.
		private var Background:Image;
		private var Back_Button:Button;
		private var Back_Button_Holder:Sprite;
		private var Brodys_Arm:Image;
		
		private var mouseX:Number = 0;
		private var mouseY:Number = 0;
		private var mouseDown:Boolean = false;
		private var sleighOL:Image;
		private var sleighUL:Image;
		
		private var iceOverlay:Image;
		
		//Particles.
		private var Placement_Explosion:Array;
		private var Snow_Particles:Particle_Loader;			
		private var Finger_Drag_Particles:Vector.<Particle_Loader>;
		private var Drager_Counter:Number;
		
		private var dropPresentQuadLeft:Quad;
		private var dropPresentQuadRight:Quad;
		
		private var Item_Spray_Explosion:Burst_Item_Building_Particle;

		//Scene variables.
		private var Game_Mode:String;
		
		private  var Tossables:Array = new Array();
		
		private static var presentPlacements:Array = [new Point(690,330),new Point(750,320),new Point(660,280),new Point(690,340),new Point(860,390),new Point(745,350),new Point(825,370)];
		
		private static var presentStartLocations:Array = [new Point(270,660),new Point(380,660),new Point(490,660),new Point(610,660),new Point(760,660),new Point(910,660),new Point(1050,660)];
		
		public function Item_Construction_Trophy_Scene(Value:String = "")
		{
			super();
			
			//Test.
			//Root.Gaba_Core_Data.Test_Prize_Room();			
			
			assets = Utils.CreateAssetManager([
				
				//Particles.
				"textures/%SCALE%x/Particles/Star_Particle.png",	
				
				"particles/Snow.png",
				"particles/Snow.pex",	
				"particles/Drag_Particle_1.png",
				"particles/Drag_Particle_2.png",
				"particles/Drag_Particle_3.png",
				"particles/Drag_Particle_4.png",
				"particles/Drag_Particle_5.png",
				"particles/Finger_Drag_Snow.pex",
				
				//Elements for screen.					
				"textures/%SCALE%x/Screen_Global_Elements/Backgrounds/PrizeRoom_Background.png",
				"textures/%SCALE%x/Screen_Global_Elements/Globals",
				"textures/%SCALE%x/Building_Item_Screen/TrophyRoom/",
				
				//Audio.				
				"audio/Trophy_Scene"
			]);
			
			Root.currentAssetsProxy = assets;
			
			assets.loadQueue(function onProgress(ratio:Number):void
			{
				if (ratio == 1){
					Starling.juggler.delayCall(function():void
					{
						// now would be a good time for a clean-up 
						System.pauseForGCIfCollectionImminent(0);
						System.gc();
					}, 0.15);
					addObjects();
				};
			});
		}
		
		private function addObjects():void
		{
			//Add Dispose Event.
			addEventListener(Root.DISPOSE,  disposeScene);

			//Create Scene.
			sceneRoot 					= new Sprite();
			sceneRoot.x 				= Costanza.STAGE_OFFSET;
			addChild(sceneRoot);

			factory = new StarlingFactory();

			//Create Sounds.
			soundmanager = new SoundManager();
			Root.AddSM( soundmanager );

			//Music.
			soundmanager.addSound("Music_Trophy_Room",					assets.getSound("Music_Trophy_Room"));
			soundmanager.addSound("Pick_Up_Item",						assets.getSound("TR_Grab_Item"));
			soundmanager.addSound("Back",								assets.getSound("TR_Home_Button"));
			soundmanager.addSound("Tree_Snowglobe_Placement",			assets.getSound("Tree_Snowglobe_Placement"));
			soundmanager.addSound("Snowman_Ginger_Drink_Placement",		assets.getSound("Snowman_Ginger_Drink_Placement"));
			soundmanager.addSound("TR_All_Collected",					assets.getSound("TR_All_Collected"));
			soundmanager.addSound("Yogaba_Cheer",						assets.getSound("Yogaba_Cheer"));
			soundmanager.addSound("EXPLODE",						assets.getSound("Small_Explosion"));

			soundmanager.addSound("Throw",						assets.getSound("gift hit"));

			soundmanager.addSound("Light1",						assets.getSound("lights 1"));
			soundmanager.addSound("Light2",						assets.getSound("lights 2"));

			soundmanager.addSound("Drag_Finger_SFX",					assets.getSound("Drag_Finger_SFX"));	
			soundmanager.addSound("Remove_Finger_SFX",					assets.getSound("Remove_Finger_SFX"));	

			soundmanager.playSound("Music_Trophy_Room", 				Costanza.musicVolume,	999);

			//Background.
			Background 					= new Image(assets.getTexture("PrizeRoom_Background"));
			Background.x 				= 0;
			Background.y 				= 0;
			sceneRoot.addChild(Background);
			
			iceOverlay 					= new Image(assets.getTexture("mountain_mask"));
			iceOverlay.x 				= 5;
			iceOverlay.y 				= 0;
			sceneRoot.addChild(iceOverlay);

			AddArmatures();

			sleighUL = new Image(assets.getTexture("sleighUL"));
			sleighUL.x = 570; sleighUL.y = 280;
			sleighUL.touchable = false;
			sceneRoot.addChild(sleighUL);

			sleighOL = new Image(assets.getTexture("sleighOL"));
			sleighOL.x = 570; sleighOL.y = 300;
			sleighOL.touchable = false;
			sceneRoot.addChild(sleighOL);

			dropPresentQuadLeft = new Quad(200,300,0x737373);
			dropPresentQuadLeft.x = 610;
			dropPresentQuadLeft.y = 100;
			dropPresentQuadLeft.alpha = 0.0;
			dropPresentQuadLeft.touchable = false;
			sceneRoot.addChild(dropPresentQuadLeft);

			dropPresentQuadRight = new Quad(180,350,0x982437);
			dropPresentQuadRight.x = 810;
			dropPresentQuadRight.y = 100;
			dropPresentQuadRight.alpha = 0.0;
			dropPresentQuadRight.touchable = false;
			sceneRoot.addChild(dropPresentQuadRight);

			//Add Prizes.
			Root.Gaba_Core_Data.Setup_Prize_Room();
			sceneRoot.addChild(Root.Gaba_Core_Data.Get_Prizes());

			//Broodys arm.
			Brodys_Arm					= new Image(assets.getTexture("DJ_Hand"));
			Brodys_Arm.x 				= 1366;
			Brodys_Arm.y 				= 200;
			sceneRoot.addChild(Brodys_Arm);

			//Add current Prize.
			sceneRoot.addChild(Root.Gaba_Core_Data.Set_Current_Prize());

			//Particles.
			Placement_Explosion = new Array();
			for(var X:int = 0; X < 5; X++){
				Placement_Explosion.push(new Glitter_Box_Particle(assets.getTexture("Star_Particle"), new Point(500, 400)));
				Placement_Explosion[X].x = -9000;
				Placement_Explosion[X].y = -9000;
				sceneRoot.addChild(Placement_Explosion[X]);
			}			

			FFParticleSystem.init(1024, false, 512, 4);
			Snow_Particles				= new Particle_Loader(sceneRoot, assets.getTexture("Snow"), assets.getXml("Snow"));
			Snow_Particles.x			= 300;
			Snow_Particles.Play(0);
			
			//Burst.
			Item_Spray_Explosion 			= new Burst_Item_Building_Particle(assets.getTexture(("Brobee_Burst")), "Ending");
			

			//Add Dragble particles.
			Finger_Drag_Particles 		= new Vector.<Particle_Loader>();
			//Finger_Drag_Particles.push(new Particle_Loader(sceneRoot, assets.getTexture("Drag_Particle_1"), assets.getXml("Finger_Drag_Snow")));
			//Finger_Drag_Particles.push(new Particle_Loader(sceneRoot, assets.getTexture("Drag_Particle_2"), assets.getXml("Finger_Drag_Snow")));
			//Finger_Drag_Particles.push(new Particle_Loader(sceneRoot, assets.getTexture("Drag_Particle_3"), assets.getXml("Finger_Drag_Snow")));
			//Finger_Drag_Particles.push(new Particle_Loader(sceneRoot, assets.getTexture("Drag_Particle_4"), assets.getXml("Finger_Drag_Snow")));
			//Finger_Drag_Particles.push(new Particle_Loader(sceneRoot, assets.getTexture("Drag_Particle_5"), assets.getXml("Finger_Drag_Snow")));
			for(var P:int = 0; P < Finger_Drag_Particles.length; P++){
				Particle_Loader(Finger_Drag_Particles[P]).Play(0);
				Particle_Loader(Finger_Drag_Particles[P]).Stop();
			}

			//Back button.
			Back_Button_Holder          = new Sprite();
			Back_Button 				= new Button(assets.getTexture("button_Boombox"));			
			Back_Button.x          		= -(Back_Button.width  / 2);
			Back_Button.y          		= -(Back_Button.height / 2);
			Back_Button_Holder.addChild(Back_Button);

			if((Costanza.VIEWPORT_HEIGHT / Costanza.VIEWPORT_WIDTH) < .75){				
				if((Costanza.VIEWPORT_HEIGHT / Costanza.VIEWPORT_WIDTH) < .6){				
					Back_Button_Holder.x 	= 130;
					Back_Button_Holder.y 	= 60;
				}else{
					Back_Button_Holder.x 	= 300;
					Back_Button_Holder.y 	= 60;
				}
			}else{
				Back_Button_Holder.x 		= 300;
				Back_Button_Holder.y 		= 60;
			}	

			Back_Button_Holder.alpha 	= 0;					
			sceneRoot.addChild(Back_Button_Holder);	

			//Game variables.
			Game_Mode					= "First_Begin";	
			Drager_Counter				= 0.0;

			SpawnPresents();

			//SCene is loaded.
			dispatchEventWith(Root.SCENE_LOADED, true);

			//Add Listeners.
			addEventListener(starling.events.EnterFrameEvent.ENTER_FRAME, 					ActivitySelection_Update_Scene);
			addEventListener(starling.events.TouchEvent.TOUCH, 								ActivitySelection_Mouse_Scene);
		}

		private function SpawnPresents():void
		{
			for(var x:int = 0; x < 7; x++)
			{
				if(Root.Gaba_Core_Data.presentInSleigh[x] == false)
				{
					Spawn_Tossable(presentStartLocations[x].x, presentStartLocations[x].y, "Gift_0" + (x+1).toString());
				}
				else
				{
					Spawn_Tossable(presentPlacements[x].x, presentPlacements[x].y, "Gift_0" + (x+1).toString());
					Tossables[x][1] = 1;
					sceneRoot.setChildIndex(Tossables[x][0], sceneRoot.getChildIndex(sleighOL));
				}
			}
		}

		//Buttons functionalities.
		private function goBack(e:starling.events.Event):void
		{
			//Remove Listeners.
			removeEventListener(starling.events.EnterFrameEvent.ENTER_FRAME, 				ActivitySelection_Update_Scene);
			removeEventListener(starling.events.TouchEvent.TOUCH, 							ActivitySelection_Mouse_Scene);
			Back_Button.removeEventListener(starling.events.Event.TRIGGERED, goBack);
			TweenMax.delayedCall(1, Total_Exit);
		}
		
		//The Burst.
		private function Setup_Burst():void{
			
			//Setup Burst.
			Item_Spray_Explosion.Prep_Burst("Ending");
			//Item_Spray_Explosion.alignPivot();
			Item_Spray_Explosion.x = Costanza.STAGE_OFFSET;
			Item_Spray_Explosion.y = 0;
			sceneRoot.addChildAt(Item_Spray_Explosion, sceneRoot.numChildren - 1);
		}
		private function Halt_Burst():void{	
			//Next call.
			Item_Spray_Explosion.Activate_Burst("Finish", Burst_Completed);
			
		}
		private function Burst_Completed():void
		{	
			//Clean Burst.
			sceneRoot.removeChild(Item_Spray_Explosion);
			Item_Spray_Explosion.Dispose_Me();
			Item_Spray_Explosion.dispose();
		}
		

		//Leave.
		private function Exit_Game():void
		{
			//Remove Listeners.	
			removeEventListener(starling.events.EnterFrameEvent.ENTER_FRAME, 				ActivitySelection_Update_Scene);
			removeEventListener(starling.events.TouchEvent.TOUCH, 							ActivitySelection_Mouse_Scene);
			Back_Button.removeEventListener(starling.events.Event.TRIGGERED, goBack);
			TweenMax.delayedCall(1, Total_Exit);
		}

		private function Total_Exit():void
		{
			soundmanager.stopAllSounds();
			TweenMax.delayedCall(0.3, dispatchEventWith,[Root.LOAD_ROOM, true, {room:1, Character:"Mira"}]);
		}

		//Clean.
		private function disposeScene():void
		{
			TweenMax.killDelayedCallsTo(PlayEndLights);
			
			//Clear Sounds.
			soundmanager.stopAllSounds();
			Root.RemoveSM( soundmanager );
			soundmanager.dispose();

			for (var i:int = 0; i < armatures.length; i++)
			{
				armSprites[i].removeEventListeners(TouchEvent.TOUCH);
				WorldClock.clock.remove(armatures[i]);
				armatures[i].dispose();
			}
			armatures.length = 0;
			armSprites.length = 0;
			
			factory.removeSkeletonData("skeleton");
			factory.removeTextureAtlas("texture");

			//Stop tweens in core.
			Root.Gaba_Core_Data.Stop_Dancing_Trophys();

			//Dispose Images.
			if(sceneRoot.contains(Background)){sceneRoot.removeChild(Background);}
			Background.base.dispose();
			Background.dispose();

			//Remove Prizes.
			sceneRoot.removeChild(Root.Gaba_Core_Data.Get_Prizes());

			//Dispose Buttons.
			TweenMax.killTweensOf(Back_Button_Holder);
			if(sceneRoot.contains(Back_Button_Holder)){sceneRoot.removeChild(Back_Button_Holder);}
			Back_Button_Holder.removeChild(Back_Button);
			Back_Button_Holder.dispose();
			Back_Button.dispose();

			//Particles.
			for(var X:int = 0; X < Placement_Explosion.length; X++)
			{
				sceneRoot.removeChild(Placement_Explosion[X]);
				Placement_Explosion[X].Dispose_Glitter_Box_Particle();
				Placement_Explosion.splice(X, 1);
				X--;
			}Placement_Explosion.length = 0;

			for(var P:int = 0; P < Finger_Drag_Particles.length; P++){										
				var ThisLoader:Particle_Loader = Finger_Drag_Particles[P];
				ThisLoader.Stop();
				Finger_Drag_Particles.splice(P, 1);
				ThisLoader.Dispose_Me(sceneRoot);
				P-=1;				
			}Finger_Drag_Particles.length = 0;

			Snow_Particles.Dispose_Me(sceneRoot);
			FFParticleSystem.dispose();

			//Clean remaning is forgoten.
			sceneRoot.removeChildren(0, (sceneRoot.numChildren - 1), true);

			//Clear Asset Manager.
			assets.purge();
			assets.dispose();
		}

		//Update.
		private function ActivitySelection_Update_Scene(e:starling.events.EnterFrameEvent):void
		{
			Update_Tossable();

			switch(Game_Mode)
			{
				case "First_Begin":{
					TweenMax.delayedCall(1.0, Delay_Calls, ["First_Begin"]);
					Game_Mode = "Prep_Item";
				}break;

				//Here we create the item we will place on the Object.
				case "Prep_Item":{
					//Create item.
					Game_Mode 				= "Show_Item";
				}break;
				case "Show_Item":
				{
					//Placement.
					Root.Gaba_Core_Data.Get_Current_Prize().x = ((Brodys_Arm.x + 160) - (Root.Gaba_Core_Data.Get_Current_Prize().width  / 2));
					Root.Gaba_Core_Data.Get_Current_Prize().y = ((Brodys_Arm.y + 190) - (Root.Gaba_Core_Data.Get_Current_Prize().height / 2));	

					//Move brodys arm.
					TweenMax.killTweensOf(Brodys_Arm);
					TweenMax.to(Brodys_Arm, 2.0, {x:800, onComplete:Delay_Calls, onCompleteParams:["Show_Item"]});

					Game_Mode = "Move_Item";
				}break;				
				case "Move_Item":{		
					//Move.
					Root.Gaba_Core_Data.Get_Current_Prize().x = ((Brodys_Arm.x + 160) - (Root.Gaba_Core_Data.Get_Current_Prize().width  / 2));
					Root.Gaba_Core_Data.Get_Current_Prize().y = ((Brodys_Arm.y + 190) - (Root.Gaba_Core_Data.Get_Current_Prize().height / 2));
					
				}break;		
				case "Pick_Up_Item":{					
					
				}break;	
				case "Moving_Item":{
					
				}break;
				case "Falling_Item":{				
					
				}break;
				case "Move_To_Position":{
					//Glitter all.
					for(var X:int = 0; X < Placement_Explosion.length; X++){						
						Placement_Explosion[X].x 			= (Root.Gaba_Core_Data.Get_Specific_Prize(X).x + (Root.Gaba_Core_Data.Get_Specific_Prize(X).width / 2));
						Placement_Explosion[X].y 			= (Root.Gaba_Core_Data.Get_Specific_Prize(X).y + (Root.Gaba_Core_Data.Get_Specific_Prize(X).height / 2));						
					}	
				}break;

				case "Rotation_Update":{
					Root.Gaba_Core_Data.Update_Dancing_Trophys();
				}break;
			}
			Drager_Counter -= 0.1;
			if(Drager_Counter <= 0.0){Drager_Counter = 0.0;}
			WorldClock.clock.advanceTime(-1);	
		}
		private function Delay_Calls(The_Call:String):void{
			switch(The_Call){
				
				case "First_Begin":{					
					
				}break;
				
				case "Show_Item":{
					
					Game_Mode = "Pick_Up_Item";
					
				}break;
				case "Ok_Item_Dropped":{
					
					TweenMax.killTweensOf(Back_Button_Holder);
					Back_Button_Holder.alpha = 1.0;
					TweenMax.to(Back_Button_Holder, 0.5, {scaleX:0.8, scaleY:0.8, yoyo:true, repeat:-1});
					Game_Mode = "Free_Play";
					
				}break;
				
				case "Move_To_Position":{
					//Dance Go.
					Game_Mode = "Rotation_Update";
					
					//End timmer.
					TweenMax.delayedCall(11.0, Delay_Calls, ["Finaly"]);	
				}break;

				case "Finaly":{
					Exit_Game();
				}break;
			}
		}
		
		private function AddArmatures():void
		{
			var armatureClip1:Sprite;
			var skeletonData:SkeletonData;
			var texture:Texture;
			var textureAtlas:StarlingTextureAtlas;
			var armature:Armature;
			
			skeletonData = ObjectDataParser.parseSkeletonData(assets.getObject("skeleton"));
			factory.addSkeletonData(skeletonData);
			
			texture = assets.getTexture("texture");
			textureAtlas = new StarlingTextureAtlas(texture, assets.getObject("texture"),true);
			factory.addTextureAtlas(textureAtlas);
			
			/****LIGHT01******/
			armature = factory.buildArmature("!!Characters/String01Size090");
			WorldClock.clock.add(armature);
			
			armatureClip1 = armature.display as Sprite;
			armatureClip1.x = 305;
			armatureClip1.y = 191;
			armatureClip1.scaleX = 0.9;
			armatureClip1.scaleY = 0.9;
			
			armatureClip1.name = "Light01";
			
			armatureClip1.addEventListener(TouchEvent.TOUCH, TouchLightHandler);
			armature.animation.gotoAndPlay("idle");
			
			armatures.push(armature);
			armSprites.push(armatureClip1);
			
			sceneRoot.addChild(armatureClip1);
			
			/****LIGHT02******/
			armature = factory.buildArmature("!!Characters/String02Size100");
			WorldClock.clock.add(armature);
			
			armatureClip1 = armature.display as Sprite;
			armatureClip1.x = 405;
			armatureClip1.y = 389;
			armatureClip1.scaleX = 1;
			armatureClip1.scaleY = 1;
			
			armatureClip1.name = "Light02";
			
			armatureClip1.addEventListener(TouchEvent.TOUCH, TouchLightHandler);
			armature.animation.gotoAndPlay("idle");
			
			armatures.push(armature);
			armSprites.push(armatureClip1);
			
			sceneRoot.addChild(armatureClip1);
			
			/****LIGHT03******/
			armature = factory.buildArmature("!!Characters/String03Size070");
			WorldClock.clock.add(armature);
			
			armatureClip1 = armature.display as Sprite;
			armatureClip1.x = 657;
			armatureClip1.y = 184;
			armatureClip1.scaleX = 0.7;
			armatureClip1.scaleY = 0.7;
			
			armatureClip1.name = "Light03";
			
			armatureClip1.addEventListener(TouchEvent.TOUCH, TouchLightHandler);
			armature.animation.gotoAndPlay("idle");
			
			armatures.push(armature);
			armSprites.push(armatureClip1);
			
			sceneRoot.addChild(armatureClip1);
			
			/****LIGHT04******/
			armature = factory.buildArmature("!!Characters/String04Size090");
			WorldClock.clock.add(armature);
			
			armatureClip1 = armature.display as Sprite;
			armatureClip1.x = 981;
			armatureClip1.y = 167;
			armatureClip1.scaleX = 0.9;
			armatureClip1.scaleY = 0.9;
			
			armatureClip1.name = "Light04";
			
			armatureClip1.addEventListener(TouchEvent.TOUCH, TouchLightHandler);
			armature.animation.gotoAndPlay("idle");
			
			armatures.push(armature);
			armSprites.push(armatureClip1);
			
			sceneRoot.addChild(armatureClip1);
			
			/****LIGHT05******/
			armature = factory.buildArmature("!!Characters/String05Size100");
			WorldClock.clock.add(armature);
			
			armatureClip1 = armature.display as Sprite;
			armatureClip1.x = 1138;
			armatureClip1.y = 422;
			armatureClip1.scaleX = 1;
			armatureClip1.scaleY = 1;
			
			armatureClip1.name = "Light05";
			
			armatureClip1.addEventListener(TouchEvent.TOUCH, TouchLightHandler);
			armature.animation.gotoAndPlay("idle");
			
			armatures.push(armature);
			armSprites.push(armatureClip1);
			
			sceneRoot.addChild(armatureClip1);
			
			/****Sleigh******/
			armature = factory.buildArmature("!!Characters/SleighFullSize100");
			WorldClock.clock.add(armature);

			armatureClip1 = armature.display as Sprite;
			armatureClip1.x = -600;
			armatureClip1.y = 500;
			armatureClip1.scaleX = 1;
			armatureClip1.scaleY = 1;

			armatureClip1.name = "Sleigh";

			armature.animation.gotoAndPlay("idle");

			armatures.push(armature);
			armSprites.push(armatureClip1);

			sceneRoot.addChild(armatureClip1);
		}

		private function TouchLightHandler(e:starling.events.TouchEvent):void
		{
			var currentTouch:Touch = e.getTouch(stage);
			if(currentTouch 		== null){return;}
			var touchName:String = (e.currentTarget as Sprite).name;
			
			if(currentTouch.phase == "began")
			{
				switch(touchName)
				{
					case "Light01":
					{
						(armatures[0] as Armature).animation.gotoAndPlay("action01");
						soundmanager.playSound("Light1");
						break;
					}
					case "Light02":
					{
						(armatures[1] as Armature).animation.gotoAndPlay("action01");
						soundmanager.playSound("Light2");
						break;
					}
					case "Light03":
					{
						(armatures[2] as Armature).animation.gotoAndPlay("action01");
						soundmanager.playSound("Light1");
						break;
					}
					case "Light04":
					{
						(armatures[3] as Armature).animation.gotoAndPlay("action01");
						soundmanager.playSound("Light2");
						break;
					}
					case "Light05":
					{
						(armatures[4] as Armature).animation.gotoAndPlay("action01");
						soundmanager.playSound("Light1");
						break;
					}
				}
			}
		}
		
		private function Init_Tossable():void
		{
			if(Tossables == null)
			{
				Tossables = new Array();
			}
			else
			{
				Destroy_Tossable();
			}
		}
		
		private function Spawn_Tossable(pX:int, pY:int, objectToSpawn:String):void
		{
			var NewTossable:Image;
			var Tossable_Props:Array = new Array();
			
			NewTossable = new Image(assets.getTexture(objectToSpawn));
			
			NewTossable.x = pX;
			NewTossable.y = pY;
			NewTossable.pivotX = NewTossable.width/2;
			NewTossable.pivotY = NewTossable.height/2;
			NewTossable.name = objectToSpawn;
			sceneRoot.addChild(NewTossable);
			var Phase:int = 0;			
			var Force_X:Number = 0;
			var Force_Y:Number = 0;
			
			Tossable_Props.push(NewTossable);
			Tossable_Props.push(Phase);
			Tossable_Props.push(Force_X);
			Tossable_Props.push(Force_Y);
			Tossable_Props.push(0);
			
			Tossables.push(Tossable_Props);
		}
		
		private function Select_Tossable(pX:int, pY:int):void
		{
			for(var X:int = 0; X < Tossables.length; X++){
				if(Tossables[X][0].getBounds(sceneRoot).contains(pX, pY))
				{
					for(var P:int = 0; P < Finger_Drag_Particles.length; P++)
					{
						sceneRoot.setChildIndex(Particle_Loader(Finger_Drag_Particles[P]), (sceneRoot.getChildIndex(Tossables[X][0]) - 1));
					}
					sceneRoot.setChildIndex(Tossables[X][0], (sceneRoot.numChildren - 1));
					Tossables[X][1] = 2;
					Tossables[X][4] = 0;
					return;
				}
			}
		}
		/*
		private function TweenToSleigh(placeNum:int,presentNum:int):void
		{
			sceneRoot.setChildIndex(Tossables[presentNum][0], sceneRoot.getChildIndex(sleighOL));
			TweenMax.to(Tossables[presentNum][0],1,{x:presentPlacements[placeNum].x, y:presentPlacements[placeNum].y});
		}
*/
		private function Unselect_Tossable(pX:int, pY:int):void{
			for(var X:int = 0; X < Tossables.length; X++)
			{
				switch(Tossables[X][1])
				{
					case 2:{
						if((dropPresentQuadLeft.getBounds(sceneRoot).contains(Tossables[X][0].x, Tossables[X][0].y)) || (dropPresentQuadRight.getBounds(sceneRoot).contains(Tossables[X][0].x, Tossables[X][0].y)))
						{
							switch(Tossables[X][0].name)
							{
								case "Gift_01":{  TweenMax.to(Tossables[X][0],1,{x:presentPlacements[0].x, y:presentPlacements[0].y}); Root.Gaba_Core_Data.presentInSleigh[0] = true; break;}
								case "Gift_02":{  TweenMax.to(Tossables[X][0],1,{x:presentPlacements[1].x, y:presentPlacements[1].y}); Root.Gaba_Core_Data.presentInSleigh[1] = true; break;}
								case "Gift_03":{  TweenMax.to(Tossables[X][0],1,{x:presentPlacements[2].x, y:presentPlacements[2].y}); Root.Gaba_Core_Data.presentInSleigh[2] = true; break;}
								case "Gift_04":{  TweenMax.to(Tossables[X][0],1,{x:presentPlacements[3].x, y:presentPlacements[3].y}); Root.Gaba_Core_Data.presentInSleigh[3] = true; break;}
								case "Gift_05":{  TweenMax.to(Tossables[X][0],1,{x:presentPlacements[4].x, y:presentPlacements[4].y}); Root.Gaba_Core_Data.presentInSleigh[4] = true; break;}
								case "Gift_06":{  TweenMax.to(Tossables[X][0],1,{x:presentPlacements[5].x, y:presentPlacements[5].y}); Root.Gaba_Core_Data.presentInSleigh[5] = true; break;}
								case "Gift_07":{  TweenMax.to(Tossables[X][0],1,{x:presentPlacements[6].x, y:presentPlacements[6].y}); Root.Gaba_Core_Data.presentInSleigh[6] = true; break;}
							}
							sceneRoot.setChildIndex(Tossables[X][0], sceneRoot.getChildIndex(sleighOL));
							Tossables[X][2] = 0;
							Tossables[X][3] = 0;
							Tossables[X][1] = 1;
						}
						else
						{
							switch(Tossables[X][0].name)
							{
								case "Gift_01":{  Root.Gaba_Core_Data.presentInSleigh[0] = false; break;}
								case "Gift_02":{  Root.Gaba_Core_Data.presentInSleigh[1] = false; break;}
								case "Gift_03":{  Root.Gaba_Core_Data.presentInSleigh[2] = false; break;}
								case "Gift_04":{  Root.Gaba_Core_Data.presentInSleigh[3] = false; break;}
								case "Gift_05":{  Root.Gaba_Core_Data.presentInSleigh[4] = false; break;}
								case "Gift_06":{  Root.Gaba_Core_Data.presentInSleigh[5] = false; break;}
								case "Gift_07":{  Root.Gaba_Core_Data.presentInSleigh[6] = false; break;}
							}
							var Angle:Number = Math.atan2((pY - Tossables[X][0].y), (pX - Tossables[X][0].x));
							var P1:Point = new Point(pX, pY);
							var P2:Point = new Point(Tossables[X][0].x, Tossables[X][0].y);
							var Speed:Number = Point.distance(P1, P2);
							if(Speed > 30){Speed = 30;}
							Tossables[X][2] = (0 + (Speed * Math.cos(Angle)));
							Tossables[X][3] = (0 + (Speed * Math.sin(Angle)));
							Tossables[X][1] = 0;
						}
					}break;
				}
			}
		}
	
		
		private function Update_Tossable():void
		{
			if(Tossables.length > 9)
			{
				sceneRoot.removeChild(Tossables[X][0]);
				Tossables.splice(0, 1);
			}
			for(var X:int = 0; X < Tossables.length; X++)
			{
				switch(Tossables[X][1]){
					case 0:{
						sceneRoot.setChildIndex(Tossables[X][0], (sceneRoot.numChildren - 1));

						Tossables[X][0].x = (Tossables[X][0].x + Tossables[X][2]);
						Tossables[X][0].y = (Tossables[X][0].y + Tossables[X][3]);

						Tossables[X][0].rotation = (Tossables[X][0].rotation + deg2rad(Tossables[X][2]));
						
						//Y Force.
						Tossables[X][3] = Math.floor(Tossables[X][3] + 2.0);

						if(Tossables[X][0].y > 660)
						{
							Tossables[X][3] = -Math.floor(Tossables[X][3] - (Tossables[X][3] / 2));
							if(Tossables[X][3] > -4)
							{
								Tossables[X][3] = 0;
							}
							Tossables[X][0].y = 660;
							Tossables[X][4] = (Tossables[X][4] + 1);
							if(Tossables[X][4] > 2){Tossables[X][3] = 0; Tossables[X][2] = 0;}
							if(Tossables[X][3] != 0)
							{
								soundmanager.playSound("Throw", Costanza.soundVolume);
							}
						}		
						//X Force.
						if(Tossables[X][0].x >= 1366 - Costanza.SCREEN_START_X)
						{
							Tossables[X][2] = -Math.floor(Tossables[X][2] - (Tossables[X][2] / 3));
							Tossables[X][0].x = 1366 - Costanza.SCREEN_START_X;
							if(Tossables[X][2] != 0)
							{
								soundmanager.playSound("Throw", Costanza.soundVolume);
							}
						}
						if(Tossables[X][0].x <= Costanza.SCREEN_START_X)
						{
							Tossables[X][2] = -Math.floor(Tossables[X][2] - (Tossables[X][2] / 3));
							Tossables[X][0].x = Costanza.SCREEN_START_X;
							if(Tossables[X][2] != 0)
							{
								soundmanager.playSound("Throw", Costanza.soundVolume);
							}
						}
						if(Tossables[X][2] < 0)
						{
							Tossables[X][2] = (Tossables[X][2] + 0.1);
							if(Tossables[X][2] >= 0)
							{
								Tossables[X][2] = 0;
							}
						}
						else if(Tossables[X][2] > 0)
						{
							Tossables[X][2] = (Tossables[X][2] - 0.1);
							if(Tossables[X][2] <= 0)
							{
								Tossables[X][2] = 0;
							}
						}
					}break;
					case 1:{
					}break;
					case 2:
					{
						Tossables[X][0].x = mouseX;
						Tossables[X][0].y = mouseY;
					}break;
				}
			}
		}
		
		private function Destroy_Tossable():void
		{
			if(Tossables == null){return;}
			for(var X:int = 0; X < Tossables.length; X++)
			{
				sceneRoot.removeChild(Tossables[X][0]);
			}
			Tossables.length = 0;
		}
		
		private function PlayEndLights():void
		{
			if((armatures[0] as Armature).animation.movementID == "allOff" || !(armatures[0] as Armature).animation.isPlaying)
			{
				(armatures[0] as Armature).animation.gotoAndPlay("action02",-1,-1,20);
			}
			if((armatures[1] as Armature).animation.movementID == "allOff" || !(armatures[1] as Armature).animation.isPlaying)
			{
				(armatures[1] as Armature).animation.gotoAndPlay("action02",-1,-1,20);
			}
			if((armatures[2] as Armature).animation.movementID == "allOff" || !(armatures[2] as Armature).animation.isPlaying)
			{
				(armatures[2] as Armature).animation.gotoAndPlay("action02",-1,-1,20);
			}
			if((armatures[3] as Armature).animation.movementID == "allOff" || !(armatures[3] as Armature).animation.isPlaying)
			{
				(armatures[3] as Armature).animation.gotoAndPlay("action02",-1,-1,20);
			}
			if((armatures[4] as Armature).animation.movementID == "allOff" || !(armatures[4] as Armature).animation.isPlaying)
			{
				(armatures[4] as Armature).animation.gotoAndPlay("action02",-1,-1,20);
			}
			TweenMax.delayedCall(3,PlayEndLights);
		}

		
		//Mouse Update.
		private function ActivitySelection_Mouse_Scene(e:starling.events.TouchEvent):void{
			var Current_Touch:Touch = e.getTouch(sceneRoot);
			if(Current_Touch 		== null){return;}		
			var Location:Point 		= new Point(Current_Touch.globalX, Current_Touch.globalY);
			
			mouseX = Current_Touch.globalX - Costanza.STAGE_OFFSET;
			mouseY = Current_Touch.globalY;
			
			switch(Current_Touch.phase)
			{				
				case "began":{	
					Select_Tossable(mouseX, mouseY);

					if(Back_Button_Holder.alpha != 0)
					{
						if(Back_Button_Holder.hitTest(Back_Button_Holder.globalToLocal(Location)))
						{							
							//Kill back button.
							soundmanager.playSound("Back");
							TweenMax.killTweensOf(Back_Button_Holder);
							//Back_Button_Holder.scaleX = 1.0;
							//Back_Button_Holder.scaleY = 1.0;
							return;							
						}
					}

					switch(Game_Mode)
					{
						case "Free_Play":
						{
							if(Root.Gaba_Core_Data.Select_Trophy(Location)){	
								soundmanager.playSound("Pick_Up_Item", Costanza.soundVolume);
								Game_Mode = "Move_Selection";
							}
						}break;
						case "Move_Item":
						case "Pick_Up_Item":{
							if(Root.Gaba_Core_Data.Get_Current_Prize().hitTest(Root.Gaba_Core_Data.Get_Current_Prize().globalToLocal(Location)) ||
								Brodys_Arm.hitTest(Brodys_Arm.globalToLocal(Location))){								
								soundmanager.playSound("Pick_Up_Item", Costanza.soundVolume);
								TweenMax.killTweensOf(Brodys_Arm);
								TweenMax.to(Brodys_Arm, 1.0, {x:1366});
								Root.Gaba_Core_Data.Get_Current_Prize().x = Location.x;
								Root.Gaba_Core_Data.Get_Current_Prize().y = Location.y;
								Game_Mode = "Moving_Item";
							}
						}break;
					}
				}break;
				case "moved":{
					//If we dont hit back play Drag sound.
					if(!soundmanager.soundIsPlaying("Drag_Finger_SFX"))
					{
						soundmanager.playSound("Drag_Finger_SFX", Costanza.soundVolume,	999);
					}
					//Update Particles.		
					if(Drager_Counter <= 0.0)
					{
						var Particle_Name:String = ("Drag_Particle_" + Math.ceil(Math.random() * 5).toString());
						Finger_Drag_Particles.push(new Particle_Loader(sceneRoot, assets.getTexture(Particle_Name), assets.getXml("Finger_Drag_Snow"), 0.8, 28, 0.0, true, new Point((Location.x - Costanza.STAGE_OFFSET), Location.y)));	
						Drager_Counter = 0.4;
					}				
					
					for(var P:int = 0; P < Finger_Drag_Particles.length; P++)
					{
						//sceneRoot.setChildIndex(Particle_Loader(Finger_Drag_Particles[P]), (sceneRoot.getChildIndex(Background) + 1));
						if(!Particle_Loader(Finger_Drag_Particles[P]).isActive())
						{
							var ThisLoader:Particle_Loader = Finger_Drag_Particles[P];
							Finger_Drag_Particles.splice(P, 1);
							ThisLoader.Dispose_Me(sceneRoot);
							P-=1;
						}
					}	

					switch(Game_Mode)
					{
						case "Move_Selection":
						{
							Root.Gaba_Core_Data.Move_Selected_Trophy(Location);							
						}break;

						case "Moving_Item":
						{
							Root.Gaba_Core_Data.Get_Current_Prize().x = (Location.x - (Root.Gaba_Core_Data.Get_Current_Prize().width  / 2)) - Costanza.STAGE_OFFSET;
							Root.Gaba_Core_Data.Get_Current_Prize().y = (Location.y - (Root.Gaba_Core_Data.Get_Current_Prize().height / 2));
						}break;
					}
				}break;
				case "ended":
				{
					Unselect_Tossable(mouseX, mouseY);
					
					//If we dont hit back play Drag sound.
					if(soundmanager.soundIsPlaying("Drag_Finger_SFX"))
					{
						soundmanager.stopSound("Drag_Finger_SFX");
						soundmanager.playSound("Remove_Finger_SFX", Costanza.soundVolume);
					}
					
					switch(Game_Mode)
					{
						case "Move_Selection":
						{
							Root.Gaba_Core_Data.Drop_Selected_Trophy();
							Game_Mode = "Free_Play";
						}break;
						
						case "Moving_Item":
						{
							//Play drop sound.
							switch(Root.Gaba_Core_Data.Get_Current_Prize_Name()){								
								case "Snow_Globe":{soundmanager.playSound(		"Tree_Snowglobe_Placement", 		Costanza.soundVolume);}break;
								case "Snow_Man":{soundmanager.playSound(		"Snowman_Ginger_Drink_Placement", 	Costanza.soundVolume);}break;
								case "Ginger_Bread":{soundmanager.playSound(	"Snowman_Ginger_Drink_Placement", 	Costanza.soundVolume);}break;
								case "Christmas_Tree":{soundmanager.playSound(	"Tree_Snowglobe_Placement", 		Costanza.soundVolume);}break;
								case "Hot_Cocoa":{soundmanager.playSound(		"Snowman_Ginger_Drink_Placement", 	Costanza.soundVolume);}break;
							}

							//Particle placement.
							Placement_Explosion[0].x 			= (Root.Gaba_Core_Data.Get_Current_Prize().x + (Root.Gaba_Core_Data.Get_Current_Prize().width / 2));
							Placement_Explosion[0].y 			= (Root.Gaba_Core_Data.Get_Current_Prize().y + (Root.Gaba_Core_Data.Get_Current_Prize().height / 2));
							Placement_Explosion[0].Activate_Glitter_Box_Particle([	0.0,
																			  	0,
																				0,
																				Root.Gaba_Core_Data.Get_Current_Prize().width,
																				Root.Gaba_Core_Data.Get_Current_Prize().height]);

							//Clean image.
							if(sceneRoot.contains(Root.Gaba_Core_Data.Get_Current_Prize())){sceneRoot.removeChild(Root.Gaba_Core_Data.Get_Current_Prize());}
							Root.Gaba_Core_Data.Drop_Current_Prize();

							switch(Root.Gaba_Core_Data.All_Trophys())
							{
								case true:
								{
									//Get Sizes.
									Root.Gaba_Core_Data.Get_Trophys_Size();

									//Glitter all.
									for(var X:int = 0; X < Placement_Explosion.length; X++){
										Placement_Explosion[X].x 			= (Root.Gaba_Core_Data.Get_Specific_Prize(X).x + (Root.Gaba_Core_Data.Get_Specific_Prize(X).width / 2));
										Placement_Explosion[X].y 			= (Root.Gaba_Core_Data.Get_Specific_Prize(X).y + (Root.Gaba_Core_Data.Get_Specific_Prize(X).height / 2));
										Placement_Explosion[X].Activate_Glitter_Box_Particle([	0.0,
																								0,
																								0,
																								Root.Gaba_Core_Data.Get_Specific_Prize(X).width,
																								Root.Gaba_Core_Data.Get_Specific_Prize(X).height]);
									}
									PlayEndLights();

									//Move to position.
									Root.Gaba_Core_Data.Align_Trophys(6.0);

									//Play sound.
									soundmanager.playSound("TR_All_Collected", 	Costanza.soundVolume);
									soundmanager.playSound("Yogaba_Cheer", 		Costanza.soundVolume);

									//Delay for next prep.
									TweenMax.delayedCall(6.0, Delay_Calls, ["Move_To_Position"]);
									
									for(var x:int = 0; x < Tossables.length;x++)
									{
										TweenMax.to(Tossables[x][0],0.1,{alpha:0,delay:5.5});
									}
									
									TweenMax.to(sleighOL,0.1,{alpha:0,delay:5.5});
									TweenMax.to(sleighUL,0.1,{alpha:0,delay:5.5});
									
									//Item_Spray_Explosion.alignPivot();
									Setup_Burst();
									TweenMax.delayedCall(5.5, function BOOM():void{soundmanager.playSound("EXPLODE",Costanza.soundVolume);Item_Spray_Explosion.Activate_Burst("Begin", Halt_Burst);});

									sceneRoot.setChildIndex(armSprites[5],sceneRoot.numChildren - 1);

									TweenMax.to(armSprites[5],4,{x:1800, delay:7,onComplete:
										function reverseSleigh():void{
											sceneRoot.setChildIndex(armSprites[5], sceneRoot.getChildIndex(iceOverlay) );
											armSprites[5].scaleX = 0.2; armSprites[5].scaleY = 0.2; armSprites[5].y = 260; armSprites[5].x = -200;
											TweenMax.to(armSprites[5],5,{bezier:[{x:100, y:150}, {x:800, y:0}, {x:1600, y:200}]});}});

									//New Update phase.
									Game_Mode = "Move_To_Position";
								}break;

								case false:{
									//Show Back button.
									Back_Button.addEventListener(starling.events.Event.TRIGGERED, goBack);
									TweenMax.killTweensOf(Back_Button_Holder);
									TweenMax.to(Back_Button_Holder, 0.5, {alpha:1});

									//Delay for next prep.
									TweenMax.delayedCall(0.1, Delay_Calls, ["Ok_Item_Dropped"]);
									Game_Mode = "Pergatory";
								}break;
							}
						}break;
					}
				}break;
			}
		}
	}
}