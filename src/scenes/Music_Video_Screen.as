package scenes
{
	
	import com.greensock.TweenMax;
	import com.greensock.events.LoaderEvent;
	import com.greensock.loading.ImageLoader;
	import com.greensock.loading.LoaderMax;
	
	import flash.desktop.NativeApplication;
	import flash.desktop.SystemIdleMode;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.filesystem.File;
	
	import starling.core.Starling;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.extensions.SoundManager;
	import starling.utils.AssetManager;
	
	import utils.SmallVideo;
	
	public class Music_Video_Screen extends starling.display.Sprite
	{
		// Assets
		private var assets:AssetManager;
		private var soundManager:SoundManager;
		private var appDir:File = File.applicationDirectory;
		
		private var sceneRoot:starling.display.Sprite;
		
		private var stageVideoPlayer:SmallVideo;
		
		private var bg:flash.display.Sprite;
		
		private var btn:flash.display.Sprite;
		private var pause:flash.display.Sprite;
		private var play:flash.display.Sprite;
		
		private var btnDown:flash.display.Sprite;
		private var pauseDown:flash.display.Sprite;
		private var playDown:flash.display.Sprite;
		
		private var slider:flash.display.Sprite;
		private var sliderBar:flash.display.Sprite;
		
		private var dragBar:Quad;
		
		private var playing:Boolean = true;
		private var sliderDragging:Boolean = false;
		
		private var imgClass:Class
		
		private var imgPath:String = "textures/"+ Costanza.SCALE_FACTOR + "x/Video/";
		
		private var loader:LoaderMax;
		
		private var videoFile:String;
		
		private var firstContextHandled:Boolean = false;
		
		public function Music_Video_Screen(charString:String="", foodStr:String="")
		{
			videoFile = CONFIG::MARKET == "itunes" ? "Videos/Video_01.flv" : "Videos/Video_01_Android.flv";
			addEventListener(Root.DISPOSE, disposeScene);
			
			addObjects();
		}
		
		private function addObjects():void
		{
			// Create main scene root
			soundManager = SoundManager.getInstance();
			
			NativeApplication.nativeApplication.systemIdleMode = SystemIdleMode.KEEP_AWAKE;
			
			sceneRoot = new starling.display.Sprite();
			addChild(sceneRoot);
			
			//Flash Stuff (display only)
			bg = new flash.display.Sprite();
			bg.x = 0;
			trace(bg.scaleX/Costanza.SCALE_FACTOR);
			bg.scaleX = bg.scaleX/Costanza.SCALE_FACTOR;
			bg.scaleY = bg.scaleY/Costanza.SCALE_FACTOR;
			Starling.current.nativeOverlay.addChild(bg);
			
			//up states
			btn = new flash.display.Sprite();
			btn.x = 280 + Costanza.STAGE_OFFSET;
			btn.y = 600;
			btn.scaleX = btn.scaleX/Costanza.SCALE_FACTOR;
			btn.scaleY = btn.scaleY/Costanza.SCALE_FACTOR;
			Starling.current.nativeOverlay.addChild(btn);
			
			play = new flash.display.Sprite();
			play.x = 550 + Costanza.STAGE_OFFSET;
			play.y = 600;
			play.scaleX = play.scaleX/Costanza.SCALE_FACTOR;
			play.scaleY = play.scaleY/Costanza.SCALE_FACTOR;
			Starling.current.nativeOverlay.addChild(play);
			
			pause = new flash.display.Sprite();
			pause.x = 680 + Costanza.STAGE_OFFSET;
			pause.y = 600;
			pause.scaleX = pause.scaleX/Costanza.SCALE_FACTOR;
			pause.scaleY = pause.scaleY/Costanza.SCALE_FACTOR;
			Starling.current.nativeOverlay.addChild(pause);
			
			//down States
			btnDown = new flash.display.Sprite();
			btnDown.x = 280 + Costanza.STAGE_OFFSET;
			btnDown.y = 600;
			btnDown.scaleX = btnDown.scaleX/Costanza.SCALE_FACTOR;
			btnDown.scaleY = btnDown.scaleY/Costanza.SCALE_FACTOR;
			btnDown.visible = false;
			Starling.current.nativeOverlay.addChild(btnDown);
			
			playDown = new flash.display.Sprite();
			playDown.x = 550 + Costanza.STAGE_OFFSET;
			playDown.y = 600;
			playDown.scaleX = playDown.scaleX/Costanza.SCALE_FACTOR;
			playDown.scaleY = playDown.scaleY/Costanza.SCALE_FACTOR;
			playDown.visible = false;
			Starling.current.nativeOverlay.addChild(playDown);
			
			pauseDown = new flash.display.Sprite();
			pauseDown.x = 680 + Costanza.STAGE_OFFSET;
			pauseDown.y = 600;
			pauseDown.scaleX = pauseDown.scaleX/Costanza.SCALE_FACTOR;
			pauseDown.scaleY = pauseDown.scaleY/Costanza.SCALE_FACTOR;
			pauseDown.visible = false;
			Starling.current.nativeOverlay.addChild(pauseDown);
			
			slider = new flash.display.Sprite();
			slider.x = 1050 + Costanza.STAGE_OFFSET;
			slider.y = 320;
			slider.scaleX = slider.scaleX/Costanza.SCALE_FACTOR;
			slider.scaleY = slider.scaleY/Costanza.SCALE_FACTOR;
			Starling.current.nativeOverlay.addChild(slider);
			
			sliderBar = new flash.display.Sprite();
			sliderBar.x = slider.x;
			sliderBar.y = 0;//slider.y; + slider.height - sliderBar.height;// - (((slider.height-sliderBar.height)*Costanza.voiceVolume));
			sliderBar.scaleX = sliderBar.scaleX/Costanza.SCALE_FACTOR;
			sliderBar.scaleY = sliderBar.scaleY/Costanza.SCALE_FACTOR;
			Starling.current.nativeOverlay.addChild(sliderBar);
			
			loader = new LoaderMax({onComplete:showScene});
			loader.append( new ImageLoader( imgPath + "BG.png", { container: bg, smoothing:true } ) );
			loader.append( new ImageLoader( imgPath + "Back_1.png", { container: btn } ) );
			loader.append( new ImageLoader( imgPath + "Play.png", { container: play } ) );
			loader.append( new ImageLoader( imgPath + "PAUSE.png", { container: pause } ) );
			loader.append( new ImageLoader( imgPath + "Back_2.png", { container: btnDown } ) );
			loader.append( new ImageLoader( imgPath + "PAUSE_Over.png", { container: pauseDown } ) );
			loader.append( new ImageLoader( imgPath + "Play_Over.png", { container: playDown } ) );
			loader.append( new ImageLoader( imgPath + "Videos_VolumeMeter.png", { container: slider } ) );
			loader.append( new ImageLoader( imgPath + "Videos_VolumeMeter_Bar.png", { container: sliderBar } ) );
			loader.load( false );
		}
		
		private function showScene(e:LoaderEvent):void
		{
			sliderBar.y = slider.y + slider.height - sliderBar.height - (((slider.height-sliderBar.height)*Costanza.voiceVolume));
			
			//overlayed invisible starling buttons for touch events
			var PLAY:Quad = new Quad(play.width,play.height);//new Image(Root.assets.getTexture("Play_1"));
			PLAY.x = 550 + Costanza.STAGE_OFFSET;
			PLAY.y = 600;
			addChild(PLAY);
			PLAY.addEventListener(starling.events.TouchEvent.TOUCH,playVid);
			
			var B:Quad = new Quad(play.width,play.height);//new Image(Root.assets.getTexture("Arrow_Left_1"));
			B.x = 280 + Costanza.STAGE_OFFSET;
			B.y = 600;
			addChild(B);
			B.addEventListener(starling.events.TouchEvent.TOUCH,goBack);
			
			var PAUSE:Quad = new Quad(play.width,play.height);//new Image(Root.assets.getTexture("Pause_1"));
			PAUSE.x = 650 + Costanza.STAGE_OFFSET;
			PAUSE.y = 600;
			addChild(PAUSE);
			PAUSE.addEventListener(starling.events.TouchEvent.TOUCH,pauseVid);
			
			dragBar = new Quad(sliderBar.width*4,sliderBar.height*4);//new Image(Root.assets.getTexture("Videos_VolumeMeter_Bar"));
			dragBar.alignPivot();
			dragBar.x = sliderBar.x + sliderBar.width/2;// + sliderBar.width/2;
			dragBar.y = sliderBar.y + sliderBar.width/2;
			addChild(dragBar);
			dragBar.addEventListener(starling.events.TouchEvent.TOUCH,dragVolume);
			
			// try playing a video - we have to make stage3D invisible in order to see the StageVideo below
			Starling.current.stage3D.visible = false;
			
			//create video player
			stageVideoPlayer = new SmallVideo(Starling.current.nativeStage);
			
			// Finished loading assets
			dispatchEventWith(Root.SCENE_LOADED,true);
			
			addEventListener(starling.events.Event.ADDED_TO_STAGE, onAddedToStage);
			
			TweenMax.delayedCall(1,startVid);
			
			Starling.current.nativeStage.addEventListener(flash.events.Event.ACTIVATE, focusGained);
			Starling.current.nativeStage.addEventListener(flash.events.Event.DEACTIVATE, focusLost);
			Starling.current.stage3D.addEventListener( starling.events.Event.CONTEXT3D_CREATE, ContextHandled );
		}
		
		private function ContextHandled( e:flash.events.Event ):void
		{
			trace( "Video.ContextHandled()" );
			if ( !firstContextHandled )
			{
				firstContextHandled = true;
				TweenMax.delayedCall( .15, 
					function():void
					{
						Starling.current.stage3D.visible = true;
						Starling.current.stage3D.visible = false;
						firstContextHandled = false;
					} );
			}
		}
		
		private function focusLost(e:flash.events.Event):void
		{
			trace( "Video.focusLost()" );
			if( playing ) { stageVideoPlayer.pauseVideo(); }
		}
		
		private function focusGained(e:flash.events.Event):void
		{
			trace( "Video.focusGained()" );
			if( playing ) { stageVideoPlayer.resumeVideo(); }
			
			//starling stage restarts when focus is lost and gained.
			//check to see if stage isnt visible, meaning for parents or more apps is visible.
			//If they are, stop the stage again manually, prevents starling buttons from working underneath more apps
			if(!Starling.current.stage3D.visible)
			{
				Starling.current.start();
			}
		}
		
		private function dragVolume(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(stage);
			if(touch == null){return;}
			
			if(touch.phase == TouchPhase.BEGAN)
			{
				sliderDragging = true;
			}
			else if(touch.phase == TouchPhase.MOVED)
			{
				if(sliderDragging)
				{
					dragBar.y = touch.globalY;
					
					if(dragBar.y > (slider.y + slider.height - sliderBar.height))
					{
						dragBar.y = slider.y + slider.height - sliderBar.height;
					}
					else if(dragBar.y < slider.y)
					{
						dragBar.y = slider.y;
					}
					sliderBar.y = dragBar.y;
					//set volume
					Costanza.voiceVolume = ((slider.y - dragBar.y)/(slider.height-sliderBar.height))+1;
					stageVideoPlayer.setVolume();
				}
				
			}
			else if(touch.phase == TouchPhase.ENDED)
			{
				sliderDragging = false;
				//Root.voiceVolume = 
				stageVideoPlayer.setVolume();
				
				Costanza.saveData.data.musicVolume = Costanza.musicVolume;
				//YGG_Music.savedVariablesObject.data.voiceVolume = Costanza.voiceVolume;				
				//YGG_Music.savedVariablesObject.flush();
				
			}
		}
		
		private function goBack(e:starling.events.TouchEvent):void
		{
			if(e.getTouch(stage).phase == TouchPhase.BEGAN) { btnDown.visible = true; }
				else if(e.getTouch(stage).phase == TouchPhase.ENDED) { 
					//soundManager.playSound("Back_Button",Costanza.soundVolume); 
					closeVideo(); }
		}
		
		private function closeVideo():void
		{
			btnDown.visible = false;
			//clears native overlay, clears video and displays starling stage again
			Starling.current.nativeOverlay.removeChild(bg);
			Starling.current.nativeOverlay.removeChild(btn);
			Starling.current.nativeOverlay.removeChild(play);
			Starling.current.nativeOverlay.removeChild(pause);
			Starling.current.nativeOverlay.removeChild(btnDown);
			Starling.current.nativeOverlay.removeChild(playDown);
			Starling.current.nativeOverlay.removeChild(pauseDown);
			Starling.current.nativeOverlay.removeChild(slider);
			Starling.current.nativeOverlay.removeChild(sliderBar);
			
			loader.dispose(true);
			//Starling.current.nativeStage.removeChild(Starling.current.nativeOverlay);
			
			stageVideoPlayer.stopVideo();
			stageVideoPlayer.destroy();
			stageVideoPlayer = null;
			
			Starling.current.stage3D.visible = true;
			//dispatchEventWith(Root.LOAD_LOBBY, true);
			TweenMax.delayedCall(0.0, dispatchEventWith,[Root.LOAD_ROOM, true, {room:1, Character:"Mira"}]);
			
			Starling.current.nativeStage.removeEventListener(flash.events.Event.ACTIVATE, focusGained);
			Starling.current.nativeStage.removeEventListener(flash.events.Event.DEACTIVATE, focusLost);
			Starling.current.stage3D.removeEventListener( starling.events.Event.CONTEXT3D_CREATE, ContextHandled );
		}
		
		//pause/play
		private function pauseVid(e:TouchEvent):void
		{
			if(e.getTouch(stage).phase == TouchPhase.BEGAN)
			{
				pauseDown.visible = true;
				if(playing){stageVideoPlayer.pauseVideo(); playing = false;}
				else if(!playing){stageVideoPlayer.resumeVideo(); playing = true;}
			}
			else if(e.getTouch(stage).phase == TouchPhase.ENDED)
			{
				pauseDown.visible = false;
			}
		}
		
		private function startVid():void
		{
			stageVideoPlayer.playVideo(videoFile);
			playing = true;
		}
		
		//restart vid
		private function playVid(e:TouchEvent):void
		{	
			if(e.getTouch(stage).phase == TouchPhase.BEGAN)
			{
				playDown.visible = true;
				stageVideoPlayer.playVideo(videoFile);
				playing = true;
			}
			else if(e.getTouch(stage).phase == TouchPhase.ENDED)
			{
				playDown.visible = false;
			}
		}
		
		private function onAddedToStage(event:starling.events.Event):void
		{
			// Finished loading assets
			dispatchEventWith(Root.SCENE_LOADED, true);
		}
		
		private function disposeScene():void
		{
			NativeApplication.nativeApplication.systemIdleMode = SystemIdleMode.NORMAL;
			//soundManager.removeSound("WidgetTheme");
			sceneRoot.removeChildren(0,sceneRoot.numChildren-1,true);
		}
		
	}
}

