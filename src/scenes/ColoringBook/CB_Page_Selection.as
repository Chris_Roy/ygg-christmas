package scenes.ColoringBook{
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.geom.Point;
	
	import scenes.ColoringBook.ColoringInterface.CB_Text_Maker;
	import scenes.ColoringBook.ColoringInterface.InterfaceButton;
	
	public class CB_Page_Selection extends Sprite{
		
		//Props.
		private var isActive:Boolean;
		private var Scrolling:Boolean;
		private var CurrentSelectedItem:int;
		private var First_X:int;
		private var Last_X:int;
		
		private var BackgroundPlate:flash.display.Sprite;		
		private var Page_Selection_Array:Array;//all are page selections.
		private var Page_Selection_Border:flash.display.Sprite;//Frame around the page.
		private var Page_Name:flash.display.Sprite;//Name plate.		
		private var Pages_Holder:flash.display.Sprite;//Holds the selections.	
		
		//Buttons.
		private var Home:InterfaceButton;			
		private var Close:InterfaceButton;
		
		//Text
		private var Make_Text:CB_Text_Maker;
		//private var Home_Text_Image:Bitmap;
		//private var Back_Text_Image:Bitmap;
		
		private var Selectable_Page_Icons:Page_Icon_Manager;
		
		public function CB_Page_Selection(Core_Assets:External_PNG_Sheet_Manager, Page:Page_Icon_Manager, Prefix:String){
			
			super();	
			
			isActive 					= true;
			Scrolling					= false;
			Selectable_Page_Icons 		= Page;
			Pages_Holder 				= new flash.display.Sprite();
			CurrentSelectedItem = 0;
			
			BackgroundPlate 			= Core_Assets.Get_Image("PageSelBG");
			Page_Selection_Border 		= Core_Assets.Get_Image("PageSelFrame");
			Page_Name					= Core_Assets.Get_Image("PagesName");	
			
			Home 						= new InterfaceButton(Core_Assets.Get_Image("PS_Home_Static"), Core_Assets.Get_Image("PS_Home_Glow"));
			Close 						= new InterfaceButton(Core_Assets.Get_Image("PS_Close_Static"), Core_Assets.Get_Image("PS_Close_Glow"));
			
			Make_Text 					= new CB_Text_Maker();
			//Home_Text_Image 			= Make_Text.Create_a_Word("Home",  0xfaa118, "Tastic", 30, 0xffffff);
			//Back_Text_Image 			= Make_Text.Create_a_Word("Close", 0xfaa118, "Tastic", 30, 0xffffff);			
			
			Home.x = ((261 * Costanza.SCALE_FACTOR) + (Home.width / 2));
			Home.y = (110 * Costanza.SCALE_FACTOR);
			//Home_Text_Image.x = (Home.x - (Home_Text_Image.width / 2)) - (3 * Costanza.SCALE_FACTOR);
			//Home_Text_Image.y = (Home.y + (20 * Costanza.SCALE_FACTOR));
			//Home_Text_Image.smoothing = true;			
			
			Close.x = ((1366 * Costanza.SCALE_FACTOR) - ((271 * Costanza.SCALE_FACTOR) + (Close.width / 2)));
			Close.y = (110 * Costanza.SCALE_FACTOR);
			//Back_Text_Image.x = (Close.x - (Back_Text_Image.width / 2)) - (3 * Costanza.SCALE_FACTOR);
			//Back_Text_Image.y = (Close.y + (20 * Costanza.SCALE_FACTOR));
			//Back_Text_Image.smoothing = true;
			
			Page_Selection_Array		= new Array();
			var MWC:int 				= (Selectable_Page_Icons.getCount() / 2);
			
			for(var X:int = 0; X < Selectable_Page_Icons.getCount(); X++){
				var Y:int = 0;
				if(X >= MWC){Y = 1;}
				var SS:flash.display.Sprite = Selectable_Page_Icons.getIcon(X);
				Page_Selection_Array.push(SS);
				SS.x = (((X * SS.width) + (X * 32)) - (Y * ((MWC * SS.width) + (MWC * 32))));
				SS.y = (Y * (250 * Costanza.SCALE_FACTOR));
				Pages_Holder.addChild(SS);				
			}
			
			Page_Name.x 			= (((1366 * Costanza.SCALE_FACTOR) / 2) - (Page_Name.width / 2));
			Page_Name.y 			= ((135 * Costanza.SCALE_FACTOR) - (Page_Name.height / 2));			
			
			Pages_Holder.addChild(Page_Selection_Border);
			
			this.addChild(BackgroundPlate);
			this.addChild(Page_Name);
			this.addChild(Pages_Holder);
			this.addChild(Home);
			this.addChild(Close);
			//this.addChild(Home_Text_Image);
			//this.addChild(Back_Text_Image);
			
			Pages_Holder.x 	= (186 * Costanza.SCALE_FACTOR);
			Pages_Holder.y 	= (260 * Costanza.SCALE_FACTOR);
			
			//var Maximum:int = ((171 * Costanza.SCALE_FACTOR) + (20 * Costanza.SCALE_FACTOR)); //use to be 370.			
			//Pages_Holder.x = (Maximum - Page_Selection_Array[CurrentSelectedItem].x);
			//var Limit:int = ((-Pages_Holder.width) + (1366 - Maximum));				
			//if(Pages_Holder.x < Limit){Pages_Holder.x = Limit;}
			//if(Pages_Holder.x > Maximum){Pages_Holder.x = Maximum;}
			
			Update_Bar();
			isActive 		= false;
			
		}
		public function Dispose_Bar():void{
			
			this.removeChild(BackgroundPlate);
			this.removeChild(Page_Name);
			this.removeChild(Pages_Holder);
			this.removeChild(Home);
			this.removeChild(Close);
			//this.removeChild(Home_Text_Image);
			//this.removeChild(Back_Text_Image);
			
			Pages_Holder.removeChild(Page_Selection_Border);
			
			//Home_Text_Image.bitmapData.dispose();
			//Home_Text_Image = null;
			
			//Back_Text_Image.bitmapData.dispose();
			//Back_Text_Image = null;
			
			Bitmap(BackgroundPlate.getChildAt(0)).bitmapData.dispose();
			BackgroundPlate.removeChildAt(0);
			BackgroundPlate = null;
			
			Bitmap(Page_Name.getChildAt(0)).bitmapData.dispose();
			Page_Name.removeChildAt(0);
			Page_Name = null;
			
			Bitmap(Page_Selection_Border.getChildAt(0)).bitmapData.dispose();
			Page_Selection_Border.removeChildAt(0);
			Page_Selection_Border = null;
			
			Home.Destroy();
			Close.Destroy();
			
			Selectable_Page_Icons = null;
			
		}		
		
		public function Click_Bar(Position:Point):int{	
			
			if(!isActive){return -1;}
			
			//Make sure we hit mask first.
			//if(this.globalToLocal(Position).x < 0){return -1;}
			//if(this.globalToLocal(Position).x > Pages_Holder.width){return -1;}
			
			for(var X:int = 0; X < Page_Selection_Array.length; X++){
				if(Page_Selection_Array[X].hitTestPoint(Position.x, Position.y, true)){
					Scrolling = true;
					First_X = this.globalToLocal(Position).x;								
					return X;
				}
			}
			
			Home.Down(Position);
			Close.Down(Position);
			
			if(BackgroundPlate.hitTestPoint(Position.x, Position.y, true)){
				First_X = 9000;	
				return 1;
			}
			
			return -1;
			
		}
		public function Up_Bar(Position:Point):String{	
			
			if(!isActive){return "Nothing";}	
			
			Scrolling 			= false;
			//CurrentSelectedItem = -1;
			
			if(Home.Up(Position)){
				Home.Reset();
				return "Home";
			}
			
			if(Close.Up(Position)){
				Close.Reset();
				return "Close";
			}
			
			for(var X:int = 0; X < Page_Selection_Array.length; X++){
				if(Page_Selection_Array[X].hitTestPoint(Position.x, Position.y, true)){
					Last_X = this.globalToLocal(Position).x;
					var distance:int = (Last_X - First_X);
					if(distance <= 0){distance = -distance;}
					if(distance < 15){
						CurrentSelectedItem = X;
						Update_Bar();
						return "Loading_Page";
					}else{return "Nothing";}
				}
			}
			
			return "Nothing";
			
		}
		public function Update_Bar():void{				
			
			if(!isActive){return;}
			
			//364.
			//var Maximum:int = Costanza.StageProportionScale; //use to be 370.
			//var Maximum:int = Costanza.VIEWPORT_WIDTH;
			
			//Pages_Holder.x = (Maximum - Page_Selection_Array[CurrentSelectedItem].x);
			//var Limit:int = ((-Pages_Holder.width) + ((1366 * Costanza.SCALE_FACTOR) - Maximum));			
			
			//if(Pages_Holder.x < Limit){Pages_Holder.x = Limit;}
			//if(Pages_Holder.x > Maximum){Pages_Holder.x = Maximum;}
			
			Page_Selection_Border.x = ((Page_Selection_Array[CurrentSelectedItem].x + (Page_Selection_Array[CurrentSelectedItem].width / 2)) - (Page_Selection_Border.width / 2));
			Page_Selection_Border.y = ((Page_Selection_Array[CurrentSelectedItem].y + (Page_Selection_Array[CurrentSelectedItem].height / 2)) - (Page_Selection_Border.height / 2));			
			
		}
		public function Move_Bar(Force:int):void{	
			
			return;
			if(!isActive){return;}
			if(!Scrolling){return;}				
			
			//Move.
			Pages_Holder.x = (Pages_Holder.x + (Force * (1 * Costanza.SCALE_FACTOR)));			
			
			var Maximum:int = Costanza.StageProportionScale;//use to be 370.
			var Limit:int = ((-Pages_Holder.width) + ((1366 * Costanza.SCALE_FACTOR) - Maximum));			
			
			if(Pages_Holder.x < Limit){Pages_Holder.x = Limit;}
			if(Pages_Holder.x > Maximum){Pages_Holder.x = Maximum;}		
			
		}		
		public function Get_Selection():int{
			return CurrentSelectedItem;
		}
		public function Get_Active():Boolean{
			return isActive;
		}
		public function Deactivate_Bar():void{	
			if(!isActive){return;}
			//Master_Holder.visible = false;
			isActive = false;			
		}
		public function Activate_Bar():void{	
			if(isActive){return;}
			//Master_Holder.visible = true;
			isActive = true;			
		}
	}
}
import flash.display.Bitmap;
import flash.display.Sprite;
import flash.geom.Point;

import scenes.ColoringBook.External_PNG_Sheet_Manager;
import scenes.ColoringBook.ColoringInterface.CB_Text_Maker;
import scenes.ColoringBook.ColoringInterface.InterfaceButton;

