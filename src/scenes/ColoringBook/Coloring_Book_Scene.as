package scenes.ColoringBook{
	
	import com.cupcake.DeviceInfo;
	import com.greensock.TweenMax;
	
	import flash.desktop.NativeApplication;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.display.StageQuality;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TouchEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.filters.BitmapFilterQuality;
	import flash.filters.GlowFilter;
	import flash.geom.ColorTransform;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.media.CameraRoll;
	import flash.system.Capabilities;
	import flash.system.System;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	import flash.utils.ByteArray;
	
	import scenes.ColoringBook.ColoringInterface.CB_Interface_HUD;
	import scenes.ColoringBook.ColoringInterface.CB_Loading_Screen;
	import scenes.ColoringBook.ColoringInterface.CB_Zoom_Selection;
	
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.events.EnterFrameEvent;
	import starling.extensions.SoundManager;
	import starling.utils.AssetManager;
	
	import utils.PNGEncoder;
	
	public class Coloring_Book_Scene extends starling.display.Sprite{
		
		//For Loading Sounds.		
		private var assets:AssetManager;
		private var soundManager:SoundManager;
		private var assetsLoaded:Boolean = false;
		private var appDir:File = File.applicationDirectory;
		
		//For saving images to cam.
		private var camSave:Boolean;	
		private var camRoll:CameraRoll;
		
		//Are Scene Holder.
		private var Scene_Holder:flash.display.Sprite;
		private var Scene_Scale:Number;
		
		//Available colors.
		private var Glitter_Restricted_Color_Uint:uint		 	= 0xffcccccc; //Used for glitter gird. (this color is what is used to differenciate glitter patern)
		private var Glitter_Restricted_Color_String:String 		= "ffcccccc"; //					   (Never use a color that is available to the user!!!!!!!!!!!)
		private var Drawing_Colors:Vector.<uint> = new 	<uint>[ 0xffffff,
																0xb3b1a3,
																0x000215,
																0x5b2f10,
																0xad6c29,
																0xfdcfa3,
																0xffad94,
																0xed1940,
																0xf5851f,
																0xffb955,
																0xffec6c,
																0xfebae3,
																0xe969a4,
																0xed2865,
																0xd70a8b,
																0xe444bd,
																0xbf99d4,
																0xa154a1,
																0x86298e,
																0x5e7bbd,
																0x809cd1,
																0xa8def2,
																0x33c1e5,
																0x009ac7,
																0x004684,
																0x22806c,
																0x77c9bc,
																0x98d966,
																0x4fb74a,
																0x2e9c4f ];	
			
		//Position Saving.
		private var Last_Mouse_Position:Point;
		private var Current_Mouse_Position:Point;		
		
		//Props.
		private var Page_Pack:String;
		private var TouchArray:Array;	
		private var Full_App_Width:int;
		private var Full_App_Height:int;		
		private var Update_Phase:String;
		private var PixColo:BitmapData;		
		private var Sticker_Counter:int;
		
		//Art Assets.
		private var Core_Assets:External_PNG_Sheet_Manager;
		private var Sticker_Assets:External_PNG_Sheet_Manager;
		private var Pages_Icons_Assets:Page_Icon_Manager;
		private var Scale_Definition_Assets:External_PNG_Sheet_Manager;
		
		//Multytouch Values.
		private var Squeeze_Last:Number;
		private var Squeeze_Current:Number;
		private var Rotation_Last:Number;
		
		//Page Props.
		private var Current_Coloring_Page:int;
		private var Next_Coloring_Page:int;		
		private var Selected_Swatch:int;
		private var Selected_Color:int;
		private var Selected_Alpha:Number;
		private var Selected_Scale:Number;
		private var Selected_Layer:int;
		private var Swatchs_Holder:Array;
		private var Swatchs_Draw_Holder:Array;
		
		//Draw Holder.
		private var Drawing_Canvas_Holder:flash.display.Sprite;	
		private var Drawing_Canvas_Bitmap:Bitmap;
		private var Drawing_Canvas_Sticker:flash.display.Sprite;
		private var Drawing_Canvas_Layers:Array;	
		private var Drawing_Undos:Array;
		private var Stickers:Array;		
		
		//Canvas layers.	
		private var Canvas:Coloring_Canvas;	
		
		//Layer of Draw.		
		private var Draw_Layer_X:int;
		private var Draw_Layer_Y:int;	
		
		//Layers need for swatching draw.
		private var Section_Draw_Sprite:flash.display.Sprite;		
		private var Section_Draw_Mask_Sprite:flash.display.Sprite;	
		private var Section_Draw_Swatch_Sprite:flash.display.Sprite;			
		private var Section_Draw_Mask_Bitmap:Bitmap;
		private var Section_Draw_Final:Bitmap;
		
		//Glitter Particle.		
		private var GlitCounter:int;
		private var GiltGo:Boolean;
		private var Glitter_Grid:BitmapData;
		
		//Hud.
		private var Interface_Hud:CB_Interface_HUD;
		private var Page_Selection_Hud:CB_Page_Selection;
		private var ZoomSelectionScreen:CB_Zoom_Selection;
		
		//Loading Screen.
		private var sLoading:CB_Loading_Screen;
		
		//Pops.		
		private var num:int = 0;		
		private var First_Time:Boolean;
		private var First_Down:String;
		private var Art_Extention:String;		
		private var Final_Dispatch:String;
		private var Max_Undos:int;
		
		public function Coloring_Book_Scene(PagePack:String){
			
			super();	
			
			//DeviceInfo.isSlow = true;
			
			NativeApplication.nativeApplication.addEventListener(flash.events.Event.ACTIVATE,   onAppActivate);
			NativeApplication.nativeApplication.addEventListener(flash.events.Event.DEACTIVATE, onAppDeactivate); 			
			
			this.x 					= Costanza.STAGE_OFFSET;			
			Final_Dispatch			= "Main";
			Art_Extention			= ("x" + Costanza.SCALE_FACTOR.toString() + "/");
			First_Down				= "Nothing";
			First_Time				= true;
			if(DeviceInfo.isSlow){Max_Undos = 1;}else{Max_Undos = 4;}
			Sticker_Counter         = 0;
			Page_Pack 				= "Christmas";					
			Full_App_Width 			= (1366 * Costanza.SCALE_FACTOR);
			Full_App_Height 		= (768 * Costanza.SCALE_FACTOR);
			Multitouch.inputMode 	= MultitouchInputMode.TOUCH_POINT;
			TouchArray 				= new Array();			
			PixColo 				= new BitmapData(1, 1, true, 0xffffff);				
			
			//Setup the scene.
			Scene_Holder 	= new flash.display.Sprite();			
			
			//Setup aspect.
			var scaleX:Number = (Costanza.VIEWPORT_WIDTH / Full_App_Width);
			var scaleY:Number = (Costanza.VIEWPORT_HEIGHT / Full_App_Height);			
			if(scaleY > scaleX){Scene_Scale = scaleY;}				
			else{Scene_Scale = scaleX;}			
			
			Scene_Holder.scaleX 	= Scene_Holder.scaleY = Scene_Scale;			
			Scene_Holder.x 			= -(((Full_App_Width * Scene_Scale) - Costanza.VIEWPORT_WIDTH) / 2);
			
			Costanza.StageProportionScale = (((Costanza.STAGE_WIDTH * Scene_Scale) - Costanza.VIEWPORT_WIDTH) / 2) + (Costanza.STAGE_OFFSET / 2);
			if(Costanza.StageProportionScale < 0){Costanza.StageProportionScale = -Costanza.StageProportionScale;}	
			
			if(Costanza.IPAD_RETINA){
				Costanza.StageProportionScale = (171 * 2);
			}
			
			Starling.current.nativeStage.addChild(Scene_Holder);
			
			sLoading = new CB_Loading_Screen();
			Scene_Holder.addChild(sLoading);
			
			assets 					= new AssetManager();
			Root.currentAssetsProxy = assets;
			assets.verbose 			= Capabilities.isDebugger;					
			assets.enqueue(appDir.resolvePath("audio/ColoringSFX"));
			assets.loadQueue(function onProgress(ratio:Number):void{
				if (ratio == 1){
					Starling.juggler.delayCall(function():void{
						// now would be a good time for a clean-up 
						System.pauseForGCIfCollectionImminent(0);
						System.gc();						
					}, 0.15);
					FirstPass();
					assetsLoaded = true;
				};
				
			});		
			
		}	
		private function onAppActivate(e:flash.events.Event):void{
			trace("----------------------------SCENE ACTIVATED-----------------------------");
			if(!assetsLoaded){
				assets.enqueue(appDir.resolvePath("audio/ColoringSFX"));
				assets.loadQueue(function onProgress(ratio:Number):void{
					if (ratio == 1){
						Starling.juggler.delayCall(function():void{
							// now would be a good time for a clean-up 
							System.pauseForGCIfCollectionImminent(0);
							System.gc();						
						}, 0.15);
						FirstPass();
						assetsLoaded = true;
					};
				});
			}
		}
		
		private function onAppDeactivate(e:flash.events.Event):void{
			trace("----------------------------SCENE DEACTIVATED-----------------------------");
			if(!assetsLoaded){
				assets.purge();
			}
		}		
		private function FirstPass():void{
			
			NativeApplication.nativeApplication.removeEventListener(flash.events.Event.ACTIVATE,   onAppActivate);
			NativeApplication.nativeApplication.removeEventListener(flash.events.Event.DEACTIVATE, onAppDeactivate);
			
			//sound Setup.
			soundManager = SoundManager.getInstance();		
			soundManager.addSound("SFX_Take_Picture", 	assets.getSound("Take_Picture_SFX"));			
			soundManager.addSound("Slide_Bar", 			assets.getSound("Slide_SFX"));
			soundManager.addSound("Click_Mouse", 		assets.getSound("Selection_SFX"));
			
			soundManager.addSound("SFX_Brush_Button", 	assets.getSound("brush button"));
			soundManager.addSound("SFX_Bucket_Button", 	assets.getSound("bucket button"));
			soundManager.addSound("SFX_Chalk_Button", 	assets.getSound("chalk button"));
			soundManager.addSound("SFX_Crayon_Button", 	assets.getSound("crayon button"));
			soundManager.addSound("SFX_Glitter_Button", 	assets.getSound("glitter button"));
			soundManager.addSound("SFX_Home_Button", 	assets.getSound("home button"));
			soundManager.addSound("SFX_Pages_Button", 	assets.getSound("pages button"));
			soundManager.addSound("SFX_Sticker_Button", 	assets.getSound("stickers button"));
			
			soundManager.addSound("SFX_Color_Pick_Button", 	assets.getSound("select individual crayons"));
			soundManager.addSound("SFX_More_Button", 	assets.getSound("more button down"));
			soundManager.addSound("SFX_Zin_Button", 	assets.getSound("zoom in button"));
			soundManager.addSound("SFX_Zout_Button", 	assets.getSound("zoom out button"));
			soundManager.addSound("SFX_Select_Sticker", 	assets.getSound("select sticker"));
			soundManager.addSound("SFX_Select_Page", 	assets.getSound("select individual pages"));			
			
			soundManager.addSound("SFX_Draw_Brush", 	assets.getSound("painting sfx"));
			soundManager.addSound("SFX_Draw_Bucket", 	assets.getSound("bucket paint sfx"));
			soundManager.addSound("SFX_Draw_Crayon", 	assets.getSound("crayon sfx"));
			soundManager.addSound("SFX_Draw_Chalk", 	assets.getSound("chalk sfx"));
			soundManager.addSound("SFX_Draw_Spray", 	assets.getSound("spray sfx"));
			soundManager.addSound("SFX_Draw_Eraser", 	assets.getSound("eraser sfx"));
			
			soundManager.stopAllSounds();
			//soundManager.playSound(Page_Pack, Costanza.musicVolume,999);				
			
			//Setup Native Display.
			addEventListener(Root.DISPOSE,  disposeScene);
			//Starling.current.stage3D.visible = false;
			Starling.current.stop();			
			Starling.current.nativeStage.addEventListener(flash.events.Event.ACTIVATE, ReFocus);
			Starling.current.nativeStage.addEventListener(flash.events.Event.ENTER_FRAME, Prep_Loader);	
			
			//Finished loading assets
			dispatchEventWith(Root.SCENE_LOADED, true);			
			
		}
		private function Prep_Loader(e:flash.events.Event):void{
			trace("Enter frame ok");
			Starling.current.nativeStage.removeEventListener(flash.events.Event.ENTER_FRAME, Prep_Loader);
			Load_Sticker_Assets();//Loadup core assets for the scene.	
		}
		private function Load_Sticker_Assets():void{
			Sticker_Assets 	= new External_PNG_Sheet_Manager("textures/ColoringBookTexture/" + Art_Extention, Load_Core_Assets);			
			Sticker_Assets.Append_Assets("Sticker_Big", ".png");			
			Sticker_Assets.Load_Assets();
		}		
		private function Load_Core_Assets():void{
			Core_Assets = new External_PNG_Sheet_Manager("textures/ColoringBookTexture/" + Art_Extention, Load_Scaling_Assets);
			Core_Assets.Append_Assets("CB_Coloring_Interface_Elements", ".png");		
			Core_Assets.Append_Assets("Page_Selection_Hud_Items", 		".png");			
			Core_Assets.Append_Assets(("Page_Selection_" + Page_Pack), 	".png");	
			Core_Assets.Append_Assets("Sticker_Small", 					".png");			
			Core_Assets.Load_Assets();
		}
		
		private function Load_Scaling_Assets():void{
			if(!DeviceInfo.isSlow){	
				Scale_Definition_Assets = new External_PNG_Sheet_Manager("textures/ColoringBookTexture/" + Art_Extention, Process_Pages_Icons);
				Scale_Definition_Assets.Append_Assets("Scale_Defines",	".png");
				Scale_Definition_Assets.Load_Assets();
			}else{Process_Pages_Icons();}
		}		
		
		private function Process_Pages_Icons():void{
			Pages_Icons_Assets = new Page_Icon_Manager(Core_Assets_Loaded);
			Pages_Icons_Assets.Prep_Page_Icon_Manager(Core_Assets, Page_Pack);
		}
		private function Core_Assets_Loaded():void{
			
			//Update Mode.
			Update_Phase 			= "Inactive";
			
			//Setup positions.
			Last_Mouse_Position 	= new Point(0, 0);
			Current_Mouse_Position 	= new Point(0, 0);	
			
			//Camroll.
			camSave 				= false;
			camRoll 				= new CameraRoll();
			if((camRoll != null) && CameraRoll.supportsAddBitmapData){camSave = true;}				
			
			ZoomSelectionScreen = new CB_Zoom_Selection(Core_Assets);
			Initialize_Glitter_Particles();	
			Initialize_Coloring_Page();
			Initialize_Draw_Section();
			
			Interface_Hud = new CB_Interface_HUD(Core_Assets, Drawing_Colors, Scale_Definition_Assets);
			Scene_Holder.addChildAt(Interface_Hud, 0);
			
			Page_Selection_Hud = new CB_Page_Selection(Core_Assets, Pages_Icons_Assets, Page_Pack);
			
			Initialize_Swatches();			
			Core_Assets.Purge_Assets();		
			
			//Next_Coloring_Page = Math.ceil(Math.random() * 8);
			Load_Canvas_From_File();			
			
			//trace("We Are Clear");
			Starling.current.nativeStage.addEventListener(flash.events.MouseEvent.MOUSE_DOWN, TouchDown);
			Starling.current.nativeStage.addEventListener(flash.events.Event.ENTER_FRAME,  Update_Scene);
			//Starling.current.nativeStage.addEventListener(flash.events.MouseEvent.MOUSE_MOVE, mMove);
			Starling.current.nativeStage.addEventListener(flash.events.MouseEvent.MOUSE_UP, TouchRelease);
			Starling.current.nativeStage.addEventListener(TouchEvent.TOUCH_MOVE, MultyTapperMoved);
			Starling.current.nativeStage.addEventListener(TouchEvent.TOUCH_BEGIN, MultyTapperBegin);
			Starling.current.nativeStage.addEventListener(TouchEvent.TOUCH_END, MultyTapperEnded);			
			
			//var mem:String = Number( System.totalMemory / 1024 / 1024 ).toFixed( 2 ) + "Mb";
			//trace( mem + " Current Mem");
			
		}
		private function ExitGame():void{			
			
			//NativeApplication.nativeApplication.systemIdleMode = SystemIdleMode.NORMAL;	
			
			//Remove Listeners.			
			Starling.current.nativeStage.removeEventListener(flash.events.MouseEvent.MOUSE_DOWN, TouchDown);
			Starling.current.nativeStage.removeEventListener(flash.events.Event.ENTER_FRAME,  Update_Scene);
			//Starling.current.nativeStage.removeEventListener(flash.events.MouseEvent.MOUSE_MOVE, mMove);
			Starling.current.nativeStage.removeEventListener(flash.events.MouseEvent.MOUSE_UP, TouchRelease);
			Starling.current.nativeStage.removeEventListener(TouchEvent.TOUCH_MOVE, MultyTapperMoved);
			Starling.current.nativeStage.removeEventListener(TouchEvent.TOUCH_BEGIN, MultyTapperBegin);
			Starling.current.nativeStage.removeEventListener(TouchEvent.TOUCH_END, MultyTapperEnded);
			Starling.current.nativeStage.removeEventListener(flash.events.Event.ACTIVATE, ReFocus);	
			//NativeApplication.nativeApplication.removeEventListener(Event.DEACTIVATE, CloseApp);	
			
			//Remove scene holder from native stage.
			Starling.current.nativeStage.removeChild(Scene_Holder);			
			
			assets.purge();
			
			Scene_Holder.removeChild(Interface_Hud);
			Interface_Hud.Destroy();
			
			if(ZoomSelectionScreen.Get_Active()){
				Scene_Holder.removeChild(ZoomSelectionScreen);
			}
			ZoomSelectionScreen.Destroy();
			
			if(Page_Selection_Hud.Get_Active()){
				Scene_Holder.removeChild(Page_Selection_Hud);
			}
			Page_Selection_Hud.Dispose_Bar();
			
			//Pages_Icons_Assets.
			Pages_Icons_Assets.Purge_Assets();
			Pages_Icons_Assets.Kill_Me();
			
			//Destroy swatches.
			Dispose_Swatches();
			Dispose_Draw_Section();
			
			//Clear Page.
			Clear_Coloring_Page();		
			
			//Destroy the render state canvas.
			Drawing_Canvas_Bitmap.bitmapData.dispose();
			Drawing_Canvas_Bitmap = null;
			
			PixColo.dispose();
			Clear_Glitter_Grid();
			
			//Clear Asset Holders.
			Core_Assets.Purge_Assets();	
			Core_Assets.Kill_Me();
			Sticker_Assets.Purge_Assets();
			Sticker_Assets.Kill_Me();
			
			if(Scale_Definition_Assets != null){
				Scale_Definition_Assets.Purge_Assets();
				Scale_Definition_Assets.Kill_Me();
			}
			
			//Clear Sounds.
			soundManager.stopAllSounds();
			
			Scene_Holder.removeChild(sLoading);
			sLoading.Destroy();
			
			Starling.current.stage3D.visible = true;
			Starling.current.start();
			
			this.addEventListener(flash.events.Event.ENTER_FRAME,  CloseFinal);
						
		}
		private function CloseFinal(e:starling.events.EnterFrameEvent):void{
			
			this.removeEventListener(flash.events.Event.ENTER_FRAME,  CloseFinal);			
			
			switch(Final_Dispatch){
				
				case "Main":{
					//dispatchEventWith(Root.LOAD_LOBBY, true);
					TweenMax.delayedCall(0.1, dispatchEventWith,[Root.LOAD_ROOM, true, {room:1, Character:"Mira"}]);	
				}break;
				case "BBF":{					
					dispatchEventWith("Load_BBF", true, "BBF");	
				}break;
				case "Ballerina":{				
					dispatchEventWith("Load_Ballerina", true, "Ballerina");	
				}break;
				
				case "Pets":{				
					dispatchEventWith("Load_Pets", true, "Pets");
				}break;
				
				case "Princess":{				
					dispatchEventWith("Load_Princess", true, "Princess");
				}break;
				
				case "Winter":{				
					dispatchEventWith("Load_Winter", true, "Winter");
				}break;				
				
			}
			
		}
		private function disposeScene():void{
			
		}
		private function ReFocus(e:flash.events.Event):void{
			if(!Starling.current.stage3D.visible){
				Starling.current.stop();
			}
		}
		private function CloseApp():void{
			Save_To_File();
		}	
		
		
		private function Update_Mouse():void{
			
			Last_Mouse_Position.x 	= Current_Mouse_Position.x;
			Last_Mouse_Position.y 	= Current_Mouse_Position.y;
			
			Current_Mouse_Position.x = Starling.current.nativeStage.mouseX;
			Current_Mouse_Position.y = Starling.current.nativeStage.mouseY;	
			
		}	
		
		private function TouchDown(e:flash.events.MouseEvent):void{			
			
			//Always rest first down.
			First_Down = "Nothing";
			Update_Mouse();//Update movements.			
			
			switch(Update_Phase){
				
				case "Awayting_Interface_Input":{
					
					var Interface_Value:String = Interface_Hud.Down(Current_Mouse_Position);			
					trace(Interface_Value + " Current Interface Action");
					
					if(Interface_Hud.Is_Yes_NO()){
						
						switch(Interface_Value){
							
							//Clear All Box.
							case "Yes":{
								soundManager.playSound("Click_Mouse", Costanza.soundVolume);
								//Disposing_Undos();
								//Disposing_Stickers();
								//Clear_Glitter_Grid();
								//Drawing_Canvas_Bitmap.bitmapData.fillRect(Drawing_Canvas_Bitmap.bitmapData.rect, 0xffffff);
								return;
							}break;
							case "No":{
								soundManager.playSound("Click_Mouse", Costanza.soundVolume);
								return;
							}break;
							
						}
						
						return;
						
					}				
					
					switch(Interface_Value){
						
						//Main Options.
						case "Home":{
							First_Down = "Home";
							soundManager.playSound("SFX_Home_Button", Costanza.musicVolume);
							return;
						}break;
						case "Page":{
							First_Down = "Page";
							soundManager.playSound("SFX_Pages_Button", Costanza.musicVolume);	
							return;
						}break;
						
						//Color Scaling values.
						case "Scale_1":{
							Selected_Scale = 0.5;
							Set_Swatchs();
							soundManager.playSound("SFX_Brush_Button", Costanza.musicVolume);	
							return;
						}break;
						case "Scale_2":{
							Selected_Scale = 1.0;
							Set_Swatchs();
							soundManager.playSound("SFX_Brush_Button", Costanza.musicVolume);	
							return;
						}break;
						case "Scale_3":{
							Selected_Scale = 1.5;
							Set_Swatchs();
							soundManager.playSound("SFX_Brush_Button", Costanza.musicVolume);	
							return;
						}break;
						
						//Tools Selection.
						case "Brush":{
							Selected_Swatch = 0;
							Selected_Color 	= Interface_Hud.Get_Selected_Color();
							Set_Swatchs();
							soundManager.playSound("SFX_Brush_Button", Costanza.musicVolume);	
							return;
						}break;
						case "Bucket":{
							Selected_Swatch = 4;
							Selected_Color 	= Interface_Hud.Get_Selected_Color();
							Set_Swatchs();
							soundManager.playSound("SFX_Bucket_Button", Costanza.musicVolume);	
							return;
						}break;
						case "Crayon":{
							Selected_Swatch = 1;
							Selected_Color 	= Interface_Hud.Get_Selected_Color();
							Set_Swatchs();
							soundManager.playSound("SFX_Crayon_Button", Costanza.musicVolume);	
							return;
						}break;
						case "Spray":{
							Selected_Swatch = 2;
							Selected_Color 	= Interface_Hud.Get_Selected_Color();
							Set_Swatchs();
							soundManager.playSound("SFX_Glitter_Button", Costanza.musicVolume);	
							return;
						}break;
						case "Chalk":{
							Selected_Swatch = 3;
							Selected_Color 	= Interface_Hud.Get_Selected_Color();
							Set_Swatchs();
							soundManager.playSound("SFX_Chalk_Button", Costanza.musicVolume);	
							return;
						}break;
						case "Eraser":{
							Selected_Swatch = 1;
							Selected_Color 	= -1;
							Set_Swatchs();
							soundManager.playSound("SFX_Brush_Button", Costanza.musicVolume);	
							return;
						}break;
						case "Sticker":{					
							if(Interface_Hud.Get_Zoom() == "In"){
								Set_Zoom(false);
								Interface_Hud.Reset_Zoom();
							}
							soundManager.playSound("SFX_Sticker_Button", Costanza.musicVolume);	
							return;
						}break;	
						
						//Scrolling Selectors.
						case "Color_Scroll":{
							Selected_Color = Interface_Hud.Get_Selected_Color();
							Set_Swatchs();					
							soundManager.playSound("SFX_Color_Pick_Button", Costanza.musicVolume);	
							Update_Phase = "Scrolling";
							return;
						}break;		
						case "Sticker_Scroll":{
							//soundManager.playSound("SFX_Color_Pick_Button", Costanza.musicVolume);	
							Update_Phase = "Scrolling";
							return;
						}break;						
						
						//More Tabs.
						case "Undo":{
							soundManager.playSound("Click_Mouse", Costanza.soundVolume);
							Using_Undo();
							return;
						}break;
						case "Recycle":{
							soundManager.playSound("Click_Mouse", Costanza.soundVolume);
							return;
						}break;
						case "Camera":{			
							soundManager.playSound("SFX_Take_Picture", Costanza.soundVolume);
							var Picture:BitmapData = new BitmapData(((1366 * Costanza.SCALE_FACTOR) - (Costanza.VIEWPORT_OFFSET * 2)), (768 * Costanza.SCALE_FACTOR), false, 0xffffff);					
							var DM:Matrix = new Matrix();
							DM.translate(-Costanza.VIEWPORT_OFFSET, 0);
							Picture.draw(Drawing_Canvas_Holder, DM);
							if(camSave){camRoll.addBitmapData(Picture);}
							Picture.dispose();
							Picture = null;
							return;
						}break;
						case "More":{
							soundManager.playSound("SFX_More_Button", Costanza.soundVolume);
							return;
						}break;
						
						//Clear All Box.
						case "Yes":{
							soundManager.playSound("Click_Mouse", Costanza.soundVolume);
							Disposing_Undos();
							Disposing_Stickers();
							Clear_Glitter_Grid();
							Drawing_Canvas_Bitmap.bitmapData.fillRect(Drawing_Canvas_Bitmap.bitmapData.rect, 0xffffff);
							return;
						}break;
						case "No":{
							soundManager.playSound("Click_Mouse", Costanza.soundVolume);
							return;
						}break;
						
						//Zoom Options.
						case "ZoomIn":{	
							soundManager.playSound("SFX_Zin_Button", Costanza.soundVolume);						
							return;
						}break;
						case "ZoomOut":{
							soundManager.playSound("SFX_Zout_Button", Costanza.soundVolume);
							return;
						}break;	
						
						case "Nothing":{
							
							//Check see if hit a sticker.
							if(Interface_Hud.Get_Mode() == "Sticker"){
								if(Interface_Value == "Nothing"){
									if(Click_Sticker(Current_Mouse_Position)){
										Interface_Hud.Show_Trash();
										soundManager.playSound("SFX_Select_Sticker", Costanza.soundVolume);
										Update_Phase = "Update_Stickers";
										return;
									}
								}
							}
							
							//Paint.
							if(Interface_Hud.Get_Mode() == "Brush" || Interface_Hud.Get_Mode() == "Crayon" || Interface_Hud.Get_Mode() == "Spray" || Interface_Hud.Get_Mode() == "Chalk" || Interface_Hud.Get_Mode() == "Eraser"){
								if(Interface_Value == "Nothing"){
									if(Choose_Coloring_Layer(Current_Mouse_Position)){
										Adding_Undo("Image");
										Scene_Holder.removeChild(Interface_Hud);										
										Update_Phase = "Painting";
										return;
									}
								}
							}
							
							//Full Draw.
							if(Interface_Hud.Get_Mode() == "Bucket"){
								if(Interface_Value == "Nothing"){
									if(Choose_Coloring_Layer(Current_Mouse_Position)){						
										soundManager.playSound("SFX_Draw_Bucket", Costanza.soundVolume);
										Adding_Undo("Image");
										Paint_Coloring_Full(Current_Mouse_Position);
										Update_Phase = "Painting_Full";
										return;
									}
								}				
							}		
							
						}break;
						
					}				
					
				}break;
				
				case "Awayting_Page_Selection_Input":{
					
					if(Page_Selection_Hud.Get_Active()){
						Page_Selection_Hud.Click_Bar(Current_Mouse_Position);
						return;
					}		
					
				}break;				
				
				case "Awayting_Zoom_Selection_Input":{
					
					if(ZoomSelectionScreen.Get_Active()){
						var Zoom_Selection_Value:String = ZoomSelectionScreen.Down(Current_Mouse_Position);			
						trace(Zoom_Selection_Value + " Current Zoom Selection Action");
						switch(Zoom_Selection_Value){	
							case "TL":{
								//trace("TL");
							}break;
							case "TR":{
								//trace("TR");
							}break;
							case "BL":{
								//trace("BL");
							}break;
							case "BR":{
								//trace("BR");
							}break;
							case "TM":{
								//trace("TM");
							}break;
							case "BM":{
								//trace("BM");
							}break;
						}
						return;
					}	
					
				}break;
			
			}			
			
		}
		private function Update_Scene(e:flash.events.Event):void{
			
			Update_Mouse();	
			
			switch(Update_Phase){
				
				case "Awayting_Interface_Input":{
					Update_Glitter_Grid();
					return;
				}break;
				
				case "Awayting_Page_Selection_Input":{					
					
					if(Page_Selection_Hud.Get_Active()){
						Page_Selection_Hud.Move_Bar((Current_Mouse_Position.x - Last_Mouse_Position.x));
						return;
					}
					
				}break;				
				
				case "Awayting_Zoom_Selection_Input":{
					return;					
				}break;			
				
				case "Scrolling":{
					Interface_Hud.Move(Current_Mouse_Position, Last_Mouse_Position);	
				}break;
				
				case "NewPagePreLoad":{
					
					Update_Phase = "NewPage";
					return;
					
				}break;
				case "NewPage":{
			
					Scene_Holder.addChildAt(Interface_Hud, 0);	
					Scene_Holder.addChildAt(Drawing_Canvas_Holder, 0);	
					Scene_Holder.removeChild(Page_Selection_Hud);					
					Next_Coloring_Page = (Page_Selection_Hud.Get_Selection() + 1);
					Update_Phase = "Loading_New_Page";
					Load_New_Coloring_Page();													
					return;
					
				}break;			
				
				case "Update_Stickers":{
					
					Update_Sticker(Current_Mouse_Position);
					Interface_Hud.Move(Current_Mouse_Position, Last_Mouse_Position);
					return;
					
				}break;		
				
				case "Painting":{
					
					switch(Interface_Hud.Get_Mode()){						
						case "Brush":{
							if(!soundManager.soundIsPlaying("SFX_Draw_Brush")){
								soundManager.playSound("SFX_Draw_Brush", Costanza.soundVolume);	
							}
						}break;
						case "Crayon":{
							if(!soundManager.soundIsPlaying("SFX_Draw_Crayon")){
								soundManager.playSound("SFX_Draw_Crayon", Costanza.soundVolume);	
							}
						}break;	
						case "Spray":{
							if(!soundManager.soundIsPlaying("SFX_Draw_Spray")){
								soundManager.playSound("SFX_Draw_Spray", Costanza.soundVolume);	
							}
						}break;	
						case "Chalk":{
							if(!soundManager.soundIsPlaying("SFX_Draw_Chalk")){
								soundManager.playSound("SFX_Draw_Chalk", Costanza.soundVolume);	
							}
						}break;	
						case "Eraser":{
							if(!soundManager.soundIsPlaying("SFX_Draw_Eraser")){
								soundManager.playSound("SFX_Draw_Eraser", Costanza.soundVolume);	
							}
						}break;
					}					
					
					var Seperations:int = int(Point.distance(Last_Mouse_Position, Current_Mouse_Position));
					if(Seperations > 10){Seperations = 10;}					
					
					if(Interface_Hud.Get_Mode() == "Spray"){
						if(Seperations > 2){Seperations = 2;}
					}
					
					var Draw_Locations:Point 	= new Point(0, 0);
					for(var X:int = 0; X < Seperations; X++){
						Draw_Locations.x = (Last_Mouse_Position.x + (((Current_Mouse_Position.x - Last_Mouse_Position.x) / Seperations) * X));
						Draw_Locations.y = (Last_Mouse_Position.y + (((Current_Mouse_Position.y - Last_Mouse_Position.y) / Seperations) * X));		
						if(Interface_Hud.Get_Mode() == "Spray"){
							Glitter_Coloring_Page(Draw_Locations);
						}else{
							Paint_Coloring_Page(Draw_Locations);
						}
						
					}				
					
					return;
					
				}break;				
				
				case "ExitPrep":{
					
					Update_Phase = "ExitToMain";
					return;
					
				}break;
				case "ExitToMain":{	
					
					Scene_Holder.addChildAt(Interface_Hud, 0);	
					Scene_Holder.addChildAt(Drawing_Canvas_Holder, 0);
					if(Page_Selection_Hud.Get_Active()){
						Page_Selection_Hud.Deactivate_Bar();
						Scene_Holder.removeChild(Page_Selection_Hud);
					}
					Save_To_File();
					ExitGame();
					Update_Phase = "Quiting";
					return;
					
				}break;
				
				
			}
			
		}
		private function TouchRelease(e:flash.events.MouseEvent):void{	
			
			Update_Mouse();
			
			var Interface_Value:String = Interface_Hud.Up(Current_Mouse_Position);			
			trace(Interface_Value + " Current Interface Action Released");
			
			switch(Update_Phase){
				
				case "Painting":{
					
					if(soundManager.soundIsPlaying("SFX_Draw_Brush")){soundManager.stopSound("SFX_Draw_Brush");}
					if(soundManager.soundIsPlaying("SFX_Draw_Crayon")){soundManager.stopSound("SFX_Draw_Crayon");}
					if(soundManager.soundIsPlaying("SFX_Draw_Spray")){soundManager.stopSound("SFX_Draw_Spray");}
					if(soundManager.soundIsPlaying("SFX_Draw_Chalk")){soundManager.stopSound("SFX_Draw_Chalk");;}
					if(soundManager.soundIsPlaying("SFX_Draw_Eraser")){soundManager.stopSound("SFX_Draw_Eraser");}
					
					Update_Phase = "Awayting_Interface_Input";					
					Interface_Hud.Show_Me();	
					Scene_Holder.addChild(Interface_Hud);
					return;
					
				}break;
				case "Update_Stickers":{
					
					Drop_Sticker(Current_Mouse_Position);
					Update_Phase = "Awayting_Interface_Input";	
					Interface_Hud.Hide_Trash();
					return;
					
				}break;
				case "Updating_Zoom_Move":{	
					
					Update_Phase = "Awayting_Interface_Input";
					Interface_Hud.Show_Me();	
					Scene_Holder.addChild(Interface_Hud);					
					return;
					
				}break;
				case "Scrolling":{
					
					switch(Interface_Value){
						
						case "Sticker_Scroll":{	
							
							var Selected_Sticker:int = Interface_Hud.Get_Selected_Sticker();						
							if(Selected_Sticker != -999){
								soundManager.playSound("SFX_Select_Sticker", Costanza.soundVolume);
								Create_Sticker((Selected_Sticker + 1), Current_Mouse_Position);		
							}else{					
								soundManager.playSound("SFX_Zout_Button", Costanza.soundVolume);								
								return;
							}
							
							Update_Phase = "Awayting_Interface_Input";
							return;	
							
						}break;
					}
					
					Update_Phase = "Awayting_Interface_Input";		
					return;
				}break;
				case "Painting_Full":{
					Update_Phase = "Awayting_Interface_Input";
					return;
				}break;				
				
				case "Awayting_Interface_Input":{	
					
					if(Interface_Hud.Is_Yes_NO()){
						switch(Interface_Value){							
							//Clear All Box.
							case "Yes_Close":{
								soundManager.playSound("Click_Mouse", Costanza.soundVolume);
								Disposing_Undos();
								Disposing_Stickers();
								Clear_Glitter_Grid();
								Drawing_Canvas_Bitmap.bitmapData.fillRect(Drawing_Canvas_Bitmap.bitmapData.rect, 0xffffff);
								return;
							}break;
							case "No_close":{
								soundManager.playSound("Click_Mouse", Costanza.soundVolume);
								return;
							}break;							
						}						
						return;						
					}else{
						switch(Interface_Value){							
							//Clear All Box.
							case "Yes_Close":{
								soundManager.playSound("Click_Mouse", Costanza.soundVolume);
								Disposing_Undos();
								Disposing_Stickers();
								Clear_Glitter_Grid();
								Drawing_Canvas_Bitmap.bitmapData.fillRect(Drawing_Canvas_Bitmap.bitmapData.rect, 0xffffff);
								return;
							}break;
							case "No_close":{
								soundManager.playSound("Click_Mouse", Costanza.soundVolume);
								return;
							}break;							
						}			
					}
					
					switch(Interface_Value){
						
						case "Home":{							
							if(First_Down != "Home"){return;}
							Scene_Holder.addChild(sLoading);	
							Update_Phase = "ExitPrep";	
							return;
						}break;
						
						case "Page":{							
							if(First_Down != "Page"){return;}
							if(!Scene_Holder.contains(Drawing_Canvas_Holder)){return;}
							if(Interface_Hud.Get_Zoom() == "In"){
								Set_Zoom(false);
								Interface_Hud.Reset_Zoom();
							}					
							Pages_Icons_Assets.Update_Icon(Drawing_Canvas_Holder, (Current_Coloring_Page - 1));
							Page_Selection_Hud.Activate_Bar();
							Scene_Holder.addChild(Page_Selection_Hud);					
							Scene_Holder.removeChild(Interface_Hud);
							Scene_Holder.removeChild(Drawing_Canvas_Holder);
							Update_Phase = "Awayting_Page_Selection_Input";
							return;
						}break;					
						
						case "ZoomIn":{						
							if(!ZoomSelectionScreen.Get_Active()){
								Scene_Holder.addChild(ZoomSelectionScreen);
								Scene_Holder.removeChild(Interface_Hud);
								ZoomSelectionScreen.Activate();	
								Update_Phase = "Awayting_Zoom_Selection_Input";
								return;
							}					
							return;
						}break;
						
						case "ZoomOut":{					
							Set_Zoom(false);
							Update_Phase = "Awayting_Interface_Input";
							return;
						}break;
						
					}
										
				}break;
				
				case "Awayting_Zoom_Selection_Input":{
					
					switch(ZoomSelectionScreen.Up(Current_Mouse_Position)){	
						case "TL":{		
							soundManager.playSound("SFX_Zin_Button", Costanza.soundVolume);	
							Scene_Holder.removeChild(ZoomSelectionScreen);
							Scene_Holder.addChild(Interface_Hud);
							ZoomSelectionScreen.Deactivate();	
							Set_Zoom(true, "TL");
							Update_Phase = "Awayting_Interface_Input";
							return;						
						}break;
						case "TR":{
							soundManager.playSound("SFX_Zin_Button", Costanza.soundVolume);	
							Scene_Holder.removeChild(ZoomSelectionScreen);
							Scene_Holder.addChild(Interface_Hud);
							ZoomSelectionScreen.Deactivate();	
							Set_Zoom(true, "TR");
							Update_Phase = "Awayting_Interface_Input";
							return;	
						}break;
						case "BL":{
							soundManager.playSound("SFX_Zin_Button", Costanza.soundVolume);	
							Scene_Holder.removeChild(ZoomSelectionScreen);
							Scene_Holder.addChild(Interface_Hud);
							ZoomSelectionScreen.Deactivate();	
							Set_Zoom(true, "BL");
							Update_Phase = "Awayting_Interface_Input";
							return;	
						}break;
						case "BR":{
							soundManager.playSound("SFX_Zin_Button", Costanza.soundVolume);	
							Scene_Holder.removeChild(ZoomSelectionScreen);
							Scene_Holder.addChild(Interface_Hud);
							ZoomSelectionScreen.Deactivate();	
							Set_Zoom(true, "BR");
							Update_Phase = "Awayting_Interface_Input";
							return;	
						}break;
						case "TM":{
							soundManager.playSound("SFX_Zin_Button", Costanza.soundVolume);	
							Scene_Holder.removeChild(ZoomSelectionScreen);
							Scene_Holder.addChild(Interface_Hud);
							ZoomSelectionScreen.Deactivate();	
							Set_Zoom(true, "TM");
							Update_Phase = "Awayting_Interface_Input";
							return;	
						}break;
						case "BM":{
							soundManager.playSound("SFX_Zin_Button", Costanza.soundVolume);	
							Scene_Holder.removeChild(ZoomSelectionScreen);
							Scene_Holder.addChild(Interface_Hud);
							ZoomSelectionScreen.Deactivate();	
							Set_Zoom(true, "BM");
							Update_Phase = "Awayting_Interface_Input";
							return;	
						}break;
					}
					
					return;						
					
				}break;
				
				case "Awayting_Page_Selection_Input":{					
						
					switch(Page_Selection_Hud.Up_Bar(Current_Mouse_Position)){
						case "Home":{
							Page_Selection_Hud.Deactivate_Bar();
							soundManager.playSound("SFX_Home_Button", Costanza.musicVolume);	
							Scene_Holder.addChild(sLoading);	
							Update_Phase = "ExitPrep";					
							return;
						}break;
						case "Close":{
							soundManager.playSound("SFX_Home_Button", Costanza.musicVolume);	
							Page_Selection_Hud.Deactivate_Bar();
							Scene_Holder.addChildAt(Interface_Hud, 0);	
							Scene_Holder.addChildAt(Drawing_Canvas_Holder, 0);
							Scene_Holder.removeChild(Page_Selection_Hud);
							Update_Phase = "Awayting_Interface_Input";	
							return;
						}break;
						case "Loading_Page":{
							soundManager.playSound("SFX_Select_Page", Costanza.musicVolume);						
							Scene_Holder.addChild(sLoading);	
							Page_Selection_Hud.Deactivate_Bar();
							Update_Phase = "NewPagePreLoad";
							return;
						}break;
					}					
					
					return;
					
				}break;		
				
			}
			
		}
		
		
		
		
		
		
		
		
		
		//Multy Touch.
		private function MultyTapperBegin(e:TouchEvent):void{
			
			if(TouchArray.length > 1){return;}
			
			var Touch_ID:Array 	= [e.touchPointID, new Point(e.stageX, e.stageY)];
			TouchArray.push(Touch_ID);
			
			if(TouchArray.length > 1){
				Squeeze_Current = Point.distance(TouchArray[0][1], TouchArray[1][1]);
				Squeeze_Last 	= Squeeze_Current;				
				Rotation_Last 	= Math.atan2((TouchArray[1][1].y - TouchArray[0][1].y), (TouchArray[1][1].x - TouchArray[0][1].x)) * (180 / Math.PI); 				
			}			
			
		}
		private function MultyTapperMoved(e:TouchEvent):void{
			
			for(var X:int = 0; X < TouchArray.length; X++){
				if(TouchArray[X][0] == e.touchPointID){
					TouchArray[X][1].x = e.stageX;
					TouchArray[X][1].y = e.stageY;
				}
			}
			
			Squeeze_Last 	= Squeeze_Current;
			if(TouchArray.length > 1){Squeeze_Current = Point.distance(TouchArray[0][1], TouchArray[1][1]);}
			
		}
		private function MultyTapperEnded(e:TouchEvent):void{
			var Touch_ID:int = e.touchPointID;
			for(var X:int = 0; X < TouchArray.length; X++){
				if(TouchArray[X][0] == Touch_ID){
					TouchArray.splice(X, 1);
					X--;
				}
			}
		}
		
		
		
		
		
		
		
		
		
		
		
		
				
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		private var Glitter_Bank:Array;		
		private function Create_Glitter_Bank():void{
			
			Glitter_Bank = new Array();			
			
			if(!DeviceInfo.isSlow){
				for(var X:int = 0; X < 360; X++){				
					var gSize:int = (Math.floor(Math.random() * 2) + 1);				
					Glitter_Bank.push(Create_Glitter_Swatch(1, (X + 1), Get_Color_Alpha(0xE8F1D4, 180)));
					Glitter_Bank.push(Create_Glitter_Swatch(1, (X + 1), Get_Color_Alpha(0xE6E8FA, 180)));				
				}	
			}else{
				Glitter_Bank.push(Create_Glitter_Swatch(1, (X + 1), Get_Color_Alpha(0xE8F1D4, 180)));
				Glitter_Bank.push(Create_Glitter_Swatch(1, (X + 1), Get_Color_Alpha(0xE6E8FA, 180)));			
			}
			
		}
		private function Create_Glitter_Swatch(Size:int, Rotation:int, Color:uint):BitmapData{			
			var Returned_Glitter_Swatch:BitmapData = new BitmapData((Size * 2), (Size * 2), true, 0x000000);
			var Glitter_Swatch:Bitmap = new Bitmap();
			Glitter_Swatch.bitmapData = new BitmapData(Size, Size, false, Color);
			var M:Matrix = new Matrix();
			M.rotate(Rotation);
			Returned_Glitter_Swatch.draw(Glitter_Swatch, M, null, null, null, true);
			Glitter_Swatch.bitmapData.dispose();
			Glitter_Swatch = null;
			return Returned_Glitter_Swatch;			
		}
		
			
		//Swatches.
		private function Initialize_Swatches():void{			
			
			Create_Glitter_Bank();
			
			Swatchs_Holder 	= new Array();
			Swatchs_Holder.push(Core_Assets.Get_Image("CB_Swatch_1"));
			Swatchs_Holder.push(Core_Assets.Get_Image("CB_Swatch_2"));
			Swatchs_Holder.push(Core_Assets.Get_Image("CB_Swatch_3"));
			Swatchs_Holder.push(Core_Assets.Get_Image("CB_Swatch_4"));
			
			Swatchs_Holder.push(Core_Assets.Get_Image("star_1"));
			Swatchs_Holder.push(Core_Assets.Get_Image("star_2"));
			Swatchs_Holder.push(Core_Assets.Get_Image("star_3"));
			
			Swatchs_Holder.push(Core_Assets.Get_Image("Glitter_1"));
			Swatchs_Holder.push(Core_Assets.Get_Image("Glitter_2"));
			
			Selected_Color = Interface_Hud.Get_Selected_Color();			
			Selected_Scale = Interface_Hud.Get_Selected_Scale();		
			
			Set_Swatchs();
			
		}
		private function Dispose_Swatches():void{
			
			Reset_Swatchs();
			
			//Clear Swatches.
			for(var SA:int = 0; SA < Swatchs_Holder.length; SA++){
				Dispose_Image(Swatchs_Holder[SA]);
			}Swatchs_Holder.length = 0;	
			
			for(var X:int = 0; X < Glitter_Bank.length; X++){	
				BitmapData(Glitter_Bank[X]).dispose();
			}Glitter_Bank.length = 0;
			
		}
		private function Set_Swatchs():void{
			
			if(Swatchs_Draw_Holder == null){Swatchs_Draw_Holder = new Array();}
			else{Reset_Swatchs();}
			
			var AreColor:uint;
			if(Selected_Color == -1){
				AreColor = 0xffffff;
			}else{
				AreColor = Drawing_Colors[Selected_Color];	 	
			}
			
			
			for(var SA:int = 0; SA < Swatchs_Holder.length; SA++){
				
				//Set Color.			
				var Color_Transform:ColorTransform		= new ColorTransform();					
				var ScaleValue:Number 					= Selected_Scale;
				
				if((SA < 4)){
					Color_Transform.color 				= AreColor;
					Color_Transform.alphaMultiplier 	= 0.1;
					Color_Transform.alphaOffset 		= 9;	
				}else if((SA >= 4) && (SA < 7)){
					Color_Transform.color 				= AreColor;	
					ScaleValue 							= (ScaleValue - 0.8);
					if(ScaleValue < 0.1){ScaleValue 	= 0.1;}
				}else{
					ScaleValue 							= 0.1;
					Color_Transform.color 				= 0xffffff;	
				}				
				
				Swatchs_Holder[SA].scaleX 				= Swatchs_Holder[SA].scaleY = ScaleValue;		
				var Swatch:Bitmap 						= new Bitmap();
				Swatch.smoothing 						= true;
				Swatch.bitmapData 						= new BitmapData(Swatchs_Holder[SA].width, Swatchs_Holder[SA].height, true, 0x000000);				
				
				var TM:Matrix 							= new Matrix();
				TM.scale(ScaleValue, ScaleValue);
				Swatch.bitmapData.drawWithQuality(Swatchs_Holder[SA], TM, Color_Transform, "normal", null, true, StageQuality.BEST);				
				
				var FillSwatch:Bitmap 					= new Bitmap();
				FillSwatch.smoothing 					= true;
				if(SA >= 7){	
					FillSwatch.bitmapData 				= new BitmapData(Swatch.width, Swatch.height, false, 0xffffff);
				}else{
					FillSwatch.bitmapData 				= new BitmapData(Swatch.width, Swatch.height, false, AreColor);
				}
				
				Swatch.cacheAsBitmap 					= true;
				FillSwatch.cacheAsBitmap 				= true;
				FillSwatch.mask 						= Swatch;
				
				var SuperHolder:flash.display.Sprite 	= new flash.display.Sprite();
				SuperHolder.addChild(FillSwatch);
				SuperHolder.addChild(Swatch);					
				
				if(SA >= 7){					
					var GlowFill:GlowFilter 			= new GlowFilter(0xffffff, 0.3, 8, 8, 2, BitmapFilterQuality.MEDIUM);
					SuperHolder.filters 				= [GlowFill];					
				}
				
				var FinalDraw:Bitmap 					= new Bitmap();
				FinalDraw.smoothing 					= true;
				FinalDraw.bitmapData 					= new BitmapData(SuperHolder.width, SuperHolder.height, true, 0x000000);
				FinalDraw.bitmapData.drawWithQuality(SuperHolder, null, null, "normal", null, true, StageQuality.BEST);
				
				Swatchs_Draw_Holder.push(FinalDraw);
				
				//Clean.
				SuperHolder.removeChild(FillSwatch);
				SuperHolder.removeChild(Swatch);	
				SuperHolder 							= null;
				
				FillSwatch.bitmapData.dispose();
				FillSwatch 								= null;
				
				Swatch.bitmapData.dispose();
				Swatch 									= null;
				
				TM 										= null;
				GlowFill 								= null;
				Color_Transform 						= null;
				
			}			
			
		}
		private function Reset_Swatchs():void{
			for(var X:int = 0; X < Swatchs_Draw_Holder.length; X++){
				Swatchs_Draw_Holder[X].bitmapData.dispose();
				Swatchs_Draw_Holder[X] = null;
			}
			Swatchs_Draw_Holder.length = 0;
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		//Coloring Page.
		private function Initialize_Coloring_Page():void{
			
			Drawing_Undos 				= new Array();
			Stickers 					= new Array();
			Current_Coloring_Page 		= 1;
			Next_Coloring_Page			= 1;			
			Canvas 						= new Coloring_Canvas();			
			Selected_Scale  			= 1.0;
			Selected_Alpha  			= 1;
			Selected_Color  			= 0;
			Selected_Layer  			= 0;
			Selected_Swatch 			= 0;		
			
			Drawing_Canvas_Layers				= new Array();
			Drawing_Canvas_Holder 				= new flash.display.Sprite();				
			Drawing_Canvas_Sticker				= new flash.display.Sprite();
			Drawing_Canvas_Bitmap 				= new Bitmap();
			Drawing_Canvas_Bitmap.bitmapData 	= new BitmapData(Full_App_Width, Full_App_Height, false, 0xffffff);
			Drawing_Canvas_Bitmap.smoothing 	= true;
			
		}	
		private function Load_New_Coloring_Page():void{
			Save_To_File();
			Clear_Coloring_Page();
			Current_Coloring_Page = Next_Coloring_Page;
			Load_Canvas_From_File();			
		}
		private function Clear_Coloring_Page():void{
			
			Clear_Glitter_Grid();
			
			Disposing_Undos();
			
			Drawing_Canvas_Holder.removeChild(Canvas.Get_Lines());
			Drawing_Canvas_Holder.removeChild(Drawing_Canvas_Sticker);
			Drawing_Canvas_Holder.removeChild(Drawing_Canvas_Bitmap);
			
			Disposing_Stickers();
			
			Canvas.Dispose_Canvases();
			
			Drawing_Canvas_Bitmap.bitmapData.fillRect(Drawing_Canvas_Bitmap.bitmapData.rect, 0xffffff);			
			
			for(var X:int = 0; X < Drawing_Canvas_Layers.length; X++){
				Dispose_Image(Drawing_Canvas_Layers[X]);
				Drawing_Canvas_Layers.splice(X, 1);
				X--;
			}Drawing_Canvas_Layers.length = 0;	
			
			Scene_Holder.removeChild(Drawing_Canvas_Holder);
			
		}
		private function Load_Canvas_From_File():void{
			
			//Extension.
			var FilePath:String 	= ((Page_Pack + "_Page_" + Current_Coloring_Page.toString()) + ".png");				
			var myFile:File;
			myFile 					= File.applicationStorageDirectory;
			myFile 					= myFile.resolvePath( FilePath );			
			var ba:ByteArray 		= new ByteArray();			
			var stream:FileStream 	= new FileStream();					
		
			if(myFile.exists){					
				
				stream.open(myFile, FileMode.READ);				
				stream.readBytes(ba);	
				stream.close();				
				
				var loader:Loader = new Loader();
				loader.contentLoaderInfo.addEventListener(flash.events.Event.COMPLETE, On_Canvas_Loaded);
				loader.loadBytes(ba);				
				
			}else{	
				Load_External_Stickers();	
			}		
			
			//clear for Leaks.
			ba.clear();
			stream 		= null;
			myFile 		= null;
			FilePath 	= null;
			
		}
		private function On_Canvas_Loaded(e:flash.events.Event):void{
			Drawing_Canvas_Bitmap.bitmapData.draw(Bitmap(e.currentTarget.loader.content).bitmapData);
			Bitmap(e.currentTarget.loader.content).bitmapData.dispose();
			e.currentTarget.loader.contentLoaderInfo.removeEventListener(flash.events.Event.COMPLETE, On_Canvas_Loaded);
			e = null;			
			Load_External_Stickers();
		}
		private function Process_Page_Loaded():void{
			//Scene_Holder.removeChild(sLoading);
			//Update_Phase = "Awayting_Interface_Input";
			//return;
			
			//trace("OK IN");
			Canvas.Load_Page(Page_Pack, Current_Coloring_Page);	
			
			Drawing_Canvas_Holder.addChild(Drawing_Canvas_Bitmap);				
			Drawing_Canvas_Holder.addChild(Canvas.Get_Lines());
			Drawing_Canvas_Holder.addChild(Drawing_Canvas_Sticker);
			Drawing_Canvas_Holder.visible = true;
			//Scene_Holder.removeChild(sLoading);
			//return;
			if(First_Time){
				First_Time = false;
				Scene_Holder.removeChild(Interface_Hud);				
				Page_Selection_Hud.Activate_Bar();
				Scene_Holder.addChild(Page_Selection_Hud);					
				Update_Phase = "Awayting_Page_Selection_Input";
			}else{
				Scene_Holder.addChildAt(Drawing_Canvas_Holder, 0);						
				Update_Phase = "Awayting_Interface_Input";					
			}
			Scene_Holder.removeChild(sLoading);
		}
		private function Choose_Coloring_Layer(Position:Point):Boolean{			
			Selected_Layer = Canvas.Select_Layer(Drawing_Canvas_Holder.globalToLocal(Position));
			if(Selected_Layer != -1){	
				Draw_Layer_X = Canvas.Get_Layer_Position(Selected_Layer).x;
				Draw_Layer_Y = Canvas.Get_Layer_Position(Selected_Layer).y;		
				return true;
			}			
			return false;			
		}
		
		
		private function Glitter_Coloring_Page(Position:Point):void{	
			
			Position 	= Drawing_Canvas_Holder.globalToLocal(Position);	
			Position.x 	= (Position.x - Draw_Layer_X);
			Position.y 	= (Position.y - Draw_Layer_Y);				
			
			//Draw some Glitter.
			for(var G:int = 0; G < 20; G++){					
				
				var gX:int = Position.x + ((Math.random() * Swatchs_Draw_Holder[Selected_Swatch].width) - (Swatchs_Draw_Holder[Selected_Swatch].width / 2));
				var gY:int = Position.y + ((Math.random() * Swatchs_Draw_Holder[Selected_Swatch].height) - (Swatchs_Draw_Holder[Selected_Swatch].height / 2));
					
				//here we check if we hit the mask.
				if(Canvas.Layer_Colision(new Point((gX + Draw_Layer_X), (gY + Draw_Layer_Y)), null, Selected_Layer)){	
					
					var GS:int = Math.floor(Math.random() * Glitter_Bank.length);	
					switch(Math.floor(Math.random() * 6)){
						case 0:{											
							Drawing_Canvas_Bitmap.bitmapData.copyPixels(Glitter_Bank[GS], new Rectangle(0, 0, Glitter_Bank[GS].width, Glitter_Bank[GS].height), new Point((gX + Draw_Layer_X), (gY + Draw_Layer_Y)), null, null, true);							
						}break;
						case 1:{											
							Drawing_Canvas_Bitmap.bitmapData.copyPixels(Glitter_Bank[GS], new Rectangle(0, 0, Glitter_Bank[GS].width, Glitter_Bank[GS].height), new Point((gX + Draw_Layer_X), (gY + Draw_Layer_Y)), null, null, true);							
						}break;
						default:{
							PixColo.fillRect(PixColo.rect, Get_Color_Alpha(Drawing_Colors[Selected_Color], 180));	
							Drawing_Canvas_Bitmap.bitmapData.copyPixels(PixColo, new Rectangle(0, 0, 1, 1), new Point((gX + Draw_Layer_X), (gY + Draw_Layer_Y)), null, null, true);
						}break;
					}					
					
					//Add the glitter.
					if(!DeviceInfo.isSlow){Edit_Glitter_Grid(new Point((gX + Draw_Layer_X), (gY + Draw_Layer_Y)), 1);}
								
					
				}
				
			}
			
		}
		
		
		
		
		
		
		
		
		private function Initialize_Draw_Section():void{
			
			Section_Draw_Sprite 						= new flash.display.Sprite();
			Section_Draw_Mask_Sprite 					= new flash.display.Sprite();
			Section_Draw_Swatch_Sprite 					= new flash.display.Sprite();
			
			Section_Draw_Swatch_Sprite.cacheAsBitmap 	= true;
			Section_Draw_Mask_Sprite.cacheAsBitmap 		= true;
			
			Section_Draw_Swatch_Sprite.mask 			= Section_Draw_Mask_Sprite;
			
			Section_Draw_Sprite.addChild(Section_Draw_Swatch_Sprite);
			Section_Draw_Sprite.addChild(Section_Draw_Mask_Sprite);
			
			Section_Draw_Mask_Bitmap 					= new Bitmap();
			Section_Draw_Mask_Bitmap.bitmapData 		= new BitmapData(1, 1, true, 0x000000);
			Section_Draw_Final 							= new Bitmap();
			Section_Draw_Final.bitmapData 				= new BitmapData(1, 1, true, 0x000000);		
			
		}
		private function Use_Draw_Section(Location:Point, Fill:Boolean = false):void{
			
			//get Mask.
			Canvas.Get_Layer_Section(Selected_Layer, Bitmap(Swatchs_Draw_Holder[Selected_Swatch]).bitmapData.rect, Location, Section_Draw_Mask_Bitmap);
			Section_Draw_Mask_Sprite.addChild(Section_Draw_Mask_Bitmap);
			
			//Add Swatch.
			Section_Draw_Swatch_Sprite.addChild(Swatchs_Draw_Holder[Selected_Swatch]);
			
			//Draw Final.
			Section_Draw_Final.bitmapData = new BitmapData(Section_Draw_Sprite.width, Section_Draw_Sprite.height, true, 0x000000);
			Section_Draw_Final.bitmapData.draw(Section_Draw_Sprite);
			
		}
		private function Clean_Draw_Section():void{
			
			//Clean Holders.			
			Section_Draw_Swatch_Sprite.removeChild(Swatchs_Draw_Holder[Selected_Swatch]);
			Section_Draw_Mask_Sprite.removeChild(Section_Draw_Mask_Bitmap);
			
			//Clean Bitdatas.
			Section_Draw_Mask_Bitmap.bitmapData.dispose();
			Section_Draw_Mask_Bitmap.bitmapData = new BitmapData(1, 1, true, 0x000000);			
			
			Section_Draw_Final.bitmapData.dispose();
			Section_Draw_Final.bitmapData = new BitmapData(1, 1, true, 0x000000);				
			
		}
		private function Dispose_Draw_Section():void{
			
			//Clean Holders.			
			//Section_Draw_Swatch_Sprite.removeChild(Swatchs_Draw_Holder[Selected_Swatch]);
			//Section_Draw_Mask_Sprite.removeChild(Section_Draw_Mask_Bitmap);
			
			//Clean Bitdatas.
			Section_Draw_Mask_Bitmap.bitmapData.dispose();				
			Section_Draw_Final.bitmapData.dispose();
			
			Section_Draw_Swatch_Sprite.mask = null;
			Section_Draw_Sprite.removeChild(Section_Draw_Swatch_Sprite);
			Section_Draw_Sprite.removeChild(Section_Draw_Mask_Sprite);
			
			Section_Draw_Sprite = null;
			Section_Draw_Mask_Sprite = null;
			Section_Draw_Swatch_Sprite = null;			
			
		}
		
		
		private function Paint_Coloring_Page(Position:Point):void{	
			
			//True Position.
			Position 	= Drawing_Canvas_Holder.globalToLocal(Position);	
			Position.x 	= (Position.x - Draw_Layer_X);
			Position.y 	= (Position.y - Draw_Layer_Y);
			
			//Get draw Peice.
			Use_Draw_Section(Position);
			
			//Draw.
			Drawing_Canvas_Bitmap.bitmapData.copyPixels(Section_Draw_Final.bitmapData,
														new Rectangle(0, 0, Section_Draw_Final.bitmapData.width, Section_Draw_Final.bitmapData.height),
														new Point((Position.x + Draw_Layer_X), (Position.y + Draw_Layer_Y)),
														null, null,
														true);
			
			if(!DeviceInfo.isSlow){
				Glitter_Grid.copyPixels(Section_Draw_Final.bitmapData,
										new Rectangle(0, 0, Section_Draw_Final.bitmapData.width, Section_Draw_Final.bitmapData.height),
										new Point((Position.x + Draw_Layer_X), (Position.y + Draw_Layer_Y)),
										null, null,
										true);
			}
			
			//Clean.
			Clean_Draw_Section();
			
		}
		private function Paint_Coloring_Full(Position:Point):void{			
			Canvas.Draw_On_This(Drawing_Canvas_Bitmap.bitmapData, Drawing_Colors[Selected_Color]);
			if(!DeviceInfo.isSlow){Canvas.Draw_On_This(Glitter_Grid, Drawing_Colors[Selected_Color]);}			
		}
		
		
		
		
		
		
		//Zoom.
		private function Set_Zoom(Activate:Boolean, Side:String = "Out"):void{
			
			if(Activate){
				
				switch(Side){
					case "TL":{
						Drawing_Canvas_Holder.scaleX = Drawing_Canvas_Holder.scaleY = 2.0;
						Drawing_Canvas_Holder.x = -Costanza.StageProportionScale;
						Drawing_Canvas_Holder.y = 0;
					}break;
					case "TR":{
						Drawing_Canvas_Holder.scaleX = Drawing_Canvas_Holder.scaleY = 2.0;
						Drawing_Canvas_Holder.x = ((-(1366 * 2) + 1366) * Costanza.SCALE_FACTOR) + Costanza.StageProportionScale;
						Drawing_Canvas_Holder.y = 0;
					}break;
					case "BL":{
						Drawing_Canvas_Holder.scaleX = Drawing_Canvas_Holder.scaleY = 2.0;
						Drawing_Canvas_Holder.x = -Costanza.StageProportionScale;
						Drawing_Canvas_Holder.y = ((-(768 * 2) + 768) * Costanza.SCALE_FACTOR);
					}break;
					case "BR":{
						Drawing_Canvas_Holder.scaleX = Drawing_Canvas_Holder.scaleY = 2.0;
						Drawing_Canvas_Holder.x = ((-(1366 * 2) + 1366) * Costanza.SCALE_FACTOR) + Costanza.StageProportionScale;
						Drawing_Canvas_Holder.y = ((-(768 * 2) + 768) * Costanza.SCALE_FACTOR);
					}break;
					case "TM":{
						Drawing_Canvas_Holder.scaleX = Drawing_Canvas_Holder.scaleY = 2.0;
						Drawing_Canvas_Holder.x = ((1366 / 2) * Costanza.SCALE_FACTOR) - (((1366 * 2) * Costanza.SCALE_FACTOR) / 2);
						Drawing_Canvas_Holder.y = 0;
					}break;
					case "BM":{
						Drawing_Canvas_Holder.scaleX = Drawing_Canvas_Holder.scaleY = 2.0;
						Drawing_Canvas_Holder.x = ((1366 / 2) * Costanza.SCALE_FACTOR) - (((1366 * 2) * Costanza.SCALE_FACTOR) / 2);
						Drawing_Canvas_Holder.y = ((-(768 * 2) + 768) * Costanza.SCALE_FACTOR);
					}break;
				}
				
				
			}else{
				Drawing_Canvas_Holder.scaleX = Drawing_Canvas_Holder.scaleY = 1.0;
				Drawing_Canvas_Holder.x = 0;
				Drawing_Canvas_Holder.y = 0;
			}			
			
		}
	
		
		//Undos.
		private function Adding_Undo(Type:String, Name:String = ""):void{
			
			if(Drawing_Undos.length > Max_Undos){
				Clear_A_Undo(Drawing_Undos[0]);
				Drawing_Undos.splice(0, 1);
			}			
			
			switch(Type){					
				case "Image":{					
					var Image_Array:Array = new Array();					
					var New_Render:BitmapData = new BitmapData(Full_App_Width, Full_App_Height, false, 0xffffff);
					New_Render.draw(Drawing_Canvas_Bitmap);					
					Image_Array.push("Image");
					Image_Array.push(New_Render);					
					Drawing_Undos.push(Image_Array);					
				}break;				
				case "Sticker":{					
					var Sticker_Array:Array = new Array();					
					Sticker_Array.push("Sticker");
					Sticker_Array.push(Name);					
					Drawing_Undos.push(Sticker_Array);					
				}break;				
			}			
			
		}
		private function Using_Undo():void{
			
			if(Drawing_Undos.length <= 0){return;}
			
			switch(Drawing_Undos[(Drawing_Undos.length - 1)][0]){					
				case "Image":{					
					Drawing_Canvas_Bitmap.bitmapData.draw(Drawing_Undos[(Drawing_Undos.length - 1)][1]);			
				}break;				
				case "Sticker":{					
					Remove_Sticker_By_Name(Drawing_Undos[(Drawing_Undos.length - 1)][1]);	
				}break;				
			}				
			
			Clear_A_Undo(Drawing_Undos[(Drawing_Undos.length - 1)]);			
			Drawing_Undos.splice((Drawing_Undos.length - 1), 1);
			
		}
		private function Remove_Undo(Name:String):void{
			for(var X:int = 0; X < Drawing_Undos.length; X++){	
				switch(Drawing_Undos[X][0]){		
					case "Sticker":{					
						if(Drawing_Undos[X][1] == Name){
							trace("RemovingUndo");
							Clear_A_Undo(Drawing_Undos[X]);
							Drawing_Undos.splice(X, 1);
							return;
						}
					}break;				
				}
			}			
		}
		private function Clear_A_Undo(Block:Array):void{
			switch(Block[0]){					
				case "Image":{					
					BitmapData(Block[1]).dispose();					
				}break;				
				case "Sticker":{					
					Block[1] = "";				
				}break;				
			}			
		}
		private function Disposing_Undos():void{
			for(var X:int = 0; X < Drawing_Undos.length; X++){				
				Clear_A_Undo(Drawing_Undos[X]);
				Drawing_Undos.splice(X, 1);
				X--;
			}
			Drawing_Undos.length = 0;
		}
			
		
		
		//Stickers.
		private function Create_Sticker(pID:int, Position:Point):void{
			
			var Sticker_Limit:int = 50;
			if(DeviceInfo.isSlow){
				Sticker_Limit = 3;
			}
			
			if(Stickers.length > Sticker_Limit){return;}
			
			Position 								= Scene_Holder.globalToLocal(Position);	
			
			var Sticker_Props:Array 				= new Array();
			var New_Sticker:flash.display.Sprite 	= Sticker_Assets.Get_Image("Sticker_Big_" + pID.toString());
			New_Sticker.getChildAt(0).x 			= -(New_Sticker.getChildAt(0).width / 2);
			New_Sticker.getChildAt(0).y 			= -(New_Sticker.getChildAt(0).height / 2);
			var Active:Boolean 						= false;
			var Off_Loc:Point 						= new Point(0, 0);			
			New_Sticker.scaleX 						= New_Sticker.scaleY = 1.0;			
			New_Sticker.x 							= ((1366 * Costanza.SCALE_FACTOR) / 2);
			New_Sticker.y 							= ((768 * Costanza.SCALE_FACTOR) / 2);
			New_Sticker.name						= ("Stick_" + Sticker_Counter); Sticker_Counter++;
			
			Sticker_Props.push(New_Sticker);
			Sticker_Props.push(Active);
			Sticker_Props.push(Off_Loc);		
			Sticker_Props.push(false);
			Sticker_Props.push(pID);
			Sticker_Props.push(1.0);			
			Sticker_Props.push(new Point(New_Sticker.x, New_Sticker.y));			
			Stickers.push(Sticker_Props);
			Drawing_Canvas_Sticker.addChild(New_Sticker);
			
			//Sticker_Trash.visible = true;
			//CBBH.Edit_Top_Buttons("Sticker");
			
			Adding_Undo("Sticker", New_Sticker.name);
			
		}
		private function Create_Premade_Sticker(pID:int, Position:Point, Scale:Number):void{
			
			if(Stickers.length > 50){return;}
			
			var Sticker_Props:Array 				= new Array();
			var New_Sticker:flash.display.Sprite 	= Sticker_Assets.Get_Image("Sticker_Big_" + pID.toString());
			New_Sticker.getChildAt(0).x 			= -(New_Sticker.getChildAt(0).width / 2);
			New_Sticker.getChildAt(0).y 			= -(New_Sticker.getChildAt(0).height / 2);
			var Active:Boolean 						= false;
			var Off_Loc:Point 						= new Point(0, 0);
			New_Sticker.name						= ("Stick_" + Sticker_Counter); Sticker_Counter++;
			
			New_Sticker.scaleX 						= New_Sticker.scaleY = Scale;
			
			New_Sticker.x 							= Position.x;
			New_Sticker.y 							= Position.y;	
			
			Sticker_Props.push(New_Sticker);
			Sticker_Props.push(Active);
			Sticker_Props.push(Off_Loc);
			Sticker_Props.push(false);
			Sticker_Props.push(pID);
			Sticker_Props.push(Scale);
			Sticker_Props.push(new Point(New_Sticker.x, New_Sticker.y));
			
			Stickers.push(Sticker_Props);
			Drawing_Canvas_Sticker.addChild(New_Sticker);
			
			//Sticker_Trash.visible = false;
			//CBBH.Edit_Top_Buttons("Idle");
			
		}
		private function Load_External_Stickers():void{
			
			//Extension.
			var FilePath:String = ((Page_Pack + "_Sticker_" + Current_Coloring_Page.toString()) + ".xml");				
			var myFile:File;
			myFile = File.applicationStorageDirectory;
			myFile = myFile.resolvePath( FilePath );			
			var Sticker_XML:XML;
			var stream:FileStream = new FileStream();
			
			if(myFile.exists){			
				
				stream.open(myFile, FileMode.READ);	
				Sticker_XML = XML(stream.readUTFBytes(stream.bytesAvailable));
				stream.close();	
				Process_Stickers(Sticker_XML);
				
			}else{
				
				Load_Glitter_Grid();
				
			}	
			
			//clear for Leaks.
			stream = null;
			Sticker_XML = null;			
			myFile = null;
			FilePath = null;
			
		}
		private function Process_Stickers(TheXML:XML):void{
			
			for(var X:int = 0; X < TheXML.ThisSticker.length(); X++){
				
				var Index:int = int(TheXML.ThisSticker[X].IdIndex.text());
				var Position:Point = new Point(int(TheXML.ThisSticker[X].pX.text()),
					int(TheXML.ThisSticker[X].pY.text()));
				var Scale:Number = Number(TheXML.ThisSticker[X].cS.text());
				
				Create_Premade_Sticker(Index, new Point(Position.x, Position.y), Scale);			
				
				Position = null;
				
			}
			
			Load_Glitter_Grid();
			
		}
		private function Click_Sticker(Position:Point):Boolean{
			
			for(var X:int = (Stickers.length - 1); X > -1; X--){
				if(!Stickers[X][1]){
					if(Stickers[X][0].hitTestPoint(Position.x, Position.y, true)){
						
						Position = Scene_Holder.globalToLocal(Position);
						
						//Sticker_Trash.visible = true;
						//CBBH.Edit_Top_Buttons("Sticker");
						
						Stickers[X][1] 		= true;
						
						Stickers[X][2].x 	= (Position.x - Stickers[X][0].x);
						Stickers[X][2].y 	= (Position.y - Stickers[X][0].y);
						
						Stickers[X][0].x 	= (Position.x - Stickers[X][2].x);
						Stickers[X][0].y 	= (Position.y - Stickers[X][2].y);		
						
						
						//Add Green Square.
						var square:flash.display.Sprite = new flash.display.Sprite();						
						square.graphics.lineStyle(3,0x00ff00);
						square.graphics.drawRect(0,0,Stickers[X][0].getChildAt(0).width, Stickers[X][0].getChildAt(0).height);
						square.x = -(square.width / 2);
						square.y = -(square.height / 2);
						Stickers[X][0].addChildAt(square, 0);
						
						Drawing_Canvas_Sticker.removeChild(Stickers[X][0]);
						Scene_Holder.addChild(Stickers[X][0]);
						return true;
					}				
				}
			}
			return false;
		}	
		private function Update_Sticker(Position:Point):void{
			
			Position = Scene_Holder.globalToLocal(Position);
			
			for(var X:int = 0; X < Stickers.length; X++){
				if(Stickers[X][1]){				
					
					if(TouchArray.length > 1){						
						
						Stickers[X][0].scaleX = Stickers[X][0].scaleY += ((Squeeze_Current - Squeeze_Last) / 100);
						
						var CurrentRotation:Number = Math.atan2((TouchArray[1][1].y - TouchArray[0][1].y), (TouchArray[1][1].x - TouchArray[0][1].x)) * (180 / Math.PI); 
						Stickers[X][0].rotation += (CurrentRotation - Rotation_Last);
						Rotation_Last = CurrentRotation;						
						
						if(Stickers[X][0].scaleX > 2.0){Stickers[X][0].scaleX = Stickers[X][0].scaleY = 2.0;}
						if(Stickers[X][0].scaleX < 0.5){Stickers[X][0].scaleX = Stickers[X][0].scaleY = 0.5;}
						
						Stickers[X][5] 		= Stickers[X][0].scaleX;
						
						Stickers[X][0].x 	= Position.x - Stickers[X][2].x;
						Stickers[X][0].y 	= Position.y - Stickers[X][2].y;
						
						Stickers[X][2].x 	= (Position.x - Stickers[X][0].x);
						Stickers[X][2].y 	= (Position.y - Stickers[X][0].y);
						
						Stickers[X][3] = false;
						
					}else{					
						
						if(Stickers[X][3]){
							Stickers[X][0].x = Position.x;
							Stickers[X][0].y = Position.y;
						}else{
							Stickers[X][0].x = Position.x - Stickers[X][2].x;
							Stickers[X][0].y = Position.y - Stickers[X][2].y;							
						}
						
						Stickers[X][6].x = Stickers[X][0].x;
						Stickers[X][6].y = Stickers[X][0].y;
						
					}
				}
			}
		}
		private function Drop_Sticker(Position:Point):void{
			
			for(var X:int = 0; X < Stickers.length; X++){
				if(Stickers[X][1]){					
					if(Interface_Hud.Get_Current_Up() == "TrashCan"){ //Hit Trash can.
						trace("Delete Sticker");
						Remove_Undo(Stickers[X][0].name);
						Stickers[X][0].removeChildAt(0);
						Scene_Holder.removeChild(Stickers[X][0]);
						Stickers[X][0].getChildAt(0).bitmapData.dispose();
						Stickers[X][0].removeChildAt(0);
						Stickers.splice(X, 1);
						X--;					
						continue;
					}else{
						Stickers[X][1] = false;
						Stickers[X][3] = false;
						Stickers[X][0].removeChildAt(0);
						Scene_Holder.removeChild(Stickers[X][0]);
						Drawing_Canvas_Sticker.addChild(Stickers[X][0]);						
					}					
				}
			}			
		}
		private function Remove_Sticker_By_Name(Name:String):void{
			
			for(var X:int = 0; X < Stickers.length; X++){
				if(Stickers[X][0].name == Name){				
					trace("Delete Sticker");					
					Drawing_Canvas_Sticker.removeChild(Stickers[X][0]);
					Stickers[X][0].getChildAt(0).bitmapData.dispose();
					Stickers[X][0].removeChildAt(0);
					Stickers.splice(X, 1);
					X--;					
					return;								
				}
			}			
		}
		private function Disposing_Stickers():void{
			for(var S:int = 0; S < Drawing_Canvas_Sticker.numChildren; S++){				
				Drawing_Canvas_Sticker.removeChildAt(S);
				S--
			}
			for(var X:int = 0; X < Stickers.length; X++){					
				Stickers[X][0].getChildAt(0).bitmapData.dispose();
				Stickers[X][0].removeChildAt(0);
				Stickers.splice(X, 1);
				X--;
			}
			Stickers.length = 0;
		}
		
		
		
		
		
		
		
		
		//Helpers.
		private function Get_Color_Alpha(Col:uint, Range:int):uint{
			
			//Bit Shift store color single values for editing if needed.
			var r:uint = Col >> 16 & 0xFF;
			var g:uint = Col >>  8 & 0xFF;
			var b:uint = Col >>  0 & 0xFF;
			
			//Random Alpha value 0 - 256.
			var a:uint = (Math.random() * Range);			
			
			//Final Color Merge.
			var argb:uint = a << 24 | r << 16 | g << 8 | b;			
			
			return argb;
		}
		private function Dispose_Image(sObject:flash.display.Sprite):void{
			if(sObject.numChildren > 1){return;}
			if(sObject.numChildren > 0){
				Bitmap(sObject.getChildAt(0)).bitmapData.dispose();
				sObject.removeChildAt(0);
			}
			sObject = null;
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		//Glitter.
		private function Initialize_Glitter_Particles():void{
		
			if(DeviceInfo.isSlow){return;}
			
			GiltGo 			= false;
			GlitCounter 	= 0;
			Glitter_Grid 	= new BitmapData((1366 * Costanza.SCALE_FACTOR), (768 * Costanza.SCALE_FACTOR), false, 0x000000);
			
		}
		private function Load_Glitter_Grid():void{
			
			if(DeviceInfo.isSlow){
				Process_Page_Loaded();
				return;
			}
			
			//Extension.
			var FilePath:String 	= ((Page_Pack + "_Glitter_" + Current_Coloring_Page.toString()) + ".png");	
			var myFile:File;
			myFile 					= File.applicationStorageDirectory;
			myFile 					= myFile.resolvePath( FilePath );			
			var ba:ByteArray 		= new ByteArray();			
			var stream:FileStream 	= new FileStream();					
			
			if(myFile.exists){					
				
				stream.open(myFile, FileMode.READ);				
				stream.readBytes(ba);	
				stream.close();				
				
				var loader:Loader = new Loader();
				loader.contentLoaderInfo.addEventListener(flash.events.Event.COMPLETE, On_Glitter_Loaded);
				loader.loadBytes(ba);				
				
			}else{	
				Process_Page_Loaded();
			}		
			
			//clear for Leaks.
			ba.clear();
			stream = null;
			myFile = null;
			FilePath = null;			
			
		}
		private function On_Glitter_Loaded(e:flash.events.Event):void{
			Glitter_Grid.draw(Bitmap(e.currentTarget.loader.content).bitmapData);
			Bitmap(e.currentTarget.loader.content).bitmapData.dispose();
			e.currentTarget.loader.contentLoaderInfo.removeEventListener(flash.events.Event.COMPLETE, On_Glitter_Loaded);
			e = null;			
			Process_Page_Loaded();
		}
		private function Clear_Glitter_Grid():void{
			
			if(DeviceInfo.isSlow){return;}
			
			for(var G:int = 0; G < GlitCounter; G++){
				var Name:String = ("glit" +  G.toString());
				if(Drawing_Canvas_Holder.getChildByName(Name) == null){continue;}			
				var rBitmap:Bitmap = Bitmap(Drawing_Canvas_Holder.getChildByName(Name));
				TweenMax.killTweensOf(rBitmap);
				Drawing_Canvas_Holder.removeChild(rBitmap);
				rBitmap.bitmapData.dispose();
				rBitmap = null;
			}
			
			GlitCounter = 0;
			
			Glitter_Grid.fillRect(Glitter_Grid.rect, 0xff000000);
			
		}
		private function Edit_Glitter_Grid(Loc:Point, s:int):void{
			
			if(DeviceInfo.isSlow){return;}
			
			if(Loc.x < 0){Loc.x = 0;}
			if(Loc.x >= (1366 * Costanza.SCALE_FACTOR)){Loc.x = (1366 * Costanza.SCALE_FACTOR);}
			if(Loc.y < 0){Loc.y = 0;}
			if(Loc.y >= (768 * Costanza.SCALE_FACTOR)){Loc.y = (768 * Costanza.SCALE_FACTOR);}	
			
			Glitter_Grid.setPixel32(Math.floor(Loc.x), Math.floor(Loc.y), Glitter_Restricted_Color_Uint);
			
		}		
		private function Update_Glitter_Grid():void{
			
			if(DeviceInfo.isSlow){return;}		
			
			if(!GiltGo){GiltGo = true; return;}
			GiltGo = false;
			
			for(var g:int = 0; g < (3000 * Costanza.SCALE_FACTOR); g++){
				
				var gX:int = Math.floor(Math.random() * (1366 * Costanza.SCALE_FACTOR));
				var gY:int = Math.floor(Math.random() * (768 * Costanza.SCALE_FACTOR));					
				
				if(Glitter_Grid.getPixel32(gX, gY).toString(16) == Glitter_Restricted_Color_String){
					//trace(Glitter_Grid.getPixel32(gX, gY).toString(16));
					Spawn_Glitter(new Point(gX, gY));
				}
				
			}
			
		}
		private function Spawn_Glitter(p:Point):void{
			
			if(DeviceInfo.isSlow){return;}
			
			var gdistance:int = -(Math.random() * 3);
			var gSpeed:Number = ((Math.random() * 10) / 10);
			var gScale:Number = int((Math.random() * 3) + 1);
			
			var bitGlit:Bitmap 	= new Bitmap();
			bitGlit.bitmapData 	= new BitmapData(1, 1, false,  Get_Color_Alpha(0xFFFFFF, 256));
			
			bitGlit.scaleX = bitGlit.scaleY = gScale;
			bitGlit.x 			= p.x;
			bitGlit.y 			= p.y;
			bitGlit.name        = ("glit" +  GlitCounter.toString());
			bitGlit.smoothing	= true;
			Drawing_Canvas_Holder.addChildAt(bitGlit, 1);			
			GlitCounter++;			
			
			TweenMax.to(bitGlit, gSpeed, {alpha:0.1, scaleX:0.1, scaleY:0.1, y:(p.y + gdistance), rotation:360, onComplete:Glitter_Complete, onCompleteParams:[bitGlit.name]});			
			
		}
		private function Glitter_Complete(Name:String):void{
			
			if(DeviceInfo.isSlow){return;}
			
			if(Drawing_Canvas_Holder.getChildByName(Name) == null){return;}			
			var rBitmap:Bitmap = Bitmap(Drawing_Canvas_Holder.getChildByName(Name));
			TweenMax.killTweensOf(rBitmap);
			Drawing_Canvas_Holder.removeChild(rBitmap);
			rBitmap.bitmapData.dispose();
			rBitmap = null;
			
		}
		
		
		
		
		
		
		
		
		//Saving.
		private function Save_To_File():void{
			Pages_Icons_Assets.Update_Icon(Drawing_Canvas_Holder, (Current_Coloring_Page - 1));	
			Save_Glitter_Grid();
			Save_Canvas();
			Save_Stickers();	
			trace("Saving Completed");
		}
		private function Save_Glitter_Grid():void{			
			
			if(DeviceInfo.isSlow){return;}
			
			var png:PNGEncoder = new PNGEncoder();			
			var byteArray:ByteArray = png.encode(Glitter_Grid);
			var filename:String = ((Page_Pack + "_Glitter_" + Current_Coloring_Page.toString()) + ".png");
			var file:File = File.applicationStorageDirectory.resolvePath( filename );
			var wr:File = new File( file.nativePath );
			var stream:FileStream = new FileStream();
			stream.open(wr, FileMode.WRITE);
			stream.writeBytes ( byteArray, 0, byteArray.length );
			stream.close();	
			wr = null;
			file = null;
			filename = null;
			png = null;
			byteArray.clear();
			byteArray = null;	
			
			
		}
		private function Save_Canvas():void{
			var png:PNGEncoder = new PNGEncoder();			
			var byteArray:ByteArray = png.encode( Drawing_Canvas_Bitmap.bitmapData );
			var filename:String = ((Page_Pack + "_Page_" + Current_Coloring_Page.toString()) + ".png");
			var file:File = File.applicationStorageDirectory.resolvePath( filename );
			var wr:File = new File( file.nativePath );
			var stream:FileStream = new FileStream();
			stream.open(wr, FileMode.WRITE);
			stream.writeBytes ( byteArray, 0, byteArray.length );
			stream.close();	
			wr = null;
			file = null;
			filename = null;
			png = null;
			byteArray.clear();
			byteArray = null;	
		}
		private function Save_Stickers():void{
			var Sticker_XML:String;			
			Sticker_XML = "<Stickers>\n";			
			for(var X:int = 0; X < Stickers.length; X++){					
				Sticker_XML = Sticker_XML + "<ThisSticker>\n";				
				Sticker_XML = Sticker_XML + "<IdIndex>" + Stickers[X][4]   		+ "</IdIndex>" + "\n";
				Sticker_XML = Sticker_XML + "<pX>"  	+ Stickers[X][0].x 		+ "</pX>"  + "\n";
				Sticker_XML = Sticker_XML + "<pY>"  	+ Stickers[X][0].y 		+ "</pY>"  + "\n";			
				Sticker_XML = Sticker_XML + "<cS>"  	+ Stickers[X][5] 		+ "</cS>"  + "\n";
				Sticker_XML = Sticker_XML + "</ThisSticker>\n";				
			}			
			Sticker_XML = Sticker_XML + "</Stickers>";
			var FilePath:String = ((Page_Pack + "_Sticker_" + Current_Coloring_Page.toString()) + ".xml");
			var myFile:File;
			myFile = File.applicationStorageDirectory;
			myFile = myFile.resolvePath( FilePath );
			var stream:FileStream = new FileStream();
			stream.open( myFile , FileMode.WRITE);
			stream.writeUTFBytes(Sticker_XML.toString());
			stream.close();
			stream 		= null;
			myFile 		= null;
			FilePath 	= null;
			Sticker_XML = null;
		}
		
		
		
		
		
		
		
	}
}