package scenes.ColoringBook
{
	import com.cupcake.DeviceInfo;
	import com.greensock.events.LoaderEvent;
	import com.greensock.loading.ImageLoader;
	import com.greensock.loading.LoaderMax;
	import com.greensock.loading.display.ContentDisplay;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.display.StageQuality;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.utils.ByteArray;
	
	import utils.PNGEncoder;

	public class Page_Icon_Manager{
		
		private var Master_Loader:LoaderMax;
	
		private var Page_Icons:Array;
		private var Usable_Icons:Array;
		private var CallBack:Function;
		private var PagePrefix:String;
		
		private var MaskIconSaver:flash.display.Sprite;
		private var New_Icon_Base:Bitmap;
		
		//Load Page Icons and prep saved colored.
		public function Page_Icon_Manager(pCallBack:Function = null){
			Page_Icons			= new Array();
			Usable_Icons		= new Array();
			CallBack			= pCallBack;
			Master_Loader 		= new LoaderMax({onComplete:Loading_Completed, onError:errorHandler});
		}	
		
		public function Prep_Page_Icon_Manager(Core_Assets:External_PNG_Sheet_Manager, Prefix:String):void{
			
			var PageCount:int 				= 1;
			PagePrefix 						= Prefix;
			MaskIconSaver 					= Core_Assets.Get_Image("PageIconMask");
			MaskIconSaver.cacheAsBitmap 	= true;			
			
			//So Based on Prefix we check for saved page colored.
			for(var X:int = 0; X < PageCount; X++){
				
				var New_Page_Icon_Data:Array 			= new Array();
				
				//Icon Image.
				var PageIcon:flash.display.Sprite 		= Core_Assets.Get_Image("CBPageSel_" + Prefix + "_" + (X + 1).toString());	
				
			
				var d_Bitmap:Bitmap 					= new Bitmap();
				d_Bitmap.bitmapData 					= new BitmapData(PageIcon.width, PageIcon.height, true, 0x000000);
				d_Bitmap.bitmapData.draw(PageIcon);
				d_Bitmap.smoothing = true;
				Bitmap(PageIcon.getChildAt(0)).bitmapData.dispose();
				PageIcon.removeChildAt(0);
				PageIcon 								= null;		
				
				//Borders.
				var Icon_Border:flash.display.Sprite 	= Core_Assets.Get_Image("PageSelGlow");	
				
				//Loaded Icon.
				var Loaded_Icon:flash.display.Sprite  	= new flash.display.Sprite();				
				var IconSaved_Name:String 				= ("CBPageIconSaved_" + Prefix + "_" + (X + 1).toString() + ".png");
				Master_Loader.append(new ImageLoader(File.applicationStorageDirectory.resolvePath(IconSaved_Name).url, {container:Loaded_Icon}));	
				
				//Populate Array.
				New_Page_Icon_Data.push(d_Bitmap);
				New_Page_Icon_Data.push(Loaded_Icon);	
				New_Page_Icon_Data.push(Icon_Border);	
				Page_Icons.push(New_Page_Icon_Data);
				
				//Check if last one.
				if(Core_Assets.Image_Exists("CBPageSel_" + Prefix + "_" + (X + 2).toString())){PageCount = (PageCount + 1);}
				
			}

			//Load Loader.			
			Master_Loader.load(true);
			
		}
		private function errorHandler(e:LoaderEvent):void{	
			trace("Not Found");
		}
		private function Loading_Completed(e:LoaderEvent):void{	
			
			for(var X:int = 0; X < Page_Icons.length; X++){
				
				//Holder for are display.
				var CreatedIcon:flash.display.Sprite = new flash.display.Sprite();				
				
				//Update Image if we have one.
				if(Page_Icons[X][1].width != 0){					
					Bitmap(Page_Icons[X][0]).bitmapData.dispose();
					Bitmap(Page_Icons[X][0]).bitmapData = new BitmapData(Page_Icons[X][1].width, Page_Icons[X][1].height, true, 0x000000);
					Bitmap(Page_Icons[X][0]).bitmapData.draw(Page_Icons[X][1]);
					trace("Loaded");
				}
				
				//Ad dBg.
				CreatedIcon.addChild(Page_Icons[X][0]);				
				//Add Border.
				CreatedIcon.addChild(Page_Icons[X][2]);
				
				Usable_Icons.push(CreatedIcon);
			}			
			
			
			New_Icon_Base 				= new Bitmap();
			New_Icon_Base.bitmapData 	= new BitmapData(1, 1, true, 0x000000);	
			
			//Now tell root we are done.
			if(CallBack != null){CallBack();}			
		}		
		public function Update_Icon(Data:flash.display.Sprite, PageId:int):void{			
			
			if(DeviceInfo.isSlow){return;}
			
			var Wisth:int = 300;
			var heist:int = 226;
			
			//Set Scale.
			var ScaleX:Number = ((Wisth * Costanza.SCALE_FACTOR) / (1024 * Costanza.SCALE_FACTOR));
			var ScaleY:Number = ((heist * Costanza.SCALE_FACTOR) / (768 * Costanza.SCALE_FACTOR));
			
			//Create new image icon.
			var mat:Matrix 				= new Matrix();
			mat.scale(ScaleX, ScaleY);
			mat.translate(((-171 * Costanza.SCALE_FACTOR) * ScaleX), 0);
			
			New_Icon_Base.bitmapData.dispose();
			New_Icon_Base.bitmapData 	= new BitmapData((Wisth * Costanza.SCALE_FACTOR), (heist * Costanza.SCALE_FACTOR), true, 0x000000);			
			New_Icon_Base.bitmapData.drawWithQuality(Data, mat, null, null, null, true, StageQuality.BEST);
			if(!DeviceInfo.isSlow){New_Icon_Base.smoothing = true;}
			
			Usable_Icons[PageId].removeChild(Page_Icons[PageId][0]);			
			
			//Now we have a full blend.
			Bitmap(Page_Icons[PageId][0]).bitmapData.dispose();
			Bitmap(Page_Icons[PageId][0]).bitmapData = new BitmapData(New_Icon_Base.width, New_Icon_Base.height, true, 0x000000);
			Bitmap(Page_Icons[PageId][0]).bitmapData.copyPixels(New_Icon_Base.bitmapData, New_Icon_Base.bitmapData.rect, new Point(0, 0), Bitmap(MaskIconSaver.getChildAt(0)).bitmapData, new Point((-2 * Costanza.SCALE_FACTOR), (-2 * Costanza.SCALE_FACTOR)), true);
						
			//Smooth it out.
			Bitmap(Page_Icons[PageId][0]).smoothing = true;
			
			Usable_Icons[PageId].addChildAt(Page_Icons[PageId][0], 0);
			
			//save to file.
			Save_To_File(Bitmap(Page_Icons[PageId][0]).bitmapData, ("CBPageIconSaved_" + PagePrefix + "_" + (PageId + 1).toString() + ".png"));
			
		}		
		private function Save_To_File(Data:BitmapData, Name:String):void{
			
			var png:PNGEncoder 			= new PNGEncoder();			
			var byteArray:ByteArray 	= png.encode(Data);				
			var filename:String 		= Name;	
			var file:File 				= File.applicationStorageDirectory.resolvePath(filename);
			var wr:File 				= new File(file.nativePath);
			var stream:FileStream 		= new FileStream();			
			
			stream.open(wr, FileMode.WRITE);			
			stream.writeBytes(byteArray, 0, byteArray.length);			
			stream.close();	
			
			wr 			= null;
			file 		= null;
			filename 	= null;
			png 		= null;
			
			byteArray.clear();
			byteArray 	= null;	
			png 		= null;
			
		}		
		public function Purge_Assets():void{
			
			CallBack = null;
			
			for(var uI:int = 0; uI < Usable_Icons.length; uI++){				
				Bitmap(Usable_Icons[uI].getChildAt(0)).bitmapData.dispose();				
				Bitmap(Sprite(Usable_Icons[uI].getChildAt(1)).getChildAt(0)).bitmapData.dispose();
				Sprite(Usable_Icons[uI].getChildAt(1)).removeChildAt(0);
				Usable_Icons[uI] = null;				
				Usable_Icons.splice(uI, 1);
				uI--;
			}Usable_Icons.length = 0;
			
			for(var pI:int = 0; pI < Page_Icons.length; pI++){
				
				Bitmap(Page_Icons[pI][0]).bitmapData.dispose();
				Page_Icons[pI][0] = null;
				
				if(Page_Icons[pI][1].width != 0){
					com.greensock.loading.display.ContentDisplay(Page_Icons[pI][1].getChildAt(0)).dispose();
				}else{
					Page_Icons[pI][1] = null;
				}
				
				Page_Icons[pI][2] = null;
				
			}
			
		}
		public function Kill_Me():void{Master_Loader.dispose(true);}		
		
		
		//Values to use.
		public function getCount():int{return Usable_Icons.length;}
		public function getIcon(Index:int):flash.display.Sprite{return Usable_Icons[Index];}
		
	}
}