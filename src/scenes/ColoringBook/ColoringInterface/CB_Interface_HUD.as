package scenes.ColoringBook.ColoringInterface{
	
	import com.greensock.TweenMax;
	
	import flash.display.Sprite;
	import flash.geom.Point;
	
	import scenes.ColoringBook.External_PNG_Sheet_Manager;
	
	public class CB_Interface_HUD extends Sprite{
		
		//Props.
		private var Current_Mode:String;
		private var Current_Up:String;
		private var Current_Zoom:String;
		
		//Huds.
		private var More_Tab:CB_HUD_MoreTab;
		private var Bottom_Tab:CB_HUD_BottomTab;
		private var Zoom_Tab:CB_HUD_ZoomTab;
		private var Trash_Tab:CB_HUD_TrashTab;
		private var Yesno_Tab:CB_HUD_YesnoTab;
		
		public function CB_Interface_HUD(Core_Assets:External_PNG_Sheet_Manager, Colors:Vector.<uint>, Scaler_Assets:External_PNG_Sheet_Manager = null){
			
			super();			
			
			More_Tab 		= new CB_HUD_MoreTab(Core_Assets);		
			Bottom_Tab 		= new CB_HUD_BottomTab(Core_Assets, Colors, Scaler_Assets);			
			Trash_Tab 		= new CB_HUD_TrashTab(Core_Assets);
			Zoom_Tab 		= new CB_HUD_ZoomTab(Core_Assets);
			Yesno_Tab 		= new CB_HUD_YesnoTab(Core_Assets);	
			
			Yesno_Tab.visible = false;
			Current_Zoom = "Out";
			
			Trash_Tab.x 	= (((1366 * Costanza.SCALE_FACTOR) - (171 * Costanza.SCALE_FACTOR)) - (60 * Costanza.SCALE_FACTOR));
			Trash_Tab.y 	= ((Trash_Tab.height / 2) + (10 * Costanza.SCALE_FACTOR));	
			
			Zoom_Tab.x 		= (((1366 * Costanza.SCALE_FACTOR) - (171 * Costanza.SCALE_FACTOR)) - (60 * Costanza.SCALE_FACTOR));
			Zoom_Tab.y 		= ((Zoom_Tab.height / 2) + (10 * Costanza.SCALE_FACTOR));		
			
			Yesno_Tab.x 	= ((1366 * Costanza.SCALE_FACTOR) / 2);
			Yesno_Tab.y 	= ((768 * Costanza.SCALE_FACTOR) / 2);
			
			Current_Mode	= "Brush";
			
			this.addChild(More_Tab);
			this.addChild(Bottom_Tab);
			this.addChild(Zoom_Tab);
			
		}
		public function Destroy():void{
			
			TweenMax.killTweensOf(this);	
			
			this.removeChild(More_Tab);
			this.removeChild(Bottom_Tab);
			try{this.removeChild(Trash_Tab);}
			catch(error:Error){}
			try{this.removeChild(Yesno_Tab);}
			catch(error:Error){}	
			try{this.removeChild(Zoom_Tab);}
			catch(error:Error){}
			
			More_Tab.Destroy();
			Bottom_Tab.Destroy();
			Trash_Tab.Destroy();
			Zoom_Tab.Destroy();
			Yesno_Tab.Destroy();
			
		}
		
		
		//Inputs.
		public function Down(cPosition:Point):String{
			
			//Yesno Tabs return.
			if(Yesno_Tab.visible){
				switch(Yesno_Tab.Down(cPosition)){				
					case "Yes":{
						//Yesno_Tab.visible = false;
						//this.removeChild(Yesno_Tab);
						return "Yes";
					}break;
					case "No":{
						//Yesno_Tab.visible = false;
						//this.removeChild(Yesno_Tab);
						return "No";
					}break;
				}
				return "Nothing";
			}
			
			//More Tabs return.
			switch(More_Tab.Down(cPosition)){				
				case "Recycle":{
					Yesno_Tab.visible = true;
					this.addChild(Yesno_Tab);
					return "Recycle";
				}break;	
				case "Undo":{
					return "Undo";
				}break;	
				case "Camera":{
					return "Camera";
				}break;	
				case "More":{
					return "More";
				}break;	
				case "Plate":{
					return "Plate";
				}break;	
			}
			
			//Bottom Tabs return.
			switch(Bottom_Tab.Down(cPosition)){				
				case "Home":{
					return "Home";
				}break;	
				case "Page":{
					return "Page";
				}break;	
				case "Sticker":{					
					Current_Mode = "Sticker";
					return "Sticker";
				}break;	
				case "Brush":{
					Current_Mode = "Brush";
					return "Brush";
				}break;	
				case "Bucket":{
					Current_Mode = "Bucket";
					return "Bucket";
				}break;	
				case "Crayon":{
					Current_Mode = "Crayon";
					return "Crayon";
				}break;	
				case "Spray":{
					Current_Mode = "Spray";
					return "Spray";
				}break;	
				case "Chalk":{
					Current_Mode = "Chalk";
					return "Chalk";
				}break;	
				case "Eraser":{
					Current_Mode = "Eraser";
					return "Eraser";
				}break;	
				case "Color_Scroll":{
					return "Color_Scroll";
				}break;	
				case "Sticker_Scroll":{
					return "Sticker_Scroll";
				}break;
				case "Plate":{
					return "Plate";
				}break;	
				case "Scale_1":{
					return "Scale_1";
				}break;	
				case "Scale_2":{
					return "Scale_2";
				}break;	
				case "Scale_3":{
					return "Scale_3";
				}break;
			}
			
			//Zoom Tabs return.
			if(Current_Mode != "Sticker"){
				switch(Zoom_Tab.Down(cPosition)){				
					case "ZoomIn":{						
						return "ZoomIn";
					}break;	
					case "ZoomOut":{
						return "ZoomOut";
					}break;
				}
			}
			
			//Trash Tabs return.
			switch(Trash_Tab.Down(cPosition)){				
				case "TrashCan":{
					return "TrashCan";
				}break;
			}		
			
			return "Nothing";
			
		}
		public function Move(cPosition:Point, lPosition):String{
			
			Current_Up = "Nothing";
			
			//Bottom Tabs return.
			switch(Bottom_Tab.Move(cPosition, lPosition)){				
				case "Scrolling_Stickers":{
					return "Scrolling_Stickers";
				}break;	
				case "Scrolling_Colors":{
					return "Scrolling_Colors";
				}break;	
				case "Scale_1":{
					return "Scale_1";
				}break;	
				case "Scale_2":{
					return "Scale_2";
				}break;	
				case "Scale_3":{
					return "Scale_3";
				}break;
				
			}
			
			switch(Trash_Tab.Up(cPosition)){				
				case "TrashCan":{
					Current_Up = "TrashCan";
					return "TrashCan";
				}break;
			}
			
			return "Nothing";
		}
		
		public function Up(cPosition:Point):String{				
			
			//More Tabs return.
			switch(More_Tab.Up(cPosition)){				
				case "Recycle":{
					return "Recycle";
				}break;	
				case "Undo":{
					return "Undo";
				}break;	
				case "Camera":{
					return "Camera";
				}break;	
				case "More":{
					return "More";
				}break;	
				case "Plate":{
					return "Plate";
				}break;	
			}
			
			//Yesno Tabs return.			
			if(Yesno_Tab.visible){
				switch(Yesno_Tab.Down(cPosition)){				
					case "Yes":{
						Yesno_Tab.visible = false;
						this.removeChild(Yesno_Tab);
						return "Yes_Close";
					}break;
					case "No":{
						Yesno_Tab.visible = false;
						this.removeChild(Yesno_Tab);
						return "No_Close";
					}break;
				}
				return "Nothing";
			}
			
			//Bottom Tabs return.
			switch(Bottom_Tab.Up(cPosition)){				
				case "Home":{
					return "Home";
				}break;	
				case "Page":{
					return "Page";
				}break;	
				case "Sticker":{
					return "Sticker";
				}break;	
				case "Brush":{
					return "Brush";
				}break;	
				case "Bucket":{
					return "Bucket";
				}break;	
				case "Crayon":{
					return "Crayon";
				}break;	
				case "Spray":{
					return "Spray";
				}break;	
				case "Chalk":{
					return "Chalk";
				}break;	
				case "Eraser":{
					return "Eraser";
				}break;	
				case "Color_Scroll":{
					return "Color_Scroll";
				}break;	
				case "Sticker_Scroll":{
					return "Sticker_Scroll";
				}break;	
				case "Plate":{
					return "Plate";
				}break;	
			}
			
			//Zoom Tabs return.
			if(Current_Mode != "Sticker"){
				switch(Zoom_Tab.Up(cPosition)){				
					case "ZoomIn":{
						Current_Zoom = "In";
						return "ZoomIn";
					}break;	
					case "ZoomOut":{
						Current_Zoom = "Out";
						return "ZoomOut";
					}break;
				}
			}
			//Trash Tabs return.
			switch(Trash_Tab.Up(cPosition)){				
				case "TrashCan":{					
					return "TrashCan";
				}break;
			}		
			
			return "Nothing";
		}
		
		public function Get_Mode():String{
			return Current_Mode;
		}
		public function Get_Current_Up():String{
			return Current_Up;
		}
		public function Get_Selected_Color():int{
			return Bottom_Tab.GetData("Color");
		}
		public function Get_Selected_Scale():Number{
			return Bottom_Tab.GetData("Scale");
		}
		public function Get_Selected_Sticker():int{
			return Bottom_Tab.GetData("Sticker");
		}
		public function Get_Zoom():String{
			return Current_Zoom;
		}
		public function Hide_Bottom_Hud():void{
			Bottom_Tab.Opperate_Mechanism();
		}
		public function Is_Yes_NO():Boolean{
			if(Yesno_Tab.visible){
				return true;
			}else{
				return false;
			}
		}
		
		public function Show_Me():void{
			this.alpha = 1.0;
			//TweenMax.killTweensOf(this);			
			//TweenMax.to(this, 0.5, {alpha:1});
		}
		
		
		public function Show_Trash():void{		
			
			More_Tab.visible 	= false;
			Bottom_Tab.visible 	= false;			
			//Zoom_Tab.visible 	= false;					
			
			this.addChild(Trash_Tab);
			this.removeChild(Zoom_Tab);
			
		}
		public function Hide_Trash():void{
			
			this.removeChild(Trash_Tab);
			this.addChild(Zoom_Tab);
			
			More_Tab.visible 	= true;
			Bottom_Tab.visible 	= true;			
			//Zoom_Tab.visible 	= true;		
			
			Bottom_Tab.Visible_Scrolls();
			
			Show_Me();		
			
		}
		
		public function Reset_Zoom():void{
			Current_Zoom = "Out";
			Zoom_Tab.Update_State();
		}
		
	}
}