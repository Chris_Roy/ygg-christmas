package scenes.ColoringBook.ColoringInterface{
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.geom.Point;
	
	import scenes.ColoringBook.External_PNG_Sheet_Manager;
	
	public class CB_HUD_YesnoTab extends Sprite{		
		
		//Text
		private var Make_Text:CB_Text_Maker;
		private var Message_Text_1_Image:Bitmap;
		private var Message_Text_2_Image:Bitmap;
		private var Yes_Text_Image:Bitmap;
		private var No_Text_Image:Bitmap;
		
		//Buttons.
		private var BackPlate:InterfaceButton;	
		
		public function CB_HUD_YesnoTab(Core_Assets:External_PNG_Sheet_Manager){
			
			super();
			
			Make_Text 						= new CB_Text_Maker();
			Message_Text_1_Image 			= Make_Text.Create_a_Word("Are you sure you", 	0xfaa118, "Tastic", 50, 0xffffff);
			Message_Text_2_Image 			= Make_Text.Create_a_Word("want to erase all?", 0xfaa118, "Tastic", 50, 0xffffff);			
			Yes_Text_Image 					= Make_Text.Create_a_Word("Yes", 	0xfaa118, "Tastic", 70, 0xffffff);
			No_Text_Image 					= Make_Text.Create_a_Word("No", 	0xfaa118, "Tastic", 70, 0xffffff);
			
			BackPlate 						= new InterfaceButton(Core_Assets.Get_Image("CB_CA_YesNo_Box"));			
			
			Message_Text_1_Image.smoothing 	= true;
			Message_Text_2_Image.smoothing 	= true;
			Yes_Text_Image.smoothing 		= true;
			No_Text_Image.smoothing 		= true;
			
			Message_Text_1_Image.x 			= -(Message_Text_1_Image.width  / 2);
			Message_Text_1_Image.y 			= ((-120 * Costanza.SCALE_FACTOR));
			
			Message_Text_2_Image.x 			= -(Message_Text_2_Image.width  / 2);
			Message_Text_2_Image.y 			= ((-50 * Costanza.SCALE_FACTOR));
			
			Yes_Text_Image.x 				= ((-155 * Costanza.SCALE_FACTOR) - (Yes_Text_Image.width  / 2));
			Yes_Text_Image.y 				= (35 * Costanza.SCALE_FACTOR);
			
			No_Text_Image.x 				= ((145 * Costanza.SCALE_FACTOR) - (No_Text_Image.width  / 2));
			No_Text_Image.y 				= (35 * Costanza.SCALE_FACTOR);
			
			this.addChild(BackPlate);
			this.addChild(Message_Text_1_Image);
			this.addChild(Message_Text_2_Image);
			this.addChild(Yes_Text_Image);
			this.addChild(No_Text_Image);		
			
		}
		public function Destroy():void{
			
			this.removeChild(BackPlate);
			this.removeChild(Message_Text_1_Image);
			this.removeChild(Message_Text_2_Image);
			this.removeChild(Yes_Text_Image);
			this.removeChild(No_Text_Image);	
			
			Make_Text = null;
			Message_Text_1_Image.bitmapData.dispose();
			Message_Text_2_Image.bitmapData.dispose();
			Yes_Text_Image.bitmapData.dispose();
			No_Text_Image.bitmapData.dispose();
			Message_Text_1_Image 	= null;
			Message_Text_2_Image 	= null;
			Yes_Text_Image 			= null;
			No_Text_Image 			= null;
			
		}
		
		//Inputs.
		public function Down(MousePosition:Point):String{			
			if(Yes_Text_Image.hitTestPoint(MousePosition.x, MousePosition.y)){return "Yes";}	
			if(No_Text_Image.hitTestPoint(MousePosition.x, MousePosition.y)){return "No";}
			return "Nothing";			
		}
		public function Up(MousePosition:Point):String{			
			if(Yes_Text_Image.hitTestPoint(MousePosition.x, MousePosition.y)){return "Yes";}	
			if(No_Text_Image.hitTestPoint(MousePosition.x, MousePosition.y)){return "No";}			
			return "Nothing";			
		}	
		
		
		
	}
}