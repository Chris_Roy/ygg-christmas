package scenes.ColoringBook.ColoringInterface{

	import com.greensock.TweenMax;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.geom.Point;
	
	import scenes.ColoringBook.External_PNG_Sheet_Manager;
	
	public class CB_Zoom_Selection extends Sprite{		
		
		private var isActive:Boolean;
		private var Make_Text:CB_Text_Maker;		
		private var Text_Image:Bitmap;
		
		private var TLz:InterfaceButton;
		private var TRz:InterfaceButton;
		private var BLz:InterfaceButton;
		private var BRz:InterfaceButton;	
		private var TMz:InterfaceButton;
		private var BMz:InterfaceButton;	
		
		private var Background:Bitmap;
		private var Selection_Square:Bitmap;
		
		public function CB_Zoom_Selection(Core_Assets:External_PNG_Sheet_Manager){
			
			super();
			
			isActive				= false;
			Make_Text 				= new CB_Text_Maker();
			Text_Image 				= Make_Text.Create_a_Word("Select Location", 0xfaa118, "Tastic", 40, 0xffffff);
			Text_Image.smoothing 	= true
			Text_Image.x 			= -(Text_Image.width / 2) + (683 * Costanza.SCALE_FACTOR);
			Text_Image.y 			= -(Text_Image.height / 2) + (384 * Costanza.SCALE_FACTOR);
			Text_Image.visible   	= true;
			
			Background = new Bitmap();
			Background.bitmapData = new BitmapData((1366 * Costanza.SCALE_FACTOR), (768 * Costanza.SCALE_FACTOR), true, 0x000000);
			Background.bitmapData.fillRect(Background.bitmapData.rect, Get_Color_Alpha(0x8c8a8a));
			
			Selection_Square = new Bitmap();
			Selection_Square.bitmapData = new BitmapData((((1366 * Costanza.SCALE_FACTOR) - (Costanza.StageProportionScale * 2)) * 0.5), ((768 * Costanza.SCALE_FACTOR) * 0.5), true, 0x000000);
			Selection_Square.bitmapData.fillRect(Background.bitmapData.rect, Get_Color_Alpha(0x81f367));
			Selection_Square.visible = false;
			
			TLz  	= new InterfaceButton(Core_Assets.Get_Image("Mag_Glass_Static"),  Core_Assets.Get_Image("Mag_Glass_Static"));
			TRz  	= new InterfaceButton(Core_Assets.Get_Image("Mag_Glass_Static"),  Core_Assets.Get_Image("Mag_Glass_Static"));
			BLz  	= new InterfaceButton(Core_Assets.Get_Image("Mag_Glass_Static"),  Core_Assets.Get_Image("Mag_Glass_Static"));
			BRz  	= new InterfaceButton(Core_Assets.Get_Image("Mag_Glass_Static"),  Core_Assets.Get_Image("Mag_Glass_Static"));	
			TMz  	= new InterfaceButton(Core_Assets.Get_Image("Mag_Glass_Static"),  Core_Assets.Get_Image("Mag_Glass_Static"));
			BMz  	= new InterfaceButton(Core_Assets.Get_Image("Mag_Glass_Static"),  Core_Assets.Get_Image("Mag_Glass_Static"));	
			
			TLz.x   = (-427 * Costanza.SCALE_FACTOR) + (683 * Costanza.SCALE_FACTOR) + Costanza.StageProportionScale - (TLz.width / 2);
			TLz.y   = (-192 * Costanza.SCALE_FACTOR) + (384 * Costanza.SCALE_FACTOR);
			
			TRz.x   = (427 * Costanza.SCALE_FACTOR) + (683 * Costanza.SCALE_FACTOR) - Costanza.StageProportionScale + (TRz.width / 2);
			TRz.y   = (-192 * Costanza.SCALE_FACTOR) + (384 * Costanza.SCALE_FACTOR);
			
			BLz.x   = (-427 * Costanza.SCALE_FACTOR) + (683 * Costanza.SCALE_FACTOR) + Costanza.StageProportionScale - (BLz.width / 2);
			BLz.y   = (192 * Costanza.SCALE_FACTOR) + (384 * Costanza.SCALE_FACTOR);
			
			BRz.x   = (427 * Costanza.SCALE_FACTOR) + (683 * Costanza.SCALE_FACTOR) - Costanza.StageProportionScale + (BRz.width / 2);
			BRz.y   = (192 * Costanza.SCALE_FACTOR) + (384 * Costanza.SCALE_FACTOR);
			
			TMz.x   = (683 * Costanza.SCALE_FACTOR);
			TMz.y   = (-192 * Costanza.SCALE_FACTOR) + (384 * Costanza.SCALE_FACTOR);
			
			BMz.x   = (683 * Costanza.SCALE_FACTOR);
			BMz.y   = (192 * Costanza.SCALE_FACTOR) + (384 * Costanza.SCALE_FACTOR);
			
			this.addChild(Background);
			this.addChild(Selection_Square);
			this.addChild(TLz);
			this.addChild(TRz);
			this.addChild(BLz);
			this.addChild(BRz);
			this.addChild(TMz);
			this.addChild(BMz);
			this.addChild(Text_Image);
			
		}
		public function Destroy():void{
			
			TweenMax.killTweensOf(Selection_Square);
			
			Make_Text = null;
			
			this.removeChild(Background);
			this.removeChild(Selection_Square);
			this.removeChild(Text_Image);
			this.removeChild(TLz);
			this.removeChild(TRz);
			this.removeChild(BLz);
			this.removeChild(BRz);
			this.removeChild(TMz);
			this.removeChild(BMz);
			
			TLz.Destroy();
			TRz.Destroy();
			BLz.Destroy();
			BRz.Destroy();
			TMz.Destroy();
			BMz.Destroy();
			
			Text_Image.bitmapData.dispose();
			Text_Image = null;
			
			Background.bitmapData.dispose();
			Background = null;
			
			Selection_Square.bitmapData.dispose();
			Selection_Square = null;
			
		}
		//Inputs.
		public function Down(MousePosition:Point):String{			
			
			if(TLz.Down(MousePosition)){SelectionOn("TL");return "TL";}
			if(TRz.Down(MousePosition)){SelectionOn("TR");return "TR";}
			if(BLz.Down(MousePosition)){SelectionOn("BL");return "BL";}
			if(BRz.Down(MousePosition)){SelectionOn("BR");return "BR";}
			if(TMz.Down(MousePosition)){SelectionOn("TM");return "TM";}
			if(BMz.Down(MousePosition)){SelectionOn("BM");return "BM";}
			
			return "Nothing";
		}
		public function Up(MousePosition:Point):String{			
			
			TLz.Reset();
			TRz.Reset();
			BLz.Reset();
			BRz.Reset();
			TMz.Reset();
			BMz.Reset();
			
			if(Selection_Square.visible){				
				TweenMax.killTweensOf(Selection_Square);			
				TweenMax.to(Selection_Square, 0.5, {alpha:0, onComplete:SelectionOff});		
			}
			
			if(TLz.Up(MousePosition)){return "TL";}
			if(TRz.Up(MousePosition)){return "TR";}
			if(BLz.Up(MousePosition)){return "BL";}
			if(BRz.Up(MousePosition)){return "BR";}
			if(TMz.Up(MousePosition)){return "TM";}
			if(BMz.Up(MousePosition)){return "BM";}
			
			return "Nothing";
		}
		public function Activate():void{isActive = true;}
		public function Deactivate():void{isActive = false;}
		public function Get_Active():Boolean{return isActive;}
		private function SelectionOn(Section:String):void{
			switch(Section){
				case "TL":{
					Selection_Square.x = Costanza.StageProportionScale;
					Selection_Square.y = 0;
				}break;
				case "TR":{
					Selection_Square.x = ((1366 * Costanza.SCALE_FACTOR) - Costanza.StageProportionScale) - Selection_Square.width;
					Selection_Square.y = 0;
				}break;
				case "TM":{
					Selection_Square.x = ((1366 * Costanza.SCALE_FACTOR) - Costanza.StageProportionScale) - Selection_Square.width - (Selection_Square.width / 2);
					Selection_Square.y = 0;
				}break;
				case "BL":{
					Selection_Square.x = Costanza.StageProportionScale;
					Selection_Square.y = Selection_Square.height;
				}break;
				case "BR":{
					Selection_Square.x = ((1366 * Costanza.SCALE_FACTOR) - Costanza.StageProportionScale) - Selection_Square.width;
					Selection_Square.y = Selection_Square.height;
				}break;
				case "BM":{
					Selection_Square.x = ((1366 * Costanza.SCALE_FACTOR) - Costanza.StageProportionScale) - Selection_Square.width - (Selection_Square.width / 2);
					Selection_Square.y = Selection_Square.height;
				}break;
			}
			Selection_Square.visible = true;
			TweenMax.killTweensOf(Selection_Square);			
			TweenMax.to(Selection_Square, 0.5, {alpha:1});
		}
		private function SelectionOff():void{
			TweenMax.killTweensOf(Selection_Square);
			Selection_Square.visible = false;
		}
		private function Get_Color_Alpha(Col:uint):uint{
			
			//Bit Shift store color single values for editing if needed.
			var r:uint = Col >> 16 & 0xFF;
			var g:uint = Col >>  8 & 0xFF;
			var b:uint = Col >>  0 & 0xFF;
			
			//Random Alpha value cap at 180.
			var a:uint = 120;
			
			//Final Color Merge.
			var argb:uint = a << 24 | r << 16 | g << 8 | b;			
			
			return argb;
		}
	}
}