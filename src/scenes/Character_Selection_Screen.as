﻿package scenes{
	
	import com.cupcake.Utils;
	import com.greensock.TweenMax;
	
	import flash.filesystem.File;
	import flash.geom.Point;
	import flash.system.System;
	
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.extensions.SoundManager;
	import starling.utils.AssetManager;

	public class Character_Selection_Screen extends Sprite {
	
		//Bubble Events.
		public static const SET_CHARACTER:String 	= "setCharacter";
		public static const LOAD_ROOM:String 		= "loadRoom";
		public static const LOAD_MENU:String 		= "loadMenu";

		//Core.
		private var soundmanager:SoundManager;
		private var assets:AssetManager;
		private var sceneRoot:Sprite;
		private var assetsLoaded:Boolean 			= false;
		private var appDir:File 					= File.applicationDirectory;
		
		//Scene assets.
		private var Background:Image;
		private var Back_Button:Button;

		//Scene vars.
		private var canTap:Boolean 	= true;
		private var lanceTaps:int 	= 1;
		private var charPositions:Array 			= [new Point(259,330), new Point(401,349), new Point(580,353), new Point(771,393), new Point(961,360)];
		private var glowPositions:Array 			= [new Point(255,326), new Point(397,345), new Point(575,349), new Point(766,388), new Point(957,357)];
		private var characters:Array 				= [];
		private var glows:Array 					= [];
		private var charStrings:Array 				= ["Muno","Foofa","Plex","Brobee","Toodee"];

		public function Character_Selection_Screen(charString:String = "", foodString:String = ""){
			assets = Utils.CreateAssetManager([
				"textures/%SCALE%x/lobby",
				"audio/lobby"
			]);

			Root.currentAssetsProxy = assets;

			assets.loadQueue(function onProgress(ratio:Number):void
			{
				if (ratio == 1){
					Starling.juggler.delayCall(function():void
					{
						// now would be a good time for a clean-up 
						System.pauseForGCIfCollectionImminent(0);
						System.gc();
						
					}, 0.15);
					addObjects();
				};
			});
		}

		private function addObjects():void{
			//Add Dispose Event.
			addEventListener(Root.DISPOSE,  disposeScene);			

			//Create Scene.
			sceneRoot 			= new Sprite();
			sceneRoot.x 		= Costanza.STAGE_OFFSET;
			addChild(sceneRoot);	

			//Create Sounds.
			soundmanager = new SoundManager();
			Root.AddSM( soundmanager );

			//SFX.
			soundmanager.addSound("Music_Character_Selection",	assets.getSound("Music_Character_Selection"));
			soundmanager.addSound("Select_1",					assets.getSound("Select_1"));
			soundmanager.addSound("Select_2",					assets.getSound("Select_2"));
			soundmanager.addSound("Select_3",					assets.getSound("Select_3"));
			soundmanager.addSound("Select_4",					assets.getSound("Select_4"));
			soundmanager.addSound("Select_5",					assets.getSound("Select_5"));
			soundmanager.addSound("LanceVO",					assets.getSound("LanceVO"));
			soundmanager.addSound("Coloring",					assets.getSound("Coloring"));
			soundmanager.addSound("Video",						assets.getSound("Video"));
			soundmanager.addSound("Keyboard",					assets.getSound("Keyboard"));
			soundmanager.addSound("Lance_Tap",					assets.getSound("Lance_Tap"));
			soundmanager.addSound("Back",						assets.getSound("Back_Button"));

			//Play music.
			soundmanager.playSound("Music_Character_Selection", Costanza.musicVolume,	999);

			//Background.
			Background 			= new Image(assets.getTexture("BG"));
			Background.x 		= 0;
			Background.y 		= 0;
			sceneRoot.addChild(Background);

			//Back button.
			Back_Button 		= new Button(assets.getTexture("Back_Arrow"));

			trace((Costanza.VIEWPORT_HEIGHT / Costanza.VIEWPORT_WIDTH));
			if((Costanza.VIEWPORT_HEIGHT / Costanza.VIEWPORT_WIDTH) < .75){
				if((Costanza.VIEWPORT_HEIGHT / Costanza.VIEWPORT_WIDTH) < .6){
					Back_Button.x 		= 10;
					Back_Button.y 		= 10;
				}else{
					Back_Button.x 		= 180;
					Back_Button.y 		= 10;
				}
			}else{
				Back_Button.x 		= 180;
				Back_Button.y 		= 10;
			}

			Back_Button.addEventListener(starling.events.Event.TRIGGERED,goBack);

			//Create character selection.
			for(var i:int = 1; i <= 5; i++){
				//Character glow.
				var glow:Image 	= new Image(assets.getTexture(i.toString() + "_Over"));
				glow.x 			= glowPositions[i-1].x;
				glow.y 			= glowPositions[i-1].y;
				glow.alpha 		= 0;
				sceneRoot.addChild(glow);

				//character frame.
				var img:Image 	= new Image(assets.getTexture(i.toString()));
				img.x 			= charPositions[i-1].x;
				img.y 			= charPositions[i-1].y;
				img.name 		= i.toString();
				img.addEventListener(TouchEvent.TOUCH, tapChar);
				sceneRoot.addChild(img);

				//Populate.
				characters.push(img);
				glows.push(glow);
			}

			//Lance hitbox.
			var hitbox:Quad 		= new Quad(Costanza.STAGE_WIDTH/1.5, Costanza.STAGE_HEIGHT/3);
			hitbox.x 				= 250;
			hitbox.y 				= 0;
			hitbox.alpha 			= 0;
			sceneRoot.addChild(hitbox);
			hitbox.addEventListener(TouchEvent.TOUCH, tapLance);			

			//Video hitbox.
			var videoBtn:Button 	= new Button(assets.getTexture("Video"));
			videoBtn.x 				= 300;
			videoBtn.y 				= (Costanza.STAGE_HEIGHT - videoBtn.height);
			videoBtn.addEventListener(starling.events.Event.TRIGGERED, tapVideo);
			sceneRoot.addChild(videoBtn);

			//Color hitbox.
			var Color_button:Button = new Button(assets.getTexture("Color_Button"));
			Color_button.x 			= 900;
			Color_button.y 			= (Costanza.STAGE_HEIGHT - Color_button.height);
			Color_button.addEventListener(starling.events.Event.TRIGGERED, tapColor);
			sceneRoot.addChild(Color_button);

			//Music hitbox.
			//var Music_button:Button = new Button(assets.getTexture("Music_Button"));
			//Music_button.x 			= 600;
			//Music_button.y 			= (Costanza.STAGE_HEIGHT - Music_button.height);
			//Music_button.addEventListener(starling.events.Event.TRIGGERED, tapMusic);
			//sceneRoot.addChild(Music_button);	

			//Add back button on top of em all.
			sceneRoot.addChild(Back_Button);

			//Startup lance.
			TweenMax.delayedCall(2, Play_Lance_Welcome);			

			//SCene is loaded.
			dispatchEventWith(Root.SCENE_LOADED, true);

			//Mouse event.
			//addEventListener(starling.events.TouchEvent.TOUCH, 	ActivitySelection_Mouse_Scene);
		}		

		//Lance Sounds.
		private function Play_Lance_Welcome():void{
			if(!soundmanager.soundIsPlaying("LanceVO")){
				soundmanager.playSound("LanceVO", Costanza.voiceVolume);
			}
		}

		private function Stop_Lance_Welcome():void{
			TweenMax.killDelayedCallsTo(Play_Lance_Welcome);
			if(soundmanager.soundIsPlaying("LanceVO")){				
				soundmanager.stopSound("LanceVO");
			}
			if(soundmanager.soundIsPlaying("Lance_Tap")){				
				soundmanager.stopSound("Lance_Tap");
			}
		}

		private function tapLance(e:TouchEvent):void{
			var touches:Vector.<Touch> = e.getTouches(this, TouchPhase.BEGAN);			
			if(touches.length == 1 && !soundmanager.soundIsPlaying("LanceVO") && canTap && !soundmanager.soundIsPlaying("Lance_Tap")){	
				soundmanager.playSound("Lance_Tap", Costanza.voiceVolume);
			}
		}

		//Buttons functionalities.
		private function goBack(e:starling.events.Event):void{			
			if(canTap){				
				Stop_Lance_Welcome();	
				soundmanager.playSound("Back");
				TweenMax.delayedCall(1.3, dispatchEventWith,[Root.LOAD_ROOM, true, {room:0, Character:"Mira"}]);				
				canTap = false;				
			}			
		}		
		private function tapVideo(e:starling.events.Event):void{			
			if (canTap){				
				Stop_Lance_Welcome();		
				soundmanager.playSound("Video", Costanza.soundVolume);					
				TweenMax.delayedCall(1.3, dispatchEventWith,[Root.LOAD_ROOM, true, {room:2, Character:"Mira"}]);				
				canTap = false;				
			}			
		}

		private function tapMusic(e:starling.events.Event):void{
			return;
			if(canTap){
				Stop_Lance_Welcome();
				TweenMax.delayedCall(1.3, dispatchEventWith,[Root.LOAD_ROOM, true, {room:3, Character:"Mira"}]);
				canTap = false;	
			}
		}
		private function tapColor(e:starling.events.Event):void{
			if(canTap){
				Stop_Lance_Welcome();
				soundmanager.playSound("Coloring", Costanza.soundVolume);
				TweenMax.delayedCall(1.3, dispatchEventWith,[Root.LOAD_ROOM, true, {room:3, Character:"Mira"}]);
				canTap = false;
			}
		}
		private function tapChar(e:TouchEvent):void{
			//return;
			var touches:Vector.<Touch> = e.getTouches(this, TouchPhase.BEGAN);

			if(canTap && touches.length == 1){
				//Grab target.
				var img:Image = (e.currentTarget as Image);

				//Play character sound.
				Stop_Lance_Welcome();
				soundmanager.playSound("Select_" + img.name, Costanza.soundVolume);
				
				//Set Game Props.
				Root.Gaba_Core_Data.Current_Character = charStrings[(int(img.name) - 1)];

				//Go to room.
				TweenMax.to(glows[(int(img.name) - 1)], 1, {alpha:1, onComplete:startGame});
				
				//close tappable.
				canTap = false;
			}
		}

		//start game call.
		private function startGame():void{
			TweenMax.delayedCall(1.3, dispatchEventWith,[Root.LOAD_ROOM, true, {room:6, Character:"Mira"}]);
		}
		//Clean scene.
		private function disposeScene():void{
			soundmanager.stopAllSounds();
			Root.RemoveSM( soundmanager );
			soundmanager.dispose();
			assets.purge();
			assets.dispose();
			sceneRoot.removeChildren(0, (sceneRoot.numChildren - 1), true);
		}			
		//Mouse Update.
		private function ActivitySelection_Mouse_Scene(e:starling.events.TouchEvent):void{
			
			var Current_Touch:Touch = e.getTouch(sceneRoot);
			if(Current_Touch 		== null){return;}		
			var Location:Point 		= new Point(Current_Touch.globalX, Current_Touch.globalY);
			
			switch(Current_Touch.phase){				
				case "began":{	
					
					
				}break;
				
				case "moved":{
					
					//glows
					//characters
					
					//glows[4].x 	= Location.x;
					//glows[4].y 	= Location.y;
					//trace(Location);
					
					
				}break;
				case "ended":{	
					
					
				}break;
				
			}
			
		}		
	}
}