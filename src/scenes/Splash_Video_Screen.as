package scenes
{
	import com.cupcake.Utils;
	import com.greensock.TweenMax;
	
	import flash.desktop.NativeApplication;
	import flash.desktop.SystemIdleMode;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filesystem.File;
	
	import starling.core.Starling;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.extensions.SoundManager;
	import starling.utils.AssetManager;
	
	import utils.AIRMobileVideo;

	public class Splash_Video_Screen extends Sprite
	{
		private var playIndex:int					= 0;
		private var playing:Boolean					= true;	
		
		private var appDir:File						= File.applicationDirectory;
		private var assets:AssetManager;
		private var skipButton:Quad					= new Quad( 100, 100 );
		private var soundManager:SoundManager;
		private var stage:Stage;
		private var stageVideoPlayer:AIRMobileVideo;
		private var videoFiles:Vector.<String>;
		private var videoWidths:Vector.<String>;
		
		public function Splash_Video_Screen(dummy:String="")
		{
			videoFiles = Utils.CreateStringVector(Costanza.VIDEO_FILES);
			videoWidths = Utils.CreateStringVector(Costanza.VIDEO_WIDTHS);
			addEventListener(Root.DISPOSE, disposeScene);
			addObjects();
		}
		
		private function addObjects():void
		{
			NativeApplication.nativeApplication.systemIdleMode = SystemIdleMode.KEEP_AWAKE;
			stage = Starling.current.nativeStage;
			
			soundManager = SoundManager.getInstance();
			
			// try playing a video - we have to make stage3D invisible in order to see the StageVideo below
			Starling.current.stage3D.visible = false;
			//create video player
			stageVideoPlayer = new AIRMobileVideo(Starling.current.nativeStage, DonePlaying);			
			// Finished loading assets
			dispatchEventWith(Root.SCENE_LOADED, true);			
			addEventListener(starling.events.Event.ADDED_TO_STAGE, onAddedToStage);			
			TweenMax.delayedCall(1, PlayNextVideo);			
			stage.addEventListener(flash.events.Event.ACTIVATE, focusGained);
			stage.addEventListener( MouseEvent.MOUSE_DOWN, SkipTouched );
		}
		
		private function SkipTouched(e:MouseEvent):void
		{
			trace( "Touch registered, checking for skip" );
			if ( skipButton.bounds.contains( stage.mouseX, stage.mouseY ) )
			{
				trace( "Skipping video" );
				stageVideoPlayer.stopVideo();
				TweenMax.delayedCall( .15, DonePlaying );
			}
		}
		
		
		private function focusGained(e:flash.events.Event):void
			{ if(!Starling.current.stage3D.visible) { Starling.current.start(); } }
		
		private function DonePlaying():void
		{
			//stageVideoPlayer.stopVideo();
			playing = false;
			playIndex++;
			
			if ( playIndex >= videoFiles.length )
			{
				Starling.current.nativeStage.removeEventListener( MouseEvent.MOUSE_DOWN, SkipTouched );
				
				stageVideoPlayer.destroy();
				stageVideoPlayer = null;
				
				Starling.current.stage3D.visible = true;
				TweenMax.delayedCall(0.1, dispatchEventWith,[Root.LOAD_ROOM, true, {room:9, Character:"Mira"}]);
				//dispatchEventWith(Root.LOAD_LOBBY, true);
			}
			else { TweenMax.delayedCall(1, PlayNextVideo); }
		}	
		
		private function PlayNextVideo():void
			{ stageVideoPlayer.playVideo( "Videos/" + videoFiles[ playIndex ] , videoWidths[ playIndex ] ); playing = true; }
			
		private function onAddedToStage(event:starling.events.Event):void
		{
			// Finished loading assets
			dispatchEventWith(Root.SCENE_LOADED,true);
		}
		
		private function disposeScene():void
		{
			Starling.current.nativeStage.removeEventListener( MouseEvent.MOUSE_DOWN, SkipTouched );
			NativeApplication.nativeApplication.systemIdleMode = SystemIdleMode.NORMAL;
		}
	}
}