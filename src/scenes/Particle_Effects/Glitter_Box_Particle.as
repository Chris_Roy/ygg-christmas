package scenes.Particle_Effects{
	
	import com.greensock.TweenMax;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.textures.Texture;
	
	public class Glitter_Box_Particle extends Sprite{
		
		private var Particle_Texture:Texture;	
		private var Particle_Bounds:Rectangle;
		private var Particle_Counter:int;
		private var Particles_Active:Boolean;
		private var Particles_Rate:Number;
		private var Visual_Background:Quad;
		
		public function Glitter_Box_Particle(Glitter_Texture:Texture, Box_Size:Point){
			
			super();
			
			//Setup core.
			Particle_Texture 		= Glitter_Texture;	
			Particle_Bounds			= new Rectangle((-Box_Size.x / 2), (-Box_Size.y / 2), Box_Size.x, Box_Size.y);
			Particle_Counter		= 0;
			Particles_Rate			= 0.2;
			
			Visual_Background		= new Quad(Box_Size.x, Box_Size.y, 0x000000);
			Visual_Background.x		= -(Visual_Background.width  / 2);
			Visual_Background.y		= -(Visual_Background.height / 2);
			Visual_Background.alpha = 0.0;
			
			this.addChild(Visual_Background);
		}
		
		public function Dispose_Glitter_Box_Particle():void{
			
			//Clean background.
			this.removeChild(Visual_Background);
			Visual_Background.dispose();
			
			//Clean All.
			for(var X:int = 0; X < this.numChildren; X++){
				
				//Depopulate into vars.
				var Cur_Glitter_Holder:Sprite 	= Sprite(this.getChildAt(X));
				var Cur_Glitter_Image:Image 	= Image(Cur_Glitter_Holder.getChildAt(0));
				
				//Depopulate.
				TweenMax.killTweensOf(Cur_Glitter_Holder);
				this.removeChild(Cur_Glitter_Holder);
				Cur_Glitter_Holder.removeChild(Cur_Glitter_Image);
				
				//Clean.
				Cur_Glitter_Image.base.dispose();
				Cur_Glitter_Image.dispose();
				Cur_Glitter_Holder.dispose();
				
				//Null em.
				Cur_Glitter_Image 	= null
				Cur_Glitter_Holder 	= null;
				
			}
			
		}
		
		public function Set_Box_Bounds(bWidth:int, bHeight:int):void{
			
			//Bounds.
			Particle_Bounds.x 			= -(bWidth / 2);
			Particle_Bounds.y 			= -(bHeight / 2);
			Particle_Bounds.width 		= bWidth;
			Particle_Bounds.height 		= bHeight;

			//Background Box.
			Visual_Background.width		= bWidth;
			Visual_Background.height	= bHeight;
			Visual_Background.x			= -(Visual_Background.width  / 2);
			Visual_Background.y			= -(Visual_Background.height / 2);
			
		}
		public function Activate_Glitter_Box_Particle(Build_Vars:Array):void{
			
			//Set particles active.
			Particles_Active = true;
			
			//Set box size.
			Set_Box_Bounds(Build_Vars[3], Build_Vars[4]);
			
			//Set delay calls.
			TweenMax.killDelayedCallsTo(Deactivate_Glitter_Box_Particle);
			TweenMax.killDelayedCallsTo(Allocate_Particle);
			TweenMax.delayedCall((Build_Vars[0] + 2.0), Deactivate_Glitter_Box_Particle);
			TweenMax.delayedCall(Build_Vars[0], Allocate_Particle);			
			
		}
		public function Deactivate_Glitter_Box_Particle():void{
			Particles_Active = false;
		}		
		
		private function Allocate_Particle():void{
			
			switch(Particles_Active){				
				case true:{TweenMax.delayedCall(Particles_Rate, Create_Glitter);}break;				
			}
			
		}
		
		private function Create_Glitter():void{
			
			//Create.
			var New_Glitter_Holder:Sprite 	= new Sprite();
			var New_Glitter_Image:Image 	= new Image(Particle_Texture);		
			
			//Position.
			New_Glitter_Image.x 			= -(New_Glitter_Image.width  / 2);
			New_Glitter_Image.y 			= -(New_Glitter_Image.height / 2);				
			New_Glitter_Holder.x 			= (Particle_Bounds.x + (Math.random() * Particle_Bounds.width));
			New_Glitter_Holder.y 			= (Particle_Bounds.y + (Math.random() * Particle_Bounds.height));
			New_Glitter_Holder.name         = ("Particle_" + Particle_Counter++);
			
			//Populate.
			New_Glitter_Holder.addChild(New_Glitter_Image);
			this.addChild(New_Glitter_Holder);
			
			//Tween.
			var Animation_Time:Number 		= ((Math.random() * 10) / 10);
			var Animation_Distance:Number 	= (New_Glitter_Holder.y - (Math.random() * 5));
			var Animation_Rotation:Number 	= ((Math.random() * 35) * (Math.PI / 180));		
			var Animation_Scale:Number 		= (((Math.random() * 20) / 10) + 1.0);	
			TweenMax.to(New_Glitter_Holder,
						Animation_Time,
						{rotation:Animation_Rotation,
						 y:Animation_Distance,
						 scaleX:Animation_Scale,
						 scaleY:Animation_Scale,
						 alpha:0.0,
						 onComplete:Completed_Glitter,
						 onCompleteParams:[New_Glitter_Holder.name]});
			
			//Get Next one.
			Allocate_Particle();
			
		}		
		
		private function Completed_Glitter(Glitter_Name:String):void{			
			
			//Depopulate into vars.
			var Cur_Glitter_Holder:Sprite 	= Sprite(this.getChildByName(Glitter_Name));
			var Cur_Glitter_Image:Image 	= Image(Cur_Glitter_Holder.getChildAt(0));
			
			//Depopulate.
			TweenMax.killTweensOf(Cur_Glitter_Holder);
			this.removeChild(Cur_Glitter_Holder);
			Cur_Glitter_Holder.removeChild(Cur_Glitter_Image);
			
			//Clean.
			Cur_Glitter_Image.base.dispose();
			Cur_Glitter_Image.dispose();
			Cur_Glitter_Holder.dispose();
			
			//Null em.
			Cur_Glitter_Image 	= null
			Cur_Glitter_Holder 	= null;
			
		}		
		
	}
}