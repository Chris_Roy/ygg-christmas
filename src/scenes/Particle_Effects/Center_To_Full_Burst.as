package scenes.Particle_Effects{
	
	import com.greensock.TweenMax;
	
	import flash.geom.Point;
	
	import starling.display.Image;
	import starling.display.Sprite;
	
	public class Center_To_Full_Burst extends Sprite{
		
		//Blast particles.
		private var Particles_Blast_Burst:Array;
		
		public function Center_To_Full_Burst(){
			
			super();
			
			//Prep particles.
			Initialize_Particles_Blast();
			
		}
		public function Dispose_Center_To_Full_Burst():void{
			
			//Clean em.
			for(var X:int = 0; X < Particles_Blast_Burst.length; X++){				
				
				Dispose_A_Particle(Particles_Blast_Burst[X]);
				Particles_Blast_Burst.splice(X, 1);
				X--;	
			}
			
			//Dead.
			Particles_Blast_Burst.length = 0;
			
		}
		
		//Particle Blast section.
		private function Initialize_Particles_Blast():void{
			
			//Instantiate.
			Particles_Blast_Burst = new Array();
			
			//Create or prep a bunch of particles.
			for(var X:int = 0; X < 90; X++){
				Particles_Blast_Burst.push(Create_Particles_Blast());				
			}			
			
		}
		private function Create_Particles_Blast():Array{	
			
			//The image.
			var Particle_Name:String 		= ("Shape0" + Math.ceil(Math.random() * 4) + "_Snowflake_0001");
			var Particle_Holder:Sprite 		= new Sprite();
			var Particle_Image:Image 		= new Image(Root.assets.getTexture(Particle_Name));	
			
			//Position.
			Particle_Image.x 				= -(Particle_Image.width / 2);
			Particle_Image.y 				= -(Particle_Image.height / 2);		
			Particle_Holder.x 				= (1366 / 2);
			Particle_Holder.y 				= (768 / 2);
			
			//Populate.
			Particle_Holder.addChild(Particle_Image);
			
			//Tween props.			
			var Particle_Speed:Number 		= ((Math.random() * 4) + 2);
			var Particle_Distance:Number 	= ((Math.random() * 800) + 400);
			var Particle_Angle:Number 		= ((Math.random() * 360) * (Math.PI / 180));
			var Particle_Destination:Point 	= new Point((Particle_Holder.x + (Particle_Distance * Math.cos(Particle_Angle))),
														(Particle_Holder.y + (Particle_Distance * Math.sin(Particle_Angle))));					
			
			//Create array.
			var Blast_Props:Array = [Particle_Holder, Particle_Destination, Particle_Speed];		
			
			//Return array.
			return Blast_Props;
			
		}
		public function Activate_Particles_Blast():void{
			
			//Animate em.
			for(var X:int = 0; X < Particles_Blast_Burst.length; X++){
				this.addChild(Particles_Blast_Burst[X][0]);				
				TweenMax.to(Particles_Blast_Burst[X][0], Particles_Blast_Burst[X][2], {alpha:0.0, x:Particles_Blast_Burst[X][1].x, y:Particles_Blast_Burst[X][1].y});			
			}
			
		}		
		private function Dispose_A_Particle(Section:Array):void{
			
			//Create props.
			var Particle_Holder:Sprite 		= Section[0];
			var Particle_Image:Image 		= Image(Particle_Holder.getChildAt(0));
			
			//Cleanup.
			TweenMax.killTweensOf(Particle_Holder);		
			this.removeChild(Particle_Holder);	
			Particle_Holder.removeChild(Particle_Image);
			
			//Dispose.
			Particle_Holder.dispose();
			Particle_Image.base.dispose();
			Particle_Image.dispose();
			
			//Null.
			Particle_Holder = null;
			Particle_Image  = null;
			
		}
	}
}