package scenes.Particle_Effects{
	
	import com.greensock.TweenMax;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.textures.Texture;
	
	public class Burst_Item_Building_Particle extends Sprite{		
		
		//Bursts.
		private var Image_1:Image;
		private var Image_1_Holder:Sprite;
		
		//Visual Basl.
		private var Screen_Blast:Quad;
		
		//Blast particles.
		private var Particles_Blast_Burst:Array;
		
		//Return call.
		private var Call_Back:Function;
		
		private var curCharacter:String;
		
		public function Burst_Item_Building_Particle(The_Texture:Texture, Character:String){
			
			super();
			
			//Set Bounds.
			this.clipRect 					= new Rectangle(0, 0, 1366, 768);			
			
			//Create the burst images.
			Image_1 						= new Image(The_Texture);
			Image_1_Holder					= new Sprite();
			Image_1_Holder.addChild(Image_1);
			
			//Prep particles.
			Initialize_Particles_Blast();
			
			//Create Blast.
			Screen_Blast 					= new Quad(1366, 768, 0x66FFFF);
					
			switch(Character){
				
				case "Plex":{	Screen_Blast.color = 0x9c3449;}break;
				case "Muno":{	Screen_Blast.color = 0x66FFFF;}break;
				case "Foofa":{	Screen_Blast.color = 0xf9f022;}break;
				case "Brobee":{	Screen_Blast.color = 0xf26625;}break;
				case "Toodee":{	Screen_Blast.color = 0x5cbb59;}break;	
				case "Ending":{	Screen_Blast.color = 0x000000;}break;	
				
			}
			
		}
		
		public function Prep_Burst(Character:String):void
		{	
			curCharacter = Character;
			
			//Position bursts.
			Image_1_Holder.pivotX			= (Image_1_Holder.width  / 2);
			Image_1_Holder.pivotY			= (Image_1_Holder.height / 2);
			
			//Position.
			switch(Character){
				
				case "Muno":{
					Image_1_Holder.x 		= 450;
					Image_1_Holder.y 		= 250;	
				}break;
				
				case "Foofa":{
					Image_1_Holder.x 		= 450;
					Image_1_Holder.y 		= 250;	
				}break;
				
				case "Plex":{
					Image_1_Holder.x 		= 450;
					Image_1_Holder.y 		= 250;	
				}break;
				
				case "Brobee":{
					Image_1_Holder.x 		= 450;
					Image_1_Holder.y 		= 250;	
				}break;
				
				case "Toodee":{
					Image_1_Holder.x 		= 450;
					Image_1_Holder.y 		= 250;	
				}break;
				
				case "Ending":{
					Image_1_Holder.x 		= 683;
					Image_1_Holder.y 		= 350;	
				}break;
				
			}			
			
			//Setup Blast.
			Screen_Blast.alpha				= 1.0;			
			
		}
		
		public function Activate_Burst(Phase:String, CallBack:Function):void{
			
			//set CallBack.
			Call_Back = CallBack;		
			
			switch(Phase){
				
				case "Begin":{
					
					//Populate bursts.
					this.addChild(Screen_Blast);					
					this.addChild(Image_1_Holder);	
					Activate_Particles_Blast();
					
					//Kill tweens.
					TweenMax.killTweensOf(Image_1_Holder);					
					TweenMax.killTweensOf(Screen_Blast);
					
					Image_1_Holder.scaleX = 0.5;
					Image_1_Holder.scaleY = 0.5;
					
					//Setup Tweens.
					TweenMax.to(Image_1_Holder, 1, {alpha:0.0, scaleX:3.0, scaleY:3.0});					
					TweenMax.to(Screen_Blast, 1, {alpha:0.0});	
					
					//Finish call.
					TweenMax.delayedCall(1.5, Call_Back);
					
				}break;
				
				case "Finish":{
					
					Call_Back();
												
				}break;
				
			}		
			
		}
		
		public function Dispose_Me():void{
			
			//Clean particles.
			Dispose_Particles_Blast();
			
			//Kill tweens.
			TweenMax.killTweensOf(Image_1);			
			TweenMax.killTweensOf(Screen_Blast);
			
			//Depopopulate bursts.
			this.removeChild(Image_1_Holder);
			Image_1_Holder.removeChild(Image_1);			
			this.removeChild(Screen_Blast);
			
			//Clean Holder.
			Image_1_Holder.dispose();
			
			//Clean images.
			Image_1.base.dispose();		
			Image_1.dispose();			
			
			//Clean quad.
			Screen_Blast.dispose();
			
		}
		
		//Particle Blast section.
		private function Initialize_Particles_Blast():void{
			
			//Instantiate.
			Particles_Blast_Burst = new Array();
			
			//Create or prep a bunch of particles.
			for(var X:int = 0; X < 90; X++){
				Particles_Blast_Burst.push(Create_Particles_Blast());				
			}			
			
		}
		private function Create_Particles_Blast():Array{
			
			//The image.
			var Particle_Name:String 		= ("ygg-confetti_000" + Math.ceil(Math.random() * 20).toString());
			var Particle_Holder:Sprite 		= new Sprite();
			var Particle_Image:Image 		= new Image(Root.assets.getTexture(Particle_Name));	
			
			//Position.
			Particle_Image.x 				= -(Particle_Image.width / 2);
			Particle_Image.y 				= -(Particle_Image.height / 2);		
			Particle_Holder.x 				= 450;
			Particle_Holder.y 				= 250;
			if(curCharacter == "Ending")
			{
				Particle_Holder.x 				= 683;
				Particle_Holder.y 				= 350;
			}
			else
			{
				Particle_Holder.x 				= 450;
				Particle_Holder.y 				= 250;
			}
			
			//Populate.
			Particle_Holder.addChild(Particle_Image);
			
			//Tween props.			
			var Particle_Speed:Number 		= ((Math.random() * 1.5) + 1);
			var Particle_Distance:Number 	= ((Math.random() * 500) + 300);
			var Particle_Angle:Number 		= ((Math.random() * 360) * (Math.PI / 180));
			var Particle_Destination:Point 	= new Point((Particle_Holder.x + (Particle_Distance * Math.cos(Particle_Angle))),
														(Particle_Holder.y + (Particle_Distance * Math.sin(Particle_Angle))));					
			
			//Create array.
			var Blast_Props:Array = [Particle_Holder, Particle_Destination, Particle_Speed];		
			
			//Return array.
			return Blast_Props;
			
		}
		private function Activate_Particles_Blast():void{
			
			//Animate em.
			for(var X:int = 0; X < Particles_Blast_Burst.length; X++)
			{
				this.addChild(Particles_Blast_Burst[X][0]);				
				TweenMax.to(Particles_Blast_Burst[X][0], Particles_Blast_Burst[X][2], {alpha:0.0, x:Particles_Blast_Burst[X][1].x, y:Particles_Blast_Burst[X][1].y});			
			}
			
		}
		private function Dispose_Particles_Blast():void{
			
			//Clean em.
			for(var X:int = 0; X < Particles_Blast_Burst.length; X++){				
				
				Dispose_A_Particle(Particles_Blast_Burst[X]);
				Particles_Blast_Burst.splice(X, 1);
				X--;	
			}
			
			//Dead.
			Particles_Blast_Burst.length = 0;
			
		}
		private function Dispose_A_Particle(Section:Array):void{
			
			//Create props.
			var Particle_Holder:Sprite 		= Section[0];
			var Particle_Image:Image 		= Image(Particle_Holder.getChildAt(0));
			
			//Cleanup.
			TweenMax.killTweensOf(Particle_Holder);		
			this.removeChild(Particle_Holder);	
			Particle_Holder.removeChild(Particle_Image);
			
			//Dispose.
			Particle_Holder.dispose();
			Particle_Image.base.dispose();
			Particle_Image.dispose();
			
			//Null.
			Particle_Holder = null;
			Particle_Image  = null;
			
		}
		
	}
}