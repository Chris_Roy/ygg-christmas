package scenes
{
	import com.cupcake.App;
	import com.cupcake.DeviceInfo;
	import com.cupcake.Utils;
	import com.greensock.TweenMax;
	import com.greensock.easing.Bounce;
	import com.greensock.events.LoaderEvent;
	import com.greensock.loading.ImageLoader;
	import com.greensock.loading.LoaderMax;
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.system.System;
	
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.extensions.PDParticleSystem;
	import starling.extensions.SoundManager;
	import starling.filters.BlurFilter;
	import starling.textures.Texture;
	import starling.utils.AssetManager;
	
	import ui.Help;
	import ui.Settings;
	
	import utils.ForParentsScreen;
	import utils.MoreApps;
	
	public class Main_Menu_Screen extends starling.display.Sprite
	{
		private var soundManager:SoundManager;
		private var assets:AssetManager;

		private var settingsPopUp:Settings;

		private var Background:Image;
		private var Help_Screen:Help;

		//buttons
		private var Play_Button:Button;
		private var Parents:Button;
		private var Options:Button;
		private var Apps:Button;

		private var canTap:Boolean = true;

		private var sceneRoot:starling.display.Sprite;

		private var appsSparkle:PDParticleSystem;	
		private var appBtnSprite:flash.display.Sprite;
		private var appImageDisplay:Button;
		private var loader:LoaderMax;
		private var ldr:ImageLoader;

		private var coverLights:Array = new Array();

		public function Main_Menu_Screen(num:int)
		{
			assets = Utils.CreateAssetManager([
				"textures/%SCALE%x/menu",
				"audio/menu",
				"particles/MoreAppsSparkleTexture.png",
				"particles/MoreAppsSparkle.pex"
			]);

			Root.currentAssetsProxy = assets;

			assets.loadQueue(function onProgress(ratio:Number):void
			{
				if (ratio == 1){
					Starling.juggler.delayCall(function():void
					{
						// now would be a good time for a clean-up 
						System.pauseForGCIfCollectionImminent(0);
						System.gc();
					}, 0.15);
					init();
				};
			});
		}

		public function MoreAppsLoaded():void
		{
			if ( MoreApps.moreAppsLoaded && MoreApps.apps.length > 0 )
			{
				//Apps.visible		= appsTxt.visible = true;
				Apps.visible		= true;
				Apps.touchable		= true;
				Apps.alpha			= 0;
				TweenMax.to( Apps, 1, { alpha: 1 } );

				//Parents.visible	= parentsTxt.visible = true;
				Parents.visible		= true;
				Parents.touchable	= true;
				Parents.alpha		= 0;
				TweenMax.to( Parents, 1, { alpha: 1 } );

				TweenMax.delayedCall(3,loadSparklyAppButton);
			}
		}

		private function loadSparklyAppButton():void
		{
			loader = new LoaderMax( { onComplete:showSparklyAppButton } );
			ldr = new ImageLoader("app-storage:/" + MoreApps.apps[0].altFile );
			Costanza.Log("app-storage:/" + MoreApps.apps[0].altFile);
			loader.append( ldr ); 
			loader.load();
		}

		private function showSparklyAppButton(e:LoaderEvent):void
		{
			var bmd:Bitmap = ldr.rawContent;

			appImageDisplay			= new Button(Texture.fromBitmap(bmd));
			appImageDisplay.filter	= BlurFilter.createDropShadow();
			appImageDisplay.name	= "TossOut";
			appImageDisplay.x		= 460 + Costanza.STAGE_OFFSET;
			appImageDisplay.y		= 768;
			appImageDisplay.addEventListener(starling.events.Event.TRIGGERED, TapApps);
			sceneRoot.addChildAt(appImageDisplay,sceneRoot.getChildIndex(Apps)+1);
			TweenMax.to(appImageDisplay,.8,{y:640,delay:0.6,ease:Bounce.easeOut, onComplete:showSparkles});

			loader.dispose(true);
			loader = null;
		}

		private function showSparkles():void
		{
			if ( assets != null )
			{
				appsSparkle = new PDParticleSystem(assets.getXml("MoreAppsSparkle"), assets.getTexture("MoreAppsSparkleTexture"));
				sceneRoot.addChildAt(appsSparkle,sceneRoot.getChildIndex(appImageDisplay)+1);
				Starling.juggler.add(appsSparkle);

				appsSparkle.emitterX = appImageDisplay.x + appImageDisplay.width/2; 
				appsSparkle.emitterY = appImageDisplay.y + appImageDisplay.height; 
				appsSparkle.start();
			}
		}

		private function init():void
		{
			addEventListener(Root.DISPOSE,  disposeScene);

			sceneRoot = new starling.display.Sprite();
			addChild(sceneRoot);

			soundManager = new SoundManager();
			Root.AddSM( soundManager );
			//SFX.
			soundManager.addSound("apps", 		assets.getSound("apps button"));
			soundManager.addSound("settings", 	assets.getSound("options button"));
			soundManager.addSound("parents", 	assets.getSound("parents button"));
			soundManager.addSound("story", 		assets.getSound("story button"));			
			//Music.
			soundManager.addSound("Menu", 		assets.getSound("Menu_Music"));
			soundManager.playSound("Menu", 		Costanza.musicVolume,999);

			addEventListener(Settings.HIDE_SETTINGS,  HideSettings);

			MoreApps.Setup( MoreAppsLoaded, "en");

			Background 					= new Image(assets.getTexture("Menu_BG"));
			Background.x = Costanza.STAGE_OFFSET;

			Apps 						= new Button(assets.getTexture("Apps_BTN"),"",assets.getTexture("Apps_BTN"));
			Apps.alignPivot();
			Apps.x 						= (Costanza.STAGE_WIDTH / 2);
			Apps.y 						= Costanza.STAGE_HEIGHT - (Apps.height / 2)-2;
			Apps.addEventListener(starling.events.Event.TRIGGERED, TapApps);
			Apps.visible 				= false;
			Apps.touchable 				= false;

			Parents 					= new Button(assets.getTexture("Parents_BTN"),"",assets.getTexture("Parents_BTN"));
			Parents.alignPivot();
			Parents.x 					= 1002 + Costanza.STAGE_OFFSET;
			Parents.y 					= Costanza.STAGE_HEIGHT - (Parents.height / 2)-2;
			Parents.addEventListener(starling.events.Event.TRIGGERED, TapParents);
			Parents.visible 			= false;
			Parents.touchable 			= false;

			Options 					= new Button(assets.getTexture("Options_BTN"),"",assets.getTexture("Options_BTN"));
			Options.alignPivot();			
			Options.x 					= 365 + Costanza.STAGE_OFFSET;
			Options.y 					= Costanza.STAGE_HEIGHT - (Options.height / 2)-2;
			Options.addEventListener(starling.events.Event.TRIGGERED, TapOptions);

			settingsPopUp 				= new Settings();
			settingsPopUp.x 			= Costanza.STAGE_WIDTH/2 - settingsPopUp.width/2;
			settingsPopUp.y 			= Costanza.STAGE_HEIGHT/2 - settingsPopUp.height/2;
			settingsPopUp.visible 		= false;
			settingsPopUp.touchable 	= false;

			//Story Button
			Play_Button 				= new Button(assets.getTexture("Play_BTN"),"", assets.getTexture("Play_BTN"));
			Play_Button.alignPivot();
			Play_Button.x 				= (Costanza.STAGE_WIDTH / 2) + 12;
			Play_Button.y 				= (Costanza.STAGE_HEIGHT / 2) - 2;
			Play_Button.scaleWhenDown = 0.9;
			Play_Button.addEventListener(starling.events.Event.TRIGGERED, EnterStory);

			//Add everything to the stage
			sceneRoot.addChild(Background);
			sceneRoot.addChild(Play_Button);
			sceneRoot.addChild(Parents);
			sceneRoot.addChild(Apps);
			sceneRoot.addChild(Options);
			
			AddCoverLights();
			
			sceneRoot.addChild(settingsPopUp);

			

			Starling.current.nativeStage.addEventListener(flash.events.Event.ACTIVATE, FocusGained);
			dispatchEventWith(Root.SCENE_LOADED,true);
			App.backPressed.add( BackPressed );
			ForParentsScreen.closing.add( PopupClosing );
			MoreApps.closing.add( PopupClosing );		
		}

		private function AddCoverLights():void
		{
			var lightVector:Vector.<Texture>;
			var lightBulb:MovieClip;
			var increment:int;
			
			for(increment = 1; increment < 9; increment++)
			{
				lightVector = new Vector.<Texture>();

				lightVector.push(assets.getTexture("Light_" + increment.toString() + "_Off"));

				lightVector.push(assets.getTexture("Light_" + increment.toString() + "_On"));

				lightBulb = new MovieClip(lightVector, 2);
				Starling.juggler.add(lightBulb);
				lightBulb.play();
				lightBulb.name = "Light" + increment.toString();
				coverLights.push(lightBulb);
			}
			
			for(increment = 2; increment < 9; increment++)
			{
				lightVector = new Vector.<Texture>();
				
				lightVector.push(assets.getTexture("Light_" + (increment).toString() + "_Off"));
				
				lightVector.push(assets.getTexture("Light_" + (increment).toString() + "_On"));
				
				lightBulb = new MovieClip(lightVector, (2));
				Starling.juggler.add(lightBulb);
				lightBulb.scaleX = -1;
				lightBulb.play();
				lightBulb.name = "Light" + increment.toString();
				coverLights.push(lightBulb);
			}
			coverLights[0].x = 525+Costanza.STAGE_OFFSET; coverLights[0].y = 302; sceneRoot.addChild(coverLights[0]);
			coverLights[1].x = 425+Costanza.STAGE_OFFSET; coverLights[1].y = 329; sceneRoot.addChild(coverLights[1]);
			coverLights[2].x = 335+Costanza.STAGE_OFFSET; coverLights[2].y = 305; sceneRoot.addChild(coverLights[2]);
			coverLights[3].x = 219+Costanza.STAGE_OFFSET; coverLights[3].y = 295; sceneRoot.addChild(coverLights[3]);
			coverLights[4].x = 220+Costanza.STAGE_OFFSET; coverLights[4].y = 414; sceneRoot.addChild(coverLights[4]);
			coverLights[5].x = 246+Costanza.STAGE_OFFSET; coverLights[5].y = 511; sceneRoot.addChild(coverLights[5]);
			coverLights[6].x = 216+Costanza.STAGE_OFFSET; coverLights[6].y = 598; sceneRoot.addChild(coverLights[6]);
			coverLights[7].x = 201+Costanza.STAGE_OFFSET; coverLights[7].y = 714; sceneRoot.addChild(coverLights[7]);
			
			coverLights[8].x = 628+310+Costanza.STAGE_OFFSET; coverLights[8].y = 329; sceneRoot.addChild(coverLights[8]);
			coverLights[9].x = 717+310+Costanza.STAGE_OFFSET; coverLights[9].y = 305; sceneRoot.addChild(coverLights[9]);
			coverLights[10].x = 834+310+Costanza.STAGE_OFFSET; coverLights[10].y = 295; sceneRoot.addChild(coverLights[10]);
			coverLights[11].x = 836+310+Costanza.STAGE_OFFSET; coverLights[11].y = 414; sceneRoot.addChild(coverLights[11]);
			coverLights[12].x = 810+310+Costanza.STAGE_OFFSET; coverLights[12].y = 511; sceneRoot.addChild(coverLights[12]);
			coverLights[13].x = 840+310+Costanza.STAGE_OFFSET; coverLights[13].y = 598; sceneRoot.addChild(coverLights[13]);
			coverLights[14].x = 855+310+Costanza.STAGE_OFFSET; coverLights[14].y = 714; sceneRoot.addChild(coverLights[14]);
		}

		private function BackPressed():void
		{
			App.Log( "Handling back pressed in main menu" );
			if ( settingsPopUp.visible ) { HideSettings(); }
			else if ( this.contains( Help_Screen ) ) { Help_Screen.destroy(); }
			else if ( DeviceInfo.platform == DeviceInfo.PLATFORM_DESKTOP ) { App.ExitApp(); }
			else { App.CheckClose(); }
		}
		
		private function PopupClosing():void { App.backPressed.add( BackPressed ); }
		
		private function focusGained(e:flash.events.Event):void
		{
			Costanza.Log("FOCUS GAINED FROM MENU");
			if(!Starling.current.stage3D.visible) { Starling.current.stop(); }
		}
		
		private function EnterStory(e:starling.events.Event):void
		{
			Costanza.Log( "Menu.EnterStory()" );
			if( canTap )
			{
				canTap = false;
				Costanza.Log("Entering Story");
				
				if(soundManager.soundIsPlaying("Menu"))
				{
					soundManager.getSoundChannel("Menu").stop();
				}
				soundManager.playSound("story",Costanza.soundVolume);			
				TweenMax.delayedCall(0.3, dispatchEventWith,[Root.LOAD_ROOM, true, {room:8, Character:"Boom"}]);
			}
		}		
		
		private function FocusGained(e:flash.events.Event):void
		{
			//starling stage restarts when focus is lost and gained.
			//check to see if stage isnt visible, meaning for parents or more apps is visible.
			//If they are, stop the stage again manually, prevents starling buttons from working underneath more apps
			if(!Starling.current.stage3D.visible) { Starling.current.stop(); }
		}

		private function TapApps(e:starling.events.Event):void
		{
			Costanza.Log("TapApps: CanTap = " + canTap.toString());
			if ( canTap )
			{
				App.backPressed.remove( BackPressed );
				canTap = false;
				Starling.current.stop();
				var moreAppsHolder:flash.display.Sprite = new flash.display.Sprite();
				moreAppsHolder.x = 171 + Costanza.STAGE_OFFSET;
				Starling.current.nativeOverlay.addChild(moreAppsHolder);
				MoreApps.Show(moreAppsHolder);
				soundManager.playSound("apps",Costanza.soundVolume);
				TweenMax.delayedCall(2, function resetTap():void { canTap = true; });
			}
		}
		
		private function TapOptions(e:starling.events.Event):void
		{
			Costanza.Log("TapOptions: CanTap = " + canTap.toString()); 
			if ( canTap )
			{
				soundManager.playSound("settings",Costanza.soundVolume);
				HideSettings();
			}
		}
		
		private function TapParents(e:starling.events.Event):void
		{
			Costanza.Log("TapParents: CanTap = " + canTap.toString());
			if ( canTap )
			{
				App.backPressed.remove( BackPressed );
				canTap = false;
				soundManager.playSound("parents",Costanza.soundVolume);
				Starling.current.stop();
				var forParentsHolder:flash.display.Sprite = new flash.display.Sprite();
				forParentsHolder.x = 171 + Costanza.STAGE_OFFSET;
				Starling.current.nativeOverlay.addChild(forParentsHolder);
				ForParentsScreen.Show(forParentsHolder,this);
				TweenMax.delayedCall(2, function resetTap():void { canTap = true; });
			}
		}
		
		public function showHelpPage():void
		{
			Help_Screen = new Help(Root.assets.getTexture("Credits"), Root.assets.getTexture("Help"));
			sceneRoot.addChild(Help_Screen);
		}
		
		public function enableButtons():void
		{
			// Costanza.Log("Menu.enableButtons() called");
			this.parent.setChildIndex(this, this.numChildren-1);
			this.touchable = true;
		}
		
		private function HideSettings():void
		{
			settingsPopUp.visible 		= !settingsPopUp.visible;
			settingsPopUp.touchable 	= !settingsPopUp.touchable;
		
			Apps.touchable = Options.touchable = Parents.touchable = Play_Button.touchable = !settingsPopUp.visible;
			
			soundManager.setVolume("Menu",Costanza.musicVolume);
			soundManager.playSound("settings",Costanza.soundVolume);
		}
		
		private function disposeScene():void
		{
			Root.RemoveSM( soundManager );
			soundManager.dispose();
			if ( assets != null )
			{
				assets.purge();
				assets.dispose();
				assets = null;
			}
			
			Costanza.Log("MENU DISPOSE");
			if ( ldr != null )
			{
				ldr.cancel();
				ldr.dispose(true);
			}
			
			TweenMax.killTweensOf(appImageDisplay);	
			sceneRoot.removeChildren(0,sceneRoot.numChildren-1,true);
			
			App.backPressed.remove( BackPressed );
			ForParentsScreen.closing.remove( PopupClosing );
			MoreApps.closing.remove( PopupClosing );			
		}
		
	}
}