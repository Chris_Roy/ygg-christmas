package
{
	import com.cupcake.App;
	
	import flash.net.SharedObject;
	
	import feathers.text.BitmapFontTextFormat;
	
	import starling.errors.AbstractClassError;

	public class Costanza
	{
		public function Costanza() { throw new AbstractClassError(); }
		
		[Embed(source = "../assets/fonts/VAGRounded-Bold.otf",
			fontName = "VAGRounded",
			fontFamily="VAGRounded",
			mimeType = "application/x-font",
			advancedAntiAliasing = "true",
			embedAsCFF="false")]
		public static const VagRounded:Class;
		
		[Embed(source = "../assets/fonts/DancingSuperserif.ttf",
			fontName = "DancingSuperserif",
			fontFamily="DancingSuperserif",
			mimeType = "application/x-font",
			advancedAntiAliasing = "true",
			embedAsCFF="false")]
		public static const Dancing_Superserif:Class;
		
		[Embed(source = "../assets/fonts/Talbot Type - Kinghorn105-Heavy.otf",
			fontName = "Kinghorn",
			fontFamily="Kinghorn",
			mimeType = "application/x-font",
			advancedAntiAliasing = "true",
			embedAsCFF="false")]
		public static const Kinghorn:Class;
		
		//story font
	//	[Embed(source = "../assets/fonts/corbelb.ttf",
	//		fontName = "Corbel",
	//		fontFamily="Corbel",
	//		mimeType = "application/x-font",
	//		advancedAntiAliasing = "true",
	//		embedAsCFF="false")]
	//	public static const Corbel:Class;

		
		public static const MIN_PAGE:int				= 1;
		public static const MAX_PAGE:int				= 3;
		public static const NABI_OFFSET:int				= 30;
		public static const PAGE_MUSIC:Array			= [0,1,2];
		public static const PAGE_MUSIC_FOLDER:String	= "audio/PageMusic";
		public static const SPLASH_FOLDER:String		= "textures/%SCALE%x/splashes";
		public static const VIDEO_FILES:String			= "Cupcake_Splash.flv";
		public static const VIDEO_WIDTHS:String			= "1366";

		public static var IPAD_RETINA:Boolean 			= false;
		public static var STAGE_WIDTH:int  				= 1366;
		public static var STAGE_HEIGHT:int				= 768;
		public static var STAGE_OFFSET:int				= 0;
		public static var SCALE_FACTOR:int				= 1;

		//Visible regions.
		public static var VIEWPORT_WIDTH:int  			= 1024;
		public static var VIEWPORT_HEIGHT:int 			= 768;
		public static var VIEWPORT_OFFSET:int 			= 171;
		
		public static var SCREEN_START_X:int			= 171;
		public static var WIDESCREEN_WIDTH_OFFSET:int	= -171;
		
		public static var saveData:SharedObject;

		public static var mipmapsEnabled:Boolean		= false;
		public static var StageProportionScale:Number   = 1.0;

		public static var LABEL_TEXT_FORMAT:BitmapFontTextFormat;
		
		public static var lastNameEntered:String		= "";
		public static var lastPageRead:int				= 0;

		//sound stuff
		public static var musicVolume:Number			= 0.4;
		public static var musicVolumeMax:Number			= 0.4;
		public static var soundVolume:Number			= 1.0;
		public static var voiceVolume:Number			= 1.0;
		
		//SET TO TRUE FOR FINAL BUILDS
		public static var allowMultitouch:Boolean		= true;
		
		public static function Log( message:String, debug:Boolean = false ):void { App.Log( message, debug ); }
		
		public static function SetSharedData( sharedObjectName:String ):void
		{
			Costanza.saveData = SharedObject.getLocal(sharedObjectName);
			(saveData.data.musicVolume >= 0) ? Costanza.musicVolume = saveData.data.musicVolume : saveData.data.musicVolume = Costanza.musicVolume;
			(saveData.data.voiceVolume >= 0) ? Costanza.voiceVolume = saveData.data.voiceVolume : saveData.data.voiceVolume = Costanza.voiceVolume;
			(saveData.data.soundVolume >= 0) ? Costanza.soundVolume = saveData.data.soundVolume : saveData.data.soundVolume = Costanza.soundVolume;
			(saveData.data.lastNameEntered) ? Costanza.lastNameEntered = saveData.data.lastNameEntered : saveData.data.lastNameEntered = Costanza.lastNameEntered;
			(saveData.data.lastPageRead) ? Costanza.lastPageRead = saveData.data.lastPageRead : saveData.data.lastPageRead = Costanza.lastPageRead;
			saveData.flush();
		}
    }
}