package
{
	import com.cupcake.App;
	import flash.display.Sprite;

	[SWF(frameRate="60", backgroundColor="#000000")]
	
	public class YGGChristmas extends Sprite
	{
		public function YGGChristmas()
		{
			// Create your list of global assets here. 
			// Instances of %SCALE% will be replaced with Costanza.SCALE_FACTOR
			var assetFiles:Array = [
				"textures/%SCALE%x/Global",
				Costanza.SPLASH_FOLDER,
				"audio/Global",
				"fonts"
			];
			
			App.Setup( this, { sharedObjectName:"AppCoreData", globalAssets:assetFiles, testRetina:false } );
        }		
    }
}