package ui
{
	import flash.desktop.NativeApplication;
	import flash.filesystem.File;
	import flash.geom.Point;
	import flash.system.Capabilities;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.extensions.SoundManager;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.AssetManager;

	public class Help extends Sprite
	{
		private var helpAssets:AssetManager;
		private var helpAppDir:File = File.applicationDirectory;
		private var soundmanager:SoundManager;
		
		private var HelpImg:Image;
		private var CreditsImg:Image;
		private var buildText:TextField;
		private var closeBTN:Quad;
		
		private var closePosition1:Point = new Point(260 + Costanza.STAGE_OFFSET,640);
		private var closePosition2:Point = new Point(235 + Costanza.STAGE_OFFSET,650);
		
		public function Help(creditsImg:Texture, helpImg:Texture)
		{
			helpAssets = new AssetManager(Costanza.SCALE_FACTOR);
			Root.currentAssetsProxy = helpAssets;
			helpAssets.verbose = Capabilities.isDebugger;
			
			soundmanager = SoundManager.getInstance();
			soundmanager.addSound( "homeButton", Root.assets.getSound("homeButton") );
			
			helpAssets.useMipMaps = Costanza.mipmapsEnabled;
			helpAssets.enqueue(
				helpAppDir.resolvePath("textures/" + Costanza.SCALE_FACTOR + "x/Help/Credits.png"),
				helpAppDir.resolvePath("textures/" + Costanza.SCALE_FACTOR + "x/Help/Help.jpg")
			);
			
			//dispatchEventWith(HIDE_COMIC,true);
			
			helpAssets.loadQueue(function onProgress(ratio:Number):void
			{
				if (ratio == 1){
					addObjects();
				};
			});
			
			/*HelpImg = new Image(helpImg);
			this.addChild(HelpImg);
			
			var creditsBTN:Quad = new Quad(250,100);
			creditsBTN.alpha = 0;
			creditsBTN.x = 520 + Costanza.STAGE_OFFSET;
			creditsBTN.y = 40;
			this.addChild(creditsBTN);
			creditsBTN.addEventListener(TouchEvent.TOUCH, showCredits);

			CreditsImg = new Image(creditsImg);
			this.addChild(CreditsImg);
			CreditsImg.visible = false;
			CreditsImg.touchable = false;
			
			closeBTN = new Quad(100,100);
			closeBTN.alpha = 0;
			closeBTN.x = closePosition1.x;
			closeBTN.y = closePosition1.y;
			this.addChild(closeBTN);
			closeBTN.addEventListener(TouchEvent.TOUCH,closeTap);
			
			buildText = new TextField(200, 50, "ver. " + getAppVersion());
			buildText.x = 200;
			buildText.y = 675;
			buildText.visible = false;
			this.addChild(buildText);*/
		}
		
		private function addObjects():void
		{
			HelpImg = new Image(helpAssets.getTexture("Help"));
			this.addChild(HelpImg);
			
			var creditsBTN:Quad 	= new Quad(100, 100, 0x000000);
			creditsBTN.alpha 		= 0;
			creditsBTN.x 			= 480 + Costanza.STAGE_OFFSET;
			creditsBTN.y 			= 90;
			creditsBTN.name 		= "creditsBTN";
			this.addChild(creditsBTN);
			//creditsBTN.addEventListener(TouchEvent.TOUCH, showCredits);
			
			CreditsImg 				= new Image(helpAssets.getTexture("Credits"));
			this.addChild(CreditsImg);
			CreditsImg.x 			= Costanza.STAGE_OFFSET;
			CreditsImg.visible 		= false;
			CreditsImg.touchable 	= false;
			
			closeBTN 				= new Quad(100, 100, 0x000000);
			closeBTN.alpha 			= 0;
			closeBTN.x 				= 1089 + Costanza.STAGE_OFFSET;
			closeBTN.y 				= 3
			creditsBTN.name 		= "closeBTN";
			this.addChild(closeBTN);
			closeBTN.addEventListener(TouchEvent.TOUCH,closeTap);
			
			buildText = new TextField(200, 50, "ver. " + getAppVersion());
			buildText.x = 980;
			buildText.y = 635;
			buildText.visible = false;
			this.addChild(buildText);
			
			CreditsImg.visible 		= true;
			CreditsImg.touchable 	= true;
			buildText.visible 		= true;
			closeBTN.x 				= 1089 + Costanza.STAGE_OFFSET;
			closeBTN.y 				= 3
			
			//addEventListener(starling.events.TouchEvent.TOUCH, 	ActivitySelection_Mouse_Scene);
			
		}
		
		private function ActivitySelection_Mouse_Scene(e:starling.events.TouchEvent):void{
			
			var Current_Touch:Touch = e.getTouch(this);
			if(Current_Touch 		== null){return;}		
			var Location:Point 		= new Point(Current_Touch.globalX, Current_Touch.globalY);
			
			switch(Current_Touch.phase){				
				case "began":{	
					
					
				}break;
				
				case "moved":{
					
					//glows
					//characters
					
					this.getChildByName("closeBTN").x 	= Location.x;
					this.getChildByName("closeBTN").y 	= Location.y;
					trace(Location);
					
					
				}break;
				case "ended":{	
					
					
				}break;
				
			}
			
		}		

		private function showCredits(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(this);
			if(touch == null){return;}
			
			if(touch.phase == TouchPhase.BEGAN)
			{
				CreditsImg.visible 		= true;
				CreditsImg.touchable 	= true;
				buildText.visible 		= true;
				closeBTN.x 				= 1089 + Costanza.STAGE_OFFSET;
				closeBTN.y 				= 3
				//soundmanager.playSound("homeButton",Costanza.soundVolume);
			}
		}
		
		private function closeTap(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(this);
			if(touch == null){return;}
			
			if(touch.phase == TouchPhase.BEGAN)
			{
				if(CreditsImg.visible)
				{
					CreditsImg.visible 		= false;
					CreditsImg.touchable 	= false;
					buildText.visible 		= false;
					closeBTN.x 				= 1089 + Costanza.STAGE_OFFSET;
					closeBTN.y 				= 3;
					destroy();
					//soundmanager.playSound("homeButton",Costanza.soundVolume);
				}
				else
				{
					trace("Bacl");
					//soundmanager.playSound("homeButton",Costanza.soundVolume);
					//goBack
					destroy();
				}
			}
		}

		private function getAppVersion():String
		{
			var appXml:XML = NativeApplication.nativeApplication.applicationDescriptor;
			var ns:Namespace = appXml.namespace();
			var appVersion:String = appXml.ns::versionNumber[0];
			return appVersion;
		}

		public function destroy():void
		{
			helpAssets.purge();
			helpAssets.dispose();
			this.removeChildren(0,this.numChildren-1,true);
			this.parent.removeChild(this,true);
		}
	}
}