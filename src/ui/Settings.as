package ui
{
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.display.Quad;

	public class Settings extends Sprite
	{
		public static const HIDE_SETTINGS:String = "hideSettings";
		
		private var musicSlider:Image;
		private var soundSlider:Image;
		private var voiceSlider:Image;
		
		private var dragging:Boolean;
		
		private var startX:Number 		= 225;//min point for slider
		private var endX:Number 		= 599;//max point for slider
		private var barWidth:Number 	= endX-startX;

		private var ok_BTN:Quad;
		
		public function Settings()
		{
			var bg:Image 	= new Image(Root.assets.getTexture("Gabba"));
			bg.x 			= 0;
			bg.y 			= 0;
			this.addChild(bg);

			trace("music" + Costanza.musicVolume + "voice" + Costanza.voiceVolume + "sound" + Costanza.soundVolume);

			//music max is 40% of the others
			musicSlider 		= new Image(Root.assets.getTexture("VolumeDrag"));
			musicSlider.x 		= startX + (barWidth*(Costanza.musicVolume/Costanza.musicVolumeMax));
			musicSlider.y 		= 151;
			musicSlider.alignPivot();
			musicSlider.name 	= "music";

			soundSlider 		= new Image(Root.assets.getTexture("VolumeDrag"));
			soundSlider.x 		= startX + (barWidth*Costanza.soundVolume);
			soundSlider.y 		= musicSlider.y + musicSlider.height + 51;
			soundSlider.alignPivot();
			soundSlider.name 	= "sound";

			voiceSlider 		= new Image(Root.assets.getTexture("VolumeDrag"));
			voiceSlider.x 		= startX + (barWidth*Costanza.voiceVolume);
			voiceSlider.y 		= soundSlider.y + soundSlider.height + 50;
			voiceSlider.alignPivot();
			voiceSlider.name 	= "voice";

			this.addChild(musicSlider);
			this.addChild(soundSlider);
			this.addChild(voiceSlider);

			//ok button
			ok_BTN 				= new Quad(240, 68, 0xFFFFFF);
			ok_BTN.x 			= this.width/2 - ok_BTN.width/2;
			ok_BTN.y 			= this.height - 105;
			ok_BTN.alpha		= 0.0;
			this.addChild(ok_BTN);
			ok_BTN.addEventListener(TouchEvent.TOUCH, closeSettings);

			musicSlider.addEventListener(TouchEvent.TOUCH,dragSlider);
			soundSlider.addEventListener(TouchEvent.TOUCH,dragSlider);
			voiceSlider.addEventListener(TouchEvent.TOUCH,dragSlider);
			
		}//end constructor

		private function closeSettings(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(this);
			if(touch == null){return;}
			if(touch.phase == TouchPhase.BEGAN)
			{
				trace("hide");
				trace(Costanza.saveData.data.soundVolume);
				//saves values to sharedobject
				Costanza.saveData.data.musicVolume = Costanza.musicVolume;
				Costanza.saveData.data.voiceVolume = Costanza.voiceVolume;
				Costanza.saveData.data.soundVolume = Costanza.soundVolume;
				
				Costanza.saveData.flush();
				
				trace("music" + Costanza.musicVolume + "voice" + Costanza.voiceVolume + "sound" + Costanza.soundVolume);
				
				dispatchEventWith(HIDE_SETTINGS, true);
				trace(Costanza.saveData.data.soundVolume);
			}
		}
		
		private function dragSlider(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(this);
			if(touch == null){return;}
			var mc:Image = e.currentTarget as Image;
			
			if(touch.phase == TouchPhase.BEGAN)
			{
				dragging = true;
			}
			else if(touch.phase == TouchPhase.MOVED)
			{
				if(dragging)
				{
					mc.x = touch.globalX - this.x;
					if(mc.x > endX){mc.x = endX;}
					if(mc.x < startX){mc.x = startX}
				}
			}
			else if(touch.phase == TouchPhase.ENDED)
			{
				dragging = false;
				setVolume(mc);
			}
		}//end dragSlider
		
		private function setVolume(mc:Image):void
		{
			switch(mc.name)
			{
				case "music":
				{
					Costanza.musicVolume = ((mc.x - startX)/barWidth)*Costanza.musicVolumeMax;
					if(mc.x == startX){Costanza.musicVolume=0;}
					break;
				}
				case "sound":
				{
					Costanza.soundVolume = (mc.x - startX)/barWidth;
					if(mc.x == startX){Costanza.soundVolume=0;}
					break;
				}
				case "voice":
				{
					Costanza.voiceVolume = (mc.x - startX)/barWidth;
					if(mc.x == startX){Costanza.voiceVolume=0;}
					break;
				}
			}
		}
	}
}