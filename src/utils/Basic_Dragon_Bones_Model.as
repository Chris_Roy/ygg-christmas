package utils{		
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import dragonBones.Armature;
	import dragonBones.Bone;
	import dragonBones.animation.WorldClock;
	import dragonBones.factorys.StarlingFactory;
	import dragonBones.objects.ObjectDataParser;
	import dragonBones.objects.SkeletonData;
	import dragonBones.textures.StarlingTextureAtlas;

	import starling.display.Image;
	import starling.display.Sprite;
	import starling.textures.Texture;
	import starling.utils.AssetManager;
	
	public class Basic_Dragon_Bones_Model extends Sprite{
		
		//Holder of Sprite animation.
		private var Master_Holder:Sprite;				
		
		//Dragon bone core.
		private var Factory:StarlingFactory;
		private var Skeleton_Data:SkeletonData;
		private var Char_Texture:Texture;
		private var Char_Texture_Atlas:StarlingTextureAtlas;
		private var Char_Armature:Armature;	
		private var Armature_Holder:Sprite;	
		private var Old_Bounds:Rectangle;
		
		//Girl.
		private var Girl_Name:String;
		
		//Paths.
		private var FLA_Path:String;
		private var Skeleton_Path:String;
		private var Char_Texture_Path:String;
		private var Atlas_Texture_Path:String;
		
		public function Basic_Dragon_Bones_Model(Assets:AssetManager, Prefix:String, Name:String, Folder:String, Holder:Sprite = null){
			
			super();
			
			//Assets Paths.
			FLA_Path 						= (Folder + "/" + Name);
			Skeleton_Path 					= (Prefix + "_skeleton");
			Char_Texture_Path 				= (Prefix + "_texture");
			Atlas_Texture_Path 				= (Prefix + "_texture");
			
			//Girl.
			Girl_Name						= Prefix;
			
			//DragonBones Setup
			Factory 						= new StarlingFactory();
			Factory.generateMipMaps 		= Costanza.mipmapsEnabled;		
			
			//Grab armature Objects.
			Skeleton_Data 					= ObjectDataParser.parseSkeletonData(Assets.getObject(Skeleton_Path));			
			Char_Texture 					= Assets.getTexture(Char_Texture_Path);
			Char_Texture_Atlas 				= new StarlingTextureAtlas(Char_Texture, Assets.getObject(Atlas_Texture_Path), true);			
			
			//Rock the Factory.
			Factory.addSkeletonData(Skeleton_Data);
			Factory.addTextureAtlas(Char_Texture_Atlas);				
			
			//Create final Armature.
			Char_Armature 					= Factory.buildArmature(FLA_Path); //Now this needs to match the FLA structur.			
			Char_Armature.animation.gotoAndPlay("idle");				
			
			//Populate the Master Clock.
			WorldClock.clock.add(Char_Armature);			
			
			//Setup final holder.
			Armature_Holder					= (Char_Armature.display as Sprite);			
			Old_Bounds						= new Rectangle(Armature_Holder.bounds.x, Armature_Holder.bounds.y, Armature_Holder.bounds.width, Armature_Holder.bounds.height);			
			Armature_Holder.x 				= -Armature_Holder.bounds.x;
			Armature_Holder.y 				= -Armature_Holder.bounds.y;
			
			Master_Holder					= new Sprite();
			Master_Holder.addChild(Armature_Holder);
			Master_Holder.x 				= 0;
			Master_Holder.y 				= 0;		
			
			//Populate.
			this.addChild(Master_Holder);

			//If master holder.
			if(Holder != null){Holder.addChild(this);}
			
		}	
		
		public function Dispose(Holder:Sprite = null):void{
			
			if(Holder != null){Holder.removeChild(this);}
			
			WorldClock.clock.remove(Char_Armature);	
			Factory.dispose(true);
			Char_Armature.dispose();
			Char_Texture_Atlas.dispose();
			Char_Texture.base.dispose();
			Char_Texture.dispose();
			
			this.removeChildren(0, (this.numChildren - 1), true);
		}
		
		public function Play_This(Animation:String):void{
			Char_Armature.animation.gotoAndPlay(Animation);				
		}
		public function Play_Random(First:int, Last:int):void{			
			var RandomAnimation:int = (Math.floor(Math.random() * (Last - First)) + First);
			Char_Armature.animation.gotoAndPlay(Char_Armature.animation.animationList[RandomAnimation]);				
		}		
		public function Hit_Me(Position:Point):Boolean{			
			if(Master_Holder.hitTest(Master_Holder.globalToLocal(Position))){return true;}
			else{return false;}
		}	
		public function Spawp_Bone_Image(Bone_Name:String, New_Image_Name:String, Assets:AssetManager, Offset:Point = null):void{			
			
			//Swap all slots.
			for(var X:int = 0; X < Bone(Char_Armature.getBone(Bone_Name)).slot.displayList.length; X++){	
				
				switch(New_Image_Name){
					
					case "Visible_Off":{
						Bone(Char_Armature.getBone(Bone_Name)).visible = false;
					}break;
					
					case "Visible_On":{
						Bone(Char_Armature.getBone(Bone_Name)).visible = true;
					}break;
					
					default:{
					
						trace("New Image -- " + New_Image_Name);
						
						var FivX:Number 																= Image(Bone(Char_Armature.getBone(Bone_Name)).slot.displayList[X]).pivotX;
						var FivY:Number 																= Image(Bone(Char_Armature.getBone(Bone_Name)).slot.displayList[X]).pivotY;	
						Image(Bone(Char_Armature.getBone(Bone_Name)).slot.displayList[X]).dispose();			
						Bone(Char_Armature.getBone(Bone_Name)).slot.displayList[X] 						= new Image(Assets.getTexture(New_Image_Name));				
						Image(Bone(Char_Armature.getBone(Bone_Name)).slot.displayList[X]).pivotX 		= FivX;
						Image(Bone(Char_Armature.getBone(Bone_Name)).slot.displayList[X]).pivotY 		= FivY;
						if(Offset != null){
							Image(Bone(Char_Armature.getBone(Bone_Name)).slot.displayList[X]).pivotX 	+= Offset.x;
							Image(Bone(Char_Armature.getBone(Bone_Name)).slot.displayList[X]).pivotY 	+= Offset.y;
						}
						
					}break;					
					
				}
				
			}
			
			//Apply new image.
			Bone(Char_Armature.getBone(Bone_Name)).display = Image(Bone(Char_Armature.getBone(Bone_Name)).slot.displayList[Bone(Char_Armature.getBone(Bone_Name)).slot.Get_Slot_Index()]);			

		}	
		public function Get_Bone_Location(Bone_Name:String, Offset:Point = null):Point{	
			
			//trace(Bone_Name);	
			//trace(Bone(Char_Armature.getBone(Bone_Name)).global);	
			//trace(Bone(Char_Armature.getBone(Bone_Name)).slot.global);	
			//Armature_Holder.x 				= -Armature_Holder.bounds.x;
			//Armature_Holder.y 				= -Armature_Holder.bounds.y;
			
			if(Offset != null){
				return new Point((Bone(Char_Armature.getBone(Bone_Name)).global.x - Old_Bounds.x - Image(Bone(Char_Armature.getBone(Bone_Name)).slot.displayList[0]).pivotX - Offset.x),
								 (Bone(Char_Armature.getBone(Bone_Name)).global.y - Old_Bounds.y - Image(Bone(Char_Armature.getBone(Bone_Name)).slot.displayList[0]).pivotY - Offset.y));	
			}else{
				return new Point((Bone(Char_Armature.getBone(Bone_Name)).global.x - Old_Bounds.x - Image(Bone(Char_Armature.getBone(Bone_Name)).slot.displayList[0]).pivotX),
								 (Bone(Char_Armature.getBone(Bone_Name)).global.y - Old_Bounds.y - Image(Bone(Char_Armature.getBone(Bone_Name)).slot.displayList[0]).pivotY));	
			}
					
		}
		
	}
}