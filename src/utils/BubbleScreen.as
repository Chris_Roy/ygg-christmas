package utils{
	
	import com.reyco1.physinjector.PhysInjector;
	import com.reyco1.physinjector.data.PhysicsObject;
	import com.reyco1.physinjector.data.PhysicsProperties;
	
	import flash.geom.Point;
	
	import Box2D.Common.Math.b2Vec2;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.extensions.SoundManager;
	
	import utils.ParticleSystemJP.ParticleEmitterJP;

	public class BubbleScreen extends Sprite{
		
		//Sound manager.
		private var soundmanager:SoundManager;		
		//Snowflakes physic.
		private var physics:PhysInjector;	
		//Particle updater.
		private var particle:ParticleEmitterJP;		
		//Holder of are blasted particles.
		private var particlesArray:Array 	= new Array();		
		//Bouds.
		private var floor:Quad;
		private var ceiling:Quad;
		private var leftWall:Quad;
		private var rightWall:Quad;		
		
		//Snowlfake properties.
		private var bubbCount:int 			= 1;
		private var bubblePositions:Array 	= [new Point(400,100), new Point(600,200), new Point(500,350), new Point(400,500), new Point(600,600), new Point(800,300)];		
		private var bubbleArray:Array 		= [];
		private var bubblePhysArray:Array 	= [];
		
		public function BubbleScreen(){
			
			//Create Sounds.
			soundmanager 					= new SoundManager();
			Root.AddSM(soundmanager);	
			soundmanager.addSound("Snow_Flake_1",	Root.assets.getSound("Snow_Flake_1"));
			soundmanager.addSound("Snow_Flake_2",	Root.assets.getSound("Snow_Flake_2"));
			soundmanager.addSound("Snow_Flake_3",	Root.assets.getSound("Snow_Flake_3"));
			soundmanager.addSound("Snow_Flake_4",	Root.assets.getSound("Snow_Flake_4"));
			soundmanager.addSound("Snow_Flake_5",	Root.assets.getSound("Snow_Flake_5"));
			soundmanager.addSound("Snow_Flake_6",	Root.assets.getSound("Snow_Flake_6"));
			
			//Snowflakes physic.
			PhysInjector.STARLING 	= true;
			var bg:Image 			= new Image(Root.assets.getTexture("loading"));
			this.addChild(bg);
			
			//Particle system.
			particle 				= new ParticleEmitterJP(new Array(),false);
			this.addChild(particle);
			particle.touchable 		= false;
		}
		
		public function start():void{
			
			//Setup the physic.
			physics 							= new PhysInjector(Starling.current.nativeStage,new b2Vec2(0,0),false);			
			
			//Bounds.
			floor 								= new Quad(Costanza.STAGE_WIDTH, 50,0xffff00);
			floor.y 							= Costanza.STAGE_HEIGHT;
			this.addChild(floor);
			var floorObject:PhysicsObject 		= physics.injectPhysics(floor, PhysInjector.SQUARE, new PhysicsProperties({isDynamic:false,linearDamping:0, friction:0, restitution:1}));
			
			ceiling 							= new Quad(Costanza.STAGE_WIDTH, 50,0xff0000);
			ceiling.y 							= -ceiling.height;
			this.addChild(ceiling);
			var ceilingObject:PhysicsObject 	= physics.injectPhysics(ceiling, PhysInjector.SQUARE, new PhysicsProperties({isDynamic:false,linearDamping:0, friction:0, restitution:1}));
			
			leftWall 							= new Quad(50, Costanza.STAGE_HEIGHT,0xff00ff);
			leftWall.x 							-= leftWall.width;
			this.addChild(leftWall);
			var leftWallObject:PhysicsObject 	= physics.injectPhysics(leftWall, PhysInjector.SQUARE, new PhysicsProperties({isDynamic:false,linearDamping:0, friction:0, restitution:1}));
			
			rightWall 							= new Quad(50, Costanza.STAGE_HEIGHT,0xffffff);
			rightWall.x 						= Costanza.STAGE_WIDTH;
			this.addChild(rightWall);
			var rightWallObject:PhysicsObject 	= physics.injectPhysics(rightWall, PhysInjector.SQUARE, new PhysicsProperties({isDynamic:false,linearDamping:0, friction:0, restitution:1}));
			leftWall.alpha 						= rightWall.alpha = floor.alpha = ceiling.alpha = 0;
			
			//Setup the snowflakes.
			for(var i:int = 0; i < 6; i++){
			
				//Random Snowflake.
				var Flake_Selection:int 		= Math.ceil(Math.random() * 5);
				
				//New snowflake.
				var bubble:MovieClip 			= new MovieClip(Root.assets.getTextures(("Shape0" + Flake_Selection.toString() + "_Snowflake_")));
				Starling.juggler.add(bubble);
				bubble.stop();
				bubble.currentFrame 			= 0;				
				bubble.x 						= bubblePositions[i].x;
				bubble.y 						= bubblePositions[i].y;				
				bubble.fps 						= 15;
				bubble.pivotX 					= bubble.width/2;
				bubble.pivotY 					= bubble.height/2;
				bubble.addEventListener(TouchEvent.TOUCH, tapBubble);
				bubble.addEventListener(Event.ENTER_FRAME, clearBubble);
				
				//Populate.
				bubbleArray.push(bubble);
				this.addChild(bubble);
				
				//add to physics.
				var bubblePhys:PhysicsObject 	= physics.injectPhysics(bubble,PhysInjector.CIRCLE, new PhysicsProperties({isDynamic:true,friction:0,linearDamping:0,restitution:1,linearVelocity:new b2Vec2(MathHelpers.randomIntRange(-3,3),MathHelpers.randomIntRange(-3,3))}));
				bubblePhysArray.push(bubblePhys);
				
			}
			
			//Populate the physic bounds.
			bubblePhysArray.push(leftWall, rightWallObject, floorObject, ceilingObject);
			
			//Add updater event.
			addEventListener(Event.ENTER_FRAME, onUpdate);
		}
		
		//Clean completed bubble.
		private function clearBubble(e:Event):void{
			if((e.currentTarget as MovieClip).currentFrame == 5){
				var i:int = int((e.currentTarget as MovieClip).name);	
				(e.currentTarget as MovieClip).removeEventListener(Event.COMPLETE,clearBubble);
				(e.currentTarget as MovieClip).stop();
				(e.currentTarget as MovieClip).visible = (e.currentTarget as MovieClip).touchable = false;
			}
		}
		
		//Hit bubble.
		private function tapBubble(e:TouchEvent):void{
			
			//Setup touch.
			var touch:Touch 	= e.getTouch(stage);
			if(touch == null){return;}
			
			//Get this snowflake.
			var mc:MovieClip 	= e.currentTarget as MovieClip;
			var index:int 		= int(mc.name);
			
			//Only check mouse down.
			if(touch.phase == TouchPhase.BEGAN){
				
				//Check count.
				bubbCount++;
				if(bubbCount > 6){bubbCount = 1;}				
				
				//Play hit sound.
				soundmanager.playSound("Snow_Flake_" + bubbCount);
				//soundManager.playSound("bubble tone " + bubbCount);
				
				//Spawn location place particle blast.
				particle.x 		= mc.x;
				particle.y 		= mc.y;
				
				//Create particles.
				if(particlesArray.length > 0){particlesArray.splice(0,1);}
				particlesArray.push(new Image(Root.assets.getTexture("ygg-confetti_000" + MathHelpers.randomIntRange(1,21))));
				particle.setArray(particlesArray);				
				particle.CreateBurst(5, 300, 300, 0.3, true, true);
				
				//Animate bubble pop.
				mc.play();
				mc.removeEventListener(TouchEvent.TOUCH,tapBubble);
			}
			
		}
		
		//Update
		protected function onUpdate(event:Event):void{
			physics.update();
		}
		
		//Clean snowflakes.
		public function clearScreen():void{
			
			//Clear Sounds.
			soundmanager.stopAllSounds();
			Root.RemoveSM(soundmanager);	
			soundmanager.dispose();
			
			//Remove event.
			removeEventListener(Event.ENTER_FRAME, onUpdate);
			
			//Clean all snowflakes.
			for(var i:int = 0; i < bubbleArray.length; i++){				
				Starling.juggler.remove(bubbleArray[i]);
				physics.removePhysics(bubbleArray[i], true);				
			}
			
			//Clean physic.
			physics.removePhysics(floor,true);
			physics.removePhysics(ceiling,true);
			physics.removePhysics(rightWall,true);
			physics.removePhysics(leftWall,true);
			
			//Dispose physic.
			for(var j:int = 0; j < bubblePhysArray.length; j++){				
				if((bubblePhysArray[j] as PhysicsObject) != null){
					(bubblePhysArray[j] as PhysicsObject).dispose();
				}
			}
			
			//Clean arrays.
			bubbleArray 	= [];
			bubblePhysArray = [];
			
			//Clean this.
			this.removeChildren(2, (this.numChildren - 1), true);
			
			//Remove Physic.
			//physics.dispose();
			//physics 		= null;
		}
	}
}