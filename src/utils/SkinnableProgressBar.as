package utils
{
    import starling.display.Image;
    import starling.display.Sprite;
    import starling.textures.Texture;

    public class SkinnableProgressBar extends Sprite
    {
        private var mBar:Image;
        private var mBackground:Image;
        
        public function SkinnableProgressBar(T1:Texture, T2:Texture,_x:Number=5,_y:Number=5)
        {
            init(T1, T2, _x,_y);
        }
        
        private function init(T1:Texture, T2:Texture,_x:Number,_y:Number):void
        {
            mBackground = new Image(T1);
            addChild(mBackground);
            
            // create progress bar quad
            
            mBar = new Image(T2);
            mBar.x = _x;
            mBar.y = _y;
            mBar.scaleX = 0;
            addChild(mBar);
        }
        
        public function get ratio():Number { return mBar.scaleX; }
        public function set ratio(value:Number):void 
        { 
            mBar.scaleX = Math.max(0.0, Math.min(1.0, value)); 
        }
    }
}