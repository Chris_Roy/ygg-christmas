package utils{
	
	import de.flintfabrik.starling.display.FFParticleSystem;
	import de.flintfabrik.starling.display.FFParticleSystem.SystemOptions;
	
	import starling.display.Sprite;
	import starling.textures.Texture;
	import flash.geom.Point;
	
	public class Particle_Loader extends Sprite{		
		
		private var This_Particle:FFParticleSystem;
		private var Transformation_ParticleConfig:Class;
		private var Transformation_ParticleTexture:Class;
		
		public function Particle_Loader(Holder:Sprite, pTexture:Texture, pPex:XML, pScale:Number = 1.0, pMax:int = 0, LifeSpan:Number = 0.0, PlayNow:Boolean = false, Location:Point = null){
			
			super();			
			
			// instantiate embedded objects
			//var T_psConfig:XML 					= pPex;
			//var T_psTexture:Texture 			= Texture.fromBitmap(new pTexture());
			var T_sysOpt:SystemOptions 			= SystemOptions.fromXML(pPex, pTexture);
			
			// create particle system
			This_Particle 						= new FFParticleSystem(T_sysOpt);
			This_Particle.touchable 			= false;
			This_Particle.scaleX 				= pScale;
			This_Particle.scaleY 				= pScale;
			This_Particle.x 					= -(T_sysOpt.sourceX * pScale);
			This_Particle.y 					= -(T_sysOpt.sourceY * pScale);			
			
			if(pMax != 0){
				This_Particle.maxNumParticles = uint(pMax);
			}
			
			if(LifeSpan != 0.0){
				This_Particle.lifespan = LifeSpan;
			}
			
			this.addChild(This_Particle);
			if(Holder != null){
				Holder.addChild(this);
			}
			
			if(PlayNow){
				Play(1);
			}
			
			if(Location != null){				
				this.x = Location.x;
				this.y = Location.y;
			}
			
		}
		public function Dispose_Me(Holder:Sprite):void{
			
			if(Holder.contains(this)){
				Holder.removeChild(this);
			}
			this.removeChild(This_Particle);	
			This_Particle.stop(true);
			This_Particle.dispose();	
			this.dispose();
			
		}	
		public function Play(Play_Time:Number):void{
			This_Particle.reset();
			This_Particle.start(Play_Time);	//0 infinite.
		}
		public function Stop():void{
			This_Particle.reset();
			This_Particle.stop(false);
		}
		public function Stop_fade():void{
			//This_Particle.reset();
			This_Particle.start(1);	//0 infinite.
		}
		public function isActive():Boolean{
			return This_Particle.playing;
		}
		
	}
	
}