package utils.ParticleSystemJP
{
	import flash.geom.Point;
	
	import starling.display.Image;
	import starling.display.Sprite;

	public class TexturedParticle extends Sprite
	{
		public var velocity:Point; // current speed at which it's traveling
		public var lifeSpan:Number; //how long before it dies,
		public var fadeOut:Boolean = false; //will this particle be fading out?
		public var rotate:Boolean = false; //will this particle be rotating?
		public function TexturedParticle(img:Image, newVelocity:Point, life:Number = 2.0) 
		{
			trace("We are making a particle here");
			velocity = newVelocity;
			lifeSpan = life;
			img.pivotX = img.width/2;
			img.pivotY = img.height/2;
			addChild(img);
		}
	}
}