package utils.ParticleSystemJP
{
	import flash.geom.Point;
	import flash.utils.getTimer;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.utils.deg2rad;
	
	import utils.MathHelpers;

	public class ParticleEmitterJP extends Sprite
	{
		//private variables
		private var particleImages:Array; //this is the image were going to be displaying, we only need 1 copy
		private var particleCount:int; //the amount of particles were going to throw
		private var particleArray:Array = new Array(); //array to hold all the particles
		
		private var useGravity:Boolean;
		private var lastTime:Number;
		private var currentTime:Number;
		
		public var GRAVITY:Number = 500;
		public var ROTATE:Number = 5;

		public function ParticleEmitterJP(images:Array, gravity:Boolean = true)
		{
			//store our image, we can change this to a vector of images and
			//randomly pick among the images to use as part of the vectors, but
			//lets keep it simple for now
			particleImages = images;
			useGravity = gravity;
			lastTime = getTimer();
			addEventListener(Event.ENTER_FRAME, Update);
		}

		public function Update(e:Event):void
		{
			//update the Timer
			currentTime = getTimer();
			var timePassed:Number = (currentTime - lastTime) * 0.001;
			//Update all the particles if any are alive
			if(particleArray.length > 0)
			{
				//go through the list backwards, to prevent fallback on removals
				for(var i:int = particleArray.length -1 ; i >= 0; i--)
				{
					//update the particles position based on trajectory and gravity(if used).
					//also reduce the lifespan
					particleArray[i].velocity.x += particleArray[i].velocity.x * timePassed;
					particleArray[i].velocity.y += particleArray[i].velocity.y * timePassed;

					if(useGravity)
					{
						//if were using gravity, pull the object more downwards
						particleArray[i].velocity.y += (GRAVITY * timePassed);
					}

					particleArray[i].x += particleArray[i].velocity.x * timePassed;
					particleArray[i].y += particleArray[i].velocity.y * timePassed;

					//if this is a rotating particle, rotate it
					if(particleArray[i].rotate)
					{
						if(particleArray[i].velocity.x >= 0)
							particleArray[i].rotation += deg2rad(ROTATE);
						else
							particleArray[i].rotation += deg2rad(-ROTATE);
					}
					particleArray[i].lifeSpan -= timePassed;
					if(particleArray[i].lifeSpan < 0.5)//start a fade out near the end of it's life
					{
						if(particleArray[i].fadeOut)
						{
							particleArray[i].alpha -= timePassed;
						}
					}
					if(particleArray[i].lifeSpan <= 0.0)
					{
						KillMe(i);
					}
				}
			}

			//set our current time to our last time for next frame
			lastTime = currentTime;
		}

		private function randomRange(minNum:Number, maxNum:Number):Number
		{
			return (Math.floor(Math.random() * (maxNum - minNum + 1)) + minNum);
		}

		private function KillMe(index:int):void
		{
			trace("removing particle: " + index);
			removeChild(particleArray[index], true);
			particleArray.splice(index,1);
		}
		
		public function setArray(array:Array):void
		{
			trace("setArray");
			particleImages = array;
		}

		//PUBLIC FUNCTIONS
		public function CreateBurst(count:int, forceVarianceX:Number = 100, forceVarianceY:Number = 100, lifeSpan:Number = 1.25, fadeOut:Boolean = false, rotate:Boolean = false):void
		{
			for(var i:int = 0; i < count; i++)
			{
				var velocity:Point = new Point(randomRange(-forceVarianceX, forceVarianceX), randomRange(-forceVarianceY, forceVarianceY));
				var particle:TexturedParticle = new TexturedParticle(new Image(particleImages[MathHelpers.randomIntRange(0,particleImages.length-1)].texture), velocity, lifeSpan);
				if(fadeOut)
				{
					particle.fadeOut = fadeOut;
				}
				particle.rotate = rotate;
				particle.touchable = false;
				addChild(particle);
				particleArray.push(particle);
			}
		}
	}
}