package{
	
	import com.cupcake.DeviceInfo;
	import com.cupcake.Utils;
	import com.greensock.TimelineMax;
	import com.greensock.TweenMax;
	
	import flash.display.BitmapData;
	import flash.system.System;
	
	import scenes.Character_Selection_Screen;
	import scenes.Main_Menu_Screen;
	import scenes.Music_Video_Screen;
	import scenes.Item_Construction_Section.Item_Construction_Scene;
	import scenes.Item_Construction_Section.Item_Construction_Trophy_Scene;
	import scenes.Splash_Video_Screen;
	import scenes.ColoringBook.Coloring_Book_Scene;
	
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.extensions.SoundManager;
	import starling.filters.WarpFilter;
	import starling.textures.Texture;
	import starling.utils.AssetManager;
	
	import scenes.Item_Construction_Section.Item_Construction_Data.Item_Construction_Core_Data;
	import utils.BubbleScreen;	
	import utils.SkinnableProgressBar;

    /** The Root class is the topmost display object in your game. 
	 *  It is a general scene switcher and will contain any globally accessable items liek menus and transition screens.
     *  It listens for bubbled events from the scenes to trigger events.
	 *  Keep this class rather lightweight: it controls the high level behaviour of your game. */
    public class Root extends Sprite{
		
		public static const DISPOSE:String			= "dispose";
		public static const SCENE_LOADED:String		= "sceneLoaded";
		public static const SHOW_TRANSITION:String	= "showTransition";
		public static const LOAD_COLOR:String		= "loadColor";
		public static const LOAD_LOBBY:String		= "loadLobby";
		public static const LOAD_ROOM:String		= "loadRoom";
		public static const LOAD_STORY:String		= "loadStory";
		
		public static var currentAssetsProxy:AssetManager;
		
		private static var sAssets:AssetManager;
		private static var sms:Vector.<SoundManager>	= new Vector.<SoundManager>;
		
		private var mActiveScene:Sprite;
		private var mOldScene:Sprite;
		private var mSceneContainer:Sprite;
		private var progressBar:SkinnableProgressBar;
        private var soundManager:SoundManager;
		private var splashCupcakeImage:Button;
		private var splashInitial:Image;
		private var splashTimeline:TimelineMax;
		private var transitionImage:Image;
		private var transitionTexture:Texture;
		private var tranData:BitmapData;
		private var tranWarpFilter:WarpFilter;
		
		//Are Transition screen.
		private var Transition_BubbleScreen:BubbleScreen;
		
		//Bypass code.
		private var Skip_Video:Boolean = false;

		//embedded load bar textures
		[Embed(source="/load_1.png")]
		private static var load_1:Class;
		
		[Embed(source="/load_2.png")]
		private static var load_2:Class;	
		
		//Core data for the app.
		public static var Gaba_Core_Data:Item_Construction_Core_Data;		

		public function Root()
		{
			addEventListener( LOAD_ROOM,		onLoadRoom );			
			addEventListener( SCENE_LOADED,		onSceneLoaded );
			addEventListener( SHOW_TRANSITION,	transitionScene );
			
			DeviceInfo.Init();
			Gaba_Core_Data = new Item_Construction_Core_Data();
		}

		public function start(background:Texture, assets:AssetManager):void
		{
			sAssets = assets;
			splashInitial = new Image(background);
			
			addChild(splashInitial);

			progressBar = new SkinnableProgressBar(Texture.fromBitmap(new load_1),Texture.fromBitmap(new load_2),5,5);
			progressBar.x = (Costanza.STAGE_WIDTH/2)  - (progressBar.width/2);
			progressBar.y = Costanza.STAGE_HEIGHT * 0.88;
			addChild(progressBar);
			
			assets.loadQueue( onProgress );
        }
		
		private function onProgress(ratio:Number):void
		{
			progressBar.ratio = ratio;
			
			if (ratio == 1){
				
				if(Skip_Video){
					removeChild(splashInitial, true);
					progressBar.removeFromParent(true);
					Skip_Load();
					return;
				}
				
				Starling.juggler.delayCall(function():void
				{
					load_1 = load_2 = null;
					progressBar.removeFromParent(true);
					Costanza.Log("Loading Menu");
					
					splashTimeline = new TimelineMax({onComplete:loadMenu});
					splashTimeline.append(new TweenMax(splashInitial, 0.5, {alpha:0,delay:0.5, onComplete:removeChild, onCompleteParams:[splashInitial,true]}));
					
					// Look for images in the splash folder
					// If there are any present, add them in alphabetical order to the timeline
					var splashFiles:Vector.<String> = Utils.GetFilenames( Costanza.SPLASH_FOLDER );
					if ( splashFiles != null && splashFiles.length > 0 )
					{
						Costanza.Log( "Found " + splashFiles.length + " splash images." );
						for each ( var fileName:String in splashFiles )
						{
							Costanza.Log( "Adding splash screen '" + fileName + "'" );
							var splash:Button		= new Button(Root.assets.getTexture(fileName));
							splash.scaleWhenDown	= 1;
							splash.alpha			= 0;
							addChild(splash);
							
							splashTimeline.append(new TweenMax(splash, 0.5, {alpha:1}));
							splashTimeline.append(new TweenMax(splash, 0.5, {alpha:0,delay:1, onComplete:removeChild, onCompleteParams:[splash,true]}));
						}
					} else { Costanza.Log( "No splash files found" ); }
					
					addEventListener(starling.events.Event.TRIGGERED, skipLoaders);
					
					function skipLoaders(evt:starling.events.Event):void
					{
						splashTimeline.timeScale += 0.5;
						removeEventListener(starling.events.Event.TRIGGERED, skipLoaders);
					}
				}, 0.15);
			};
		}
		
		private function Skip_Load():void{		
			
			mSceneContainer = new Sprite();
			addChild(mSceneContainer);
			
			transitionImage 			= new Image(Root.assets.getTexture("Christmas_Splash"));
			transitionImage.alpha 		= 0;
			transitionImage.touchable 	= false;
			if(Costanza.IPAD_RETINA){
				transitionImage.x		= Costanza.STAGE_OFFSET;
			}
			addChild(transitionImage);
			
			//showScene(Snow_Globe_Screen, false);
			showStaticScene(Character_Selection_Screen, -1);
			//showStaticScene(Coloring_Book_Scene, 		false, 2);
			//showStaticScene(Main_Menu_Screen, -1);
			//showStaticScene(Item_Construction_Trophy_Scene, -1);
			
			System.pauseForGCIfCollectionImminent(0);
			System.gc();
			
		}	

		private function loadMenu():void
		{
			mSceneContainer = new Sprite();
			addChild(mSceneContainer);

			transitionImage 			= new Image(Root.assets.getTexture("Christmas_Splash"));
			transitionImage.alpha 		= 0;
			transitionImage.touchable 	= false;
			if(Costanza.IPAD_RETINA)
			{
				transitionImage.x		= Costanza.STAGE_OFFSET;
			}
			addChild(transitionImage);

			if(!Skip_Video){
				showStaticScene(Splash_Video_Screen, true, 99);
			}else{
				showStaticScene(Main_Menu_Screen,    true,  -1);
			}

			System.pauseForGCIfCollectionImminent(0);
			System.gc();
		}

		public function transitionScene(pageNum:int=-1):void
		{
			Costanza.Log("-------- TRANSITION --------");
			
			if(pageNum == -1)//default screen
			{
				transitionImage.x 			= 0;
				if(Costanza.IPAD_RETINA)
				{
					transitionImage.x		= Costanza.STAGE_OFFSET;
				}
				transitionImage.y 			= 0;
				transitionImage.alpha 		= 0;
				transitionImage.touchable 	= true;				
				TweenMax.to(transitionImage,0.15,{alpha:1});
				
			}else{
				
				if(pageNum == 99){return;}
				
				//Bubbles.
				Transition_BubbleScreen 			= new BubbleScreen();
				addChild(Transition_BubbleScreen);				
				Transition_BubbleScreen.x 			= 0;
				Transition_BubbleScreen.y 			= 0;
				Transition_BubbleScreen.alpha 		= 0.0;
				Transition_BubbleScreen.touchable 	= true;
				Transition_BubbleScreen.start();
				TweenMax.to(Transition_BubbleScreen, 0.5, {alpha:1.0});
				
			}
		}

		private function onSceneLoaded(event:starling.events.Event):void{
			
			Costanza.Log("SCENE LOADED");
			
			if(Transition_BubbleScreen != null){
			
				TweenMax.to(Transition_BubbleScreen, 0.5, {alpha:0.0, onComplete:Remove_Bubbles});	
				
			}else{
			
				TweenMax.to(transitionImage,0.2,{alpha:0, delay:0.4, onComplete:hideTransition});
				function hideTransition():void
				{
					transitionImage.touchable = false;
				}
				
			}	
			
		}

		private function Remove_Bubbles():void{			
			removeChild(Transition_BubbleScreen, true);
			if(Transition_BubbleScreen != null){
				Transition_BubbleScreen.touchable = false;
				Transition_BubbleScreen.clearScreen();
				Transition_BubbleScreen = null;
			}
		}

        private function onLoadRoom(event:starling.events.Event, Vars:Object):void{
			
            Costanza.Log("Switching Scenes");
			switch(Vars.room)
			{
				case 0:{ //Main Menu.
					showStaticScene(Main_Menu_Screen, 			true, -1); //-1
				}break;				
				
				case 1:{ //Lobby.
					showStaticScene(Character_Selection_Screen, true, 2); //-1
				}break;
				
				case 2:{ //Video.
					showStaticScene(Music_Video_Screen, 		true, 2); //-1
				}break;
				
				case 3:{ //Coloring.
					showStaticScene(Coloring_Book_Scene, 		false, 2); //-1
				}break;
				
				case 4:{ //Activity.
					showStaticScene(Coloring_Book_Scene, 		false, 2);					
				}break;
				
				case 6:{ //Games.
					showStaticScene(Item_Construction_Scene, 		true, 2);					
				}break;
				
				case 7:{ //Trophy.
					showStaticScene(Item_Construction_Trophy_Scene, 		true, 2);					
				}break;
				
				case 8:{ //Game Lobby.
					showStaticScene(Character_Selection_Screen, true, -1);
				}break;
				
				case 9:{ //Main Menu.
					showStaticScene(Main_Menu_Screen, 			false, -1); //-1
				}break;	
				
				default:{
					Costanza.Log( "Root.onLoadRoom(): unknown room '" + Vars.room + "'" );
				}break;
			}
		}

		public function showScene(screen:Class, transition:Boolean = true, comicPage:int=-1):void
		{
			Costanza.Log("Root.showScene()");
			if ( transition ) { transitionScene(comicPage); }
			if ( comicPage == -1 ) { addPage(screen); }
		}

		public function showStaticScene(screen:Class, transition:Boolean = true, comicPage:int=-1):void
		{
			Costanza.Log("Root.showStaticScene()");
			if(transition) { transitionScene( comicPage ); }			
			addStaticPage(screen);
		}

		private function addPage(screen:Class,num:int=-1):void
		{
			if (mActiveScene)
			{
				mOldScene = mActiveScene;
				mOldScene.touchable = false;
				TweenMax.delayedCall(1.5, KillPage);
			}
			Starling.juggler.delayCall(function():void
			{
				if(num > -1){mActiveScene = new screen(num);}
				else
				{
					mActiveScene = new screen(num);
					mActiveScene.touchable = false;
				}
				mActiveScene.x 			= 0;
				mActiveScene.touchable 	= true;
				//TweenMax.to(mActiveScene, 1, {x:0 });
				mSceneContainer.addChild(mActiveScene);
			},0.2);
		}

		private function KillPage():void
		{
			Costanza.Log("Destroying old Scene");
			mActiveScene.touchable = true;
			mOldScene.broadcastEventWith(DISPOSE);
			mOldScene.removeFromParent(true);
		}

		private function addStaticPage(screen:Class,num:int=-1):void
		{
			if (mActiveScene)
			{
				mActiveScene.broadcastEventWith(DISPOSE);
				mActiveScene.removeFromParent(true);
			}
			Starling.juggler.delayCall(function():void
			{
				if(num > -1){mActiveScene = new screen(num);}
				else
				{
					mActiveScene = new screen(num);
				}
				mSceneContainer.addChild(mActiveScene);
			},0.2);
		}

		public static function get assets():AssetManager { return sAssets; }
		
		public static function AddSM( sm:SoundManager ):void { if ( sm != null && sms.indexOf( sm ) == -1 ) { sms.push( sm ); } }
		public static function RemoveSM( sm:SoundManager ):void { if ( sm != null ) { while ( sms.indexOf( sm ) > -1 ) { sms.splice( sms.indexOf( sm ), 1 ); } } }
		public static function MuteAll(mute:Boolean=true):void { for each ( var sm:SoundManager in sms ) { if ( sm != null ) { sm.muteAll(mute); } } }
	}
}